#!/bin/sh -e

# To be used with the same args as if it were called by uscan:
# debian/orig-tar.sh --upstream-version <version> <file>
SOURCE_NAME=syrthes
VERSION=$2
DEBIAN_VERSION=$VERSION-dfsg1
UPSTREAM_TARBALL=../${SOURCE_NAME}_$VERSION.orig.tar.gz
DEBIAN_SOURCE_DIR=${SOURCE_NAME}-$DEBIAN_VERSION
DFSG_TARBALL=../${SOURCE_NAME}_$DEBIAN_VERSION.orig.tar.gz

# extract the upstream archive
mkdir "$DEBIAN_SOURCE_DIR"
tar xf "$UPSTREAM_TARBALL" -C "$DEBIAN_SOURCE_DIR" --strip 1
# repack into orig.tar.gz without unwanted files
tar -c -z -X debian/orig-tar.exclude -f "$DFSG_TARBALL" ${DEBIAN_SOURCE_DIR}/
rm -rf ${DEBIAN_SOURCE_DIR}
echo "syrthes: Applied DFSG removals and renamed archive to `basename ${DFSG_TARBALL}`"

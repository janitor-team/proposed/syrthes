SYRTHES for Debian
------------------

This file is used to keep track of packaging choices, known issues and TO-DOs.

==========================================
0- TO-DO
==========================================

* code_saturne?
* Several flavors (sequential, MPI)?

==========================================
1- UPSTREAM TARBALL
==========================================

The upstream source tarball has to be extracted from one of its binary
deliveries. See debian/orig-{get.sh,tar.sh,tar.exclude} for details.

Also the documentation provided with the upstream archive is not DFSG-free:
* source files for PDFs aren't provided
* the PDF files have the following license:
    Any total or partial modification, reproduction, new use, distribution
    or extraction of elements of this document or its content, without the
    express and prior written consent of EDF is strictly forbidden.
    Failure to comply to the above provisions will expose to sanctions.
Hence these files have been removed from the original upstream archive along
with generated binary files. See debian/orig-tar.exclude for the complete
list of the removed files.

==============================================
2- HOW THE SOURCE PACKAGE IS HANDLED USING GIT
==============================================

The work on this package takes place on the alioth git repository.
It requires an alioth account with grant to the debian-science group.

Cloning the repository:

 $ git clone <my-alioth-login>@git.debian.org:/git/debian-science/packages/syrthes.git

The repository has 2 remote branches:
* origin/upstream
* origin/master

HEAD is binded to master. 
master fetched localy by the clone:

 $ git branch
 * master

A packaging cycle starts by downloading the new upstream tarball using the
process detailed above (see §1). It should triggers the script
debian/orig-tar.sh to remove non DFSG-free parts of the tarball and append
the -dfsg part to upstream version.

Then the tarball can be imported into git, provided the upstream and
pristin-tar branches are tracked locally:

 $ git fetch origin upstream
 $ git fetch origin pristine-tar
 $ git-import-orig --pristine-tar /path/to/upstream-dfsg/tarball

It will import, tag and merge into master the new upstream release.

When the import is completed we switch back to the master branch to work
on the packaging:

  $ git checkout master

==============================================
3- QUILT
==============================================

The upstream source tree is patched using quilt. See the quilt documentation
in /usr/share/doc/quilt/.

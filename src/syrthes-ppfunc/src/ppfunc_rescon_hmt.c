/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
#include "ppfunc_proto.h"
#include "ppfunc_const.h"


extern FILE *fdata,*ff;
extern int model;

char motcle[CHLONG];
char ch[CHLONG];
char chs[CHLONG];

int list_ilist[100][100];   /* 100 listes de 100 entiers */
char list_formule[100][CHLONG]; /* 100 listes de chaines de CHLONG caracteres */
char list_formule1[100][CHLONG]; /* 100 listes de chaines de CHLONG caracteres */
char list_formule2[100][CHLONG]; /* 100 listes de chaines de CHLONG caracteres */
char list_formule3[100][CHLONG]; /* 100 listes de chaines de CHLONG caracteres */
char list_formule4[100][CHLONG]; /* 100 listes de chaines de CHLONG caracteres */
char list_formule5[100][CHLONG]; /* 100 listes de chaines de CHLONG caracteres */
char list_formule6[100][CHLONG]; /* 100 listes de chaines de CHLONG caracteres */
char list_condi[100][CHLONG];   /* 100 listes de chaines de CHLONG caracteres */

/*|======================================================================|
  | SYRTHES 4.3/PPFONC         2008                        COPYRIGHT EDF |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Interpretation des resistances de contact                            |
  |======================================================================| */
void lire_ecrire_rescon_hmt()
{
  int i,j,nbcoef,ndim;
  int i1,i2,i3,i4,id,ifin,ifin2,ok=1,ii,nb,nr,n,pos;
  double val;
  char *suite,*egal,*chfin="\\";
  int nbliste,numlist;



  fprintf(ff,"/*******************************************************************/ \n");
  fprintf(ff,"void user_hmt_rescon_fct(struct Maillage maillnodes,struct MaillageCL maillnodeus,\n");
  fprintf(ff,"		               double *t,double *pv,double *pt,\n");
  fprintf(ff,"    	               double *tcor,double *pvcor,double *ptcor,\n");
  fprintf(ff,"		               struct Contact rescon,double tempss,\n");
  fprintf(ff,"		               struct SDparall sdparall)\n");
  fprintf(ff,"/*******************************************************************/ \n");
  fprintf(ff,"{\n");
  fprintf(ff,"  extern int nbVar;\n\n");
  fprintf(ff,"  int i,j,nr,ne,num;\n");
  fprintf(ff,"  double x,y,z,tt;\n\n");
  fprintf(ff,"  double T1,T2,PV1,PV2,PT1,PT2;\n\n");
  fprintf(ff,"  prepare_paires_rc(maillnodes,rescon,t,tcor,sdparall);\n\n");
  fprintf(ff,"  prepare_paires_rc(maillnodes,rescon,pv,pvcor,sdparall);\n\n");
  fprintf(ff,"  if (nbVar==3) prepare_paires_rc(maillnodes,rescon,pt,ptcor,sdparall);\n\n");
  fprintf(ff,"  tt=tempss;\n\n");

  rescon_hmt();

  
  fprintf(ff,"}\n");

}

/*|======================================================================|
  | SYRTHES 4.3/PPFONC         2008                        COPYRIGHT EDF |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Lecture/Ecriture des conditions aux limites                          |
  |======================================================================| */
void rescon_hmt()
{
  int i,j,nbcoef,ndim;
  int i1,i2,i3,i4,id,ifin,ifin2,ok=1,ii,nb,nr,n,pos;
  double val;
  char *suite,*egal,*chfin="\\";
  int nbliste,numlist;



  /* lecture des RC */
  /* -------------- */

  numlist=0;  fseek(fdata,0,SEEK_SET);

  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  /* traitement des suites de lignes */
	lignesuite2 : suite=strchr(ch,chfin[0]);
	  if (suite)
	    {
	      strncpy(suite," \0",2); /* on remplace l'\ de la chaine ch par un blanc */
	      fgets(chs,CHLONG,fdata);
	      strcat(ch,chs);
	      goto lignesuite2;
	    }
	  
	  
	  extr_motcle_(motcle,ch,&i1,&i2);
	  if (!strcmp(motcle,"CLIM_HMT_FCT")) 
	    {
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;
	      if (!strcmp(motcle,"RES_CONTACT")) 
		{
		  ifin=rep_ch(list_formule1[numlist],ch+id);
		  ifin2=rep_ch(list_formule2[numlist],ch+id+ifin); ifin+=ifin2;
		  if (model==3) {ifin2=rep_ch(list_formule3[numlist],ch+id+ifin); ifin+=ifin2;}
		  rep_listint(list_ilist[numlist],&nb,ch+id+ifin);
		  if (list_ilist[numlist][0]!=-1)
		    for (pos=n=0;n<nb;n++) 
		      {
			sprintf(list_condi[numlist]+pos,"nr==%2d",list_ilist[numlist][n]); pos+=6;
			if (n!=nb-1) {sprintf(list_condi[numlist]+pos," || ");pos+=4;}
			else sprintf(list_condi[numlist]+pos,"\0");
		      }
		  numlist++;
		}
	    }
	}
    }
  
  /* ecriture des conditions  */
  /* ------------------------ */
  if (numlist==1 && list_ilist[0][0]==-1)
    {
      fprintf(ff,"    for (i=0;i<rescon.nelem;i++)\n");  
      fprintf(ff,"    	{\n");  
      fprintf(ff,"    	  ne=rescon.numf[i];    \n");         
      fprintf(ff,"    	  nr=maillnodeus.nrefe[ne]; \n");     
      fprintf(ff,"    	  for (j=0;j<rescon.ndmat;j++) \n");  
      fprintf(ff,"    	    {\n");  
      fprintf(ff,"    	      num=maillnodeus.node[j][i];  \n");  
      fprintf(ff,"            x=maillnodes.coord[0][num]; \n");
      fprintf(ff,"            y=maillnodes.coord[1][num]; \n");
      fprintf(ff,"            if (maillnodes.ndim==3) z=maillnodes.coord[2][num]; \n");
      fprintf(ff,"    	      T1=t[num];                \n");     
      fprintf(ff,"    	      T2=tcor[num];             \n");     
      fprintf(ff,"    	      rescon.g[ADR_T][j][i]=%s;   \n",list_formule1[0]);     
      fprintf(ff,"    	      PV1=pv[num];                \n");     
      fprintf(ff,"    	      PV2=pvcor[num];             \n");     
      fprintf(ff,"    	      rescon.g[ADR_PV][j][i]=%s;   \n",list_formule2[0]);    
      if (model==3){
	fprintf(ff,"    	      PT1=pv[num];                \n");     
	fprintf(ff,"    	      PT2=pvcor[num];             \n");     
	fprintf(ff,"    	      rescon.g[ADR_PT][j][i]=%s;   \n",list_formule3[0]);    
      }
      fprintf(ff,"    	    }\n");   
      fprintf(ff,"    	}\n");   
    }
  
  else if (numlist>=1)
    {
      fprintf(ff,"    for (i=0;i<rescon.nelem;i++)\n");  
      fprintf(ff,"    	{\n");  
      fprintf(ff,"    	 ne=rescon.numf[i];    \n");         
      fprintf(ff,"    	 nr=maillnodeus.nrefe[ne]; \n");     
      fprintf(ff,"       for (j=0;j<rescon.ndmat;j++) \n");  
      fprintf(ff,"        {\n");  
      fprintf(ff,"    	    num=maillnodeus.node[j][i];  \n");  
      fprintf(ff,"          x=maillnodes.coord[0][num]; \n");
      fprintf(ff,"          y=maillnodes.coord[1][num]; \n");
      fprintf(ff,"          if (maillnodes.ndim==3) z=maillnodes.coord[2][num]; \n");

      fprintf(ff,"    	    T1=t[num];                \n");     
      fprintf(ff,"    	    T2=tcor[num];             \n");     
      fprintf(ff,"          if (%s) \n",list_condi[0]);
      fprintf(ff,"            { \n");
      fprintf(ff,"              rescon.g[ADR_T][j][i]=%s; \n",list_formule1[0]);
      fprintf(ff,"            } \n");
      for (j=1;j<numlist;j++)
	{
	  fprintf(ff,"          else if (%s) \n",list_condi[j]);
	  fprintf(ff,"            { \n");
	  fprintf(ff,"              rescon.g[ADR_T][j][i]=%s; \n",list_formule1[j]);
	  fprintf(ff,"            } \n");
	}

      /* PV */
      fprintf(ff,"    	    PV1=pv[num];                \n");     
      fprintf(ff,"    	    PV2=pvcor[num];             \n");     
      fprintf(ff,"          if (%s) \n",list_condi[0]);
      fprintf(ff,"            { \n");
      fprintf(ff,"              rescon.g[ADR_PV][j][i]=%s; \n",list_formule2[0]);
      fprintf(ff,"            } \n");
      for (j=1;j<numlist;j++)
	{
	  fprintf(ff,"          else if (%s) \n",list_condi[j]);
	  fprintf(ff,"            { \n");
	  fprintf(ff,"              rescon.g[ADR_PV][j][i]=%s; \n",list_formule2[j]);
	  fprintf(ff,"            } \n");
	}
      
      /* pt */
      if (model==3){
	fprintf(ff,"    	    PT1=pt[num];                \n");     
	fprintf(ff,"    	    PT2=ptcor[num];             \n");     
	fprintf(ff,"          if (%s) \n",list_condi[0]);
	fprintf(ff,"            { \n");
	fprintf(ff,"              rescon.g[ADR_PT][j][i]=%s; \n",list_formule3[0]);
	fprintf(ff,"            } \n");
	for (j=1;j<numlist;j++)
	  {
	    fprintf(ff,"          else if (%s) \n",list_condi[j]);
	    fprintf(ff,"            { \n");
	    fprintf(ff,"              rescon.g[ADR_PT][j][i]=%s; \n",list_formule3[j]);
	    fprintf(ff,"            } \n");
	  }
      }
      
      fprintf(ff,"        } \n");
      fprintf(ff,"    	}\n");   
    }

}


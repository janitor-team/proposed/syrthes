#ifndef _TREE_H_
#define _TREE_H_
struct element
{
    rp_int num;
    struct element *suivant;
} ;


struct node
{
    char name[50];
    double xc;
    double yc;
    double zc;
    double sizx;
    double sizy;
    double sizz;
    struct element  *lelement;
    struct child *lfils;
} ;

struct child
{
    char name[50];
    struct node *fils;
    struct child *suivant;
} ;

    


#endif


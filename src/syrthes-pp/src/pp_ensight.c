/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdlib.h>
# include <stdio.h>
#include <string.h>
# include "pp_usertype.h"
# include "pp_bd.h"
# include "pp_proto.h"

/*|======================================================================|
  | SYRTHES PARALLELE                                          SEPT 2003 |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | Ecriture au format Ensight des partitions du maillage generees       |
  | par METIS                                                            |
  |                                                                      |
  |======================================================================|*/
void pp_ecrire_ensight (struct Maillage maillnodes,
			struct MaillageBord maillnodebord,rp_int ndiele,
			rp_int nparts,rp_int *numdome,rp_int *numdomebord)
{
  
  /* variables : 
     ----------- */
  FILE *fide2,*fide3;
  rp_int i,j,*nbel,*nbelbord,necrit;
  double zz=0;

  printf("\n *** ECRIRE_ENSIGHT : Ecriture d'un fichier Ensight des partitions du maillage\n");
  printf("                       --> domaine.case\n");
 
  /* compte du nombre d'elements par partition */
  nbel=(rp_int*)malloc(nparts*sizeof(rp_int));
  for (i=0;i<nparts;i++) nbel[i]=0;
  for (i=0;i<maillnodes.nelem;i++) nbel[numdome[i]]++;

  nbelbord=(rp_int*)malloc(nparts*sizeof(rp_int));
  for (i=0;i<nparts;i++) nbelbord[i]=0;
  for (i=0;i<maillnodebord.nelem;i++) nbelbord[numdomebord[i]]++;
                                
  /* ouverture des fichiers */
  fide2=fopen("domaine.geom","w");
  fide3=fopen("domaine.case","w");
  
  /* ecriture de l'entete */
  fprintf(fide2,"CALCUL SYRTHES : test\n");
  fprintf(fide2,"CALCUL SYRTHES : test\n");
  fprintf(fide2,"node id given\n");
  fprintf(fide2,"element id given\n");
  fprintf(fide2,"coordinates\n");


  /*   2.1- CAS BIDIMENSIONNEL :           
       ========================  */

  if(ndiele==2)
    {
      /* ecriture des noeuds */
      fprintf(fide2,"%8d\n",maillnodes.npoin);
      for (i=0;i<maillnodes.npoin;i++)
	fprintf(fide2,"%8d%12.5E%12.5E%12.5E\n",i+1,maillnodes.coord[0][i],maillnodes.coord[1][i],zz);

      fprintf(fide2,"part%8d\n",1);
      fprintf(fide2,"  Solide entier tria3\n");
      fprintf(fide2,"tria3\n");
      
      /* ecriture des elements */  
      fprintf(fide2,"%8d\n",maillnodes.nelem);
      for (i=0;i<maillnodes.nelem;i++)
	fprintf(fide2,"%8d%8d%8d%8d\n",i+1,
		maillnodes.node[0][i]+1,maillnodes.node[1][i]+1,maillnodes.node[2][i]+1);
      
      
      /* ecriture des partitions */  
      for(i=0;i<nparts;i++)
	{ 
	  fprintf(fide2,"part%8d\n",i+2);
	  fprintf(fide2,"  Partition %d\n",i+1);
	  fprintf(fide2,"tria3\n");
	  fprintf(fide2,"%8d\n",nbel[i]);
	  for(j=0;j<maillnodes.nelem;j++)
	    if (numdome[j]==i)
	      fprintf(fide2,"%8d%8d%8d%8d\n",j+1,
		      maillnodes.node[0][j]+1,maillnodes.node[1][j]+1,maillnodes.node[2][j]+1);
	}
      
    }
  
  /*   2.2- CAS TRIDIMENSIONNEL :          
       ========================= */
  else
  
    {
      /* ecriture des noeuds */
      fprintf(fide2,"%8d\n",maillnodes.npoin);
      for (i=0;i<maillnodes.npoin;i++)
	  fprintf(fide2,"%8d%12.5E%12.5E%12.5E\n",i+1,maillnodes.coord[0][i],
		  maillnodes.coord[1][i],maillnodes.coord[2][i]);

      
      fprintf(fide2,"part%8d\n",1);
      fprintf(fide2,"  Solide entier tetra4\n");
      fprintf(fide2,"tetra4\n");
      
      /* ecriture des elements */  
      fprintf(fide2,"%8d\n",maillnodes.nelem);
      for (i=0;i<maillnodes.nelem;i++)
	fprintf(fide2,"%8d%8d%8d%8d%8d\n",i+1,
		maillnodes.node[0][i]+1,maillnodes.node[1][i]+1,maillnodes.node[2][i]+1,maillnodes.node[3][i]+1);

      /* ecriture des partitions */  
      for(i=0;i<nparts;i++)
	{ 
	  fprintf(fide2,"part%8d\n",i+2);
	  fprintf(fide2,"  Partition %d\n",i+1);
	  fprintf(fide2,"tetra4\n");
	  fprintf(fide2,"%8d\n",nbel[i]);
	  for(j=0;j<maillnodes.nelem;j++)
	    if (numdome[j]==i)
	      fprintf(fide2,"%8d%8d%8d%8d%8d\n",j+1,
		      maillnodes.node[0][j]+1,maillnodes.node[1][j]+1,maillnodes.node[2][j]+1,maillnodes.node[3][j]+1);
	}

    }  

  /* nombre de part deja ecrites */
  necrit=nparts+2;
  printf("                       ==> ecriture du maillage volumique OK\n");


  /* ECRITURE DES MAILLAGES DE BORD */
  /* ============================== */

  /*   2.1- CAS BIDIMENSIONNEL :           
       ========================  */

  if(ndiele==2)
    {

      /* ecriture de tous les elts de bord */  
      fprintf(fide2,"part%8d\n",necrit+1);
      fprintf(fide2,"  Bord entier bar2\n");
      fprintf(fide2,"bar2\n");
      
      fprintf(fide2,"%8d\n",maillnodebord.nelem);
      for (i=0;i<maillnodebord.nelem;i++)
	fprintf(fide2,"%8d%8d%8d\n",i+1,
		maillnodebord.node[0][i]+1,maillnodebord.node[1][i]+1);
      
      
      /* ecriture des partitions */  
      for(i=0;i<nparts;i++)
	{ 
	  fprintf(fide2,"part%8d\n",necrit+2+i);
	  fprintf(fide2,"  Partition Bord %d\n",i+1);
	  fprintf(fide2,"bar2\n");
	  fprintf(fide2,"%8d\n",nbelbord[i]);
	  for(j=0;j<maillnodebord.nelem;j++)
	    if (numdomebord[j]==i)
	      fprintf(fide2,"%8d%8d%8d\n",j+1,
		      maillnodebord.node[0][j]+1,maillnodebord.node[1][j]+1);
	}
      
    }
  
  /*   2.2- CAS TRIDIMENSIONNEL :          
       ========================= */
  else
  
    {
      fprintf(fide2,"part%8d\n",necrit+1);
      fprintf(fide2,"  Solide Bord tria3\n");
      fprintf(fide2,"tria3\n");
      
      /* ecriture de tous les elts de bord */  
      fprintf(fide2,"%8d\n",maillnodebord.nelem);
      for (i=0;i<maillnodebord.nelem;i++)
	fprintf(fide2,"%8d%8d%8d%8d\n",i+1,
		maillnodebord.node[0][i]+1,maillnodebord.node[1][i]+1,maillnodebord.node[2][i]+1);

      /* ecriture des partitions */  
      for(i=0;i<nparts;i++)
	{ 
	  fprintf(fide2,"part%8d\n",necrit+2+i);
	  fprintf(fide2,"  Partition Bord %d\n",i+1);
	  fprintf(fide2,"tria3\n");
	  fprintf(fide2,"%8d\n",nbelbord[i]);
	  for(j=0;j<maillnodebord.nelem;j++)
	    if (numdomebord[j]==i)
	      fprintf(fide2,"%8d%8d%8d%8d\n",j+1,
		      maillnodebord.node[0][j]+1,maillnodebord.node[1][j]+1,maillnodebord.node[2][j]+1);
	}

    }  




  printf("                       ==> ecriture du maillage du bord OK\n");




  /* Fichier CASE */
  fprintf(fide3,"FORMAT\n");
  fprintf(fide3,"type: ensight\n");
  fprintf(fide3,"GEOMETRY\n");
  fprintf(fide3,"model: ");
  fprintf(fide3,"domaine.geom");
  

  printf("                       ==> ecriture du fichier 'case' OK\n");

  /* fermeture des fichiers */
  fclose(fide2);
  fclose(fide3);
 
}
    

    
/*|======================================================================|
  | SYRTHES PARALLELE                                          SEPT 2003 |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | Ecriture au format Ensight des partitions du maillage generees       |
  | par METIS                                                            |
  |                                                                      |
  |======================================================================|*/
void pp_ecrire_ensight_gold (struct Maillage maillnodes,
			     struct MaillageBord maillnodebord,rp_int ndiele,
			     rp_int nparts,rp_int *numdome,rp_int *numdomebord)
{
  
  /* variables : 
     ----------- */
  FILE *fgeom,*fcase;
  rp_int i,j,n,nn,nl,npoinloc,*nbel,*nbelbord,necrit,numpart;
  rp_int *no1d,*numloc,*numglob;
  double zz=0;
  float *coosp;
  char ch[80];

  printf("\n *** ECRIRE_ENSIGHT : Ecriture d'un fichier Ensight gold des partitions du maillage\n");
  printf("                       --> domaine.case\n");
 
  /* compte du nombre d'elements par partition */
  nbel=(rp_int*)malloc(nparts*sizeof(rp_int));
  for (i=0;i<nparts;i++) nbel[i]=0;
  for (i=0;i<maillnodes.nelem;i++) nbel[numdome[i]]++;

  nbelbord=(rp_int*)malloc(nparts*sizeof(rp_int));
  for (i=0;i<nparts;i++) nbelbord[i]=0;
  for (i=0;i<maillnodebord.nelem;i++) nbelbord[numdomebord[i]]++;
                                
  /* ouverture des fichiers */
  fgeom=fopen("domaine.geom","wb");
  fcase=fopen("domaine.case","w");
  

  strncpy(ch,"C Binary\0",9);             fwrite(ch,sizeof(char),80,fgeom);
  strncpy(ch,"CALCUL SYRTHES\0",15);      fwrite(ch,sizeof(char),80,fgeom);
  strncpy(ch,"conduction/rayonnement thermique\0",33);  fwrite(ch,sizeof(char),80,fgeom);
  strncpy(ch,"node id assign\0",15);      fwrite(ch,sizeof(char),80,fgeom);
  strncpy(ch,"element id assign\0",18 );  fwrite(ch,sizeof(char),80,fgeom);

  /* ecriture des coordonnees */
  printf("\n   Ecriture des %d noeuds au format Ensight\n",maillnodes.npoin);
  strncpy(ch,"part\0",5);                 fwrite(ch,sizeof(char),80,fgeom);
  numpart=1;                              fwrite(&numpart,sizeof(rp_int),1,fgeom);
  strncpy(ch,"Domaine solide\0",15);      fwrite(ch,sizeof(char),80,fgeom);
  strncpy(ch,"coordinates\0",12);         fwrite(ch,sizeof(char),80,fgeom);
  fwrite(&maillnodes.npoin,sizeof(rp_int),1,fgeom);


  /* coordonnees */
  coosp=(float*)malloc(maillnodes.npoin*sizeof(float));
  for (n=0;n<maillnodes.npoin;n++) coosp[n]=(float)maillnodes.coord[0][n];
  fwrite(coosp,sizeof(float),maillnodes.npoin,fgeom);
  
  for (n=0;n<maillnodes.npoin;n++) coosp[n]=(float)maillnodes.coord[1][n];
  fwrite(coosp,sizeof(float),maillnodes.npoin,fgeom);
  
  if (maillnodes.ndim==2)
    for (n=0;n<maillnodes.npoin;n++) coosp[n]=0.;
  else
    for (n=0;n<maillnodes.npoin;n++) coosp[n]=(float)maillnodes.coord[2][n];
  fwrite(coosp,sizeof(float),maillnodes.npoin,fgeom);
  
  free(coosp);

  /* ecriture des elements */


  printf("   Ecriture des %d elements au format Ensight\n",maillnodes.nelem);
  
  /* passage des elements en table 1D */
  
  no1d=(rp_int*)malloc(maillnodes.nelem*maillnodes.ndmat*sizeof(rp_int));
  for (nn=0,n=0;n<maillnodes.nelem;n++) 
    for (i=0;i<maillnodes.ndmat;i++)
      { 
	no1d[nn]=maillnodes.node[i][n]+1;  /* decalage de la numerotation */
	nn++;
      }
  
  if (ndiele==1)
    {
      strncpy(ch,"bar2\0",5 );  fwrite(ch,sizeof(char),80,fgeom);
      fwrite(&(maillnodes.nelem),sizeof(rp_int),1,fgeom);
    }
  
  else if (ndiele==2)
    {
      strncpy(ch,"tria3\0",6 );  fwrite(ch,sizeof(char),80,fgeom);
      fwrite(&(maillnodes.nelem),sizeof(rp_int),1,fgeom);
    }
  
  else if (ndiele==3)
    {  
      strncpy(ch,"tetra4\0",7 );  fwrite(ch,sizeof(char),80,fgeom);
      fwrite(&(maillnodes.nelem),sizeof(rp_int),1,fgeom);
    }
      
  fwrite(no1d,sizeof(rp_int),maillnodes.nelem*maillnodes.ndmat,fgeom);
  free(no1d);

  
  /* numero de la part courante */
  numpart=2;

  /* creation des part pour les partitions */
  /* ------------------------------------- */

  numloc=(rp_int*)malloc(maillnodes.npoin*sizeof(rp_int));

  for(i=0;i<nparts;i++)
    {
      for (n=0;n<maillnodes.npoin;n++) numloc[n]=-1;
      
      for (nl=0,n=0;n<maillnodes.nelem;n++) 
	if (numdome[n]==i)
	    {
	      for (j=0;j<maillnodes.ndmat;j++) 
		{
		  if (numloc[maillnodes.node[j][n]]<0)
		    {
		      numloc[maillnodes.node[j][n]]=nl;
		      nl++;
		    }
		}
	    }
      npoinloc=nl;
      numglob=(rp_int*)malloc(npoinloc*sizeof(rp_int));

      for (n=0;n<maillnodes.npoin;n++)  
	if (numloc[n]>=0) numglob[numloc[n]]=n;

      /* ecriture de l'entete de la part */
      strncpy(ch,"part\0",5 );  fwrite(ch,sizeof(char),80,fgeom);
      fwrite(&numpart,sizeof(rp_int),1,fgeom);
      sprintf(ch,"Partition %3d\0",i);fwrite(ch,sizeof(char),80,fgeom);
      strncpy(ch,"coordinates\0",12);  fwrite(ch,sizeof(char),80,fgeom);
      fwrite(&npoinloc,sizeof(rp_int),1,fgeom);


      numpart++;
      
      /* ecriture de l'entete des coord */
      
      coosp=(float*)malloc(npoinloc*sizeof(float));
      for (n=0;n<npoinloc;n++) coosp[n]=(float)maillnodes.coord[0][numglob[n]];
      fwrite(coosp,sizeof(float),npoinloc,fgeom);
      for (n=0;n<npoinloc;n++) coosp[n]=(float)maillnodes.coord[1][numglob[n]];
      fwrite(coosp,sizeof(float),npoinloc,fgeom);
      if (maillnodes.ndim==2)
	for (n=0;n<npoinloc;n++) coosp[n]=0.;
      else
	for (n=0;n<npoinloc;n++) coosp[n]=(float)maillnodes.coord[2][numglob[n]];
      fwrite(coosp,sizeof(float),npoinloc,fgeom);
      free(coosp);
      
      
      /* ecriture de l'entete des elts */
      if (ndiele==1)
	{
	  strncpy(ch,"bar2\0",5);  fwrite(ch,sizeof(char),80,fgeom);
	  fwrite(nbel+i,sizeof(rp_int),1,fgeom);
	  no1d=(rp_int*)malloc(nbel[i]*maillnodes.ndmat*sizeof(rp_int));
	  for (nn=0,n=0;n<maillnodes.nelem;n++) 
	    if (numdome[n]==i) 
	      {
		no1d[nn]=numloc[maillnodes.node[0][n]] + 1;      /* decalage de la numerotation */
		no1d[nn+1]=numloc[maillnodes.node[1][n]] + 1;    /* decalage de la numerotation */
		nn+=2;
	      }
	  fwrite(no1d,sizeof(rp_int),nbel[i]*2,fgeom);
	  free(no1d);
	}
      
      else if (ndiele==2)
	{
	  strncpy(ch,"tria3\0",6 );  fwrite(ch,sizeof(char),80,fgeom);
	  fwrite(nbel+i,sizeof(rp_int),1,fgeom);
	  no1d=(rp_int*)malloc(nbel[i]*3*sizeof(rp_int));
	  for (nn=0,n=0;n<maillnodes.nelem;n++)                         /* decalage de la numerotation */
	    if (numdome[n]==i) {for (j=0;j<3;j++) no1d[nn+j]=numloc[maillnodes.node[j][n]] + 1; nn+=3;}
	  fwrite(no1d,sizeof(rp_int),nbel[i]*3,fgeom);
	  free(no1d);
	}
      
      else if (ndiele==3)
	{
	  strncpy(ch,"tetra4\0",7);  fwrite(ch,sizeof(char),80,fgeom);
	  fwrite(nbel+i,sizeof(rp_int),1,fgeom);
	  no1d=(rp_int*)malloc(nbel[i]*4*sizeof(rp_int));
	  for (nn=0,n=0;n<maillnodes.nelem;n++)                        /* decalage de la numerotation */
	    if (numdome[n]==i) {for (j=0;j<4;j++) no1d[nn+j]=numloc[maillnodes.node[j][n]]+1; nn+=4;}
	  fwrite(no1d,sizeof(rp_int),nbel[i]*4,fgeom);
	  free(no1d);
	}
    }/* fin de la boucle sur les parts */







  /* Fichier CASE */
  fprintf(fcase,"FORMAT\n");
  fprintf(fcase,"type: ensight gold\n");
  fprintf(fcase,"GEOMETRY\n");
  fprintf(fcase,"model: domaine.geom\n");


  printf("                       ==> ecriture du fichier 'case' OK\n");

  /* fermeture des fichiers */
  fclose(fgeom);
  fclose(fcase);
 
}
    

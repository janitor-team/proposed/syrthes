/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/times.h>
#include <sys/time.h>


#include "pp_usertype.h"
#include "pp_bd.h"
#include "pp_tree.h"
#include "pp_proto.h"


/*|======================================================================|
  | SYRTHES 3.2                MAI 97         COPYRIGHT EDF/SIMULOG 1997|
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture de syrthes.env                                        |
  |======================================================================| */
void imprime_maillage(struct Maillage maillnodes)
{
  rp_int i,j;

  printf("\n lire_donnees : Table des noeuds\n");
  for (i=0;i<maillnodes.npoin;i++) 
    {
      printf("\n noeud %d coord : ",i);
      for (j=0;j<maillnodes.ndim;j++) printf(" %f",maillnodes.coord[j][i]);
    }
  printf("\n lire_donnees : Table des elements\n");
  for (i=0;i<maillnodes.nelem;i++) 
    {
      printf("\n elt %d  noeuds: ",i);
      for (j=0;j<maillnodes.ndmat;j++) printf(" %d",maillnodes.node[j][i]);
    }
  printf("\n\n");
}


/*|======================================================================|
  | SYRTHES 3.2                MAI 97         COPYRIGHT EDF/SIMULOG 1997|
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture de syrthes.env                                        |
  |======================================================================| */
void verif_alloue_int1d(char *chaine,rp_int *pointeur)
{
  if (!pointeur)
    {
      printf("\n\n ERREUR D'ALLOCATION dans la fonction %s\n\n",chaine);
      exit(1);
    }
}
/*|======================================================================|
  | SYRTHES 3.2                MAI 97         COPYRIGHT EDF/SIMULOG 1997|
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture de syrthes.env                                        |
  |======================================================================| */
void verif_alloue_int2d(rp_int idim,char *chaine,int **pointeur)
{
  rp_int i,err=0;
  
  if (!pointeur)
    err=1;
  else 
    for (i=0;i<idim;i++)
      if (!pointeur[i]) err=1;

  if (err)
    {
      printf("\n\n ERREUR D'ALLOCATION dans la fonction %s\n\n",chaine);
      exit(1);
    }
}
    
/*|======================================================================|
  | SYRTHES 3.2                MAI 97         COPYRIGHT EDF/SIMULOG 1997|
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture de syrthes.env                                        |
  |======================================================================| */
void verif_alloue_double1d(char *chaine,double *pointeur)
{
  if (!pointeur)
    {
      printf("\n\n ERREUR D'ALLOCATION dans la fonction %s\n\n",chaine);
      exit(1);
    }
}
/*|======================================================================|
  | SYRTHES 3.2                MAI 97         COPYRIGHT EDF/SIMULOG 1997|
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture de syrthes.env                                        |
  |======================================================================| */
void verif_alloue_double2d(int idim,char *chaine,double **pointeur)
{
  int i,err=0;
  
  if (!pointeur)
    err=1;
  else 
    for (i=0;i<idim;i++)
      if (!pointeur[i]) err=1;

  if (err)
    {
      printf("\n\n ERREUR D'ALLOCATION dans la fonction %s\n\n",chaine);
      exit(1);
    }
}
    
/*|======================================================================|
  | SYRTHES 3.2                MAI 97         COPYRIGHT EDF/SIMULOG 1997|
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture de syrthes.env                                        |
  |======================================================================| */
void verif_alloue_char(char *chaine,char *pointeur)
{
  if (!pointeur)
    {
      printf("\n\n ERREUR D'ALLOCATION dans la fonction %s\n\n",chaine);
      exit(1);
    }
} 



/*----------------------------------------------------------------------------*/
/* Permutation des octets pour passage de "little endian" a "big endian"      */
/* (Y. Fournier)                                                              */
/*----------------------------------------------------------------------------*/
void fic_bin_f__endswap
(
 void  *buf,   /* Tampon contenant les elements                               */
 size_t size,  /* Taille d'un element                                         */
 size_t nitems /* Nombre d'elements                                           */
)
{
  char  tmpswap;
  char *ptr = (char *)buf;

  size_t i, j, shift;
  for (j = 0; j < nitems; j++) {
    shift = j * size;
    for (i = 0; i < (size / 2); i++) {
      tmpswap = *(ptr + shift + i);
      *(ptr + shift + i ) = *(ptr + shift + (size - 1) - i);
      *(ptr + shift + (size - 1) - i) = tmpswap;
    }
  }
}

/*|======================================================================|
  | SYRTHES 3.2                JANV 95         COPYRIGHT EDF/SIMULOG 1995|
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | elague_tree                                                          |
  |         Faire du menage dans l'arbre                                 |
  |======================================================================| */
void elague_tree(struct node *pere)

{
  struct element *fa1,*fa2;
  struct child *f1;
  
  if (pere->lfils)
    {
      fa1=pere->lelement;
      pere->lelement=NULL;
      while(fa1)
	{
	  fa2=fa1;
	  fa1=fa1->suivant;
	  free(fa2);
	}
      
      f1=pere->lfils;
      while (f1)
	{
	  elague_tree(f1->fils);
	  f1 = f1->suivant;
	}
    }  
}	    

/*|======================================================================|
  | SYRTHES 3.2                JUIN 95         COPYRIGHT EDF/SIMULOG 1995|
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | tuer_tree                                                            |
  |         Tuer l'arbre...                                              |
  |======================================================================| */
void tuer_tree(struct node *pere,int nbfils)

{
  struct element *fa1,*fa2;
  struct child *f1;

  /* destruction de la liste de facettes */
  fa1=pere->lelement;
  pere->lelement=NULL;
  while(fa1)
    {
      fa2=fa1;
      fa1=fa1->suivant;
      free(fa2);
    }
  /* destruction des fils */
  f1=pere->lfils;
  while (f1)
    {
      tuer_tree(f1->fils,nbfils);
      f1 = f1->suivant;
    }

}
/*|======================================================================|
  | SYRTHES 4.3                2008                        COPYRIGHT EDF |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | cpusyrt                                                              |
  |                                                                      |
  |======================================================================| */
double cpusyrt()
{
  struct tms cpu;

  times(&cpu);
  /*  return((double)cpu.tms_utime/(double)CLK_TCK);*/
  return((double)cpu.tms_utime/100.);
}

/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#include <stdio.h>
#include "stdlib.h"
#include <string.h>
#include "pp_usertype.h"
#include "pp_bd.h"
#include "pp_proto.h"


/*|======================================================================|
  | SYRTHES 4.3                                        COPYRIGHT EDF 2007|
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |  Donnees de la transformation geometrique pour la periodicite        |
  |  dans le cas ou ce n'est ni une translation ni une rotation          | 
  |======================================================================| */
void util_transfo_perio(rp_int ndim,
			rp_int num, 
			double x,double y,double z,
			double *xt, double *yt, double *zt)
{
  rp_int i;

  rp_int matransfo=0;
  /* si on programme une transformation, mettre "matransfo=1"              */

  /*    a partir des coordonnees du point (x,y,z)                          */
  /*    et de la reference auquel il appartient (une des nb1 references    */
  /*    de la liste list_ref1) fournir, les coordonnees du point           */
  /*    apres transformation periodique (xt,yt,zt)                         */


  if (matransfo)
    {

      if (num==1)
	{
	  *xt = x+4;
	  *yt = y+4;
	  if (ndim==3) *zt = z+4;
	}
      else if (num==2)
	{
	  *xt = x+2;
	  *yt = y+2;
	  if (ndim==3) *zt = z+2;
	}
    }
}
/*|======================================================================|
  | SYRTHES 3.2                MAI 97         COPYRIGHT EDF/SIMULOG 1997 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |                                                                      |
  |======================================================================| */
void extr_motcle_(char* mc,char *ch,rp_int *i1,rp_int *i2)
{
  rp_int i=0,j=0;
  char *c;

  c=ch;
  while(!strncmp(c," ",1) && i<strlen(ch)-1) {c++;i++;}
  if (i==strlen(ch)-1) 
    {
      *i1=*i2=0;
    }
  else
    {
      j=i;
      while(strncmp(c,"=",1) && j<strlen(ch)-1) {c++;j++;}
      if (j==strlen(ch)-1) 
	{
	  printf("Erreur sur la chaine %s\n",ch);
	  printf("On attend un symbole '='\n",ch);
	  exit(1);
	}
      else
	{
	  strncpy(mc,ch+i,j-i);
	  strcpy(mc+j-i,"\0");
	  /*	  *i1=i; *i2=j+1; */
	  *i1=i; *i2=j;
	}
    }
  
}

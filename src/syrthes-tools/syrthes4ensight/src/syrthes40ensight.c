#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#define FR 0
#define EN 1

/* ------------------------------------------- */

#define POST_TYPEWIDTH 32
#define SYRTHES_LANG 0

#define MAX_REF 1000

/* ------------------------------------------- */

#if POST_TYPEWIDTH == 32
  typedef int rp_int;
#elif POST_TYPEWIDTH == 64
  typedef long rp_int;
#else
  #error "Incorrect user-supplied value fo POST_TYPEWIDTH"
#endif


#define max(a,b) (a>b  ? a:b)

FILE  *fsyrg,*fsyrr,*fgeom,*fcase,*fvar;

rp_int *numglob_ssd[500];
rp_int numglob_ssd_nb[500];
rp_int *numglob_ref[500];
rp_int numglob_ref_nb[500];
rp_int ecrgeom=1;
rp_int noVol=0,noSurf=0,noPlain=0;


void entete(rp_int*,rp_int*,rp_int*,rp_int*,rp_int*,rp_int*,rp_int*);
void geom_syr(rp_int,rp_int,rp_int,rp_int,rp_int,rp_int**,double**,rp_int*,rp_int*,rp_int**,rp_int*,rp_int,rp_int**,rp_int*);


void geom_syr_coord(rp_int,rp_int,double**);
void geom_ens_coord_ascii(rp_int,rp_int,double**);
void geom_ens_coord_bin(rp_int,rp_int,double**);



void geom_syr_node(rp_int,rp_int,rp_int,rp_int**,rp_int*);
void geom_ens_node_ascii(rp_int,rp_int,rp_int,rp_int,rp_int**,rp_int*,rp_int*);
void geom_ens_node_bin  (rp_int,rp_int,rp_int,rp_int,rp_int**,rp_int*,rp_int*,rp_int,double**);

void geom_syr_node_bord(rp_int,rp_int,rp_int**,rp_int*);
void geom_ens_node_bord_ascii(rp_int,rp_int,rp_int,rp_int**,rp_int*,rp_int);
void geom_ens_node_bord_bin  (rp_int,rp_int,rp_int,rp_int**,rp_int*,rp_int,rp_int,double**);

int resu_syr_entete(rp_int,rp_int*,double*);
int resu_syr_nomvar(rp_int*,char*,rp_int*);
void resu_syr_var(rp_int,double*);
void ecrit_var_ensight_ascii(FILE *,rp_int,rp_int,rp_int,double,char*,double *,int,int*);
void ecrit_var_ensight_bin  (FILE *,rp_int,rp_int,rp_int,double,char*,double *);


/*|======================================================================|
  | SYRTHES 4.3 - Utilitaires                         COPYRIGHT EDF 2008 |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | syrthes4ensight                                                      |
  |        Transformation d'un resultat SYRTHES 4.x   au format Ensight  |
  |======================================================================| */
main (argc,argv)

    rp_int	argc;
    char *argv[];
{
  rp_int version,numarg,ok;
  rp_int i,lbase,n,geo=0,resu=0,bin=0,ens=0;

  rp_int ndim,ndiele,nelem,nbno,nbface,npoin,nt,noface,nelembord,nbre;
  rp_int *discr,nbscal,npdt,npartcree;
  rp_int **node,**nodebord;
  rp_int *nrefn,*nrefe,**nrefac,*nrefebord;
  double *temps,**coord,*var;
  char nomfichcase[200],nomfichgeom[200],nomfichvar[200];
  char nomutilens[200],nomutilgeom[200],nomutilresu[200];
  char ch[200],*nomvar[90],nombase[90],nomnum[6];
  char *s;
  
  numarg=0;
  while (++numarg < argc) {

    s = argv[numarg];

    if (strcmp (s, "-h") == 0)
      {    
	printf("\n syrthes4ensight : file conversion tool from SYRTHES 4.x to Ensight\n");
	printf("      Auteur  : I. Rupp \n");
	printf("        Usage : \n");
	printf("           syrthes4ensight [-h] [-b] [-u] [--noSurf] [--noVol or --noPlain] -m file.syr -r file.res -o ensight_file\n");
	printf("                --> -h : help\n");
	printf("                --> -b : Ensight gold binary files \n");
	printf("                --> -u : for binary files only : no geometry writing \n");
	printf("                --> --noSurf  : no writing of surfacic parts\n");
	printf("                --> --noVol   : no writing of volumic parts\n");
	printf("                --> --noPlain : no writing of the whole domain (only volumic parts are written)\n\n");
	printf("           syrthes4ensight  -h  : informations\n\n");
	printf("           syrthes4ensight          -m file.syr -r file.res -o ensight_file\n");
	printf("           syrthes4ensight --noSurf -m file.syr -r file.res -o ensight_file\n");
	printf("           syrthes4ensight  -b      -m file.syr -r file.res -o ensight_file\n");
	printf("           syrthes4ensight  -b -u   -m file.syr -r file.res -o ensight_file\n");
	printf("           syrthes4ensight          -m file.syr             -o ensight_file   (mesh conversion only)\n");
	exit(1);
      }
    else if (strcmp (s, "-m") == 0){
      s = argv[++numarg];
      strcpy(nomutilgeom,argv[numarg]);
      geo=1;
    }
    else if (strcmp (s, "-r") == 0){
      s = argv[++numarg];
      strcpy(nomutilresu,argv[numarg]);
      resu=1;
    }
    else if (strcmp (s, "-o") == 0){
      s = argv[++numarg];
      strcpy(nomutilens,argv[numarg]);
      ens=1;
    }
    else if (strcmp (s, "-b") == 0){
      bin=1;
    }
    else if (strcmp (s, "-u") == 0){
      ecrgeom=0;
    }
    else if (strcmp (s, "--noVol") == 0){
      noVol=1;
    }
    else if (strcmp (s, "--noSurf") == 0){
      noSurf=1;
    }
    else if (strcmp (s, "--noPlain") == 0){
      noPlain=1;
    }
  }

  /* il faut au moins un type de volume ! */
  if (noVol && noPlain) noPlain=0;

  ok=1;
  if (geo==0 && resu==0) {printf("  You have to give at least a geometric file or a result file\n"); ok=0;}
  if (ens==0) {printf("  Ensight file missing\n"); ok=0;}
  if (!ok){
      printf("  --> type 'syrthes4ensight -h' for help\n");
      exit(1);
    }


  printf("\n\n");
  printf("\n\n\n");
  printf("    *****************************************************\n");
  printf("    *               SYRTHES4ENSIGHT                    *\n");
  printf("    *****************************************************\n");
  if (SYRTHES_LANG == FR)
    printf("    *    CONVERSION DES RESULTATS AU FORMAT ENSIGHT     *\n");
  else if (SYRTHES_LANG == EN)
    printf("    *         CONVERT RESULTS TO ENSIGHT FORMAT         * \n");
  printf("    *****************************************************\n\n");


  if (geo==0) printf(" Conversion of geometry format only\n");

  /* -------------------------------------------------------------------- */
  /*                           ouverture des fichiers                     */
  /* -------------------------------------------------------------------- */
  if (geo)
    {
      if ((fsyrg=fopen(nomutilgeom,"r")) == NULL)
	{
	  printf("Unable to open %s\n",nomutilgeom);	
	  exit(1) ;
	}
    }
  
  if (resu)
    {
      if ((fsyrr=fopen(nomutilresu,"r")) == NULL)
	{
	  printf("Unable to open %s\n",nomutilresu);	
	  exit(1) ;
	}
    }


  /* contitution des noms des fichiers geom et case */

  lbase=strlen(nomutilens);

  strcpy(nombase,nomutilens);

  strcpy(nomfichcase,nomutilens);
  strncat(nomfichcase,".ensight.case",13); 
  
  strcpy(nomfichgeom,nomutilens);
  strncat(nomfichgeom,".ensight.geom",13);
    
  if (geo) printf("--> geometry file name : %s\n",nomfichgeom);	
  printf("--> case file name     : %s\n",nomfichcase);	

  if (geo && !bin){
    if ((fgeom=fopen(nomfichgeom,"w")) == NULL) 
      {
	printf("Unable to open %s\n",nomfichgeom);	
	exit(1) ;
      }
  }
  else if (geo && bin){
    if ((fgeom=fopen(nomfichgeom,"wb")) == NULL) 
      {
	printf("Unable to open %s\n",nomfichgeom);	
	exit(1) ;
      }
  }
  
  if ((fcase=fopen(nomfichcase,"w")) == NULL) 
    {
      printf("Unable to open %s\n",nomfichcase);	
      exit(1) ;
    }
  


  /* -------------------------------------------------------------------- */
  /*              traitement de la geometrie                              */
  /* -------------------------------------------------------------------- */

  if (geo)
    {

      /* lecture de la geometrie Syrthes */
      entete(&ndim,&ndiele,&npoin,&nelem,&nbno,&nelembord,&version);
      

      /* coordonnees */
      /* ----------- */
      coord=(double**)malloc(ndim * sizeof(double*));
      for (n=0;n<ndim;n++) coord[n]=(double*)malloc(npoin* sizeof(double));
      
      geom_syr_coord(ndim,npoin,coord);
      if (bin)
	geom_ens_coord_bin(ndim,npoin,coord);
      else
	geom_ens_coord_ascii(ndim,npoin,coord);


      if (!bin){
	for (n=0;n<ndim;n++) free(coord[n]);
	free(coord);
      }


      /* elements */
      /* -------- */
      node=(rp_int**)malloc(nbno * sizeof(rp_int*));
      for (n=0;n<nbno;n++) node[n]=(rp_int*)malloc(nelem* sizeof(rp_int));
      nrefe=(rp_int*)malloc(nelem * sizeof(rp_int));
      
      geom_syr_node(ndim,nelem,nbno,node,nrefe);
      if (bin)
	geom_ens_node_bin(ndim,ndiele,nelem,nbno,node,nrefe,
			  &npartcree,npoin,coord);
      else
	geom_ens_node_ascii(ndim,ndiele,nelem,nbno,node,nrefe,
			    &npartcree);

      for (n=0;n<nbno;n++) free(node[n]);
      free(node);


      /* elements de bord */
      /* ---------------- */
      if (noSurf==0)
	{
	  nodebord=(rp_int**)malloc(ndim * sizeof(rp_int*));
	  for (n=0;n<ndim;n++) nodebord[n]=(rp_int*)malloc(nelembord* sizeof(rp_int));
	  nrefebord=(rp_int*)malloc(nelembord * sizeof(rp_int));
	  
	  geom_syr_node_bord(ndim,nelembord,nodebord,nrefebord);
	  if (bin)
	    geom_ens_node_bord_bin(ndim,ndiele,nelembord,nodebord,nrefebord,
				   npartcree,npoin,coord);
	  else
	    geom_ens_node_bord_ascii(ndim,ndiele,nelembord,nodebord,nrefebord,
				     npartcree);
	  
	  for (n=0;n<ndim;n++) free(nodebord[n]);
	  free(nodebord);
	  free(nrefebord);
	}
     
      if (bin){
	for (n=0;n<ndim;n++) free(coord[n]);
	free(coord);
      }
      
    }

  /* -------------------------------------------------------------------- */
  /*                     traitement des resultats                         */
  /* -------------------------------------------------------------------- */

  if (resu)
    {
      printf("\n\n Conversion of results\n");
      
      for (i=0;i<90;i++) nomvar[i]=(char*)malloc(12*sizeof(char));
      temps=(double*)malloc(100000*sizeof(double));
      discr=(rp_int*)malloc(1000*sizeof(rp_int));
      

      fseek(fsyrr,0,SEEK_SET);
      
     
      npdt=0;
      
      fgets(ch,200,fsyrr);
      if (feof(fsyrr))
	{
	  printf("Result file empty !\n\n");
	  exit(1);
	}

      while(resu_syr_entete(version,&nt,temps+npdt)>0)
	{
	  npdt++;

	  /* attention : dimension max du tableau 'temps' en dur */
	  if (npdt>100000){
	    printf("\n  Impossible to convert more than 100000 time steps \n");
	    exit(1);
	  }

	  sprintf(nomnum,"%.5d",npdt);
	  
	  i=0;
	  while(resu_syr_nomvar(&nbre,nomvar[i],discr+i)==1)
	    {
	      /* ouverture du fichier resultat ensight */
	      strcpy(nomfichvar,nombase);
	      strncat(nomfichvar,".",1);
	      strncat(nomfichvar,nomvar[i],strlen(nomvar[i]));
	      strncat(nomfichvar,".",1);
	      strncat(nomfichvar,nomnum,5);
	      
	      var=(double*)malloc(nbre*sizeof(double));
	      resu_syr_var(nbre,var);
	      
	      if (bin)
		{
		  fvar=fopen(nomfichvar,"wb");
		  ecrit_var_ensight_bin(fvar,discr[i],nbre,nbno,
					temps[i],nomvar[i],var);
		}
	      else
		{
		  fvar=fopen(nomfichvar,"w");
		  ecrit_var_ensight_ascii(fvar,discr[i],nbre,nbno,
					  temps[i],nomvar[i],var,nelem,nrefe);
		}
	      free(var);
	      fclose(fvar);
	      i++;
	    }
	  nbscal=i;
	}

    } /* fin de si resu */


  free(nrefe);

  /* -------------------------------------------------------------------- */
  /*                       ecriture du fichier CASE                       */
  /* -------------------------------------------------------------------- */
  fprintf(fcase,"FORMAT\n");
  if (bin)
    fprintf(fcase,"type: ensight gold\n");
  else
    fprintf(fcase,"type: ensight\n");
  
  fprintf(fcase,"GEOMETRY\n");
  fprintf(fcase,"model: %s\n",nomfichgeom);

  if (resu)
    {
      fprintf(fcase,"VARIABLE\n");
      for (i=0;i<nbscal;i++)
	{
	  if (discr[i]==3)
	    fprintf(fcase,"scalar per node: 1     %s        %s.%s.*****\n",
		    nomvar[i],nombase,nomvar[i]);
	  else if (discr[i]==2) 
	    fprintf(fcase,"scalar per element: 1     %s        %s.%s.*****\n",
		    nomvar[i],nombase,nomvar[i]);
	}      
      fprintf(fcase,"TIME\n");
      fprintf(fcase,"time set:%15d\n",1);
      fprintf(fcase,"number of steps:%15d\n",npdt);
      fprintf(fcase,"filename start number:     1\n");
      fprintf(fcase,"filename increment:        1\n");
      fprintf(fcase,"time values:             %12.5e\n",temps[0]);
      for (i=1;i<npdt;i++)
	fprintf(fcase,"                         %12.5e\n",temps[i]);
    }


  printf("\n\n\n");
  printf("    *****************************************************\n");
  printf("    *                  S Y R T H E S                    *\n");
  printf("    *****************************************************\n");
  if (SYRTHES_LANG == FR)
    printf("    *          SYRTHES4ENSIGHT : FIN NORMALE            *\n");
  else if (SYRTHES_LANG == EN)
    printf("    *         SYRTHES4ENSIGHT : CONVERSION OK           * \n");
  printf("    *****************************************************\n\n");
  

  exit(0);

}

/*|======================================================================|
  | SYRTHES - Utilitaires      JUIL 95                                   |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | entete                                                               |
  |        lecture de l'entete du fichier geom de Syrthes                |
  |======================================================================| */
void entete(rp_int *ndim,rp_int *ndiele,rp_int *npoin,rp_int *nelem,rp_int *nbno,
	    rp_int *nelembord, rp_int *version)
{
  char ch[90];

  fgets(ch,90,fsyrg);

    /* entete version 4.3 */
    if (!strncmp(ch,"C*V4.0",6))
      {
	*version=40;
	fgets(ch,90,fsyrg);
	fgets(ch,90,fsyrg);
	fscanf(fsyrg,"%s%s%s%d\n",ch,ch,ch,ndim);
	fscanf(fsyrg,"%s%s%s%s%s%d\n",ch,ch,ch,ch,ch,ndiele);
	fscanf(fsyrg,"%s%s%s%s%s%d",ch,ch,ch,ch,ch,npoin);
	/*	fscanf(fsyrg,"%s%s%s%s%d",ch,ch,ch,ch,nelem); */
        fgets(ch,90,fsyrg); fgets(ch,23,fsyrg); fscanf(fsyrg,"%d",nelem);
	fscanf(fsyrg,"%s%s%s%s%s%s%d",ch,ch,ch,ch,ch,ch,nelembord);
	fscanf(fsyrg,"%s%s%s%s%s%s%s%d",ch,ch,ch,ch,ch,ch,ch,nbno);
      }
    else
      /* entete version 3.x */
      {
	*version=33;
	fgets(ch,90,fsyrg);
	fgets(ch,90,fsyrg);
	fscanf(fsyrg,"%s%s%s%d%s%s%s%s%d\n",ch,ch,ch,ndim,ch,ch,ch,ch,ndiele);
	fscanf(fsyrg,"%s%s%s%s%s%d",ch,ch,ch,ch,ch,npoin);
	fscanf(fsyrg,"%s%s%s%s%d",ch,ch,ch,ch,nelem);
	fscanf(fsyrg,"%s%s%s%s%s%s%s%d",ch,ch,ch,ch,ch,ch,ch,nbno);
	*nelembord=0;
      }

    printf("\n\nSYRTHES MESH :\n");
    printf("------------------\n");
    printf("   Dimension                   %d\n",*ndim);
    printf("   Elements dimension          %d\n",*ndiele);
    printf("   Number of nodes             %d\n",*npoin);
    printf("   Number of elements          %d\n",*nelem);
    printf("   Number of nodes per element %d\n",*nbno);
}
/*|======================================================================|
  | SYRTHES - Utilitaires      JUIL 95                                   |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | geom_syr                                                             |
  |        lecture de la geometrie du fichier Syrthes                    |
  |======================================================================| */
void geom_syr_coord(rp_int ndim,rp_int npoin,double **coord)
{
    char ch[90],ccc[3];
    double z;
    rp_int n,i,j,ii,np1;
    rp_int version,nbligne_entete;


    /* lecture du numero de version du fichier */
    rewind(fsyrg);
    fgets(ch,90,fsyrg);
    nbligne_entete=11;
    if (!strncmp(ch,"C*V4.0",6)) 
      {
	version=40;
	nbligne_entete=13;
      }
    else
      version=33;
    

    /* lecture du fichier */
    rewind(fsyrg);
    for (n=0;n<nbligne_entete;n++) fgets(ch,90,fsyrg);

    /* coordonnees noeuds maillage SYRTHES */
    if (ndim==2) 
      {
	for (n=0;n<npoin;n++) 
	  { 
	    fgets(ccc,2,fsyrg);
	    fscanf(fsyrg,"%d%d%lf%lf%lf",&i,&ii,(coord[0]+n),(coord[1]+n),&z);
	    fgets(ch,90,fsyrg);
	  }
      }
    else
      {
	for (n=0;n<npoin;n++) 
	  { 
	    fgets(ccc,2,fsyrg);
	    fscanf(fsyrg,"%d%d%lf%lf%lf",&i,&ii,(coord[0]+n),(coord[1]+n),(coord[2]+n));
	    fgets(ch,90,fsyrg);
	  }
      }
    
    printf("\n   End of reading coordinates\n");
     
}

/*|======================================================================|
  | SYRTHES - Utilitaires      JUIL 95                                   |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | geom_syr                                                             |
  |        lecture de la geometrie du fichier Syrthes                    |
  |======================================================================| */
void geom_syr_node(rp_int ndim,rp_int nelem,rp_int nbno,rp_int **node,rp_int *nrefe)
{
  rp_int i,j,n;
    char ch[90];

    /* connectivite maillage SYRTHES */
    fgets(ch,90,fsyrg);
    fgets(ch,90,fsyrg);
    fgets(ch,90,fsyrg);
    
    if (nbno==6)
      for (i=0;i<nelem;i++)
	  fscanf(fsyrg,"%d%d%d%d%d%d%d%d",&n,(nrefe+i),
		    (node[0]+i),(node[1]+i),(node[2]+i),(node[3]+i),
		    (node[4]+i),(node[5]+i)); 

    else if (nbno==3)
      for (i=0;i<nelem;i++)
	  fscanf(fsyrg,"%d%d%d%d%d",&n,(nrefe+i),
		    (node[0]+i),(node[1]+i),(node[2]+i));

    else if (nbno==2)
      for (i=0;i<nelem;i++)
	  fscanf(fsyrg,"%d%d%d%d",&n,(nrefe+i),
		    (node[0]+i),(node[1]+i));

    else if (nbno==4)
      for (i=0;i<nelem;i++)
	  fscanf(fsyrg,"%d%d%d%d%d%d",&n,(nrefe+i),
		 (node[0]+i),(node[1]+i),(node[2]+i),(node[3]+i));

    else if (nbno==10)
      for (i=0;i<nelem;i++)
	fscanf(fsyrg,"%d%d%d%d%d%d%d%d%d%d%d%d",&n,(nrefe+i),
	       (node[0]+i),(node[1]+i),(node[2]+i),(node[3]+i),
	       (node[4]+i),(node[5]+i),(node[6]+i),(node[7]+i),
	       (node[8]+i),(node[9]+i));


    fgets(ch,90,fsyrg);
    printf("\n   End of reading connectivity\n\n");

    /* on decale la numerotation de 1 pour etre coherent*/
    for (i=0;i<nelem;i++)
      for (j=0;j<nbno;j++)
	node[j][i]--;

}
/*|======================================================================|
  | SYRTHES - Utilitaires      JUIL 95                                   |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | geom_syr                                                             |
  |        lecture de la geometrie du fichier Syrthes                    |
  |======================================================================| */
void geom_syr_node_bord(rp_int ndim,rp_int nelembord,rp_int **nodebord,rp_int *nrefebord)
{
    char ch[90],ccc[3];
    double z;
    rp_int n,i,j,ii,np1;
    rp_int version,nbligne_entete;


    /* connectivite maillage de bord SYRTHES */
    fgets(ch,90,fsyrg);
    fgets(ch,90,fsyrg);
    fgets(ch,90,fsyrg);
    
    if (ndim==2)
      for (i=0;i<nelembord;i++)
	fscanf(fsyrg,"%d%d%d%d",&n,(nrefebord+i),(nodebord[0]+i),(nodebord[1]+i));
    
    
    else if (ndim==3)
      for (i=0;i<nelembord;i++)
	fscanf(fsyrg,"%d%d%d%d%d",&n,(nrefebord+i),
	       (nodebord[0]+i),(nodebord[1]+i),(nodebord[2]+i));
    
    /* on decale la numerotation de 1 pour etre coherent*/
    for (i=0;i<nelembord;i++)
      for (j=0;j<ndim;j++)
	nodebord[j][i]--;
}

/*|======================================================================|
  | SYRTHES - Utilitaires      JUIL 95                                   |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | combien_scalaires                                                    |
  |        lecture de l'enete du fichier resu pour determiner le nombre  |
  |        de scalaires a lire                                           |
  |======================================================================| */
void combien_scalaires(FILE *fsyrr,rp_int *nbscal,rp_int *npoin,rp_int *nelem,rp_int *version)
{
  rp_int n1,n,i;
  char ch[90],chrien[20],chlong[200];


  /* lecture de l'entete du fichier resultat */
  fseek(fsyrr,0,SEEK_SET);

  /* determination de la version du fichier */
  fgets(ch,90,fsyrr);

  /* entete version 4.3 */
  if (!strncmp(ch,"*V4.0",5))
    {
      *version=40;
      fgets(ch,120,fsyrr);
      fgets(ch,120,fsyrr);
      fscanf(fsyrr,"%4s%4s%1s%d%1s%7s%d%1s%6s%d\n",
	     chrien,chrien,chrien,&n,chrien,chrien,&n,chrien,chrien,nbscal);
      fscanf(fsyrr,"%4s%5s%1s%d%1s%5s%1s%d\n",
	     chrien,chrien,chrien,npoin,chrien,chrien,chrien,nelem);
    }
  else
    {
      for (i=0;i<3;i++) fgets(chlong,200,fsyrr);
      if (!strncmp(chlong+39,"NBNP2",5))
	{
	  *version=33;
	  fscanf(fsyrr,"%s%d%d%d%d%d%d",ch,&n,&n,nelem,&n1,npoin,nbscal);
	}
      else
	{
	  *version=34;
	  fscanf(fsyrr,"%s%d%d%d%d%d",ch,&n,&n,nelem,npoin,nbscal);
	}
    }

  printf("  --> number of variables per time step = %d\n\n",*nbscal);
}

/*|======================================================================|
  | SYRTHES - Utilitaires      JUIL 95                                   |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | resu_syr                                                             |
  |        lecture des resultats Syrthes                                 |
  |======================================================================| */
int resu_syr_entete(rp_int version,rp_int *nt,double *temps)
{
  rp_int i;
  double dt;
  char ch[200],chrien[20];
  
  /* on passe l'entete (-1 ligne deja lue avant pour voir s'il reste
     un pas de temps a lire)                                         */
  
  fgets(ch,200,fsyrr);
  if (feof(fsyrr))
    return -1;
  else
    {
      fgets(ch,200,fsyrr);
      fscanf(fsyrr,"%s%d%s%lf%s%lf\n",chrien,nt,chrien,temps,chrien,&dt);
      fgets(ch,200,fsyrr);
      printf("      Time step %d (%f seconds)\n",*nt,*temps);
      return 1;
    }
  
  
}

/*|======================================================================|
  | SYRTHES - Utilitaires      JUIL 95                                   |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | resu_syr                                                             |
  |        lecture des resultats Syrthes                                 |
  |======================================================================| */
void resu_syr_var(rp_int npoin,double *var)
{
    rp_int nbl,i,j;

    nbl=npoin/6;
    for (i=0;i<nbl;i++) 
      {
        j=i*6;
        fscanf(fsyrr,"%lf%lf%lf%lf%lf%lf",
                  (var+j),(var+j+1),(var+j+2),
           	  (var+j+3),(var+j+4),(var+j+5));
      }
    for (i=nbl*6;i<npoin;i++) fscanf(fsyrr,"%lf",(var+i));

    
}
/*|======================================================================|
  | SYRTHES - Utilitaires      JUIL 95                                   |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | resu_syr                                                             |
  |        lecture des resultats Syrthes                                 |
  |======================================================================| */
int resu_syr_nomvar(rp_int *nbre,char *nomvar,rp_int *discr)
{
  char ch[200],chrien[90];

    /* on lit,a priori la ligne qui contient le nom de la variable */
  fscanf(fsyrr,"%s",ch);

    /* c'est la fin de fichier */
    if (feof(fsyrr))
      return -1;

    if (strncmp(ch,"***VAR",6)){      /* on n'a pas lu une nouvelle variable */
      fgets(ch,200,fsyrr);            /* on lit la fin de la ligne */
      return 0;
    }

    /* on lit encore une variable du meme pas de temps */
    fscanf(fsyrr,"%s%s%d%s%d",nomvar, chrien,discr, chrien,nbre); 
    
    if (*discr==3)
      printf("       --> Reading variable %s (on nodes)\n",nomvar);
    else if (*discr==2)
      printf("       --> Reading variable %s (on elements)\n",nomvar);

    return 1;
}
/*|======================================================================|
  | SYRTHES - Utilitaires      JUIL 95                                   |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | geom_ens                                                             |
  |        eciture de la geometrie au format Ensight                     |
  |======================================================================| */
void geom_ens_coord_ascii(rp_int ndim,rp_int npoin,double **coord)
{
  rp_int n;
  
  /* ecriture des coorodonnees */

  fprintf(fgeom,"CALCUL SYRTHES\n");                                                         
  fprintf(fgeom,"conduction/rayonnement thermique\n"); 
  fprintf(fgeom,"node id given\n");  
  fprintf(fgeom,"element id given\n");  

  printf("\n   Writing %d nodes with Ensight format\n",npoin);
  fprintf(fgeom,"coordinates \n%8d\n",npoin);
  if (ndim==2)
    for (n=0;n<npoin;n++) 
      fprintf(fgeom,"%8d%12.5e%12.5e%12.5e\n",n+1,coord[0][n],coord[1][n],0.);
  else
    for (n=0;n<npoin;n++) 
      fprintf(fgeom,"%8d%12.5e%12.5e%12.5e\n",n+1,coord[0][n],coord[1][n],coord[2][n]);
}
/*|======================================================================|
  | SYRTHES - Utilitaires      JUIL 95                                   |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | geom_ens                                                             |
  |        eciture de la geometrie au format Ensight                     |
  |======================================================================| */
void geom_ens_coord_bin(rp_int ndim,rp_int npoin,double **coord)
{
  rp_int n,numpart;
  float *coosp;
  char ch[80];

  /* ecriture de l'entete */
  strncpy(ch,"C Binary\0",9);             fwrite(ch,sizeof(char),80,fgeom);
  strncpy(ch,"CALCUL SYRTHES\0",15);      fwrite(ch,sizeof(char),80,fgeom);
  strncpy(ch,"Thermal conduction/radiation\0",29);  fwrite(ch,sizeof(char),80,fgeom);
  strncpy(ch,"node id assign\0",15);      fwrite(ch,sizeof(char),80,fgeom);
  strncpy(ch,"element id assign\0",18 );  fwrite(ch,sizeof(char),80,fgeom);

  /* ecriture des coordonnees */
  printf("\n   Writing %d nodes with Ensight format\n",npoin);
  strncpy(ch,"part\0",5);                 fwrite(ch,sizeof(char),80,fgeom);
  numpart=1;                              fwrite(&numpart,sizeof(rp_int),1,fgeom);
  strncpy(ch,"Solid volume\0",13);      fwrite(ch,sizeof(char),80,fgeom);
  strncpy(ch,"coordinates\0",12);         fwrite(ch,sizeof(char),80,fgeom);
  fwrite(&npoin,sizeof(rp_int),1,fgeom);

  if (ecrgeom)
    {
      coosp=(float*)malloc(npoin*sizeof(float));
      for (n=0;n<npoin;n++) coosp[n]=(float)coord[0][n];
      fwrite(coosp,sizeof(float),npoin,fgeom);
      
      for (n=0;n<npoin;n++) coosp[n]=(float)coord[1][n];
      fwrite(coosp,sizeof(float),npoin,fgeom);
      
      if (ndim==2)
	for (n=0;n<npoin;n++) coosp[n]=0.;
      else
	for (n=0;n<npoin;n++) coosp[n]=(float)coord[2][n];
      fwrite(coosp,sizeof(float),npoin,fgeom);
      
      free(coosp);
    }

}
/*|======================================================================|
  | SYRTHES - Utilitaires      JUIL 95                                   |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | geom_ens_node                                                        |
  |        eciture de la geometrie au format Ensight                     |
  |======================================================================| */
void geom_ens_node_ascii(rp_int ndim,rp_int ndiele,rp_int nelem,rp_int nbno,rp_int **node,rp_int *nrefe,
			 rp_int *npartcree)
{
  rp_int i,j,n,numpart;
  char ch[80];
  rp_int iref[MAX_REF];


  /* ecriture des elements */
  if (noPlain==0)
    {
      printf("   Writing %d cells with Ensight format\n",nelem);
      if (ndiele==1)
	{
	  fprintf(fgeom,"part       1\n  Solid volume bar2\nbar2\n%8d\n",nelem);
	  for (n=0;n<nelem;n++) fprintf(fgeom,"%8d%8d%8d\n",n+1,node[0][n]+1,node[1][n]+1);
	}
      
      else if (ndiele==2)
	{
	  fprintf(fgeom,"part       1\n  Solid volume tria3\ntria3\n%8d\n",nelem);
	  for (n=0;n<nelem;n++) fprintf(fgeom,"%8d%8d%8d%8d\n",n+1,node[0][n]+1,node[1][n]+1,node[2][n]+1);
	}
      
      else if (ndiele==3)
	{  
	  fprintf(fgeom,"part       1\n  Solid volume tetra4\ntetra4\n%8d\n",nelem);
	  for (n=0;n<nelem;n++) 
	    {
	      fprintf(fgeom,"%8d",n+1);
	      for (j=0;j<4;j++) fprintf(fgeom,"%8d",node[j][n]+1);
	      fprintf(fgeom,"\n");
	    }
	}
      
      /* numero de la part courante */
      numpart=1;
    }
  else
    numpart=0;

  /* creation de part pour les proprietes physiques */
  /* ---------------------------------------------- */
  if (noVol==0)
    {
      for (i=0;i<MAX_REF;i++) iref[i]=0;
      
      /* on compte combien il y d'elements de chaque couleur */
      for (n=0;n<nelem;n++)
	if (nrefe[n]>0) iref[nrefe[n]]++;
      
      printf("\n");
      for (i=0;i<MAX_REF;i++) 
	if (iref[i]>0) 
	  {
	    printf("   Volume %2d : number of cells = %8d\n",i,iref[i]);
	    
	    if (ndiele==1)
	      {
		numpart++;
		fprintf(fgeom,"part %8d\n  Volume %3d\nbar2\n%8d\n",numpart,i,iref[i]);
		for (n=0;n<nelem;n++) 
		  if (nrefe[n]==i) 
		    {
		      fprintf(fgeom,"%8d%8d%8d\n",n+1,node[0][n]+1,node[1][n]+1); 
		    }
	      }
	    else if (ndiele==2)
	      {
		numpart++;
		fprintf(fgeom,"part %8d\n  Volume %3d\ntria3\n%8d\n",numpart,i,iref[i]);
		for (n=0;n<nelem;n++) 
		  if (nrefe[n]==i) 
		    fprintf(fgeom,"%8d%8d%8d%8d\n",n+1,node[0][n]+1,node[1][n]+1,node[2][n]+1); 
	      }
	    else
	      {
		numpart++;
		fprintf(fgeom,"part %8d\n  Volume %3d\ntetra4\n%8d\n",numpart,i,iref[i]);
		for (n=0;n<nelem;n++) 
		  if (nrefe[n]==i)
		    {
		      fprintf(fgeom,"%8d",n+1);
		      for (j=0;j<4;j++) fprintf(fgeom,"%8d",node[j][n]+1);
		      fprintf(fgeom,"\n");
		    }
	      }
	  }
    }

  *npartcree=numpart;
}

/*|======================================================================|
  | SYRTHES - Utilitaires      JUIL 95                                   |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | geom_ens_node                                                        |
  |        eciture de la geometrie au format Ensight                     |
  |======================================================================| */
void geom_ens_node_bin(rp_int ndim,rp_int ndiele,rp_int nelem,rp_int nbno,rp_int **node,rp_int *nrefe,
		       rp_int *npartcree,
		       rp_int npoin,double **coord)

{
  rp_int i,j,nn,n,nl,npoinloc,numpart;
  rp_int iref[MAX_REF],*no1d,*numloc,*numglob;
  char ch[80];
  float *coosp;

  for (n=0;n<500;n++) numglob_ssd[n]=NULL;
  for (n=0;n<500;n++) numglob_ssd_nb[n]=0;

  /* ecriture des elements */

  printf("   Writing %d elements with Ensight format\n",nelem);
  
  /* passage des elements en table 1D */
  
  if (ecrgeom)
    {

      no1d=(rp_int*)malloc(nelem*nbno*sizeof(rp_int));
      for (nn=0,n=0;n<nelem;n++) 
	for (i=0;i<nbno;i++)
	  { 
	    no1d[nn]=node[i][n]+1;  /* decalage de la numerotation */
	    nn++;
	  }
      
      if (ndiele==1)
	{
	  strncpy(ch,"bar2\0",5 );  fwrite(ch,sizeof(char),80,fgeom);
	  fwrite(&nelem,sizeof(rp_int),1,fgeom);
	}
      
      else if (ndiele==2)
	{
	  strncpy(ch,"tria3\0",6 );  fwrite(ch,sizeof(char),80,fgeom);
	  fwrite(&nelem,sizeof(rp_int),1,fgeom);
	}
      
      else if (ndiele==3)
	{  
	  strncpy(ch,"tetra4\0",7 );  fwrite(ch,sizeof(char),80,fgeom);
	  fwrite(&nelem,sizeof(rp_int),1,fgeom);
	}
      
      fwrite(no1d,sizeof(rp_int),nelem*nbno,fgeom);
      free(no1d);

    }
  /* numero de la part courante */
  numpart=2;

  /* creation de part pour les proprietes physiques */
  /* ---------------------------------------------- */

  for (n=0;n<MAX_REF;n++) iref[n]=0;

  numloc=(rp_int*)malloc(npoin*sizeof(rp_int));

  /* on compte combien il y d'elements de chaque couleur */
  for (n=0;n<nelem;n++)
    if (nrefe[n]>0) iref[nrefe[n]]++;
  
  for (n=0;n<MAX_REF;n++) 
    if (iref[n]>0) printf("   Number of elements of volume %2d = %8d\n",n,iref[n]);
  

  /* pour chaque part.... */
  for (i=1;i<MAX_REF;i++)
    if (iref[i]>0)
      {
	for (n=0;n<npoin;n++) numloc[n]=-1;
	for (nl=0,n=0;n<nelem;n++) 
	  if (nrefe[n]==i)
	    {
	      for (j=0;j<nbno;j++) 
		{
		  if (numloc[node[j][n]]<0)
		    {
		      numloc[node[j][n]]=nl;
		      nl++;
		    }
		}
	    }
	npoinloc=nl;
	numglob=(rp_int*)malloc(npoinloc*sizeof(rp_int));

	for (n=0;n<npoin;n++) 
	  if (numloc[n]>=0) numglob[numloc[n]]=n;

	/* sauvegarde des numeros globaux des noeuds de la part */
	numglob_ssd_nb[numpart]=npoinloc;
	numglob_ssd[numpart]=(rp_int*)malloc(npoinloc*sizeof(rp_int));
	for (n=0;n<npoinloc;n++) numglob_ssd[numpart][n]=numglob[n];
	
	/* ecriture de l'entete de la part */
	
	if (ecrgeom)

	  {
	    strncpy(ch,"part\0",5 );  fwrite(ch,sizeof(char),80,fgeom);
	    fwrite(&numpart,sizeof(rp_int),1,fgeom);
	    sprintf(ch,"Volume %3d\0",i);fwrite(ch,sizeof(char),80,fgeom);
	    strncpy(ch,"coordinates\0",12);  fwrite(ch,sizeof(char),80,fgeom);
	    fwrite(&npoinloc,sizeof(rp_int),1,fgeom);
	  }

	numpart++;
	    
	if (ecrgeom)
	  {
	    /* ecriture de l'entete des coord */
	    
	    coosp=(float*)malloc(npoinloc*sizeof(float));
	    for (n=0;n<npoinloc;n++) coosp[n]=(float)coord[0][numglob[n]];
	    fwrite(coosp,sizeof(float),npoinloc,fgeom);
	    for (n=0;n<npoinloc;n++) coosp[n]=(float)coord[1][numglob[n]];
	    fwrite(coosp,sizeof(float),npoinloc,fgeom);
	    if (ndim==2)
	      for (n=0;n<npoinloc;n++) coosp[n]=0.;
	    else
	      for (n=0;n<npoinloc;n++) coosp[n]=(float)coord[2][numglob[n]];
	    fwrite(coosp,sizeof(float),npoinloc,fgeom);
	    free(coosp);
	
	
	    /* ecriture de l'entete des elts */
	    printf("   Ecriture des %d elements au format Ensight\n",iref[i]);
	    if (ndiele==1)
	      {
		strncpy(ch,"bar2\0",5);  fwrite(ch,sizeof(char),80,fgeom);
		fwrite(iref+i,sizeof(rp_int),1,fgeom);
		no1d=(rp_int*)malloc(iref[i]*nbno*sizeof(rp_int));
		for (nn=0,n=0;n<nelem;n++) 
		  if (nrefe[n]==i) 
		    {
		      no1d[nn]=numloc[node[0][n]] + 1;      /* decalage de la numerotation */
		      no1d[nn+1]=numloc[node[1][n]] + 1;    /* decalage de la numerotation */
		      nn+=2;
		    }
		fwrite(no1d,sizeof(rp_int),iref[i]*2,fgeom);
		free(no1d);
	      }
	    
	    else if (ndiele==2)
	      {
		strncpy(ch,"tria3\0",6 );  fwrite(ch,sizeof(char),80,fgeom);
		fwrite(iref+i,sizeof(rp_int),1,fgeom);
		no1d=(rp_int*)malloc(iref[i]*3*sizeof(rp_int));
		for (nn=0,n=0;n<nelem;n++)                         /* decalage de la numerotation */
		  if (nrefe[n]==i) {for (j=0;j<3;j++) no1d[nn+j]=numloc[node[j][n]] + 1; nn+=3;}
		fwrite(no1d,sizeof(rp_int),iref[i]*3,fgeom);
		free(no1d);
	      }
	    
	    else if (ndiele==3)
	      {
		strncpy(ch,"tetra4\0",7);  fwrite(ch,sizeof(char),80,fgeom);
		fwrite(iref+i,sizeof(rp_int),1,fgeom);
		no1d=(rp_int*)malloc(iref[i]*4*sizeof(rp_int));
		for (nn=0,n=0;n<nelem;n++)                        /* decalage de la numerotation */
		  if (nrefe[n]==i) {for (j=0;j<4;j++) no1d[nn+j]=numloc[node[j][n]]+1; nn+=4;}
		fwrite(no1d,sizeof(rp_int),iref[i]*4,fgeom);
		free(no1d);
	      }

	  } /*fin du if ecrgeom */
      }

  *npartcree=numpart;

  
}

/*|======================================================================|
  | SYRTHES - Utilitaires      JUIL 95                                   |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | geom_ens                                                             |
  |        eciture de la geometrie au format Ensight                     |
  |======================================================================| */
void geom_ens_node_bord_ascii(rp_int ndim,rp_int ndiele,
			      rp_int nelembord,rp_int **nodebord,rp_int *nrefebord,
			      rp_int npartcree)
{
  rp_int i,j,n,nelemd,nbsselt,numpart;
  rp_int iref[MAX_REF];


  numpart=npartcree;

  /* creation de la part pour le maillage de bord   */
  /* cas de Syrthes version 4.x                     */
  /* ---------------------------------------------- */
  numpart++;

  if (ndiele==2) 
    {
      fprintf(fgeom,"part %8d\n  Solid border bar2\nbar2\n%8d\n",numpart,nelembord);
      for (n=0;n<nelembord;n++) fprintf(fgeom,"%8d%8d%8d\n",n+1,nodebord[0][n]+1,nodebord[1][n]+1);
    }
  else if (ndiele==3) 
    {
      fprintf(fgeom,"part %8d\n  Solid border tria3\ntria3\n%8d\n",numpart,nelembord);
      for (n=0;n<nelembord;n++) fprintf(fgeom,"%8d%8d%8d%8d\n",
					n+1,nodebord[0][n]+1,nodebord[1][n]+1,nodebord[2][n]+1);
    }
  
  /* creation d'une part par numero de reference */
  for (n=0;n<MAX_REF;n++) iref[n]=0;
  
  /* on compte combien il y d'elements de chaque couleur */
  for (n=0;n<nelembord;n++)
    if (nrefebord[n]>0) iref[nrefebord[n]]++;
  
  printf("\n");
  for (n=0;n<MAX_REF;n++) 
    if (iref[n]>0) 
      printf("   Surface %2d : number of border elements = %8d\n",n,iref[n]);
  
  
  
  for (i=0;i<MAX_REF;i++)
    if (iref[i]>0)
      {
	if (ndiele==2) /* alors elements de bord sont des poutres */
	  {
	    numpart++;
	    fprintf(fgeom,"part %8d\n  Surface %3d\nbar2\n%8d\n",numpart,i,iref[i]);
	    for (n=0;n<nelembord;n++) 
	      if (nrefebord[n]==i) 
		fprintf(fgeom,"%8d%8d%8d\n",n+1,nodebord[0][n]+1,nodebord[1][n]+1); 
	  }
	else if (ndiele==3) /* alors elements de bord sont des triangles */
	  {
	    numpart++;
	    fprintf(fgeom,"part %8d\n  Surface %3d\ntria3\n%8d\n",numpart,i,iref[i]);
	    for (n=0;n<nelembord;n++) 
	      if (nrefebord[n]==i)
		fprintf(fgeom,"%8d%8d%8d%8d\n",n+1,nodebord[0][n]+1,nodebord[1][n]+1,nodebord[2][n]+1); 
	  }
      }
  
}


/*|======================================================================|
  | SYRTHES - Utilitaires      JUIL 95                                   |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | geom_ens                                                             |
  |        eciture de la geometrie au format Ensight                     |
  |======================================================================| */
void geom_ens_node_bord_bin(rp_int ndim,rp_int ndiele,
			    rp_int nelembord,rp_int **nodebord,rp_int *nrefebord,
			    rp_int npartcree,
			    rp_int npoin,double **coord)
{
  rp_int i,j,n,nl,npoinloc,numpart,nn;
  rp_int iref[MAX_REF],*numloc,*numglob,*no1d;
  float *coosp;
  char ch[80];


  for (n=0;n<500;n++) numglob_ref[n]=NULL;
  for (n=0;n<500;n++) numglob_ref_nb[n]=0;

  numpart=npartcree;


  numloc=(rp_int*)malloc(npoin*sizeof(rp_int));
  for (n=0;n<MAX_REF;n++) iref[n]=0;
      
  /* on compte combien il y de faces chaque couleur */
  for (n=0;n<nelembord;n++)
    if (nrefebord[n]>0) iref[nrefebord[n]]++;
  
  for (i=1;i<MAX_REF;i++)
    if (iref[i]>0)
      {
	printf("   Number of faces with reference %2d = %10d\n",i,iref[i]);
	/* marquage des noeuds associes au x faces de cette couleur  */
	    
	for (n=0;n<npoin;n++) numloc[n]=-1;

	for (nl=0,n=0;n<nelembord;n++) 
	  if (nrefebord[n]==i)
	    {
	      if (numloc[nodebord[0][n]]<0) {numloc[nodebord[0][n]]=nl; nl++;}
	      if (numloc[nodebord[1][n]]<0) {numloc[nodebord[1][n]]=nl; nl++;}
	      if (ndim==3 && numloc[nodebord[2][n]]<0) {numloc[nodebord[2][n]]=nl; nl++;}
	    }

	npoinloc=nl;
	numglob=(rp_int*)malloc(npoinloc*sizeof(rp_int));
	for (n=0;n<npoin;n++)  if (numloc[n]>=0) numglob[numloc[n]]=n; 

	/* sauvegarde des numeros globaux des noeuds de la part */
	numglob_ref_nb[numpart]=npoinloc;
	numglob_ref[numpart]=(rp_int*)malloc(npoinloc*sizeof(rp_int));
	for (n=0;n<npoinloc;n++) numglob_ref[numpart][n]=numglob[n];

	if (ecrgeom)
	  {
	    /* ecriture de l'entete de la part */
	    strncpy(ch,"part\0",5);  fwrite(ch,sizeof(char),80,fgeom);
	    fwrite(&numpart,sizeof(rp_int),1,fgeom);
	    sprintf(ch,"Surface %3d\0",i);fwrite(ch,sizeof(char),80,fgeom);
	    strncpy(ch,"coordinates\0",12);  fwrite(ch,sizeof(char),80,fgeom);
	    fwrite(&npoinloc,sizeof(rp_int),1,fgeom);
	  }

	numpart++;

	if (ecrgeom)
	  {
	    /* ecriture de l'entete des coord */
	    coosp=(float*)malloc(npoinloc*sizeof(float));
	    for (n=0;n<npoinloc;n++) coosp[n]=(float)coord[0][numglob[n]];
	    fwrite(coosp,sizeof(float),npoinloc,fgeom);
	    for (n=0;n<npoinloc;n++) coosp[n]=(float)coord[1][numglob[n]];
	    fwrite(coosp,sizeof(float),npoinloc,fgeom);
	    if (ndim==2)
	      for (n=0;n<npoinloc;n++) coosp[n]=0.;
	    else
	      for (n=0;n<npoinloc;n++) coosp[n]=(float)coord[2][numglob[n]];
	    fwrite(coosp,sizeof(float),npoinloc,fgeom);
	    free(coosp);
	    
	    /* ecriture de l'entete des faces */
	    printf("   Writing %d faces with Ensight format\n",iref[i]);
	    if (ndiele==2)
	      {
		strncpy(ch,"bar2\0",5);  fwrite(ch,sizeof(char),80,fgeom);
		fwrite(iref+i,sizeof(rp_int),1,fgeom);
		no1d=(rp_int*)malloc(iref[i]*2*sizeof(rp_int));
		for (nn=0,n=0;n<nelembord;n++)
		  if (nrefebord[n]==i)
		    {
		      no1d[nn]=numloc[nodebord[0][n]] + 1;    /* decalage de la numerotation */
		      no1d[nn+1]=numloc[nodebord[1][n]] + 1;  /* decalage de la numerotation */
		      nn+=2;
		    }
		fwrite(no1d,sizeof(rp_int),iref[i]*2,fgeom);
		free(no1d);
	      }
	    
	    else if (ndiele==3)
	      {
		strncpy(ch,"tria3\0",6);  fwrite(ch,sizeof(char),80,fgeom);
		fwrite(iref+i,sizeof(rp_int),1,fgeom);
		no1d=(rp_int*)malloc(iref[i]*3*sizeof(rp_int));
		for (nn=0,n=0;n<nelembord;n++)
		  if (nrefebord[n]==i)
		    {
		      no1d[nn]=numloc[nodebord[0][n]] + 1;         /* decalage de la numerotation */
		      no1d[nn+1]=numloc[nodebord[1][n]] + 1;       /* decalage de la numerotation */
		      no1d[nn+2]=numloc[nodebord[2][n]] + 1;       /* decalage de la numerotation */
		      nn+=3;
		    }
		fwrite(no1d,sizeof(rp_int),iref[i]*3,fgeom);
		free(no1d);
	      }
	  } /* fin du if ecrgeom */
      }

  free(numloc);
}




/*|======================================================================|
  | SYRTHES - Utilitaires      JUIL 95                                   |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | ecrit_var_ensigh                                                     |
  |        eciture d'une varaible au format Ensight                      |
  |======================================================================| */
void ecrit_var_ensight_ascii(FILE *fvar,rp_int discr,rp_int nbre,rp_int nbno,
			     double temps,char *nomvar,double *var,
			     rp_int nelem,rp_int *nrefe)
{
  rp_int nbl,i,j,n,ne,nn,numpart;
  rp_int iref[MAX_REF],*itrav;
  
  if (discr==3)
    {
      fprintf(fvar,"%s a t= %12.5e (variable on nodes)\n",nomvar,temps);
      
    }
  else if (discr==2)
    {
      fprintf(fvar,"%s a t= %12.5e (variable on elements)\n",nomvar,temps);
      if (noPlain==0)
	{
	  fprintf(fvar,"part 1\n");
	  if (nbno==2)
	    fprintf(fvar,"bar2\n");
	  else if (nbno==3)
	    fprintf(fvar,"tria3\n");
	  else if (nbno==4)
	    fprintf(fvar,"tetra4\n");
	}
    }
  
  /* si on veut tout le maillage ou si on est sur les noeuds */
  if (noPlain==0 || discr==3)
    {
      nbl=nbre/6;
      for (i=0;i<nbl;i++) 
	{
	  j=i*6;
	  fprintf(fvar,"%12.5e%12.5e%12.5e%12.5e%12.5e%12.5e\n",
		  *(var+j),*(var+j+1),*(var+j+2),
		  *(var+j+3),*(var+j+4),*(var+j+5));
	}
      for (i=nbl*6;i<nbre;i++) fprintf(fvar,"%12.5e",*(var+i));
      fprintf(fvar,"\n");
    }


  /* si c'est une variable sur les elements, il faut la reecrire pour chaque sous-part de volume */
  if (discr==2 && noVol==0)
    {
      for (n=0;n<MAX_REF;n++) iref[n]=0;
      
      if (noPlain==1) numpart=1; 
      else numpart=2;
      
      itrav=(rp_int*)malloc(nelem*sizeof(rp_int)); 
      
      /* on compte combien il y d'elements de chaque couleur */
      for (n=0;n<nelem;n++)
	if (nrefe[n]>0) iref[nrefe[n]]++;
      
      for (n=0;n<MAX_REF;n++) 
	if (iref[n]>0) 
	  {
	    printf("               Volume %2d : number of cells = %8d\n",n,iref[n]);
	    for (ne=0,nn=0;ne<nelem;ne++) 
	      if (nrefe[ne]==n) {itrav[nn]=ne;nn++;}

	    fprintf(fvar,"part %d\n",numpart);
	    if (nbno==2)
	      fprintf(fvar,"bar2\n");
	    else if (nbno==3)
	      fprintf(fvar,"tria3\n");
	    else if (nbno==4)
	      fprintf(fvar,"tetra4\n");

	    nbl=nn/6;
	    for (i=0;i<nbl;i++) 
	      {
		j=i*6;
		fprintf(fvar,"%12.5e%12.5e%12.5e%12.5e%12.5e%12.5e\n",
			var[itrav[j]],var[itrav[j+1]],var[itrav[j+2]],
			var[itrav[j+3]],var[itrav[j+4]],var[itrav[j+5]]);
	      }
	    for (i=nbl*6;i<nn;i++) fprintf(fvar,"%12.5e",var[itrav[i]]);
	    fprintf(fvar,"\n");
	    
	    numpart++;
	  }
      free(itrav);
    }



}
/*|======================================================================|
  | SYRTHES - Utilitaires      JUIL 95                                   |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | ecrit_var_ensigh                                                     |
  |        eciture d'une varaible au format Ensight                      |
  |======================================================================| */
void ecrit_var_ensight_bin(FILE *fvar,rp_int discr,rp_int nbre,rp_int nbno,
			   double temps,char *nomvar,double *var)
{
  rp_int nbl,n,i,j,un=1;
  char ch[80];
  float  *varsp;
  

  if (discr==3)
    {
      sprintf(ch,"%s a t= %12.5e (variable on nodes)\0",nomvar,temps);
      fwrite(ch,sizeof(char),80,fvar);
      strncpy(ch,"part\0",5);  fwrite(ch,sizeof(char),80,fvar);
      fwrite(&un,sizeof(rp_int),1,fvar);
      strncpy(ch,"coordinates\0",12);  fwrite(ch,sizeof(char),80,fvar);
      varsp=(float*)malloc(nbre*sizeof(float));
      for (i=0;i<nbre;i++) varsp[i]=(float)var[i];
      fwrite(varsp,sizeof(float),nbre,fvar);

      /* ecriture de la variable sur les part ssd */
      for (n=0;n<500;n++)
	if (numglob_ssd_nb[n]>0)
	  {
	    for (i=0;i<numglob_ssd_nb[n];i++) varsp[i]=(float)var[numglob_ssd[n][i]];
	    strncpy(ch,"part\0",5);  fwrite(ch,sizeof(char),80,fvar);
	    fwrite(&n,sizeof(rp_int),1,fvar);
	    strncpy(ch,"coordinates\0",12);  fwrite(ch,sizeof(char),80,fvar);
	    fwrite(varsp,sizeof(float),numglob_ssd_nb[n],fvar);
	  }
      
      /* ecriture de la variable sur les part ref */
      for (n=0;n<500;n++)
	if (numglob_ref_nb[n]>0)
	  {
	    for (i=0;i<numglob_ref_nb[n];i++) varsp[i]=(float)var[numglob_ref[n][i]];
	    strncpy(ch,"part\0",5);  fwrite(ch,sizeof(char),80,fvar);
	    fwrite(&n,sizeof(rp_int),1,fvar);
	    strncpy(ch,"coordinates\0",12);  fwrite(ch,sizeof(char),80,fvar);
	    fwrite(varsp,sizeof(float),numglob_ref_nb[n],fvar);
	  }
      
      free(varsp);
    }

  else if (discr==1)
    {
      sprintf(ch,"%s a t= %12.5e (variable on elements)\0",nomvar,temps);
      fwrite(ch,sizeof(char),80,fvar);
      strncpy(ch,"part\0",5);  fwrite(ch,sizeof(char),80,fvar);
      fwrite(&un,sizeof(rp_int),1,fvar);

      if (nbno==2)
	strncpy(ch,"bar2\0",5);
      if (nbno==3)
	strncpy(ch,"tria3\0",6);
      else if (nbno==4)
	strncpy(ch,"tetra4\0",7);

      fwrite(ch,sizeof(char),80,fvar);

      varsp=(float*)malloc(nbre*sizeof(float));
      for (i=0;i<nbre;i++) varsp[i]=(float)var[i];
      fwrite(varsp,sizeof(float),nbre,fvar);
      free(varsp);     
    }

}

#!/bin/bash

#This file is for people installing the SYRTHES GUI,
#You may need first to install Python and different packages.
#To simplify the installation,
#included  are the packages sources, (alternatively you may download them from the web)
#The ones included should work together and be compatible.
#Note also that according the computer, compiling qt can be quite long (several hours).

# To help, here is the shell which does the installation automatically.
# To use it, create a directory (with the name you want)
# In that directory create two sub-directories
#     opt
#     src

# put in src the uncompressed sources of Python and all the external librairies
# ie the files
#      Python-3.4.4.tgz,
#      sip-4.18.1.tar.gz,
#      qt-everywhere-opensource-src-5.7.0.tar.gz,
#      PyQt5_gpl-5.7.tar.gz,
#      PyQtChart_gpl-5.7.tar.gz,
#      numpy-1.9.2.tar.gz
#
# Use unzip on all packages, so you should have :
#      Python-3.4.4,
#      sip-4.18.1,
#      qt-everywhere-opensource-src-5.7.0,
#      PyQt5_gpl-5.7,
#      PyQtChart_gpl-5.7,
#      numpy-1.9.2
#
# The present shell should be at the same level as directories src et opt.
# So you should have the following organisation.
# |
# |_opt______Python-3.4.4
# |      |___qt
# |_src__|___PyQt5_gpl-5.7
# |    __|___PyQtChart_gpl-5.7
# |      |___Python-3.4.4
# |      |___qt-everywhere-opensource-src-5.7.0
# |      |___sip-4.18.1
# |      |___numpy-1.9.2
# |_Python_Install_script.sh
#
#
#----------------------------------------------------------
# run the shell below named build_Python_for_SYRTHES_ihm.sh
# whose commands are below:
# You will have to answer some questions (licence of qt) - answer yes
# If you plan to use a more recent python, then replace in the following commands
# Python-3.4.4 by Python-3.x
#
# Normally, this long installation of Python and packages has to be done only once !
# and is not intented to be done by end-user,
# but only by experienced people installing the package.
# First a try with the pre-compiled version of SYRTHES GUI should be tempted.
#
# Once this has been done
# To build the stand alone SYRTHES
# ihm (with no packages necessary or conflict with another Python version)
# go in the directory syrthes-gui/Sources_syrthes-GUI_1.0.0
#
# using the python just installed, type the command :
# python setup_cxfl.py build
#
#
# To test, if the SYRTHES ihm starts, type :
# ./SyrthesMain
#-----------------------------------------------------------------------------------
#
# Compilation de Python-3.4.4
ici=`pwd`
echo "Compilation de Python-3.4.4"
echo "Compilation de Python-3.4.4">py_compil.log

python_path=$ici/opt/Python-3.4.4

cd $ici/src/Python-3.4.4
./configure --prefix=$python_path --enable-shared 2>&1| tee -a $ici/py_compil.log
make 2>&1| tee -a $ici/py_compil.log
make install 2>&1| tee -a $ici/py_compil.log

# Create symbolic link for python3.4 to python
cd $python_path/bin
ln -s python3.4 python

export PATH=$python_path/bin:${PATH}
export LD_LIBRARY_PATH=$python_path/lib:${LD_LIBRARY_PATH}
export PYTHONPATH=${python_path}/lib/python3.4:${PYTHONPATH}

# Compilation de sip-4.18

echo "Compilation de sip-4.18"
echo "Compilation de sip-4.18">>sip_compil.log

cd  $ici/src/sip-4.18.1
$python_path/bin/python configure.py 2>&1| tee -a $ici/sip_compil.log
make 2>&1| tee -a $ici/sip_compil.log
make install 2>&1| tee -a $ici/sip_compil.log

echo "Compilation de numpy-1.9.2"
echo "Compilation de numpy-1.9.2">>numpy_compil.log

cd $ici/src/numpy-1.9.2
$python_path/bin/python setup.py build 2>&1| tee -a $ici/numpy_compil.log
$python_path/bin/python setup.py install 2>&1| tee -a $ici/numpy_compil.log

# Compilation de Qt

echo "Compilation de Qt 5.7.0"
echo "Compilation de Qt">>qt_compil.log

cd $ici/src/qt-everywhere-opensource-src-5.7.0
qt_path=$ici/opt/qt

./configure --prefix=$qt_path -opensource -confirm-license -qt-xcb 2>&1| tee -a $ici/qt_compil.log
make 2>&1| tee -a $ici/qt_compil.log
make install 2>&1| tee -a $ici/qt_compil.log

export PATH=$qt_path/bin:${PATH}
export LD_LIBRARY_PATH=$qt_path/lib:${LD_LIBRARY_PATH}

# Compilation de PyQt-x11-gpl-4.7

echo "Compilation de PyQt-x11-gpl-4.7"
echo "Compilation de PyQt-x11-gpl-4.7">>pyqt_compil.log

cd $ici/src/PyQt5_gpl-5.7
$python_path/bin/python3 configure.py -q $ici/opt/qt/bin/qmake --confirm-license --verbose 2>&1| tee -a $ici/pyqt_compil.log
make 2>&1| tee -a $ici/pyqt_compil.log
make install 2>&1| tee -a $ici/pyqt_compil.log

# Compilation de PyCharts-5.7.0

echo "Compilation de PyCharts-5.7"
echo "Compilation de PyCharts-5.7">>qtchart_compil.log

cd $ici/src/PyQtChart_gpl-5.7/
$python_path/bin/python configure.py --qtchart-version=2.1.0 2>&1| tee -a $ici/qtchart_compil.log
make 2>&1| tee -a $ici/qtchart_compil.log
make install 2>&1| tee -a $ici/qtchart_compil.log

echo "Compilation de Syrthes GUI"
echo "Compilation de Syrthes GUI">>syrthes_compil.log

cd $ici/src
Install.sh | tee -a $ici/syrthes_compil.log

rm -rf $ici/exe.linux-x86_64
mkdir $ici/exe.linux-x86_64

cp -R $ici/src/install/build/exe.linux-x86_64-3.4/* $ici/exe.linux-x86_64

echo "The application GUI is built"
echo "Do you want to install the whole application 'yes'(y) or 'no'(n)"
read answer

ans1="y"
ans2="yes"

if [ "$answer" != "y" ] ; then
  exit
fi

echo "Build and install Syrthes application"

cd $ici/../syrthes-install
syrthes_install.py

#------------------------------------------------------------------
#Hope it will work for you
#Christophe Peniguel

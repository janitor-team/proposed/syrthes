# -*- coding: utf-8 -*-

from PyQt5 import QtCore, QtGui

class CpStatusTip(object):
    def CpStatusTip(self, parent=None):

        #CpStatustip dictionnary
        Cpdic_status={
            #calculation_progress
            self.progressBar : "Calculation progress estimation in %",#barre de progressions de Syrthes
            #self.Cb_Tep : "117",#combobox du paramètre d'évolution du temps
            self.Cb_Top : "Combo box to choose the probe to be drawn on graph 1 (defined in Output Window)",#combobox du choix du point dont lequle on veut l'évolution de la température
            self.textBrowser_3 : "Last 200 lines of the listing file",#texte des 100 dernières lignes du fichier de sorties
            self.textEdit_2 : "Window containing the complete listing file",#texte éditable de tout le fichier de sortie jusqu'à sa fin à l'instant t
            self.btnResetScale : "Scale reset button",
            self.Screenshot_pb : "Screenshot of the time evolution curve (png format)",
            self.Gnuplot_pb : "Write the Gnuplot data file (dat format)",
            self.PlotWindow : "Plot of the probes versus time (in s) - use mouse wheel to magnify, middle button to translate the curves and left button to create a rectangular selection ",
            self.Cb_Vars[0] : "Combo box to choose the physical quantity to be plotted",
            self.Cb_Vars[1] : "Combo box to choose the physical quantity to be plotted",
            self.Cb_Vars[2] : "Combo box to choose the physical quantity to be plotted",
            self.Cb_Vars[3] : "Combo box to choose the physical quantity to be plotted",
            self.Cb_Types[0] : "Combo box to choose the type of physical quantity to be plotted",
            self.Cb_Types[1] : "Combo box to choose the type of physical quantity to be plotted",
            self.Cb_Types[2] : "Combo box to choose the type of physical quantity to be plotted",
            self.Cb_Types[3] : "Combo box to choose the type of physical quantity to be plotted",
            self.Rb_ButtonGroups[0].button(1) : "Scale the curve on the left axis",
            self.Rb_ButtonGroups[0].button(2) : "Hide the curve",
            self.Rb_ButtonGroups[0].button(3) : "Scale the curve on the right axis",
            self.Rb_ButtonGroups[1].button(1) : "Scale the curve on the left axis",
            self.Rb_ButtonGroups[1].button(2) : "Hide the curve",
            self.Rb_ButtonGroups[1].button(3) : "Scale the curve on the right axis",
            self.Rb_ButtonGroups[2].button(1) : "Scale the curve on the left axis",
            self.Rb_ButtonGroups[2].button(2) : "Hide the curve",
            self.Rb_ButtonGroups[2].button(3) : "Scale the curve on the right axis",
            self.Rb_ButtonGroups[3].button(1) : "Scale the curve on the left axis",
            self.Rb_ButtonGroups[3].button(2) : "Hide the curve",
            self.Rb_ButtonGroups[3].button(3) : "Scale the curve on the right axis",

            self.cbStyle_tab1 : "Line style of Graph 1",
            self.cbStyle_tab2 : "Line style of Graph 2",
            self.cbStyle_tab3 : "Line style of Graph 3",
            self.cbStyle_tab4 : "Line style of Graph 4",
            self.Cb_Top2 : "Combo box to choose the probe number to be drawn on graph 2 (defined in Output window)", 
            self.Cb_Top3 : "Combo box to choose the probe number to be drawn on graph 3 (defined in Output window)",
            self.Cb_Top4 : "Combo box to choose the probe number to be drawn on graph 4 (defined in Output window)"} 

        for wid in self.widget:
            wid.setStatusTip(QtCore.QCoreApplication.translate("MainWindow", Cpdic_status[wid]))
        pass
    

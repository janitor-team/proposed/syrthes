# -*- coding: utf-8 -*-

from ui_Calculation_progress import Ui_Calculation_progress
from CpStatusTip import CpStatusTip # classe des messages de la barre de statut du suivis de calcul
from CpToolTip import CpToolTip # classe des infobulles du suivis de calcul
from CpWhatsThis import CpWhatsThis # classe des what's this du suivis de calcul
from syrthesIHMContext import syrthesIHMContext

#-------------------------------------------------------------------------------
# importation des bibliothèques standard
#-------------------------------------------------------------------------------

import os, sys, string, subprocess, shutil, time, datetime, filecmp, threading, re

#-------------------------------------------------------------------------------
# importation des bibliothèques IHM
#-------------------------------------------------------------------------------
from PyQt5 import QtCore, QtWidgets, QtGui

try:
    from PyQt5 import QtChart
except:
    import QtChart

# numpy est utile pour cx_freeze en particulier sur ubuntu
import numpy
import PyQt5.Qt as Qt
from PyQt5.QtCore import pyqtSignal

try:
    if os.name=='nt':
        import winreg
    # import PyQt5.Qwt6 as Qwt
except:
    print("ATTENTION PROBLEME A L'IMPORTATION DE PYQWT OU _WINREG")



class calcView(QtWidgets.QMainWindow, Ui_Calculation_progress, CpStatusTip, CpToolTip, CpWhatsThis, object):
# classe du suivis de calcul
    Syrthes_stopping = pyqtSignal()
    Syrthes_completed = pyqtSignal()

    def __init__(self, Home_form_copy, Control_form_copy, Filename_form_copy, Output_2D_form_copy, Output_3D_form_copy, Running_options_form_copy, case_copy, lastDir_copy, runProcess_copy, parent=None): # fonction d'initialisation de la fenêtre de suivis de calcul
        # respecter l'ordre des appels
        QtWidgets.QMainWindow.__init__(self)
        Ui_Calculation_progress.__init__(self)
        CpStatusTip.__init__(self)
        CpToolTip.__init__(self)
        CpWhatsThis.__init__(self)
        Ui_Calculation_progress.setupUi(self,self)

        self.Home_form_copy = Home_form_copy
        self.Control_form_copy = Control_form_copy
        self.Filename_form_copy = Filename_form_copy
        self.Output_2D_form_copy = Output_2D_form_copy
        self.Output_3D_form_copy = Output_3D_form_copy
        self.Running_options_form_copy = Running_options_form_copy
        self.case_copy = case_copy
        self.lastDir_copy = lastDir_copy
        self.runProcess_copy = runProcess_copy
        self.nline=0
        self.running=False
        self.step=-1
        self.substep=0
        self.history_names=[]
        self.surface_balance_names=[]
        self.volume_balance_names=[]
        self.updateCalcView()

    def updateCalcView(self):
        # définir le style de QGroupBox
        path = syrthesIHMContext.getExeAbsDirPath()
        qssname = path + os.sep + "22x22" + os.sep + "stylesheet.qss"
        if os.access(qssname, os.F_OK) :
            qss = open(qssname, "r")
            qstr = ""
            for line in qss.readlines() :
                qstr += line
            self.centralwidget.setStyleSheet(qstr)

        self.dictProbe = {} # empty dictionnary. will be of format { "N": filename, ...}, value = "-1" if probe is not found in any files
        self.listColor = [Qt.Qt.green, Qt.Qt.red, Qt.Qt.blue, Qt.Qt.darkYellow, Qt.Qt.cyan, Qt.Qt.magenta, Qt.Qt.gray, Qt.Qt.darkGreen, Qt.Qt.darkRed, Qt.Qt.darkBlue, Qt.Qt.darkGray, Qt.Qt.darkCyan, Qt.Qt.darkMagenta, Qt.Qt.black, Qt.Qt.yellow]
        nbcl = len(self.listColor)
        for cl in range(nbcl) :
            self.listColor.append(self.listColor[cl])
        nbcl = len(self.listColor)
        for cl in range(nbcl) :
            self.listColor.append(self.listColor[cl])
        nbcl = len(self.listColor)
        for cl in range(nbcl) :
            self.listColor.append(self.listColor[cl])
        self.fn = self.fontInfo().family()
        self.iLastLineListing = -1
        self.tickflag=True

        ##########################################################################
        # Combobox - choix de sondes
        # curves - préparer les courbes vides pour chaque sonde
        self.__init_Cb_Tops()

        ##########################################################################
        # create Combobox - nature variable (température, pression...)
        self.Cb_Vars = []
        self.Cb_Types = []
        self.Rb_ButtonGroups = []
        self.gridLayout_tabs = []
        self.gridLayout_tabs.append(self.gridLayout_tab1)
        self.gridLayout_tabs.append(self.gridLayout_tab2)
        self.gridLayout_tabs.append(self.gridLayout_tab3)
        self.gridLayout_tabs.append(self.gridLayout_tab4)

        for i in range(self.tabPlot.count()) :
            self.Cb_Types.append(QtWidgets.QComboBox(self.centralwidget))
            self.Cb_Vars.append(QtWidgets.QComboBox(self.centralwidget))
            sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
            sizePolicy.setHorizontalStretch(0)
            sizePolicy.setVerticalStretch(0)
            sizePolicy.setHeightForWidth(self.Cb_Vars[i].sizePolicy().hasHeightForWidth())
            sizePolicy.setHeightForWidth(self.Cb_Types[i].sizePolicy().hasHeightForWidth())
            self.Cb_Types[i].setSizePolicy(sizePolicy)
            self.Cb_Types[i].setObjectName("Cb_Type_tab" + str(i+1))
            self.Cb_Types[i].addItem("History")
            self.Cb_Vars[i].setSizePolicy(sizePolicy)
            self.Cb_Vars[i].setObjectName("Cb_Var_tab" + str(i+1))
            if self.nbRefSURF != 0 : self.Cb_Types[i].addItem("Surface balance:")
            if self.nbRefVOL != 0 : self.Cb_Types[i].addItem("Volume balance:")
            self.gridLayout_tabs[i].addWidget(self.Cb_Types[i], 1, 1, 1, 1) # 2e ligne, 2e colonne, 1 span horizontal, 2 span vertical
            self.gridLayout_tabs[i].addWidget(self.Cb_Vars[i], 1, 2, 1, 1) # 2e ligne, 3e colonne, 1 span horizontal, 2 span vertical

            self.Rb_ButtonGroups.append(QtWidgets.QButtonGroup(self.centralwidget))
            yleft = QtWidgets.QRadioButton("yleft")
            hide = QtWidgets.QRadioButton("hide")
            yright = QtWidgets.QRadioButton("yright")
            horizontalLayout = QtWidgets.QHBoxLayout()
            horizontalLayout.setObjectName("horizontalLayout"+str(i+1))
            self.Rb_ButtonGroups[i].addButton(yleft,1)
            self.Rb_ButtonGroups[i].addButton(hide,2)
            self.Rb_ButtonGroups[i].addButton(yright,3)
            yleft.setChecked(True)

            horizontalLayout.addWidget(yleft)
            horizontalLayout.addWidget(hide)

            self.gridLayout_tabs[i].addLayout(horizontalLayout, 3, 1, 1, 1)
            self.gridLayout_tabs[i].addWidget(yright, 3, 2, 1, 1)
        ##########################################################################
        # create Combobox - line style
        self.ne_pas_repondre_style = False
        self.Cb_LineStyles = []
        self.Cb_LineStyles.append(self.cbStyle_tab1)
        self.Cb_LineStyles.append(self.cbStyle_tab2)
        self.Cb_LineStyles.append(self.cbStyle_tab3)
        self.Cb_LineStyles.append(self.cbStyle_tab4)

        for i in range(self.tabPlot.count()) :
            # line style
            self.Cb_LineStyles[i].setIconSize(QtCore.QSize(52,12))

            # "———————"
            #self.Cb_LineStyles[i].addItem(chr(151) + chr(151) + chr(151) + chr(150) + chr(150))
            iconpath = syrthesIHMContext.getExeAbsDirPath() + os.sep + "22x22" + os.sep + "dash52x12.png"
            icon = QtGui.QIcon(iconpath)
            self.Cb_LineStyles[i].addItem("")
            self.Cb_LineStyles[i].setItemIcon(0, icon)

            # "— — — — — — —"
            #self.Cb_LineStyles[i].addItem(chr(150) + ' ' + chr(150) + ' ' + chr(150) + ' ' + chr(150) + ' ' + chr(150))
            iconpath = syrthesIHMContext.getExeAbsDirPath() + os.sep + "22x22" + os.sep + "dashespace52x12.png"
            icon = QtGui.QIcon(iconpath)
            self.Cb_LineStyles[i].addItem("")
            self.Cb_LineStyles[i].setItemIcon(1, icon)

            # "•••••••••"
            #self.Cb_LineStyles[i].addItem(chr(149) + chr(149) + chr(149) + chr(149) + chr(149) + chr(149) + chr(149) + chr(149) + chr(149) + chr(149))
            iconpath = syrthesIHMContext.getExeAbsDirPath() + os.sep + "22x22" + os.sep + "dot52x12.png"
            icon = QtGui.QIcon(iconpath)
            self.Cb_LineStyles[i].addItem("")
            self.Cb_LineStyles[i].setItemIcon(2, icon)

            # "— • — • — • —"
            #self.Cb_LineStyles[i].addItem(chr(150) + ' ' + chr(183) + ' ' + chr(150) + ' ' + chr(183) + ' ' + chr(150))
            iconpath = syrthesIHMContext.getExeAbsDirPath() + os.sep + "22x22" + os.sep + "dashdot52x12.png"
            icon = QtGui.QIcon(iconpath)
            self.Cb_LineStyles[i].addItem("")
            self.Cb_LineStyles[i].setItemIcon(3, icon)

        ###########################################################################
        # plot
        self.xposold=None
        self.yposold=None
        self.mark=False

        self.plot = QtChart.QChart()
        self.plot.setObjectName("plot")
        self.plot.setBackgroundBrush(self.palette().brush(self.backgroundRole()))
        self.plot.setPlotAreaBackgroundVisible(True)
        self.plot.setPlotAreaBackgroundBrush(QtGui.QBrush(Qt.Qt.white))
        self.plot.legend().setAlignment(QtCore.Qt.AlignBottom)
        self.plot.setAcceptHoverEvents(True)
        self.plot.setMargins(QtCore.QMargins(0, 0, 0, 0))

        self.axisX = QtChart.QValueAxis()
        self.axisX.setRange(0, 1000)
        self.axisX.setTickCount(6)
        self.axisX.setTitleText('Time (s)')
        self.axisX.setLabelFormat("%i")
        self.axisX.setMinorTickCount(4)
        self.plot.addAxis(self.axisX, QtCore.Qt.AlignBottom)

        self.axisY = QtChart.QValueAxis()
        self.axisY.setRange(0, 100)
        self.axisY.setTickCount(9)
        self.axisY.setTitleText('Temperature (°C)')
        self.axisY.setLabelFormat("%i")
        self.axisY.setMinorTickCount(3)
        self.plot.addAxis(self.axisY, QtCore.Qt.AlignLeft)

        self.axisRY = QtChart.QValueAxis()
        self.axisRY.setRange(0, 100)
        self.axisRY.setTickCount(9)
        self.axisRY.setTitleText('Temperature (°C)')
        self.axisRY.setLabelFormat("%i")
        self.axisRY.setMinorTickCount(3)

        self.PlotWindow = QtChart.QChartView(self.plot, self)
        self.verticalLayout.addWidget(self.PlotWindow)

        self.RubberBand = QtWidgets.QRubberBand(QtWidgets.QRubberBand.Rectangle, self.PlotWindow)

        ###########################################################################
        # check availability of data

        self.marker = QtWidgets.QGraphicsTextItem('Awaiting data', self.plot)
        self.marker.setPos(250, 150)
        if not os.access(self.case_copy.getHisFullPath(0), os.F_OK):
            self.marker.show()
            self.mark=True
        else:
            self.marker.hide()
            self.mark=False

        # enregistrer les coordonnées de la sonde dans l'objet curves[j]
        # curves - préparer les courbes vides pour chaque sonde
        self.curves = {}

        if self.nbProbes > 0 :
            self.Cb_Tops[0].setCurrentIndex(0)
        ###########################################################################
        # déterminer le nombre de réf bilan surfacique/volumique

        ###########################################################################
        # réaffacter tabPlot à la classe fille (clPlotTab) pour plus de fonctionnalité !
        #self.tabPlot = clPlotTab(self.tabPlot)
        #print "tab2 :",self.tabPlot
        #print "tabtab :",self.tabPlot
        self.tabPlot.initView()

        ###########################################################################
        # signaux - slot
        self.Cb_Top.currentIndexChanged.connect(self.Cb_Top_Clb2)
        self.Cb_Top2.currentIndexChanged.connect(self.Cb_Top_Clb2)
        self.Cb_Top3.currentIndexChanged.connect(self.Cb_Top_Clb2)
        self.Cb_Top4.currentIndexChanged.connect(self.Cb_Top_Clb2)

        self.Cb_Vars[0].currentIndexChanged.connect(self.Cb_Var_Clb)
        self.Cb_Vars[1].currentIndexChanged.connect(self.Cb_Var_Clb)
        self.Cb_Vars[2].currentIndexChanged.connect(self.Cb_Var_Clb)
        self.Cb_Vars[3].currentIndexChanged.connect(self.Cb_Var_Clb)

        self.Cb_Types[0].currentIndexChanged.connect(self.Cb_Type_Clb)
        self.Cb_Types[1].currentIndexChanged.connect(self.Cb_Type_Clb)
        self.Cb_Types[2].currentIndexChanged.connect(self.Cb_Type_Clb)
        self.Cb_Types[3].currentIndexChanged.connect(self.Cb_Type_Clb)

        self.cbStyle_tab1.currentIndexChanged.connect(self.Cb_Style_Clb)
        self.cbStyle_tab2.currentIndexChanged.connect(self.Cb_Style_Clb)
        self.cbStyle_tab3.currentIndexChanged.connect(self.Cb_Style_Clb)
        self.cbStyle_tab4.currentIndexChanged.connect(self.Cb_Style_Clb)

        self.Rb_ButtonGroups[0].buttonClicked[int].connect(self.Rb_ButtonGroup_Clb)
        self.Rb_ButtonGroups[1].buttonClicked[int].connect(self.Rb_ButtonGroup_Clb)
        self.Rb_ButtonGroups[2].buttonClicked[int].connect(self.Rb_ButtonGroup_Clb)
        self.Rb_ButtonGroups[3].buttonClicked[int].connect(self.Rb_ButtonGroup_Clb)

        self.Screenshot_pb.clicked.connect(self.Screenshot)
        self.Gnuplot_pb.clicked.connect(self.Gnuplot)
        self.btnResetScale.clicked.connect(self.ResetScale)
        self.tabWidget.currentChanged.connect(self.RefreshListing)

        self.widget=[self.progressBar,
                     self.Cb_Top,
                     self.btnResetScale,
                     self.textBrowser_3,
                     self.textEdit_2,
                     self.Screenshot_pb,
                     self.Gnuplot_pb,
                     self.PlotWindow,
                     self.Cb_Vars[0],
                     self.Cb_Vars[1],
                     self.Cb_Vars[2],
                     self.Cb_Vars[3],
                     self.Cb_Types[0],
                     self.Cb_Types[1],
                     self.Cb_Types[2],
                     self.Cb_Types[3],
                     self.Rb_ButtonGroups[0].button(1),
                     self.Rb_ButtonGroups[0].button(2),
                     self.Rb_ButtonGroups[0].button(3),
                     self.Rb_ButtonGroups[1].button(1),
                     self.Rb_ButtonGroups[1].button(2),
                     self.Rb_ButtonGroups[1].button(3),
                     self.Rb_ButtonGroups[2].button(1),
                     self.Rb_ButtonGroups[2].button(2),
                     self.Rb_ButtonGroups[2].button(3),
                     self.Rb_ButtonGroups[3].button(1),
                     self.Rb_ButtonGroups[3].button(2),
                     self.Rb_ButtonGroups[3].button(3),
                     self.cbStyle_tab1,
                     self.cbStyle_tab2,
                     self.cbStyle_tab3,
                     self.cbStyle_tab4,
                     self.Cb_Top2,
                     self.Cb_Top3,
                     self.Cb_Top4]

        self.CpStatusTip()
        self.CpToolTip()
        self.CpWhatsThis()

        ###########################################################################
        # coups d'horloge pour le rafraîchissement de l'IHM du suivis de calcul
        self.timer= QtCore.QTimer()
        ticker=self.timer.singleShot(1000, self.Time_Clb)

        ###########################################################################
        # zoom
        self.plot.wheelEvent = self.wheel_event

        self.PlotWindow.mousePressEvent=self.mouse_press
        self.PlotWindow.mouseMoveEvent=self.mouse_move
        self.PlotWindow.mouseReleaseEvent=self.raz_mouses
        self.plot.resizeEvent = self.resizeChart


    def __init_Cb_Tops(self):
        # Combobox - choix de sondes
        self.Cb_Top.setEnabled(True)
        self.Cb_Tops = []
        self.Cb_Tops.append(self.Cb_Top)
        self.Cb_Tops.append(self.Cb_Top2)
        self.Cb_Tops.append(self.Cb_Top3)
        self.Cb_Tops.append(self.Cb_Top4)

        if self.Home_form_copy.Dim_Comb.currentIndex()==0:
            Table = self.Output_3D_form_copy.Op_Dc_3D_table
            TableSURF = self.Output_3D_form_copy.Op_Sb_3D_table
            TableVOL = self.Output_3D_form_copy.Op_Vb_3D_table
        else:
            Table=self.Output_2D_form_copy.Op_Dc_2D_table
            TableSURF = self.Output_2D_form_copy.Op_Sb_2D_table
            TableVOL = self.Output_2D_form_copy.Op_Vb_2D_table

        # remplir les Cb_Top - bug potentiel : hasn't checked Table.item(i, 4)!=None yet
        # other possible bugs : i, j, ...

        pixmap = QtGui.QPixmap(10,10) # mod GA 2011 (10,10) inside of 30*30
        for Cb_Top in self.Cb_Tops :
            i = 0 # ligne non vide dans le tableau output
            j = 0 # numéro de courbe dans le combobox - devra-t-il correspondre au numéro de ligne dans le tableau ?
            while i<Table.rowCount():
                if ((Table.cellWidget(i, 0).isChecked()==True) and
                    (Table.item(i, 1)!=None and Table.item(i, 2)!=None)): # ligne checké et non vide
                    if Table.item(i, 1).text() != "" and Table.item(i,2).text() != "" :
                        if Cb_Top.findText(str(i+1))==-1:
                            Cb_Top.insertItem(j, str(i+1))

                            # créer un carré de couleur self.listColor[j]
                            pixmap.fill(QtGui.QColor(self.listColor[i]));
                            Cb_Top.setItemData(j, QtCore.QVariant(pixmap), Qt.Qt.DecorationRole)

                            pass # end if not yet listed in Cb_top
                        j=j+1
                i=i+1 # end while

            Cb_Top.addItem("None")
            Cb_Top.setCurrentIndex(-1) # combobox vide au départ

        # sauvegarder le nombre de sondes pour utiliser plus tard
        self.nbProbes = self.Cb_Tops[0].count() - 1
        self.probesList = [int(self.Cb_Tops[0].itemText(i)) - 1 for i in range(self.Cb_Tops[0].count() - 1)]
        # déterminer le nombre de réf Bilan SURF pour utiliser plus tard
        i = 0 # ligne non vide dans le tableau output
        j = 0 # numéro de courbe dans le combobox - devra-t-il correspondre au numéro de ligne dans le tableau ?
        while i<TableSURF.rowCount():
            if TableSURF.cellWidget(i, 0).isChecked() and (TableSURF.item(i, 1)!=None): # ligne checké et non vide
                if TableSURF.item(i, 1).text() != "" :
                    j=j+1
            i=i+1 # end while
        self.nbRefSURF = j

        # déterminer le nombre de réf Bilan VOL pour utiliser plus tard
        i = 0 # ligne non vide dans le tableau output
        j = 0 # numéro de courbe dans le combobox - devra-t-il correspondre au numéro de ligne dans le tableau ?
        while i<TableVOL.rowCount():
            if TableVOL.cellWidget(i, 0).isChecked() and (TableVOL.item(i, 1)!=None): # ligne checké et non vide
                if TableVOL.item(i, 1).text() != "" :
                    j=j+1
            i=i+1 # end while
        self.nbRefVOL = j
        print(j)



    def showCoordinates(self, theEvent):
        if not self.plot.isUnderMouse():
            return
        position = theEvent.pos()
        result=[]
        aSeriesList = self.plot.series()
        if len(aSeriesList) == 0:
            return
        yAxisTitle = "%s"%(self.plot.axisY().titleText())

        yunit=''
        ylabel='y'
        yrunit=''
        yrlabel='yr'
        xunit=''
        xlabel='x'

        if yAxisTitle.find("°C") != -1:
            ylabel='T'
            yunit='°C'
        elif yAxisTitle.find("Pa") != -1:
            ylabel='P'
            yunit="Pa"
        elif yAxisTitle.find("W") !=-1:
            ylabel='P'
            yunit="W"

        aAxesList = self.plot.axes()
        axesNb = len(aAxesList)

        aPosVal = self.plot.mapToValue(position, aSeriesList[0])
        aValues = []
        aValues.append( (xlabel, aPosVal.x(), xunit) )
        aValues.append( (ylabel, aPosVal.y(), yunit) )
        if (axesNb == 3):
            # find series attached to right Y axis
            aRYaxis = aAxesList[2]
            aRSeries = None
            for aSeries in aSeriesList:
                if aRYaxis in aSeries.attachedAxes():
                    aRSeries = aSeries
                    break
            if not aRSeries is None:
                aPosVal = self.plot.mapToValue(position, aRSeries)
                aValues.append( (yrlabel, aPosVal.y(), yrunit) )
        for aVal in aValues:
            result.append("%s=%.6g %s" % aVal)

        if len(result) == 3:
            self.plot.setToolTip(QtWidgets.QApplication.translate("MainWindow", '[ '+result[0] +' , ' +result[1]+' , ' +result[2]+' ]'))
        else:
            self.plot.setToolTip(QtWidgets.QApplication.translate("MainWindow", '[ '+result[0] +' , ' +result[1]+' ]'))
        pass



    def updateCb_Top(self, tabIndex): # fonction de rappel lorsque le combobox type de variable est changé
        if self.Cb_Types[tabIndex].currentText() == "Surface balance:" :
            if self.Home_form_copy.Dim_Comb.currentIndex()==0:
                Table = self.Output_3D_form_copy.Op_Sb_3D_table
            else:
                Table = self.Output_2D_form_copy.Op_Sb_2D_table

        elif self.Cb_Types[tabIndex].currentText() == "Volume balance:" :
            if self.Home_form_copy.Dim_Comb.currentIndex()==0:
                Table = self.Output_3D_form_copy.Op_Vb_3D_table
            else:
                Table = self.Output_2D_form_copy.Op_Vb_2D_table
        else:
            if self.Home_form_copy.Dim_Comb.currentIndex()==0:
                Table = self.Output_3D_form_copy.Op_Dc_3D_table
            else:
                Table = self.Output_2D_form_copy.Op_Dc_2D_table

        i = 0 # ligne non vide dans le tableau output
        j = 0 # numéro de courbe dans le combobox - devra-t-il correspondre au numéro de ligne dans le tableau ?
        Cb_Top = self.Cb_Tops[tabIndex]
        while Cb_Top.count() > 0 : Cb_Top.removeItem(0) # clear combobox
        pixmap = QtGui.QPixmap(10,10) # mod GA 2011 (10,10) inside of 30*30
        while i<Table.rowCount():
            if ((Table.cellWidget(i, 0).isChecked()==True) and
                (Table.item(i, 1)!=None)): # ligne checké et non vide
                if Table.item(i,1).text() != "" :
                    if Cb_Top.findText(str(i+1))==-1:
                        Cb_Top.insertItem(j, str(i+1))

                        # créer un carré de couleur self.listColor[j]
                        pixmap.fill(QtGui.QColor(self.listColor[i]));
                        Cb_Top.setItemData(j, QtCore.QVariant(pixmap), Qt.Qt.DecorationRole)

                        pass # end if not yet listed in Cb_top
                    j=j+1
            i=i+1 # end while
        Cb_Top.addItem("None")
        Cb_Top.setCurrentIndex(Cb_Top.findText("None")) # combobox vide au départ

    def updateCb_Var(self, tabIndex):
        if self.Cb_Types[tabIndex].currentText() == "Surface balance:":
            self.Cb_Vars[tabIndex].clear()
            self.Cb_Vars[tabIndex].addItems(self.surface_balance_names)
        elif self.Cb_Types[tabIndex].currentText() == "Volume balance:":
            self.Cb_Vars[tabIndex].clear()
            self.Cb_Vars[tabIndex].addItems(self.volume_balance_names)
        else:
            self.Cb_Vars[tabIndex].clear()
            self.Cb_Vars[tabIndex].addItems(self.history_names)

    def updateCb_Vars(self):
        # create Combobox - nature variable (température, pression...)
        for i in range(self.tabPlot.count()):
            if self.Cb_Types[i].currentText() == "Surface balance:":
                self.Cb_Vars[i].clear()
                self.Cb_Vars[i].addItems(self.surface_balance_names)
                self.Cb_Vars[i].setCurrentIndex(0)
            elif self.Cb_Types[i].currentText() == "Volume balance:":
                self.Cb_Vars[i].clear()
                self.Cb_Vars[i].addItems(self.volume_balance_names)
                self.Cb_Vars[i].setCurrentIndex(0)
            else:
                self.Cb_Vars[i].clear()
                self.Cb_Vars[i].addItems(self.history_names)
                self.Cb_Vars[i].setCurrentIndex(0)


    def addHistoryCurves(self):
        if self.Home_form_copy.Dim_Comb.currentIndex()==0:
            Table = self.Output_3D_form_copy.Op_Dc_3D_table
            TableSURF = self.Output_3D_form_copy.Op_Sb_3D_table
            TableVOL = self.Output_3D_form_copy.Op_Vb_3D_table
        else:
            Table=self.Output_2D_form_copy.Op_Dc_2D_table
            TableSURF = self.Output_2D_form_copy.Op_Sb_2D_table
            TableVOL = self.Output_2D_form_copy.Op_Vb_2D_table

        j = 0 # nombre de courbes existantes
        for name in self.history_names:
            i = 0 # numéro de ligne-1 dans le tableau output
            while i<Table.rowCount():
                if ((Table.cellWidget(i, 0).isChecked()==True) and
                    (Table.item(i, 1)!=None and Table.item(i, 2)!=None)): # ligne checké et non vide
                    if Table.item(i, 1).text() != "" and Table.item(i,2).text() != "" :
                        legend = name+" "
                        if self.Home_form_copy.Dim_Comb.currentIndex()==0: # 3D
                            try :
                                legend = legend + Table.item(i,4).text() # par défaut légende = user commentaire du sonde
                            except :
                                pass
                            if legend == name+" " : # sinon légende = coordonnées du sonde
                                legend = legend + "["+str(Table.item(i,1).text())+";"+str(Table.item(i,2).text())+";"+str(Table.item(i,3).text())+"]"
                        else:
                            try :
                                legend = legend + Table.item(i,3).text()
                            except :
                                pass
                            if legend == name+" " :
                                legend = legend + "["+str(Table.item(i,1).text())+";"+str(Table.item(i,2).text())+"]"

                        curve = clCurve(legend)
                        curve.attach(self.plot)
                        self.curves[0,j] = curve
                        self.curves[0,j].setColor(self.listColor[i])
                        self.curves[0,j].x = Table.item(i,1).text()
                        self.curves[0,j].y = Table.item(i,2).text()
                        if self.Home_form_copy.Dim_Comb.currentIndex()==0:
                            self.curves[0,j].z = Table.item(i,3).text()
                            self.curves[0,j].dimension = 3
                        else:
                            self.curves[0,j].z = '999'
                            self.curves[0,j].dimension = 2
                        j=j+1
                        pass # end if
                i=i+1 # end while
        print("nbCurves:", len(self.curves))
        pass

    def addSurfaceCurves(self):
        if self.Home_form_copy.Dim_Comb.currentIndex()==0:
            Table = self.Output_3D_form_copy.Op_Dc_3D_table
            TableSURF = self.Output_3D_form_copy.Op_Sb_3D_table
            TableVOL = self.Output_3D_form_copy.Op_Vb_3D_table
        else:
            Table=self.Output_2D_form_copy.Op_Dc_2D_table
            TableSURF = self.Output_2D_form_copy.Op_Sb_2D_table
            TableVOL = self.Output_2D_form_copy.Op_Vb_2D_table


        # curves for surface balance -> ...
        j = 0
        # j = len(self.history_names)*self.nbProbes # nombre de courbes existantes
        for name in self.surface_balance_names:
            i = 0 # numéro de ligne-1 dans le tableau Surface balance
            while i<TableSURF.rowCount():
                if (TableSURF.cellWidget(i, 0).isChecked()==True) and (TableSURF.item(i, 1)!=None): # ligne checké et non vide
                    if TableSURF.item(i, 1).text() != "" :
                        legend = ""
                        try :
                            legend = TableSURF.item(i,2).text() # par défaut légende = user commentaire du sonde
                        except :
                            pass
                        if legend == "" : # sinon légende = coordonnées du sonde
                            legend = name + " " + str(TableSURF.item(i, 1).text())

                        curve = clCurve(legend)
                        curve.attach(self.plot)
                        self.curves[1,j] = curve
                        self.curves[1,j].setColor(self.listColor[i])
                        self.curves[1,j].x = TableSURF.item(i,1).text()
                        self.curves[1,j].y = '999'
                        self.curves[1,j].z = '999'
                        j=j+1
                        pass # end if
                i=i+1 # end while
        print("nbCurves:", len(self.curves))
        pass

    def addVolumeCurves(self):
        if self.Home_form_copy.Dim_Comb.currentIndex()==0:
            Table = self.Output_3D_form_copy.Op_Dc_3D_table
            TableSURF = self.Output_3D_form_copy.Op_Sb_3D_table
            TableVOL = self.Output_3D_form_copy.Op_Vb_3D_table
        else:
            Table=self.Output_2D_form_copy.Op_Dc_2D_table
            TableSURF = self.Output_2D_form_copy.Op_Sb_2D_table
            TableVOL = self.Output_2D_form_copy.Op_Vb_2D_table

        # curves for volume balance -> ...
        j = 0
        # j = len(self.history_names)*self.nbProbes + len(self.surface_balance_names)*self.nbRefSURF# nombre de courbes existantes
        # j - nombre de courbes existantes
        for name in self.volume_balance_names:
            i = 0 # numéro de ligne-1 dans le tableau Volume balance
            while i<TableVOL.rowCount():
                if (TableVOL.cellWidget(i, 0).isChecked()==True) and (TableVOL.item(i, 1)!=None): # ligne checké et non vide
                    if TableVOL.item(i, 1).text() != "" :
                        legend = ""
                        try :
                            legend = TableVOL.item(i,2).text() # par défaut légende = user commentaire du sonde
                        except :
                            pass
                        if legend == "" : # sinon légende = coordonnées du sonde
                            legend = name + " " + str(TableVOL.item(i, 1).text())

                        curve = clCurve(legend)
                        curve.attach(self.plot)
                        # self.curves.append(curve)
                        self.curves[2,j] = curve
                        self.curves[2,j].setColor(self.listColor[i])
                        self.curves[2,j].x = TableVOL.item(i,1).text()
                        self.curves[2,j].y = '999'
                        self.curves[2,j].z = '999'
                        j=j+1
                        pass # end if
                i=i+1 # end while
        print("nbCurves:", len(self.curves))
        pass

    def Time_Clb(self, parent=None): # Fonction de rappel des coups d'horloge pour le rafraîchissement de l'IHM du suivis de calcul
        self.setCursor(QtCore.Qt.BusyCursor)
        for i in range(4) :
            Cb_Top = self.Cb_Tops[i]
            if Cb_Top.currentText() != '':
                #self.read_cp_file2(self.tabPlot.curveIndexLinked[i])
                #self.read_cp_SURF(self.tabPlot.curveIndexLinked[i])
                #self.read_cp_VOL(self.tabPlot.curveIndexLinked[i])
                if self.Cb_Types[i].currentText() == "Surface balance:":
                    self.read_cp_SURF(self.tabPlot.curveIndexLinked[i])
                elif self.Cb_Types[i].currentText() == "Volume balance:":
                    self.read_cp_VOL(self.tabPlot.curveIndexLinked[i])
                else:
                    self.read_cp_file2(self.tabPlot.curveIndexLinked[i])
            pass
        self.updateAxes()
        self.timer= QtCore.QTimer()

        # afficher listing dans les onglets
        listing = self.case_copy.dirPath + os.sep + str(self.Running_options_form_copy.Ro_Ln_le.text())
        #if (not os.access(listing, os.F_OK)) or (not os.access(self.case_copy.getHisFullPath(0), os.F_OK)):
        #if (not os.access(listing, os.F_OK)) and (not os.access(self.case_copy.getHisFullPath(0), os.F_OK)) or (not os.access(listing, os.F_OK)):
        if (not os.access(listing, os.F_OK)):
            #print '1 self.tickflag : ',self.tickflag
            # listing or file .his n'existe pas ou n'est pas créé
            # Mais on ne fait self.tickflag = False qu'après avoir vérifié self.runProcess_copy.poll()
            # pour éviter le cas où ces fichiers sont en train d'être créé

            try:
                # while the process has not terminated yet -> wait
                if self.runProcess_copy.poll() == None :
                    ticker=self.timer.singleShot(500, self.Time_Clb)
                    return

                # if the process has terminated
                if self.runProcess_copy.poll() != None :
                    self.tickflag = False # stop IHM from iterating
                    #Main.Syrthes_stopping()
                    print("Syrthes_stopping : process terminated")
                    ## self.emit(SIGNAL("Syrthes_stopping"))
                    self.Syrthes_stopping.emit()
                    #restore cursor
                    self.unsetCursor()
                # printing Error Message
                self.afficherStdout()
                self.afficherStderr()

            except:
                print("Warning : SYRTHES hasn't run yet")
                self.tickflag = False
                #Main.Syrthes_stopping()
                ## self.emit(SIGNAL("Syrthes_stopping"))
                self.Syrthes_stopping.emit()
                #restore cursor
                self.unsetCursor()

        else:
            #print '2 self.tickflag : ',self.tickflag
            listfile=open(self.case_copy.dirPath + os.sep + str(self.Running_options_form_copy.Ro_Ln_le.text()), "r")
            lines=listfile.readlines()
            length=len(lines)
            textlines=''

            self.textBrowser_3.clear()
            #monospace
            myCharFormat = QtGui.QTextCharFormat()
            myCharFormat.setFontFixedPitch(True)
            self.textBrowser_3.setCurrentCharFormat(myCharFormat)

            i=0
            Tstep=-1
            count=length-self.nline
            #step=-1
            #substep=0
            for line in lines:
                #print '****************************************'
                #print line
                #print '****************************************'
                #self.textBrowser_3.insertPlainText(line)
                if i>=len(lines)-200:
                    self.textBrowser_3.insertPlainText(line)

                # trouver 'cpu time' --> fin de simulation ou bouton Stop actionné
                if line.find('cpu time=') != -1:
                    #print "progress =", int((Tstep/float(self.Control_form_copy.Le_Nts.text()))*100)
                    self.tickflag=False

                    # wait until the process really terminates
                    if self.runProcess_copy != None :
                        if self.runProcess_copy.poll() == None :
                            listfile.close()
                            ticker=self.timer.singleShot(500, self.Time_Clb)
                            return

                        # print SYRTHES message
                        """self.textBrowser_4.clear()
                        myCharFormat = QtGui.QTextCharFormat()
                        myCharFormat.setFontFixedPitch(True) #monospace
                        self.textBrowser_4.setCurrentCharFormat(myCharFormat) """
                        #namefout = self.case_copy.dirPath + os.sep + "stdout.txt"
                        #if os.access(namefout, os.F_OK) :
                        #    fout = open(namefout, "r+")
                        #    foutlines = fout.readlines()
                        #    for sline in foutlines :
                        #        if '\n' in sline :
                        #            print sline.split('\n')[0] # \n has not to be printed
                        #        else:
                        #            print sline
                        #        """self.textBrowser_4.insertPlainText(unicode(sline,"utf-8"))"""
                        #    if os.access(namefout, os.F_OK) :
                        #        fout.close()
                        # Modification BERTIN 23/05/2014 remplacement par l'appel des methodes :
                        self.afficherStdout()
                        self.afficherStderr()
                        #Main.Syrthes_stopping()
                        print("Syrthes_stopping : syrthes message")
                        self.Syrthes_stopping.emit()
                        ## self.emit(SIGNAL("Syrthes_stopping"))
                        #restore cursor
                        self.unsetCursor()
                if line.find('NTSYR=') != -1:
                    Tstep=line.split()
                    Tstep=float(Tstep[2])

                if i >= self.nline and not self.running :
                    if line.find("PRE-PROCESSOR  FOR  PARALLEL  COMPUTATION") != -1 or line.find("PRE-PROCESSEUR  POUR  TRAITEMENT  PARALLELE") != -1:
                        self.substep = 0
                        self.groupBox.setTitle(QtCore.QCoreApplication.translate("Calculation_progress", "Pre processing"))
                        self.step = 9
                        pass
                    if line.find(" CONDUCTION INITIALIZATIONS") != -1 or line.find(" INITIALISATIONS POUR LA CONDUCTION") != -1:
                        self.substep = 0
                        self.groupBox.setTitle(QtCore.QCoreApplication.translate("Calculation_progress", "Conduction initialization"))
                        self.step = 14
                        pass
                    if line.find("END OF PRE-PROCESSOR FOR PARALLEL COMPUTATION") != -1 or line.find("FIN NORMALE DU PRE-PROCESSING PARALLELE") != -1:
                        pass
                    if line.find(" RADIATION INITIALIZATIONS") != -1 or line.find("INITIALISATIONS POUR LE RAYONNEMENT") != -1:
                        self.substep = 0
                        self.groupBox.setTitle(QtCore.QCoreApplication.translate("Calculation_progress", "Radiation initialization"))
                        self.step = 13
                        pass
                    if line.find(" END OF INITIALIZATION PHASE") != -1 or line.find(" FIN DE LA PHASE D'INITIALISATION") != -1:
                        self.running = True
                        pass
                    if line.find(" *** ") != -1:
                        self.substep=self.substep+1
                        progress=int(100*self.substep/self.step)
                        self.progressBar.setValue(progress)
                        pass
                    pass

                i=i+1
            if Tstep>0:
                self.groupBox.setTitle(QtCore.QCoreApplication.translate("Calculation_progress", "Progress of Syrthes run"))
                Gtsn=float(self.Control_form_copy.Le_Nts.text())
                if self.Control_form_copy.Ch_res_cal.isChecked()==True:
# isa                    resfile=open(self.case_copy.dirPath + os.sep + self.Filename_form_copy.Fn_Rs_lne.text()+'.res', "r")
                    resfile=open(self.case_copy.dirPath + os.sep + self.Filename_form_copy.Fn_Rs_lne.text(), "r")
                    #print '!!!!!!!!!!!!!',resfile
                    reslines=resfile.readlines()
                    Nrt=reslines[3].split()
                    Nrt=int(Nrt[1])
                    progress=int(((Tstep-Nrt)//(Gtsn-Nrt))*100)
                    resfile.close()
                else:
                    progress=int((Tstep//Gtsn)*100)
                self.progressBar.setValue(progress)

                if progress == 100 :
                    # supprimer syrthes.run
                    #Main.Syrthes_stopping()
                    print("Syrthes_completed")
                    self.Syrthes_completed.emit()
##                     self.emit(SIGNAL("Syrthes_completed"))
                    #self.emit(SIGNAL("Syrthes_stopping"))
                    #restore cursor
                    self.unsetCursor()
            self.nline = length
            self.textBrowser_3.moveCursor(QtGui.QTextCursor.Down, QtGui.QTextCursor.MoveAnchor)
            listfile.close()

        if self.tickflag==True:
            ticker=self.timer.singleShot(1000, self.Time_Clb)

    def afficherStderr(self):
        nameferr = self.case_copy.dirPath + os.sep + "stderr.txt"
        #self.textBrowser_4.clear()
        myCharFormat = QtGui.QTextCharFormat()
        myCharFormat.setFontFixedPitch(True) #monospace
        self.textBrowser_4.setCurrentCharFormat(myCharFormat)
        if os.access(nameferr, os.F_OK) :
            ferr = open(nameferr, "r+")
            ferrlines = ferr.readlines()
            errorflag = False
            for sline in ferrlines :
                if '\n' in sline :
                    print(sline.split('\n')[0]) # \n has not to be printed
                else:
                    print(sline)
                self.textBrowser_4.insertPlainText(str(sline,"utf-8"))
                if ("Stop Syrthes execution" in sline) or ("Traceback" in sline) :
                    errorflag = True

            if errorflag :
                #QMessageBox.information(self, 'Error', "Physical error or Unrecognized advanced keywords", QMessageBox.Ok)
                self.tabWidget.insertTab(2, self.tab_log, "Log")
                self.tabWidget.setCurrentIndex(2)
                #self.textBrowser_4.setFocus()

            if os.access(nameferr, os.F_OK) :
                ferr.close()
        pass

    def afficherStdout(self):
        namefout = self.case_copy.dirPath + os.sep + "stdout.txt"
        self.textBrowser_4.clear()
        myCharFormat = QtGui.QTextCharFormat()
        myCharFormat.setFontFixedPitch(True) #monospace
        self.textBrowser_4.setCurrentCharFormat(myCharFormat)
        if os.access(namefout, os.F_OK) :
            fout = open(namefout, "r+")
            foutlines = fout.readlines()
            for sline in foutlines :
                if '\n' in sline :
                    print(sline.split('\n')[0]) # \n has not to be printed
                else:
                    print(sline)
                self.textBrowser_4.insertPlainText(str(sline))
            if os.access(namefout, os.F_OK) :
                fout.close()
        pass

    def read_cp_file2(self, curve_index): # fonction de lecture des fichiers de sortis pour le suivis de calcul
        # en mode Run, cette fonction sera appelée environ tous les 1000 milisecondes
        # chercher le fichier contenant la sonde en question
        if curve_index == (-1,-1) : return
        probe_index=0
        if self.curves:
            probe_index = self.Cb_Tops[self.curves[curve_index].tabIndexLinked].currentIndex()
        self.DefDictProbe()
        if self.Cb_Top.currentText()!='':
            hisname = self.dictProbe[str(probe_index)]
        else:
            hisname = self.dictProbe['0'] # situate at the first probe if 1st time

        if os.access(hisname, os.F_OK):
            cp_file=open(hisname, "r")
            if self.Cb_Top.currentText()!='':
                # récupérer les coordonnées de la sonde en question
                if self.Home_form_copy.Dim_Comb.currentIndex()==0:
                    table=self.Output_3D_form_copy.Op_Dc_3D_table
                    x=float(table.item(self.probesList[probe_index],1).text())
                    y=float(table.item(self.probesList[probe_index],2).text())
                    z=float(table.item(self.probesList[probe_index],3).text())
                else:
                    table=self.Output_2D_form_copy.Op_Dc_2D_table
                    x=float(table.item(self.probesList[probe_index],1).text())
                    y=float(table.item(self.probesList[probe_index],2).text())

                # définir la colonne contenant la coordonnée x (ix)
                ix = 0

                # charger les vecteurs Time_i et Temper_i
                Time_i = []
                Temper_i = []
                vP_i = []
                taP_i = []
                l0=[]
                valueMap={}
                for line in cp_file:
                    if l0 == [] :
                        # Save the table header
                        l0 = line.split()
                        i = -1
                        for word in l0:
                            if "x" == word.strip():
                                ix=i
                            i=i+1
                            pass
                        continue

                    line=line.split()

                    if self.Home_form_copy.Dim_Comb.currentIndex()==0:
                        if not(float(line[ix])==x and float(line[ix+1])==y and float(line[ix+2])==z):
                            continue
                    else:
                        if not(float(line[ix])==x and float(line[ix+1])==y):
                            continue

                    if self.curves == {}:
                        for word in l0[1:ix+1]:
                            if word != "time":
                                self.history_names.append(word)
                                pass
                            pass
                        self.updateCb_Vars()
                        #self.updateCurves()
                        self.addHistoryCurves()
                        self.curves[curve_index].tabIndexLinked = 0
                        pass

                    if list(valueMap.keys()) == []:
                        for word in l0[1:ix+1]:
                            valueMap[word]=[]
                            pass
                        pass

                    i=0
                    for word in l0[1:ix+1]:
                        valueMap[word].append(float(line[i]))
                        i=i+1
                        pass

                    #Time_i.append(float(line[0]))
                    #Temper_i.append(float(line[1]))
                    #if ix > 2 : # si modèle 2 ou 3 équations
                    #    vP_i.append(float(line[2]))
                    #if ix == 4 : # si modèle 3 équations
                    #    taP_i.append(float(line[3]))

                # pression vapeur --> courbe i+4
                # pression totale --> courbe i+8
                tabIndexLink = self.curves[curve_index].tabIndexLinked
                currentVarText = self.Cb_Vars[tabIndexLink].currentText()
                # mettre à jour la courbe correspondant à la sonde en question
                #print self.curves[curve_index].valueMap["time"]
                if valueMap["time"] != [] :
                    #self.curves[curve_index].taP = taP_i
                    #print currentVarText, self.curves[curve_index].valueMap.keys()
                    self.curves[curve_index].valueMap.update(valueMap)
                    self.curves[curve_index].refreshCurve(str(currentVarText))
                    self.curves[curve_index].setStyle(self.Cb_LineStyles[tabIndexLink].currentIndex()+1)
                    self.curves[curve_index].attach(self.plot)
                    if self.mark==True:
                        self.marker.hide()
                        self.mark=False
                else:
                    # mis à jour les liens entre les onglets et les courbes
                    # désaffecter cet onglet (tabIndexLinked) de cette courbe vide
                    # contrôler si cette courbe courbe appartient à un autre onglet (variable "ailleur")
                    ailleur = -1
                    for ti in range(self.tabPlot.nbActif) :
                        if ti != currentTabIndex and self.tabPlot.curveIndexLinked[ti] == curve_index :
                            ailleur = ti
                            break
                    self.curves[curve_index].tabIndexLinked = ailleur
                    self.tabPlot.curveIndexLinked[currentTabIndex] = -1,-1
                    self.curves[curve_index].razData()
                    self.curves[curve_index].detach()
                    # s'il n'y a plus de courbes, il faudra mettre "Awaiting data" au milieu
            cp_file.close()

    def read_cp_SURF(self, curve_index):
        fluname = self.case_copy.fluname
        #SURF Time= 1.00000000e+00 Balance   1 * Temp_Flux=  1.17689e-01 * Vapor_Flux=  5.31073e-03 * Dry_Air_Flux=  0.00000e+00

        float_string="-?[0-9]+\.[0-9]+e(\+|-)[0-9]+"
        string_string="[a-zA-Z\_]*"
        int_string="[0-9]+"

        balance_pattern=re.compile(r'(?P<btype>[a-zA-Z]+)\s+Time=\s+(?P<time>'+float_string+')\s+Balance\s+(?P<balance>'+int_string+')\s+')
        var_pattern=re.compile(r'\s*(?P<name>'+string_string+')\s*=\s*(?P<value>'+float_string+')\s*')

        surf_balance_list=[]
        vol_balance_list=[]
        btype=""
        balance = -1
        ti = -1
        if os.access(fluname, os.F_OK) :
            cp_file=open(fluname, "r+")
            currentTabIndex = self.tabPlot.currentIndex()
            Time_i = []
            valueMap = {}
            l0 = []
            l1 = []

            for line in cp_file:
                for word in line.split('*'):
                    if balance_pattern.match(word):
                        bp_match=balance_pattern.match(word)
                        btype=bp_match.group("btype")
                        ti=bp_match.group("time")
                        balance=bp_match.group("balance")
                        if "time" not in list(valueMap.keys()):
                            valueMap["time"] = []
                            pass
                    if var_pattern.match(word):
                        if btype == "SURF":
                            vp_match=var_pattern.match(word)
                            name = vp_match.group("name")
                            if name not in list(valueMap.keys()):
                                valueMap[name] = []
                                l0.append(name)
                                pass
                            pass
                        pass
                    pass
                if self.surface_balance_names == [] and btype == "SURF":
                    self.surface_balance_names = l0
                    self.addSurfaceCurves()
                    cp_file.close()
                    return
                    pass
                pass

            cp_file=open(fluname, "r+")
            for line in cp_file:
                i=0
                for word in line.split('*'):
                    if balance_pattern.match(word):
                        bp_match=balance_pattern.match(word)
                        btype=bp_match.group("btype")
                        ti=float(bp_match.group("time"))
                        balance=bp_match.group("balance")
                        if "time" not in list(valueMap.keys()):
                            valueMap["time"] = []
                            pass
                    elif var_pattern.match(word):
                        if btype == "SURF" and (int(balance) == curve_index[1] + 1 - i*self.nbRefSURF):
                            vp_match=var_pattern.match(word)
                            name = vp_match.group("name")
                            value = float(vp_match.group("value"))
                            if name not in list(valueMap.keys()):
                                valueMap[name] = []
                                pass
                            valueMap[name].append(value)
                            valueMap["time"].append(ti)
                            pass
                        i=i+1
                        pass
                    pass
                pass


            # mettre à jour la courbe correspondant à la sonde en question
            tabIndexLink = self.curves[curve_index].tabIndexLinked
            currentVarText = self.Cb_Vars[tabIndexLink].currentText()
            if valueMap["time"] != [] :
                self.curves[curve_index].valueMap.update(valueMap)
                if currentVarText in list(valueMap.keys()):
                    self.curves[curve_index].refreshCurve(str(currentVarText))
                    self.curves[curve_index].setStyle(self.Cb_LineStyles[currentTabIndex].currentIndex()+1)
                    self.curves[curve_index].attach(self.plot)
                if self.mark==True:
                    self.marker.hide()
                    self.mark=False
            else:
                # mis à jour les liens entre les onglets et les courbes
                # désaffecter cet onglet (tabIndexLinked) de cette courbe vide
                # contrôler si cette courbe courbe appartient à un autre onglet (variable "ailleur")
                ailleur = -1
                for ti in range(self.tabPlot.nbActif) :
                    if ti != currentTabIndex and self.tabPlot.curveIndexLinked[ti] == curve_index :
                        ailleur = ti
                        break
                self.curves[curve_index].tabIndexLinked = ailleur
                self.tabPlot.curveIndexLinked[currentTabIndex] = -1,-1
                self.curves[curve_index].razData()
                self.curves[curve_index].detach()
                # s'il n'y a plus de courbes, il faudra mettre "Awaiting data" au milieu
            cp_file.close()

    def read_cp_VOL(self, curve_index):
        fluname = self.case_copy.fluname
        #VOL Time= 1.00000000e+00 Balance   1 * Temp_Flux=  1.17689e-01 * Vapor_Flux=  5.31073e-03 * Dry_Air_Flux=  0.00000e+00

        float_string="-?[0-9]+\.[0-9]+e(\+|-)[0-9]+"
        string_string="[a-zA-Z\_]*"
        int_string="[0-9]+"

        balance_pattern=re.compile(r'(?P<btype>[a-zA-Z]+)\s+Time=\s+(?P<time>'+float_string+')\s+Balance\s+(?P<balance>'+int_string+')\s+')
        var_pattern=re.compile(r'\s*(?P<name>'+string_string+')\s*=\s*(?P<value>'+float_string+')\s*')

        surf_balance_list=[]
        vol_balance_list=[]
        btype=""
        balance = -1
        ti = -1
        if os.access(fluname, os.F_OK) :
            cp_file=open(fluname, "r+")
            currentTabIndex = self.tabPlot.currentIndex()
            Time_i = []
            valueMap = {}
            l0 = []

            for line in cp_file:
                for word in line.split('*'):
                    if balance_pattern.match(word):
                        bp_match=balance_pattern.match(word)
                        btype=bp_match.group("btype")
                        ti=bp_match.group("time")
                        balance=bp_match.group("balance")
                        if "time" not in list(valueMap.keys()):
                            valueMap["time"] = []
                            pass
                    if var_pattern.match(word):
                        if btype == "VOL":
                            vp_match=var_pattern.match(word)
                            name = vp_match.group("name")
                            if name not in list(valueMap.keys()):
                                valueMap[name] = []
                                l0.append(name)
                                pass
                            pass
                        pass
                    pass
                if self.volume_balance_names == [] and btype == "VOL":
                    self.volume_balance_names = l0
                    self.addVolumeCurves()
                    cp_file.close()
                    return
                    pass
                pass

            cp_file=open(fluname, "r+")
            for line in cp_file:
                i=0
                for word in line.split('*'):
                    if balance_pattern.match(word):
                        bp_match=balance_pattern.match(word)
                        btype=bp_match.group("btype")
                        ti=float(bp_match.group("time"))
                        balance=bp_match.group("balance")
                        if "time" not in list(valueMap.keys()):
                            valueMap["time"] = []
                            pass
                    elif var_pattern.match(word):
                        if btype == "VOL" and (int(balance) == curve_index[1] + 1 - i*self.nbRefVOL):
                            vp_match=var_pattern.match(word)
                            name = vp_match.group("name")
                            value = float(vp_match.group("value"))
                            if name not in list(valueMap.keys()):
                                valueMap[name] = []
                                pass
                            valueMap[name].append(value)
                            valueMap["time"].append(ti)
                            pass
                        i=i+1
                        pass
                    pass
                pass


            # mettre à jour la courbe correspondant à la sonde en question
            tabIndexLink = self.curves[curve_index].tabIndexLinked
            currentVarText = self.Cb_Vars[tabIndexLink].currentText()
            if valueMap["time"] != [] :
                self.curves[curve_index].valueMap.update(valueMap)
                if currentVarText in list(valueMap.keys()):
                    self.curves[curve_index].refreshCurve(str(currentVarText))
                    self.curves[curve_index].setStyle(self.Cb_LineStyles[currentTabIndex].currentIndex()+1)
                    self.curves[curve_index].attach(self.plot)
                if self.mark==True:
                    self.marker.hide()
                    self.mark=False
            else:
                # mis à jour les liens entre les onglets et les courbes
                # désaffecter cet onglet (tabIndexLinked) de cette courbe vide
                # contrôler si cette courbe courbe appartient à un autre onglet (variable "ailleur")
                ailleur = -1
                for ti in range(self.tabPlot.nbActif) :
                    if ti != currentTabIndex and self.tabPlot.curveIndexLinked[ti] == curve_index :
                        ailleur = ti
                        break
                self.curves[curve_index].tabIndexLinked = ailleur
                self.tabPlot.curveIndexLinked[currentTabIndex] = -1,-1
                self.curves[curve_index].razData()
                self.curves[curve_index].detach()
                # s'il n'y a plus de courbes, il faudra mettre "Awaiting data" au milieu
            cp_file.close()
            pass
        pass

    def DefDictProbe(self, parent=None):
        """
        Procedure defining the couples probe number <-> file name
        Will be called in self.Cb_Top_Clb, i.e. each time users change the probe
        """
        ix=0
        for probei in range(self.nbProbes): # loop for probes
            # search for coordinates of this probe
            if self.Home_form_copy.Dim_Comb.currentIndex()==0:
                table=self.Output_3D_form_copy.Op_Dc_3D_table
                x=float(table.item(self.probesList[probei],1).text())
                y=float(table.item(self.probesList[probei],2).text())
                z=float(table.item(self.probesList[probei],3).text())
            else:
                table=self.Output_2D_form_copy.Op_Dc_2D_table
                x=float(table.item(self.probesList[probei],1).text())
                y=float(table.item(self.probesList[probei],2).text())

            if self.case_copy.getNbProc() > 1:
                # parcourir tous les fichiers .his (autant nombreux que les proc)
                found = False # by default, probe is not found
                for filei in range(self.case_copy.getNbProc()):
                    if os.access(self.case_copy.getHisFullPath(filei), os.F_OK): # .../PART/prefix_part0000n.his
                        cp_file=open(self.case_copy.getHisFullPath(filei), "r")
                        l1=''
                        l0=[]
                        # looking in all lines in the current file .his
                        lineNo = 0 # line number in file .his
                        for line in cp_file.readlines():
                            lineNo += 1
                            if l0 == [] :
                                # Save the table header
                                l0 = line.split()
                                i = -1
                                for word in l0:
                                    if "x" == word.strip():
                                        ix=i
                                    i=i+1
                                    pass
                                continue
                            # if lineNo > self.nbProbes :
                            #     break # we don't have to read all lines in a file but the same line as the number of probes
                            if l1 == '' :
                                l1 = line.split() # save the first line to l1 (l + one)

                            l = line.split() # 1st, 2nd line and so on
                            if l[0] != l1[0] : # stop because we only want to look at the first time step
                                break

                            # looking for probe
                            if self.Home_form_copy.Dim_Comb.currentIndex()==0: # 3D
                                if float(l[ix])==x and float(l[ix+1])==y and float(l[ix+2])==z: # probe 3D found
                                    self.dictProbe[str(probei)] = self.case_copy.getHisFullPath(filei)
                                    found = True
                                    break # break loop of lines because the probe has been found
                            else: # 2D
                                if float(l[ix])==x and float(l[ix+1])==y: # probe 2D found
                                    self.dictProbe[str(probei)] = self.case_copy.getHisFullPath(filei)
                                    found = True
                                    break # break loop of lines because the probe has been found
                            pass # pass looking in all lines in the current file

                        cp_file.close() # close current file to go to next file
                        if found == True:
                            break # break loop of files .his because the probe has been found
            else: # nbProc = 1
                found = False
                if os.access(self.case_copy.getHisFullPath(0), os.F_OK):
                    cp_file=open(self.case_copy.getHisFullPath(0), "r")
                    l1=''
                    l0=[]
                    # looking in all lines in the current file .his
                    for line in cp_file.readlines():
                        if l0 == [] :
                            # Save the table header
                            l0 = line.split()
                            i=-1
                            for word in l0:
                                if "x" == word.strip():
                                    ix=i
                                i=i+1
                                pass
                            continue
                        if l1 == '' :
                            l1 = line.split() # save the first line to l1 (l + one)

                        l = line.split() # 1st, 2nd line and so on
                        if l[0] != l1[0] : # stop because we only want to look at the first time step
                            break

                        # looking for probe
                        if self.Home_form_copy.Dim_Comb.currentIndex()==0: # 3D
                            if float(l[ix])==x and float(l[ix+1])==y and float(l[ix+2])==z: # probe 3D found
                                self.dictProbe[str(probei)] = self.case_copy.getHisFullPath(0)
                                found = True
                                break
                        else: # 2D
                            if float(l[ix])==x and float(l[ix+1])==y: # probe 2D found
                                self.dictProbe[str(probei)] = self.case_copy.getHisFullPath(0)
                                found = True
                                break
                        pass # pass looking in all lines in the current file
                    cp_file.close()
            if found != True: # alert users that the probe doesn't exist
                #QMessageBox.question(self, 'Message',
                #                     "Probe not found. Please try another probe.", QMessageBox.Ok)
                self.dictProbe[str(probei)] = "-1"
            pass # loop for cbTop (probes)
        self.dictProbe[str(self.nbProbes)] = "-2"

    def Cb_Top_Clb2(self): # fonction de rappel permettant le changement de la sonde à afficher dans le graphique
    # 2 tableaux sont importants ici :
    # appartenance d'une courbe à un ou plusieurs onglets self.tabPlot.curveIndexLinked[index_onglet] = index_courbe
    # appartenance d'un onglet à une courbe self.curves[index_curve].tabIndexLinked = index_onglet
        Cb_Top = QtWidgets.QApplication.focusWidget()

        if not (Cb_Top in self.Cb_Tops) :
            # not a combobox
            return
        if Cb_Top.currentIndex() == -1 :
            return

        # raccourcis :
        oldTypeIndex,oldCurveIndex = self.tabPlot.curveIndexLinked[self.tabPlot.currentIndex()]
        curve_index_base = Cb_Top.currentIndex()
        currentTabIndex = self.tabPlot.currentIndex()
        # temp --> courbe i
        # ex. 4 sondes --> pression vapeur --> courbe i+4
        # ex. 4 sondes --> pression totale --> courbe i+8
        # ex. 4 sondes --> surface balance --> courbe i+12
        # ex. 4 sondes + 2 ref surfaces balance --> volume balance --> courbe i + 14
        newCurveIndex = curve_index_base
        if curve_index_base == len(self.curves):
            curve_index_base = 0

        currentVarIndex = self.Cb_Vars[currentTabIndex].currentIndex()
        ## currentVarText = self.Cb_Vars[currentTabIndex].currentText()
        currentVarText = self.Cb_Vars[currentTabIndex].currentText()
        currentTypeText = self.Cb_Types[currentTabIndex].currentText()
        currentTypeIndex = self.Cb_Types[currentTabIndex].currentIndex()

        newCurveIndex = curve_index_base
        #if curve_index_base == len(self.curves[currentTypeIndex]):
        #    curve_index_base = 0

        if currentVarText in self.history_names:
            for i in range(len(self.history_names)):
                if currentVarText == self.history_names[i]:
                    newCurveIndex = curve_index_base + i*self.nbProbes
        elif currentVarText in self.surface_balance_names: # pression totale
            for i in range(len(self.surface_balance_names)):
                if currentVarText == self.surface_balance_names[i] and currentTypeText == "Surface balance:":
                    newCurveIndex = curve_index_base + i*self.nbRefSURF
        elif currentVarText in self.volume_balance_names: # pression totale
            for i in range(len(self.volume_balance_names)):
                if currentVarText == self.volume_balance_names[i] and currentTypeText == "Volume balance:":
                    newCurveIndex = curve_index_base + i*self.nbRefVOL

        # ne plus souhaitable :
        # sortir si la courbe choisie appatient à un autre onglet
        #if self.curves[newCurveIndex].tabIndexLinked != -1 :
        #    self.Cb_Tops[currentTabIndex].setCurrentIndex(oldCurveIndex)
        #    return

        # mis à jour les liens entre les onglets et les courbes

        if Cb_Top.currentText() != "None" :
            self.curves[currentTypeIndex, newCurveIndex].tabIndexLinked = currentTabIndex
            self.tabPlot.curveIndexLinked[currentTabIndex] = currentTypeIndex,newCurveIndex

        # désaffecter cet onglet (tabIndexLinked) à l'ancienne courbe
        # et détacher l'ancienne courbe
        # précédent contrôle ne plus applicable -> contrôler si l'ancienne courbe (old curve)...
        # ...appartient à un autre onglet (variable "ailleur")
        ailleur = -1
        for ti in range(self.tabPlot.nbActif) :
            if ti != currentTabIndex and self.tabPlot.curveIndexLinked[ti] == (oldTypeIndex,oldCurveIndex) :
                ailleur = ti
                break
        if ailleur == -1 : # l'ancienne courbe n'a pas de duplicata -> détacher
            if oldCurveIndex != -1 :
                self.curves[oldTypeIndex,oldCurveIndex].tabIndexLinked = -1
                self.curves[oldTypeIndex,oldCurveIndex].detach()
        else :
            if oldCurveIndex != -1:
                self.curves[oldTypeIndex,oldCurveIndex].tabIndexLinked = ailleur

        # contrôler si la nouvelle courbe appartient à un autre onglet (variable "ailleur")
        # pour homogénéiser son style
        if Cb_Top.currentText() != "None" :
            ailleur = -1
            for ti in range(self.tabPlot.nbActif) :
                if ti != currentTabIndex and self.tabPlot.curveIndexLinked[ti] == (currentTypeIndex,newCurveIndex) :
                    ailleur = ti
                    # changer le style de ce duplicata
                    self.ne_pas_repondre_style = True
                    self.Cb_LineStyles[ti].setCurrentIndex(self.Cb_LineStyles[currentTabIndex].currentIndex())

            self.ne_pas_repondre_style = False



        if Cb_Top.currentText()=="None": # probe "None" :
            self.tabPlot.curveIndexLinked[currentTabIndex] = -1,-1
        elif currentTypeText != "Surface balance:" and currentTypeText != "Volume balance:" :
            # on travaille sur les sondes
            self.DefDictProbe()

            # check availability of data

            if self.dictProbe[str(Cb_Top.currentIndex())] == "-1" : # probe is not found
                if not os.access(self.case_copy.getHisFullPath(0), os.F_OK):
                    QtWidgets.QMessageBox.information(self, 'Message', "Result files not found.", QtWidgets.QMessageBox.Ok)
                else:
                    QtWidgets.QMessageBox.information(self, 'Message', "Probe not found. Please try another probe.", QtWidgets.QMessageBox.Ok)
                Cb_Top.setCurrentIndex(Cb_Top.count()-1)
                self.tabPlot.curveIndexLinked[currentTabIndex] = -1,-1

            else: # probe found
                self.read_cp_file2(self.tabPlot.curveIndexLinked[currentTabIndex])

        else:
            # à contrôler l'accès
            if self.Cb_Types[currentTabIndex].currentText() == "Surface balance:":
                self.read_cp_SURF(self.tabPlot.curveIndexLinked[currentTabIndex])
            elif self.Cb_Types[currentTabIndex].currentText() == "Volume balance:":
                self.read_cp_VOL(self.tabPlot.curveIndexLinked[currentTabIndex])

        self.updateTitle()

        if self.tabPlot.boolAwaitingData() : # s'il n'y a aucune graphique à tracer
            self.marker.show()
            self.mark=True
        else:
            self.marker.hide()
            self.mark=False

        self.updateAxes()
        pass

    def Cb_Var_Clb(self): # fonction de rappel permettant le changement du type de variable à afficher dans le graphique
        Cb_Var = QtWidgets.QApplication.focusWidget()

        if not (Cb_Var in self.Cb_Vars) :
            # not a combobox de type de variable
            return

        if Cb_Var.currentIndex() == -1 :
            return

        currentTabIndex = self.tabPlot.currentIndex()
        currentTypeIndex = self.Cb_Types[currentTabIndex].currentIndex()
        oldTypeIndex,oldCurveIndex = self.tabPlot.curveIndexLinked[self.tabPlot.currentIndex()]
        self.tabPlot.curveIndexLinked[currentTabIndex] = -1,-1 # l'onglet n'est plus lié à aucune courbe
        self.updateCb_Top(currentTabIndex)

        self.Rb_ButtonGroups[currentTabIndex].button(1).setChecked(True)
        self.tabPlot.yRightEnabled[currentTabIndex] = False

        if oldCurveIndex == -1 :
            self.updateAxes()
            return

        # désaffecter cet onglet (tabIndexLinked) à l'ancienne courbe
        # et détacher l'ancienne courbe
        # précédent contrôle ne plus applicable -> contrôler si l'ancienne courbe (old curve)...
        # ...appartient à un autre onglet (variable "ailleur")
        ailleur = -1
        for ti in range(self.tabPlot.nbActif) :
            if ti != currentTabIndex and self.tabPlot.curveIndexLinked[ti] == (oldTypeIndex,oldCurveIndex) :
                ailleur = ti
                break
        if ailleur == -1 : # l'ancienne courbe n'a pas de duplicata -> détacher
            self.curves[oldTypeIndex,oldCurveIndex].tabIndexLinked = -1
            self.curves[oldTypeIndex,oldCurveIndex].detach()
        else :
            self.curves[oldTypeIndex,oldCurveIndex].tabIndexLinked = ailleur

        self.updateTitle()

        if self.tabPlot.boolAwaitingData() : # s'il n'y a aucune graphique à tracer
            self.marker.show()
            self.mark=True
        else:
            self.marker.hide()
            self.mark=False

        self.updateAxes()
        pass

    def Cb_Type_Clb(self):
        currentTabIndex = self.tabPlot.currentIndex()
        oldTypeIndex,oldCurveIndex = self.tabPlot.curveIndexLinked[self.tabPlot.currentIndex()]
        #self.updateCb_Top(currentTabIndex)
        self.tabPlot.curveIndexLinked[currentTabIndex] = -1,-1 # l'onglet n'est plus lié à aucune courbe

        self.Rb_ButtonGroups[currentTabIndex].button(1).setChecked(True)
        self.tabPlot.yRightEnabled[currentTabIndex] = False

        # désaffecter cet onglet (tabIndexLinked) à l'ancienne courbe
        # et détacher l'ancienne courbe
        # précédent contrôle ne plus applicable -> contrôler si l'ancienne courbe (old curve)...
        # ...appartient à un autre onglet (variable "ailleur")
        ailleur = -1
        for ti in range(self.tabPlot.nbActif) :
            if ti != currentTabIndex and self.tabPlot.curveIndexLinked[ti] == (oldTypeIndex,oldCurveIndex) :
                ailleur = ti
                break
        if ailleur == -1 : # l'ancienne courbe n'a pas de duplicata -> détacher
            if (oldTypeIndex,oldCurveIndex) != (-1,-1):
                self.curves[oldTypeIndex,oldCurveIndex].tabIndexLinked = -1
                self.curves[oldTypeIndex,oldCurveIndex].detach()
        else :
            if (oldTypeIndex,oldCurveIndex) != (-1,-1):
                self.curves[oldTypeIndex,oldCurveIndex].tabIndexLinked = ailleur

        if self.Cb_Types[currentTabIndex].currentText() == "Surface balance:" and self.surface_balance_names == []:
            self.read_cp_SURF(self.tabPlot.curveIndexLinked[currentTabIndex])
        elif self.Cb_Types[currentTabIndex].currentText() == "Volume balance:" and self.volume_balance_names == []:
            self.read_cp_VOL(self.tabPlot.curveIndexLinked[currentTabIndex])
        elif self.history_names == []:
            self.read_cp_file2(self.tabPlot.curveIndexLinked[currentTabIndex])
            pass

        self.updateCb_Var(currentTabIndex)
        self.updateCb_Top(currentTabIndex)

        if self.tabPlot.boolAwaitingData() : # s'il n'y a aucune graphique à tracer
            self.marker.show()
            self.mark=True
        else:
            self.marker.hide()
            self.mark=False

        self.updateAxes()
        pass

    def Rb_ButtonGroup_Clb(self,index):
        currentTabIndex = self.tabPlot.currentIndex()
        curve_index_base = self.Cb_Tops[currentTabIndex].currentIndex()

        currentTypeIndex,currentCurveIndex = self.tabPlot.curveIndexLinked[currentTabIndex]
        if currentCurveIndex == -1:
            return

        currentVarIndex = self.Cb_Vars[currentTabIndex].currentIndex()
        currentVarText = self.Cb_Vars[currentTabIndex].currentText()
        currentTypeText = self.Cb_Types[currentTabIndex].currentText()
        currentTypeIndex = self.Cb_Types[currentTabIndex].currentIndex()
        currentCurve = self.curves[currentTypeIndex,currentCurveIndex]

        if index == 2:
            currentCurve.detach()

            if self.tabPlot.boolAwaitingData() : # s'il n'y a aucune graphique à tracer
                self.marker.show()
                self.mark=True
            else:
                self.marker.hide()
                self.mark=False
        else:
            if currentTypeText != "Surface balance:" and currentTypeText != "Volume balance:" :
                self.read_cp_file2(self.tabPlot.curveIndexLinked[currentTabIndex])
            else:
                if self.Cb_Types[currentTabIndex].currentText() == "Surface balance:":
                    self.read_cp_SURF(self.tabPlot.curveIndexLinked[currentTabIndex])
                elif self.Cb_Types[currentTabIndex].currentText() == "Volume balance:":
                    self.read_cp_VOL(self.tabPlot.curveIndexLinked[currentTabIndex])

            aYAxis = self.plot.axisY(currentCurve)
            if index == 1:
                self.tabPlot.yRightEnabled[currentTabIndex] = False
                if not aYAxis is self.axisY:
                    currentCurve.detachAxis(self.axisRY)
                    currentCurve.attachAxis(self.axisY)

            elif index == 3:
                self.tabPlot.yRightEnabled[currentTabIndex] = True
                if not aYAxis is self.axisRY:
                    currentCurve.detachAxis(self.axisY)
                    if len(self.plot.axes()) == 2:
                        self.plot.addAxis(self.axisRY, QtCore.Qt.AlignRight)
                    currentCurve.attachAxis(self.axisRY)
        # remove second Y axis if it is not used
        if len(self.plot.axes()) == 3:
            isRYUsed = False
            for aSerie in self.plot.series():
                if self.axisRY in aSerie.attachedAxes():
                    isRYUsed = True
                    break

            if not isRYUsed:
                self.plot.removeAxis(self.axisRY)

        self.updateTitle()

        isYRightEnabled=False
        for i in range(self.tabPlot.count()):
            if self.tabPlot.yRightEnabled[i]:
                isYRightEnabled=True
                pass
            pass
        self.updateAxes()

        pass

    def Cb_Style_Clb(self):
        if self.ne_pas_repondre_style == True :
            return

        Cb_Style = QtWidgets.QApplication.focusWidget()

        if not (Cb_Style in self.Cb_LineStyles) :
            # not a combobox linestyle
            return

        # raccourcis
        currentTabIndex = self.tabPlot.currentIndex()
        currentTypeIndex,currentCurveIndex = self.tabPlot.curveIndexLinked[currentTabIndex]

        # set style
        # index 0 -> style 0+1 -> Qt.Qt.SolidLine
        # index 1 -> style 1+1 -> Qt.Qt.DashLine
        # index 2 -> style 2+1 -> Qt.Qt.DotLine
        # index 3 -> style 3+1 -> Qt.Qt.DastDotLine
        self.curves[currentTypeIndex,currentCurveIndex].setStyle(Cb_Style.currentIndex()+1)


        # contrôler si la courbe...
        # ...appartient à un autre onglet (variable "ailleur")
        ailleur = -1
        for ti in range(self.tabPlot.nbActif) :
            if ti != currentTabIndex and self.tabPlot.curveIndexLinked[ti] == (currentTypeIndex,currentCurveIndex) :
                ailleur = ti
                # changer le style de ce duplicata
                self.ne_pas_repondre_style = True
                self.Cb_LineStyles[ti].setCurrentIndex(Cb_Style.currentIndex())

        self.ne_pas_repondre_style = False
        self.updateAxes()

    def updateTitle(self):
        code = ['','']
        isYRightEnabled = False
        for ti in range(self.tabPlot.nbActif) :
            axisId=0
            if self.tabPlot.yRightEnabled[ti]:
                isYRightEnabled = True
                axisId=1
            if self.Cb_Tops[ti].currentText() != '' and self.Cb_Tops[ti].currentText() != 'None' :
                if code[axisId].find(str(self.Cb_Vars[ti].currentText())) == -1:
                    code[axisId] = code[axisId] +' '+ self.Cb_Vars[ti].currentText()

        title = ['', '']
        for i in range(len(code)):
            if code[i].find('Temperature of probe') != -1 :
                title[i] = title + "Temp.(°C) ; "
            elif code[i].find('Vapor pressure of probe') != -1 :
                title[i] = title[i] + "Vap. P.(Pa) ; "
            elif code[i].find('Total pressure of probe') != -1 :
                title[i] = title[i] + "Tot. P.(Pa) ; "
            elif code[i].find('Surface balance') != -1 :
                title[i] = title[i] + "Surf. bal.(W) ; "
            elif code[i].find('Volume balance') != -1 :
                title[i] = title[i] + "Vol. bal.(W)"
            else:
                title[i] = title[i] + code[i]

        # modifier le titre des axes du plot
        text = [None,None]
        for i in range(len(code)):
##             text = Qwt.QwtText(title[i])
##             text.setFont(QFont(self.fn, 8, QFont.Normal))
            if i == 0:
                self.axisY.setTitleText(title[i])
            elif i == 1 and isYRightEnabled:
                self.axisRY.setTitleText(title[i])
            pass

    def ResetScale(self, parent=None): # auto rescale function
        self.plot.zoomReset()
        self.updateAxes()


    def showTime(self):
        t = datetime.datetime.now()
        now = datetime.datetime.fromtimestamp(time.mktime(t.timetuple()))
        #print now.ctime()

    def RefreshListing(self, parent=None):
        if self.tabWidget.currentIndex()==1: # on vient de basculer sur l'onglet full listing
            if os.access(self.case_copy.dirPath + os.sep+str(self.Running_options_form_copy.Ro_Ln_le.text()), os.F_OK):
                listfile=open(self.case_copy.dirPath + os.sep + str(self.Running_options_form_copy.Ro_Ln_le.text()), "r")
                lines=listfile.readlines()
                self.textEdit_2.moveCursor(QtGui.QTextCursor.End, QtGui.QTextCursor.MoveAnchor)
                #monospace
                myCharFormat = QtGui.QTextCharFormat()
                myCharFormat.setFontFixedPitch(True)
                self.textEdit_2.setCurrentCharFormat(myCharFormat)

                i = self.iLastLineListing + 1 # à chaque nouveau calcul (Run Syrthes), self.iLastLineListing devient -1, donc i = 0
                if i == 0 : # nouveau listing -> effacer le contenu de textEdit_2
                    self.textEdit_2.clear()

                while i < len(lines): # charger les nouvelles lignes de listing
                    lstr = "" # le faire au moins une ligne
                    lstr = lstr + lines[i]
                    i = i + 1
                    while (i % 2000 != 0) and (i < len(lines)) : # combiner x lignes dans une chaîne pour gagner du temps
                        lstr = lstr + lines[i]
                        i = i + 1

                    self.textEdit_2.insertPlainText(lstr) # "coller" x lignes à la fois dans textEdit_2 pour gagner du temps

                self.iLastLineListing = len(lines) - 1 # enregistrer le numéro de la dernière ligne chargée pour ne pas repartir de 0 la prochaine fois

                self.textEdit_2.moveCursor(QtGui.QTextCursor.Start, QtGui.QTextCursor.MoveAnchor)
                listfile.close()
        pass


    def Screenshot(self, parent=None): # fonction de rappel de la capture d'écran
        self.originalPixmap=self.PlotWindow.grab()
        format= "png"

        screenshotPath = self.lastDir_copy + self.tr(os.sep+"untitled.")+format

        aFilter = self.tr("*.%1;;All Files (*)").replace("%1", format)
        fileName=QtWidgets.QFileDialog.getSaveFileName(self, self.tr("Save As"), screenshotPath, aFilter)[0]
        if fileName != "":
            self.originalPixmap.save(fileName, str(format))
            self.lastDir_copy = str(fileName).rsplit(os.sep, 1)[0] #update lastDir
        self.initSYRTHESFont()

    def __printGnuplotDat(self, valueMap,fileName):

        towrite = open(fileName, 'w')
        nbTime = len(valueMap["time"])
        line = "time"
        for key in list(valueMap.keys()):
            if key != "time":
                line = line+" "+str(key[0])+"_"+str(key[1]+1)
                pass
            pass
        towrite.write("%s\n"%(line))

        for i in range(nbTime):
            line = "%.9e"%(valueMap["time"][i])
            for key in list(valueMap.keys()):
                if key != "time":
                    line = line + " %.9e"%(valueMap[key][1][i])
                    pass
                pass
            towrite.write("%s\n"%(line))
            pass
        towrite.close()
        pass

    def __printGnuplotCmd(self,valueMap, fileName):
        datName = "%s"%(fileName)
        cmdName = fileName.replace(".dat",".gnu")
        # set xlabel "Vitesse"
        # set ylabel "Distance"

        # plot "freinage.dat" using 1:2 title 'sec' with linespoints , \
        #     "freinage.dat" using 1:3 title 'mouille' with linespoints
        # pause -1
        # quit

        towrite = open(cmdName, 'w')

        line = "set title \"\"\n"
        towrite.write(line)
        line = "set xlabel \"time\"\n"
        yline = ""
        yline2 = ""
        towrite.write(line)
        i = 1

        aAxesList = self.plot.axes()
        axesNb = len(aAxesList)
        if axesNb < 2:
            return
        xmin = aAxesList[0].min()
        xmax = aAxesList[0].max()
        ymin = aAxesList[1].min()
        ymax = aAxesList[1].max()
        line = "set xrange [%.9e:%.9e]\n"%(xmin,xmax)
        line+= "set yrange [%.9e:%.9e]\n"%(ymin,ymax)
        yrmin = 0
        yrmax = 0
        if axesNb == 3:
            yrmin = aAxesList[2].min()
            yrmax = aAxesList[2].max()
            line+= "set y2range [%.9e:%.9e]\n"%(yrmin,yrmax)
        towrite.write(line)
        line = "plot "
        scaleline = "set autoscale y\n"
        ticsline = "set ytics nomirror\n"
        for key in list(valueMap.keys()):
            if key != "time":
                i=i+1
                if valueMap[key][0] != "":
                    line = line + "\"" + datName + "\" using 1:"+str(i)+" title '" + valueMap[key][0] + "' with linespoints"
                else:
                    line = line + "\"" + datName + "\" using 1:"+str(i)+" title '" + \
                           str(key[0]) + "_" + str(key[1]+1) + "' with linespoints"

                if (yline == "") and (axesNb == 2):
                    yline = "set ylabel \"" + key[0] + "\"\n"
                    line = line + " axes x1y1"
                    pass
                elif (yline2 == "") and (axesNb == 3):
                    yline2 = "set y2label \"" + key[0] + "\"\n"
                    line = line + " axes x2y2"
                    scaleline = scaleline + "set autoscale y2\n"
                    ticsline = ticsline + "set y2tics\n"
                if i-1 != len(list(valueMap.keys()))-1:
                    line = line + ','
                pass
            pass
        towrite.write(yline)
        towrite.write(yline2)
        towrite.write(ticsline)
        towrite.write("set grid\n")
        towrite.write(scaleline)
        towrite.write(line+"\n")
        towrite.write("pause -1\n")
        towrite.write("quit\n")
        towrite.close()
        pass

    def Gnuplot(self, parent=None): # fonction de rappel de creation des fichiers gnuplots
        self.originalPixmap = self.PlotWindow.grab()
        format= "dat"

        toGnuplot = {}

        for i in range(len(self.tabPlot.tabs)):
            if self.tabPlot.curveIndexLinked[i][0]+1 and self.tabPlot.curveIndexLinked[i][1]+1:
                u = self.tabPlot.curveIndexLinked[i][0]
                v = self.tabPlot.curveIndexLinked[i][1]
                name = str(self.Cb_Vars[i].currentText())
                toGnuplot[name,v] = self.curves[u,v].legend,self.curves[u,v].valueMap[name]
                pass
            pass

        if list(toGnuplot.keys()):
            toGnuplot["time"] =  self.curves[u,v].valueMap["time"]
        else:
            return

        # print "--------------------------"
        pass


        screenshotPath = self.lastDir_copy + self.tr(os.sep+"untitled.")+format

        aFilter = self.tr("*.%1;;All Files (*)").replace("%1", format)
        fileName=QtWidgets.QFileDialog.getSaveFileName(self, self.tr("Save As"),
                            screenshotPath, aFilter)[0]

        if fileName != "":
            self.__printGnuplotDat(toGnuplot,fileName)
            self.__printGnuplotCmd(toGnuplot,fileName)
            self.lastDir_copy = str(fileName).rsplit(os.sep, 1)[0] #update lastDir
        self.initSYRTHESFont()


    # fonction de rappel prennant en compte les évènement de la roulette de la souris
    # dans la fenêtre de suivis de calcul afin de faire le zoom
    def wheel_event(self, event):
        ## pos=event.pos()
        delta=event.delta() # 15°-step-mousewheel --> delta = 15*8 =  120. Attention finer step on modern mouse will cause problem
        if delta > 0:
            self.plot.zoom(1.2)
        else:
            self.plot.zoom(0.8)
        event.ignore()


    def mouse_press(self, theEvent):
        self.RubberBandPos = theEvent.pos()
        self.RubberBand.setGeometry(self.RubberBandPos.x(), self.RubberBandPos.y(), 0, 0)


    # fonction de rappel faisant appel au coordonnée de la souris
    # pendant son déplacement pour faire glisser le graphique
    def mouse_move(self, event):
        if not self.plot.isUnderMouse():
            return

        self.showCoordinates(event)
        if event.buttons() != Qt.Qt.NoButton:
            if self.xposold==None or self.yposold==None: # Si il n'y a pas de coordonée précédente on prend les coordonnées actuelles
                self.xposold=event.x()
                self.yposold=event.y()

        xpos=event.x()# prise des coordonnées actuelles
        ypos=event.y()
        if event.buttons() & Qt.Qt.MidButton:
            self.plot.scroll(self.xposold - xpos, ypos - self.yposold)
        elif event.buttons() & Qt.Qt.RightButton:
            if ypos > self.yposold:
                self.plot.zoom(1.05)
            elif ypos < self.yposold:
                self.plot.zoom(0.95)
        elif event.buttons() & Qt.Qt.LeftButton:
            self.RubberBand.show()
            self.RubberBand.setGeometry(QtCore.QRect(self.RubberBandPos, event.pos()).normalized())

        self.xposold=xpos # enregistrement des positions actuels dans les positions anciennes
        self.yposold=ypos


    def raz_mouses(self, event): # fonction de remise à zéro des coordonnées précédente
        self.xposold=None
        self.yposold=None
        self.RubberBand.hide()
        aSelectRect = self.RubberBand.geometry()
        if aSelectRect.width() > 0 and aSelectRect.height() > 0:
            self.plot.zoomIn(QtCore.QRectF(aSelectRect))
            self.RubberBand.setGeometry(0,0,0,0)

    def initSYRTHESFont(self):
        # (re)set font for the application
        return
        if not syrthesIHMContext.isEmbedded():
            return
        myFont = app.font()
        myFont.setFamily('Sans')
        myFont.setStyleHint(QFont.SansSerif, QFont.PreferMatch)
        myFont.setPointSize(10)
        app.setFont(myFont)

    def updateAxes(self):
        Xmin = 0
        Xmax = 0
        aYRanges = {}
        isFirst = True
        for aSeries in self.plot.series():
            aYaxis = self.plot.axes(QtCore.Qt.Vertical, aSeries)[0]
            if isFirst:
                Xmin = aSeries.Xmin
                Xmax = aSeries.Xmax
                isFirst = False
            else:
                if Xmin > aSeries.Xmin: Xmin = aSeries.Xmin
                if Xmax < aSeries.Xmax: Xmax = aSeries.Xmax

            if aYaxis in aYRanges:
                if aYRanges[aYaxis][0] > aSeries.Ymin:
                    aYRanges[aYaxis][0] = aSeries.Ymin

                if aYRanges[aYaxis][1] < aSeries.Ymax:
                    aYRanges[aYaxis][1] = aSeries.Ymax
            else:
                aYRanges[aYaxis] = [aSeries.Ymin, aSeries.Ymax]


        self.axisX.setRange(Xmin, Xmax)
        for aAxis in list(aYRanges.keys()):
            aAxis.setRange(aYRanges[aAxis][0], aYRanges[aAxis][1])


    def resizeChart(self, event):
        aSize = event.newSize()
        self.marker.setPos(aSize.width()/2 - 50, aSize.height()/2 - 30)



class clCurve(QtChart.QLineSeries):
    def __init__(self, legend="", parent=None):
        QtChart.QLineSeries.__init__(self, parent)
        QtChart.QLineSeries(parent)
        self.setName(legend)
        self.legend = legend
        self.valueMap = {}
        self.Time = []
        self.Temper = []
        self.vP = [] # vapor pressure
        self.taP = [] # total air pressure
        self.tabIndexLinked = -1
        self.pen = Qt.QPen(Qt.Qt.green)
        self.pen.setWidth(2)
        self.color = Qt.Qt.black

        # coordonnées de la sonde
        self.dimension = -1
        self.x = ""
        self.y = ""
        self.z = ""

        self.Xmin = 0
        self.Xmax = 0
        self.Ymin = 0
        self.Ymax = 0

    def setColor(self, color):
        self.color = color
        self.pen.setColor(color)
        self.setPen(self.pen)


    def setStyle(self, style):
        styles = [None, Qt.Qt.SolidLine, Qt.Qt.DashLine, Qt.Qt.DotLine, Qt.Qt.DashDotLine]
        self.pen.setStyle(styles[style])
        self.setPen(self.pen)

    def printProbe(self):
        if self.dimension == 3 :
            return '[' + str(self.x) + ";" + str(self.y) + ";" + str(self.z) + ']'
        else:
            return '[' + str(self.x) + ";" + str(self.y) + ']'

    def refreshCurve(self, name):
        aXList = self.valueMap["time"]
        aYList = self.valueMap[name]
        self.clear()
        aX = None
        aY = None
        for i in range(len(aXList)):
            if aX is None:
                self.Xmax = self.Xmin = aXList[i]
                self.Ymax = self.Ymin = aYList[i]
            aX = aXList[i]
            aY = aYList[i]
            self.append(aX, aY)
            if self.Xmin > aX: self.Xmin = aX
            if self.Xmax < aX: self.Xmax = aX
            if self.Ymin > aY: self.Ymin = aY
            if self.Ymax < aY: self.Ymax = aY

    def razData(self):
        self.valueMap={}
        self.clear()

    def attach(self, thePlot):
        ## Attach the curve only once and only if it has points
        if (self.count() > 0) and (self.chart() is None):
            thePlot.addSeries(self)
            self.attachAxis(thePlot.axisX())
            self.attachAxis(thePlot.axisY())

    def detach(self):
        if not self.chart() is None:
            self.chart().removeSeries(self)

# -*- coding: utf-8 -*-
# user tool for saving a references table
from syrthesIHMContext import syrthesIHMContext

def saveTable(Table, keyWord, savfil, nbRef, boolCombo):
    # Tool for saving tables. Not applicable for : Vch_St_TPv_table, Vch_St_TPvPt_table, Mhp_table and Mrp_table
    # For those 4 tables, only boolFilledRow function (below) is used

    #Table.setCurrentCell(0, 1+int(boolCombo)) # Positionnement du tableau des conditions initial à la première case à exploiter (?)

    for i in range(Table.rowCount()):
        # refresh "check" for a new loop
        # Choice of value in combobox (if any) 0 : CONT ; 1 : FUNC ; 2 : PROG ; by default : 0
        # and choice of begin column where the first value is supposed to be

        if boolCombo == True:
            checkCombo = Table.cellWidget(i,1).currentIndex()
            beginCol = 2 # there is one combobox, table with 2 comboboxes can't be treated by this function
        else :
            checkCombo = 0
            beginCol = 1
        check = boolFilledRow(Table, i, beginCol) # by default, check true only if all cells are filled in

        if checkCombo == 2 : # combobox of type "PROG"
            # check if reference fields only are filled
            if nbRef == 1: # 1 references
                check = Table.item(i,Table.columnCount()-2)!=None and Table.item(i,Table.columnCount()-2).text()!=str('')
            elif nbRef == 2: # 2 references
                check = (Table.item(i,Table.columnCount()-2)!=None) and (Table.item(i,Table.columnCount()-2).text()!=str('')) and (Table.item(i,Table.columnCount()-3)!=None) and (Table.item(i,Table.columnCount()-3).text!=str(''))

        if (check):
            #print i
            # Ecriture d'une ligne de commentaire utilisateur
            savfil.write("/+ ")
            if (Table.item(i, Table.columnCount()-1)!=None):
                Item=Table.item(i, Table.columnCount()-1)
                text=Item.text()
                savfil.write(str(text))
            savfil.write("\n")

            # Ecriture du mot clé
            if Table.cellWidget(i, 0).isChecked()==False:
                savfil.write("/# ") # Ecriture d'une ligne de mot clé non prise en compte
            cmdWord = keyWord[checkCombo]
            if cmdWord != "":
                savfil.write(cmdWord + " ") # writ key word : ex :  CINI_T

            # Ecriture des valeurs
            if checkCombo!=2: # if Constant or Function
                for j in range(Table.columnCount())[beginCol:-1]:
                    savfil.write(Table.item(i, j).text())
                    savfil.write(" ")
                    # insert "-1" between 2 references
                    if (nbRef == 2) and (j == Table.columnCount()-3) :
                        savfil.write("-1 ")
            else: # if PROG, write only the reference(s)
                if nbRef == 2: # if 2 references, insert -1 for separate 2 groups
                    savfil.write(Table.item(i, Table.columnCount()-3).text())
                    savfil.write(" -1 ")
                if nbRef != 0:
                    savfil.write(Table.item(i, Table.columnCount()-2).text())
                    savfil.write(" ")
            savfil.write("\n")

        if (not boolFilledRow(Table, i, beginCol)) and (not boolEmptyRow(Table, i, beginCol)) and checkCombo != 2:
            # not filled but not empty AND the row is not of type PROGRAM
            syrthesIHMContext.notFullyFilledException = True

def boolFilledRow(Table, i, beginCol):
    # check if the row i of Table is empty or not (from the column beginCol)
    check = True # suppose that the row is filled
    for j in range(Table.columnCount())[beginCol:-1]:
        if (Table.item(i, j) == None) :
            check = False # the row is not filled anymore
            break
        elif (Table.item(i, j).text() == str('')) :
            check = False # the row is not filled anymore
            break
    return check

def boolEmptyRow(Table, i, beginCol):
    """check if the row i of Table is empty or not (from the column beginCol)"""
    check = True # suppose that the row is empty
    for j in range(Table.columnCount())[beginCol:-1] :
        if (Table.item(i, j) != None) :
            if Table.item(i, j).text() != str('') :
                check = False # the row is not empty anymore
                break
    return check

def boolFilledTable(Table, beginCol):
    # check if the Table is empty or not (from the column beginCol
    check = False
    for i in range(Table.rowCount()):
        if Table.cellWidget(i, 0).isChecked() and boolFilledRow(Table, i, beginCol):
            check = True
    return check

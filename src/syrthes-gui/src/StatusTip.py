# -*- coding: utf-8 -*-

from PyQt5 import QtWidgets, QtCore

#class StatusTip(object):

def StatusTip(self, parent=None):
    
#Statustip dictionnary
    self.dic_status={#Mainwindow
                self.action_New_file : "Open a new SYRTHES file", #action de la barre d'outil:action nouveau fichier
                self.action_Open : "Open an existing SYRTHES file", #action ouvrir
                self.action_Save : "Save a SYRTHES file", #action sauvegarder
                self.action_Quit : "Quit SYRTHES GUI", #action quitter
                self.action_Screenshot : "Screenshot", #action capture d'écran
                self.action_Calculation_Progress : "Activate the calculation progress window", #action suivis de calcul
                self.action_Run_Syrthes : "Button to run SYRTHES", #action de lancement de Syrthes
                self.action_Stop_Syrthes : "Stop the current SYRTHES execution", # arrêt de Syrthes
                self.treeWidget : "Dynamic tree of parameters and options",#arborescence
                #Home_form
                self.syrthesIHMCollector.Home_form.lineEdit_7 : "Title of the calculation (optional)",#champ éditable case title
                self.syrthesIHMCollector.Home_form.Ho_Ud_but : "Field to describe the current SYRTHES calculation (optional)",#bouton use description...
                self.syrthesIHMCollector.Home_form.Dim_Comb : "Dimension of the problem (to be selected by user)",#Combobox de la dimension
                self.syrthesIHMCollector.Home_form.Ho_Tr_ch : "Activation of complex radiation module (use 'help/What's this' for details)",#radio bouton pour le rayonnement
                self.syrthesIHMCollector.Home_form.Ho_Hm_ch : "Activation of complex humidity module (use 'help/What's this' for details)",#radio bouton pour l'humidité
                self.syrthesIHMCollector.Home_form.Ho_Ch_ch : "Activation of conjugate heat transfer option (use 'help/What's this' for details)",#radio bouton pour le transfert de chaleur
                self.syrthesIHMCollector.Home_form.Ho_fluid1d_ch : "Activation of module 1D fluid flow option (use 'help/What's this' for details)",#radio bouton pour le module CDF 1D
                self.syrthesIHMCollector.Home_form.Ho_fluid0d_ch : "Activation of module 0D fluid flow option (use 'help/What's this' for details)",#radio bouton pour le module CDF 0D
                #Control_form
                self.syrthesIHMCollector.Control_form.Ch_res_cal : "Restart calculation (use 'help/What's this' for details)",#checkbox de la reprise de calcul
                self.syrthesIHMCollector.Control_form.lineEdit_39 : "Value of the new restart time (use 'help/What's this' for details)",#champ éditable du temp de reprise
                self.syrthesIHMCollector.Control_form.Le_Nts : "Global number of time step (use 'help/What's this' for details)",#champ éditable du nombre de pas de temp global
                self.syrthesIHMCollector.Control_form.comb_time_st : "Options for the time steps",#combobox du type de pas de temp
                self.syrthesIHMCollector.Control_form.Le_auto_It : "Initial time step (use 'help/What's this' for details)",#champ éditable du temp initial
                self.syrthesIHMCollector.Control_form.Le_auto_Mt : "Maximal temperature variation between two time steps (use 'help/What's this' for details)",#champ éditable de la variation de température maximum
                self.syrthesIHMCollector.Control_form.Le_auto_Mts : "Upper bound for the allowed time step (use 'help/What's this' for details)",#champ éditable du pas de temp maximum
                self.syrthesIHMCollector.Control_form.Le_auto_Mpv : "Maximal vapor pressure variation between two time steps (use 'help/What's this' for details)",#champ éditable de la variation de pression de vapeur maximum,
                self.syrthesIHMCollector.Control_form.Le_auto_Mpt : "Maximal air pressure variation between two time steps (use 'help/What's this' for details)",#champ éditable de la variation de pression d'air maximum,
                self.syrthesIHMCollector.Control_form.Le_const_Ts : "Time step value (use 'help/What's this' for details)",#champ éditable du pas de temp
                self.syrthesIHMCollector.Control_form.By_Block_table : "Time steps per block (use 'help/What's this' for details)",#tableau des pas de temp par block
                self.syrthesIHMCollector.Control_form.lineEdit_43 : "Solver precision (use 'help/What's this' for details)",#champ éditable de la précision du solver
                self.syrthesIHMCollector.Control_form.Le_Mni : "Maximum number of iteration allowed in the solver (use 'help/What's this' for details)",#champ éditable du nombre maximum d'itération
                self.syrthesIHMCollector.Control_form.Vap_Sp_le : "Solver precision (use 'help/What's this' for details)",#champ éditable de la précision du solver 
                self.syrthesIHMCollector.Control_form.Vap_Mn_le : "Maximum number of iteration allowed in the solver (use 'help/What's this' for details)",#champ éditable du nombre maximum d'itération
                self.syrthesIHMCollector.Control_form.Ap_Sp_le : "Solver precision (use 'help/What's this' for details)",#champ éditable de la précision du solver
                self.syrthesIHMCollector.Control_form.Ap_Mn_le : "Maximum number of iteration allowed in the solver (use 'help/What's this' for details)",#champ éditable du nombre maximum d'itération                    
                #Boundary_conditions_form
                self.syrthesIHMCollector.Boundary_conditions_cond_form.Heat_ex_table : "Heat exchange table (use 'help/What's this' for details)",#tableau de l'échange de chaleur
                self.syrthesIHMCollector.Boundary_conditions_cond_form.Cont_res_table : "Contact resistance table (use 'help/What's this' for details)",#tableau de la réseistance de contact
                self.syrthesIHMCollector.Boundary_conditions_cond_form.Flux_cond_table : "Flux table (use 'help/What's this' for details)",#tableau de la condition du flux
                self.syrthesIHMCollector.Boundary_conditions_cond_form.Diric_cond_table : "Dirichlet table (use 'help/What's this' for details)",#tableau de la condition Dirichlet
                self.syrthesIHMCollector.Boundary_conditions_cond_form.Inf_rad_table : "Radiation table (use 'help/What's this' for details)",#tableau de la radiation infinit
                #Physical_properties_2D_form 
                self.syrthesIHMCollector.Physical_prop_2D_form.Iso_cond_2D_table : "Isotropic conductivity table (use 'help/What's this' for details)",#tableau de la conductivité isotropic
                self.syrthesIHMCollector.Physical_prop_2D_form.Ort_cond_2D_table : "Orthotropic conductivity table (use 'help/What's this' for details)",#tableau de la conductivité orthotropic
                self.syrthesIHMCollector.Physical_prop_2D_form.Ani_cond_2D_table : "Anisotropic conductivity table (use 'help/What's this' for details)",#tableau de la conductivité anisotropic
                #Physical_properties_3D_form
                self.syrthesIHMCollector.Physical_prop_3D_form.Iso_cond_3D_table : "Isotropic conductivity table (use 'help/What's this' for details)",#tableau de la conductivité isotropic
                self.syrthesIHMCollector.Physical_prop_3D_form.Ort_cond_3D_table : "Orthotropic conductivity table (use 'help/What's this' for details)",#tableau de la conductivité orthotropic
                self.syrthesIHMCollector.Physical_prop_3D_form.Ani_cond_3D_table : "Anisotropic conductivity table (use 'help/What's this' for details)",#tableau de la conductivité anisotropic
                #Volumetric_conditions
                self.syrthesIHMCollector.Initial_conditions_cond_form.Init_T_table : "Initial temperature (use 'help/What's this' for details)",#tableau de la temperature initial
                self.syrthesIHMCollector.Volumetric_conditions_cond_form.Vol_so_table : "Volumetric source (use 'help/What's this' for details)",#tableau du volume source
                #Periodicity_2D_form
                #self.P2_pr_cb : "43",#checkbox de la periodicté de rotation
                self.syrthesIHMCollector.Periodicity_2D_form.Per_2D_rot_table : "2D Periodicity in rotation",#tableau de la periodicité de rotation
                #self.P2_pt_cb : "45",#checkbox de la periodicité de translation
                self.syrthesIHMCollector.Periodicity_2D_form.Per_2D_tra_table : "2D Periodicity in translation (use 'help/What's this' for details)",#tableau de la periodicité de translation
                #Periodicity_3D_form
                #self.P3_Pr_cb : "47",#checkbox de la periodicité de rotation
                self.syrthesIHMCollector.Periodicity_3D_form.Per_3D_rot_table : "3D Periodicity in rotation (use 'help/What's this' for details)",#tableau de la perriodicité de rotation
                #self.P3_Pt_cb : "49",#checkbox de la periodicité de translation
                self.syrthesIHMCollector.Periodicity_3D_form.Per_3D_tra_table : "3D Periodicity in translation (use 'help/What's this' for details)",#tableau de la periodicité de translation
                #FileNames_form
                self.syrthesIHMCollector.Filename_form.Fn_Cd_lne : "Conduction mesh file name (use 'help/What's this' for details)",#champ éditable du fichier de conduction
                self.syrthesIHMCollector.Filename_form.Fn_Cd_but : "Activate file selection window",#bouton de saisie du fichier de conduction
                self.syrthesIHMCollector.Filename_form.Fn_Rs_lne : "Restart file name (use 'help/What's this' for details)",#champ éditable du fichier de reprise
                self.syrthesIHMCollector.Filename_form.Fn_Rs_but : "Activate file selection window",#bouton de saisie du fichier de reprise
                self.syrthesIHMCollector.Filename_form.Fn_Mt_lne : "Weather data file name (use 'help/What's this' for details)",#champ éditable du fichier météo
                self.syrthesIHMCollector.Filename_form.Fn_Mt_but : "Activate file selection window",#bouton de saisie du fichier météo
                self.syrthesIHMCollector.Filename_form.Fn_Rnp_lne : "Basename of the result files (use 'help/What's this' for details)",#champ éditable du préfix de fichier
                self.syrthesIHMCollector.Filename_form.Fn_Rnp_but : "Activate file selection window",#bouton de saisie du préfix de fichier
                self.syrthesIHMCollector.Filename_form.Fn_Rm_lne : "Radiation mesh file name (use 'help/What's this' for details)",#champ éditable du fichier de maillage rayonnement format Syrthes
                self.syrthesIHMCollector.Filename_form.Fn_Rm_but : "Activate file selection window",#bouton de saisie du fichier de maillage rayonneemnt format Syrthes
                #Running_options_form
                self.syrthesIHMCollector.Running_options_form.Ro_Ln_le : "Listing name (listing by default)",#champ d'édition du nomde listing
                self.syrthesIHMCollector.Running_options_form.Ro_Ln_pb : "Activate file selection window",#bouton de choix de fichier listing
                self.syrthesIHMCollector.Running_options_form.Ro_Cr_cb : "Option for results format translation",#combobox de la conversion de résultat
                self.syrthesIHMCollector.Running_options_form.Ro_Dp_cb : "Mesh partitionning (already done or not)",#combobox du positionnement de domaine
                self.syrthesIHMCollector.Running_options_form.Ro_Pre_cb : "Preprocessing for 1D Fluid (already done or not)",#combobox de preprocessing pour le fluid 1D
##                    self.syrthesIHMCollector.Solar_aspect_form.spinBox_4 : "79",#spinbox du nombre de processeur
##                    self.Ro_Pre_cb : "80",#checkbox de l'utilisation d' outils de pre-processing
##                    self.Ro_If_cb : "81",#checkbox de l'utilisation de fonctions interpretées
##                    self.Ro_Ff_cb : "82",#checkbox de l'utilisation de la conversion de format de fichier
##                    self.Ro_Li_cb : "83",#checkbox de l'utilisation du linking
##                    self.Ro_Ex_cb : "84",#checkbox de l'utilisation de l'écxecution
##                    self.Ro_Pos_cb : "85",#checkbox de l'utilisataion d'outil de post-processing
##                    self.Ro_Pos_cmb : "86",#Combobox due l'outil de post-processing à utiliser
                self.syrthesIHMCollector.Running_options_form.Ro_Rs_Pb : "Button to run SYRTHES (use 'help/What's this' for details)",#Bouton des lancement de Syrthes
                self.syrthesIHMCollector.Running_options_form.Ro_Np_sb_cd : "Choice of the number of processors to be used for conduction (use 'help/What's this' for details)",#Spinbox pour le nombre de processeur en conduction
                self.syrthesIHMCollector.Running_options_form.Ro_Np_sb_ry : "Choice of the number of processors to be used for radiation (use 'help/What's this' for details)",#Spinbox pour le nombre de processeur en rayonnement
                #Radiation_parameters_form
                self.syrthesIHMCollector.Spectral_parameters_form.Rp_Sb_table : "Table to set wave lenght band definition (use 'help/What's this' for details)",#tableau des paramètres de rayonnement
                #Material_properties_form
                self.syrthesIHMCollector.Material_radiation_properties_form.Mrp_table : "Table to define radiation properties (use 'help/What's this' for details)",#tableau des paramètres des matériaux en rayonnement
                #Boundary_condition_rad_form
                self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Scf_lne : "References of conduction faces coupled with radiation (use 'help/What's this' for details)",#champ d'édition de la conduction de faces solides
                self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rf_lne : "References of radiation faces coupled with conduction (use 'help/What's this' for details)",#champ d'édition du rayonnement des faces
                self.syrthesIHMCollector.Boundary_conditions_rad_form.pushButton : "Develop detailed user comment windows",#bouton User comments du champ d'édition de la conduction de faces solides
                self.syrthesIHMCollector.Boundary_conditions_rad_form.pushButton_2 : "User comments allowing to mention detailed information (text only)",#bouton User comments du champ d'édition du rayonnement des faces
                self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Scf_te : "Develop detailed user comment windows",#champ d'édition User comments de la conduction de faces solides
                self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rf_te : "User comments allowing to mention detailed information (text only)",#champ d'édition User comments du rayonnement des faces
                self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Irt_table : "Table setting temperature on some radiation faces",#Tableau de la température imposé
                self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Irf_table : "Table setting flux (per band) on some radiation faces",#Tableau du flux imposé
                self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rpa_chb : "Activating the aperture problem option (not to be activated by default)",#checkbox du problème de rayonnement avec ouverture
                self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rpa_le : "Temperature of the open space (use 'help/What's this' for details)",#champ d'édition du problème de rayonnement avec ouverture
                #View_factor_2D_form
##                    self.syrthesIHMCollector.View_factor_3D_form.Vfm_2D_sb : "67",#spinbox du nombre de face de rayonnement 2D
##                    self.syrthesIHMCollector.View_factor_2D_form.Vfm_2D_cmb : "68",#combobox du management des facteurs de forme 2D
                self.syrthesIHMCollector.View_factor_2D_form.Vf_Ip_2D_table : "Table to define internal points in 2D (use 'help/What's this' for details)",#tableau des points internes 2D
                self.syrthesIHMCollector.View_factor_2D_form.Vf_Sy_2D_table : "Table defining symetries (use 'help/What's this' for details)",#tableau des symétrie 2D
                self.syrthesIHMCollector.View_factor_2D_form.Vf_Pe_2D_table : "Table defining periodicity of rotation (use 'help/What's this' for details)",#tableau des périodicités 2D
                #View_factor_3D_form
##                    self.syrthesIHMCollector.View_factor_3D_form.Vfm_3D_sb : "72",#spinbox du nombre de face de rayonnement 3D
##                    self.syrthesIHMCollector.View_factor_3D_form.Vfm_3D_cmb : "75",#combobox du management des facteurs de forme 3D
                self.syrthesIHMCollector.View_factor_3D_form.Vf_Ip_3D_table : "Table to define internal points in 3D (use 'help/What's this' for details)",#tableau des points internes 3D
                self.syrthesIHMCollector.View_factor_3D_form.Vf_Sy_3D_table : "Table defining symetries (use 'help/What's this' for details)",#tableau des symétrie 3D
                self.syrthesIHMCollector.View_factor_3D_form.Vf_Pe_3D_table : "Table defining periodicity of rotation (use 'help/What's this' for details)",#tableau des périodicités 3D
                self.syrthesIHMCollector.View_factor_2D_form.Vfm_2D_cmb : "Option for view factors management (use 'help/What's this' for details)",
                self.syrthesIHMCollector.View_factor_3D_form.Vfm_3D_cmb : "Option for view factors management (use 'help/What's this' for details)",
                #Solar_aspect_form
                self.syrthesIHMCollector.Solar_aspect_form.Sa_cmb : "Solar modelling choice (not completely fixed in progress by IR/CP)",#combobox de la modélisation solaire
                self.syrthesIHMCollector.Solar_aspect_form.spinBox : "Azimuth from 0 deg to 90 deg (use 'help/What's this' for details)",#spinbox de l'azimuth
                self.syrthesIHMCollector.Solar_aspect_form.spinBox_2 : "Height from 0 deg to 90 deg (use 'help/What's this' for details)",#spinbox de la hauteur
                self.syrthesIHMCollector.Solar_aspect_form.spinBox_3 : "Latitude (from 0 deg to 90 deg) (use 'help/What's this' for details)",#spinbox des degrés en latitude
                self.syrthesIHMCollector.Solar_aspect_form.spinBox_4 : "Latitude (from 0 minute to 60 minutes) (use 'help/What's this' for details)",#spinbox des minutes en latitude
                self.syrthesIHMCollector.Solar_aspect_form.spinBox_5 : "Longitude (from 0 deg to 90 deg) (use 'help/What's this' for details)",#spinbox des degrés en longitude
                self.syrthesIHMCollector.Solar_aspect_form.Asm_Lat_cmb : "Hemisphere (use 'help/What's this' for details)",
                self.syrthesIHMCollector.Solar_aspect_form.Asm_Lng_cmb : "West/East relative to Greenwich (use 'help/What's this' for details)" ,
                self.syrthesIHMCollector.Solar_aspect_form.Csm_Db_table : "User distribution source flux table (use 'help/What's this' for details)",
                self.syrthesIHMCollector.Solar_aspect_form.spinBox_6 : "Longitude (from 0 minute to 60 minutes) (use 'help/What's this' for details)",#spinbox des minutes en longitude
                self.syrthesIHMCollector.Solar_aspect_form.dateTimeEdit : "In a generic year : date to be used for the calculation",#date                    
                #self.syrthesIHMCollector.Solar_aspect_form.Csm_ad_cb : "Automatic Solar Flux distribution on bands",#Repartition automatique du flux solaire constant sur les bandes
                self.syrthesIHMCollector.Solar_aspect_form.doubleSpinBox_5 : "Coefficient entering sky modelling (use 'help/What's this' for details)",#doublespinbox du coefficient de clareté du ciel
                self.syrthesIHMCollector.Solar_aspect_form.doubleSpinBox_6 : "Coefficient entering sky modelling (use 'help/What's this' for details)",#doublespinbox du coefficient de clareté du ciel
##                    self.horizontalSlider : "North/South choice (use 'help/What's this' for details)",#slider nord/sud
##                    self.horizontalSlider_2 : "West/East choice (use 'help/What's this' for details)",#slider ouest/est
##                    self.syrthesIHMCollector.Solar_aspect_form.doubleSpinBox_7 : "102",#doublespinbox des references des faces de l'horizon
##                    self.syrthesIHMCollector.Solar_aspect_form.doubleSpinBox_8 : "103",#doublespinbox de la temperature d'émissivité de l'horizon
##                    self.lineEdit : "104",#champ d'édition de la temperature d'émissivité de l'horizon
##                    self.syrthesIHMCollector.Solar_aspect_form.doubleSpinBox_9 : "105",#doublespinbox du coefficient de la modélisation de l'ombrage
##                    self.lineEdit_2 : "106",#champ d'édition des références du coefficient de la modélisation de l'ombrage
                self.syrthesIHMCollector.Solar_aspect_form.Sa_Sht_table : "Solar heat balance (use 'help/What's this' for details)",#tableau de l'équilibrage de la chaleur solaire
                self.syrthesIHMCollector.Solar_aspect_form.Sa_Hm_table : "Horizon modelling table (use 'help/What's this' for details)",#tableau de la modélisation de l'horizon
                self.syrthesIHMCollector.Solar_aspect_form.Sa_Sm_table :"Table defining shadowing coefficients for screen effects (use 'help/What's this' for details)",#tableau de la modélisation de l'ombrage
                self.syrthesIHMCollector.Solar_aspect_form.Csm_cmb : "Option for spectral energy redistribution (use 'help/What's this' for details)", 
                self.syrthesIHMCollector.Solar_aspect_form.lineEdit : "Option for spectral energy redistribution (use 'help/What's this' for details)",
                #Humidity_model_form
                self.syrthesIHMCollector.Home_form.Hm_cmb : "Selection of the humidity models (use 'help/What's this' for details)",#combobox du modèle d'humidité
                #Boundary_conditions_TPv_form
                self.syrthesIHMCollector.Boundary_conditions_TPv_form.Bc_TPv_table : "Table defining boundary conditions for Temperature T and Vapor pressure Pv",#tableau des conditions au limite TPv
                #Boundary_conditions_TPvPt_form
                self.syrthesIHMCollector.Boundary_conditions_TPvPt_form.Bc_TPvPt_table : "Table defining boundary conditions for Temperature T Vapor pressure Pv and total pressure Pt (use 'help/What's this' for details)",#tableau des conditions au limite TPvPt
                #Boundary_conditions_TPv_form
                self.syrthesIHMCollector.Contact_resistance_humidity_TPv_form.Cont_res_hum_TPv_table : "Table defining contact resistance for Temperature T and Vapor pressure Pv",#tableau des resistances de contact TPv
                #Boundary_conditions_TPvPt_form
                self.syrthesIHMCollector.Contact_resistance_humidity_TPvPt_form.Cont_res_hum_TPvPt_table : "Table defining contact resistance for Temperature T Vapor pressure Pv and total pressure Pt (use 'help/What's this' for details)",#tableau des resistances de contacts TPvPt
                #Volumetric_conditions_hum_TPv_form
                self.syrthesIHMCollector.Initial_conditions_hum_TPv_form.Vch_Ic_TPv_table : "Initial condition for Temperature T  and Vapor Pressure Pv (use 'help/What's this' for details)",#tableau des conditions initial TPv
                self.syrthesIHMCollector.Volumetric_conditions_hum_TPv_form.Vch_St_TPv_table : "Source term for Temperature T and Vapor pressure Pv (use 'help/What's this' for details)",#tableau des termes sources TPv
                #Volumetric_conditions_hum_TPvPt_form
                self.syrthesIHMCollector.Initial_conditions_hum_TPvPt_form.Vch_Ic_TPvPt_table : "Initial conditions for temperature T Vapor pressure Pv and totale pressure Pt (use 'help/What's this' for details)",#tableau des conditions initial TPvPt
                self.syrthesIHMCollector.Volumetric_conditions_hum_TPvPt_form.Vch_St_TPvPt_table : "Source terms for temperature T vapor pressure Pv and totale pressure Pt (use 'help/What's this' for details)",#tableau des termes sources TPvPt
                #Material_humidity_properties_form
                self.syrthesIHMCollector.Material_humidity_properties_2D_form.Mhp_iso_2D_table : "Table to select materials in an existing SYRTHES material library (use 'help/What's this' for details)",#tableau des propriétés des matériaux en humidité
                self.syrthesIHMCollector.Material_humidity_properties_2D_form.Mhp_aniso_2D_table : "Table to select materials in an existing SYRTHES material library (use 'help/What's this' for details)",#tableau des propriétés des matériaux en humidité
                self.syrthesIHMCollector.Material_humidity_properties_3D_form.Mhp_iso_3D_table : "Table to select materials in an existing SYRTHES material library (use 'help/What's this' for details)",#tableau des propriétés des matériaux en humidité
                self.syrthesIHMCollector.Material_humidity_properties_3D_form.Mhp_aniso_3D_table : "Table to select materials in an existing SYRTHES material library (use 'help/What's this' for details)",#tableau des propriétés des matériaux en humidité
                #Conjugate heat transfer
                self.syrthesIHMCollector.Conjugate_heat_transfer_form.Cht_Sc_table : "Table defining surface coupling with CFD codes (use 'help/What's this' for details)",
                self.syrthesIHMCollector.Conjugate_heat_transfer_form.Cht_Vc_table : "Table defining volumic coupling with CFD codes (use 'help/What's this' for details)",
##                    self.Cht_Sf_le : "72",#champ d'édition des faces solides
##                    self.Cht_Sf_tb : "75",#bouton user comments des faces solides
##                    self.Cht_Sf_te : "102",# champ dédition de texte des faces solides
##                    self.Cht_Se_le : "103",#champ d'édition des éléments solides
##                    self.Cht_Se_tb : "104",#bouton user comments des éléments solides
##                    self.Cht_Se_te : "105"# champ dédition de texte des éléments solides
                self.syrthesIHMCollector.User_C_function_form.Cfunc_lne : "General purpose SYRTHES user c file (use 'help/What's this' for details)", # bouton user C function
                self.syrthesIHMCollector.User_C_function_form.Cfunc_but : "General purpose SYRTHES user c file (use 'help/What's this' for details)", # bouton user C function
                self.syrthesIHMCollector.User_C_function_form.Cfunc_cond_lne : "c file specific to conduction (use 'help/What's this' for details)",# bouton user C function pour la conduction
                self.syrthesIHMCollector.User_C_function_form.Cfunc_cond_but : "c file specific to conduction (use 'help/What's this' for details)",# bouton user C function pour la conduction
                self.syrthesIHMCollector.User_C_function_form.Cfunc_ray_lne : "c file specific for radiation (use 'help/What's this' for details)",# bouton user C function pour le rayonnement
                self.syrthesIHMCollector.User_C_function_form.Cfunc_ray_but : "c file specific for radiation (use 'help/What's this' for details)",# bouton user C function pour le rayonnement
                self.syrthesIHMCollector.User_C_function_form.Cfunc_hmt_lne : "c file specific for humidity models (use 'help/What's this' for details)", # bouton user C function pour la humidité
                self.syrthesIHMCollector.User_C_function_form.Cfunc_hmt_but : "c file specific for humidity models (use 'help/What's this' for details)", # bouton user C function pour la humidité
                self.syrthesIHMCollector.User_C_function_form.Cfunc_fluid1d_lne : "c file specific for 1D fluid flow (use 'help/What's this' for details)",# bouton user C function pour cfd1d
                self.syrthesIHMCollector.User_C_function_form.Cfunc_fluid1d_but : "c file specific for 1D fluid flow (use 'help/What's this' for details)",# bouton user C function pour cfd1d
                self.syrthesIHMCollector.User_C_function_form.Cfunc_other_lne : "c files specific to users (use 'help/What's this' for details)", # mode avancé
                self.syrthesIHMCollector.User_C_function_form.Cfunc_other_but : "c files specific to users (use 'help/What's this' for details)", # mode avancé
                self.syrthesIHMCollector.User_C_function_form.Cfunc_test_compile : "Test compilation of user files (use 'help/What's this' for details)" ,
                self.syrthesIHMCollector.Advanced_mode_form.Advanced_cmd_table : "Advanced user mode to define SYRTHES keywords and values (use 'help/What's this' for details)",# mode avancé
                # Output
                self.syrthesIHMCollector.Output_2D_form.Tf_cb_2D_Op : "Choice for the 2D field results (use 'help/What's this' for details)", 
                self.syrthesIHMCollector.Output_2D_form.Cb_2D_Op : "Intermediate results : Time step number (integer) or instant frequency (seconds) or list of instants (seconds) - use 'help/What's this' for details", 
                self.syrthesIHMCollector.Output_2D_form.Cb2_2D_Op : "Output : Time step number (integer) or instant frequency (seconds) or list of instants (seconds) - use 'help/What's this' for details",
                self.syrthesIHMCollector.Output_2D_form.Le_2D_Op : "Intermediate results : Time step number (integer) or instant frequency (seconds) or list of instants (seconds) - use 'help/What's this' for details",
                self.syrthesIHMCollector.Output_2D_form.Le2_2D_Op : "Output : Time step number (integer) or instant frequency (seconds) or list of instants (seconds) - use 'help/What's this' for details",
                self.syrthesIHMCollector.Output_2D_form.Op_Dc_2D_table : "Table to specify the location of probes (in m)- use 'help/What's this' for details", 
                self.syrthesIHMCollector.Output_2D_form.Op_Sb_2D_table : "Heat Balance through some surfaces identified by references - use 'help/What's this' for details)", 
                self.syrthesIHMCollector.Output_2D_form.Op_Vb_2D_table : "Volume Heat Balance in domains identified by element references (use 'help/What's this' for details)",
                self.syrthesIHMCollector.Output_3D_form.Tf_cb_3D_Op : "Activation of the results field option (use 'help/What's this' for details)", 
                self.syrthesIHMCollector.Output_3D_form.Cb_3D_Op : "Intermediate results : Choice for the 3D field results - use 'help/What's this' for details", 
                self.syrthesIHMCollector.Output_3D_form.Cb2_3D_Op : "Output : Choice for the 3D field results - use 'help/What's this' for details",
                self.syrthesIHMCollector.Output_3D_form.Le_3D_Op : "Intermediate results : Time step number (integer) or instant frequency (seconds) or list of instants (seconds)- use 'help/What's this' for details", 
                self.syrthesIHMCollector.Output_3D_form.Le2_3D_Op : "Output : Time step number (integer) or instant frequency (seconds) or list of instants (seconds) - use 'help/What's this' for details",
                self.syrthesIHMCollector.Output_3D_form.Op_Dc_3D_table : "Table to specify the location of probes (in m) - use 'help/What's this' for details", 
                self.syrthesIHMCollector.Output_3D_form.Op_Sb_3D_table : "Heat Balance through some surfaces identified by surface references (use 'help/What's this' for details)", 
                self.syrthesIHMCollector.Output_3D_form.Op_Vb_3D_table : "Volume Heat Balance in domains identified by element references (use 'help/What's this' for details)",
                self.syrthesIHMCollector.Output_3D_form.Hf_cb_3D_Op : "Choice for heat flux field results",
                self.syrthesIHMCollector.Output_3D_form.Mt_cb_3D_Op : "Choice for maximal temperature field results",
                self.syrthesIHMCollector.Output_3D_form.F_cb_3D_Op : "Choice for 3D fields results",
                self.syrthesIHMCollector.Output_2D_form.Hf_cb_2D_Op : "Choice for heat flux field results",
                self.syrthesIHMCollector.Output_2D_form.Mt_cb_2D_Op : "Choice for maximal temperature field results",
                self.syrthesIHMCollector.Output_2D_form.F_cb_2D_Op : "Choice for 3D fields results",
                self.syrthesIHMCollector.Home_form.Ho_Ds_te : "User description of the case (use 'help/What's this' for details)",
                self.syrthesIHMCollector.Solar_aspect_form.Sa_chb : "Activate solar models (use 'help/What's this' for details)",
                #self.verticalLayout : "verticalLayout",
                #self.btnRemove_tab2 : "btnRemove_tab2",
                #self.btnRemove_tab3 : "btnRemove_tab3",
                #self.btnRemove_tab4 : "btnRemove_tab4",
                #self.cbStyle_tab1 : "cbStyle_tab1",
                #self.cbStyle_tab2 : "cbStyle_tab2",
                #self.cbStyle_tab3 : "cbStyle_tab3",
                #self.cbStyle_tab4 : "cbStyle_tab4",
                #self.Cb_Top2 : "Cb_Top2", 
                #self.Cb_Top3 : "Cb_Top3",
                #self.Cb_Top4 : "Cb_Top4"

                #fluid1d
                self.syrthesIHMCollector.Initial_conditions_fluid1d_form.Init_TV_table : "1D fluid Initial temperature and velocity (use 'help/What's this' for details)", #tableau des temperature et vitesse initiales
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Heat_ex_table : "1D fluid Heat exchange table (use 'help/What's this' for details)", #tableau de l'échange de chaleur
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Flux_cond_table : "1D fluid Flux table (use 'help/What's this' for details)", #tableau de la condition du flux
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Inlet_3D_table : "1D fluid Inlet 3D table (use 'help/What's this' for details)", #tableau Inlet 3D
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_cl : "1D fluid Closed loop table (use 'help/What's this' for details)",
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_th_cl : "1D fluid Closed loop table (use 'help/What's this' for details)",
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_table_DP : "1D fluid Delta Pressure table (use 'help/What's this' for details)",
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Coupling_table : "Solid faces couples with 1D fluid table (set -1 to select all references)",
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_1df_lne : "References of 1D fluid faces coupled with conduction (use 'help/What's this' for details)",#champ d'édition du fluide 1D des faces
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_1dfUc_lne : "User comments allowing to mention detailed information (text only)",#champ d'édition User comments du fluide 1D des faces
                self.syrthesIHMCollector.Physical_prop_fluid1d_form.Prop_fluid1d_table : "1D fluid properties table (use 'help/What's this' for details)", # tableau des proprietes du fluide
                self.syrthesIHMCollector.Geometrie_fluid1d_form.Geom_fluid1d_table : "1D fluid geometrie table (use 'help/What's this' for details)", # tableau pour la geometrie pour fluid1d
                self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Source_fluid1d_table : "1D fluid source term table (use 'help/What's this' for details)", # tableau terme source fluid1d
                self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Linear_head_fluid1d_table : "1D fluid linear head losses table (use 'help/What's this' for details)", # tableau perte de charge reguliere fluid1d
                self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Singular_head_fluid1d_table : "1D fluid singular head losses table (use 'help/What's this' for details)", # tableau perte de charge singuliere fluid1d
                self.syrthesIHMCollector.Control_fluid1d_form.Le_const_Ts_fluid1d : "1D fluid time step value (use 'help/What's this' for details)",#champ éditable du pas de temp FLUIDE 1D
                self.syrthesIHMCollector.Control_fluid1d_form.Cb_solid_Ts_fluid1d : "Set 1D fluid time step value to solid time step", #checkbox pour indiquer que le pas de temps FLUIDE 1D est egal au pas de temps solide
                self.syrthesIHMCollector.Filename_form.Fn_fluid1d_lne : "1D fluid mesh file",
                self.syrthesIHMCollector.Filename_form.Fn_fluid1d_but : "Activate file selection window",
                self.syrthesIHMCollector.Physical_prop_fluid1d_form.Le_Gravity_x : "Gravity X",
                self.syrthesIHMCollector.Physical_prop_fluid1d_form.Le_Gravity_y : "Gravity Y",
                self.syrthesIHMCollector.Physical_prop_fluid1d_form.Le_Gravity_z : "Gravity Z",
                self.syrthesIHMCollector.Boundary_conditions_fluid0d_form.Heat_ex_fluid0d_table : "0D fluid Boundary Conditions(use 'help/What's this' for details)",
                self.syrthesIHMCollector.Physical_properties_fluid0d_form.Prop_fluid0d_table : "0D fluid Physical properties (use 'help/What's this' for details)",
                self.syrthesIHMCollector.Geometrie_fluid0d_form.Geom_fluid0d_table : "0D fluid geometrie table (use 'help/What's this' for details)", # tableau pour la geometrie pour fluid0d
                self.syrthesIHMCollector.Volumetric_conditions_fluid0d_form.Volumetric_conditions_fluid0d_table : "0D volumetric conditions (use 'help/What's this' for details)"
               } 

    for wid in self.widget:
         wid.setStatusTip(QtCore.QCoreApplication.translate("MainWindow", self.dic_status[wid]))
    pass

#!/bin/bash
#
#------------------------------------------------------
# Erase directories " build ", files *.o 
# files Makefile.in, resume, setup_ini, sythes.profile 
# Erase installation of librairise hdf, med, openmpi, metis 
# of the directory extern-librairies/src and extern-librairies/opt
# Util just for new install or dev
#------------------------------------------------------
rm -f config_env.pyc config_makeFileIN.pyc
rm -f resume
rm -f Makefile.in 
rm -f ../Makefile.in
rm -f ../Makefile2.in
rm -f *.log ../*.log
rm -f `find .. -name *.o`
rm -fr `find .. -name build`
#
rm -rf ../../arch
#
rm -fr ../../extern-libraries/src/TMP
rm -fr ../../extern-libraries/opt

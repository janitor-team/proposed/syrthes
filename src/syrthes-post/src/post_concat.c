/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>

# include "post_usertype.h"
# include "post_bd.h"
# include "post_proto.h"
# include "post_abs.h"



/*|======================================================================|
  | SYRTHES PARALLELE                                           MAI 2008 |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | Creation des divers tables locales et de transferts pour la          |
  | resolution en parallele                                              |
  |                                                                      |
  |======================================================================|*/
rp_int post_cherche_min(rp_int npoin,struct SDparall sdparall)
{
  rp_int i;
  rp_int nump_min;

  /* recherche du numero max du noeud global */
  nump_min=1000000000;
  for (i=0;i<npoin;i++)
    nump_min=min(sdparall.npglob[i],nump_min);

  return nump_min;
}
  
/*|======================================================================|
  | SYRTHES PARALLELE                                           MAI 2008 |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | Creation des divers tables locales et de transferts pour la          |
  | resolution en parallele                                              |
  |                                                                      |
  |======================================================================|*/
rp_int post_cherche_max(rp_int npoin,struct SDparall sdparall)
{
  rp_int i;
  rp_int nump_max;

  /* recherche du numero max du noeud global */
  nump_max=0;
  for (i=0;i<npoin;i++)
    nump_max=max(sdparall.npglob[i],nump_max);

  return nump_max;
}
  
/*|======================================================================|
  | SYRTHES PARALLELE                                           MAI 2008 |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | Allocation des tableaux                                              |
  |                                                                      |
  |                                                                      |
  |======================================================================|*/
void post_alloue_tab(rp_int geom,rp_int resu,rp_int n,rp_int nump_max,
		     struct Maillage maillnodes,struct Maillage maillnodebord,
		     struct Maillage *toutnodes,struct Maillage *toutnodebord)
{
  rp_int i,nb;

  if (n==0) /* initialisations lors de la 1ere partition et allocation*/
    {
      toutnodes->ndim   = maillnodes.ndim;
      toutnodes->ndiele = maillnodes.ndiele;
      toutnodes->nelem  = maillnodes.nelem;
      toutnodes->npoin  = nump_max;
      toutnodes->ndmat  = maillnodes.ndmat;

      toutnodes->node   = (rp_int**)malloc(toutnodes->ndmat*sizeof(rp_int*));
      for (i=0;i<toutnodes->ndmat;i++) toutnodes->node[i]=(rp_int*)malloc(toutnodes->nelem*sizeof(rp_int));
      toutnodes->nref   = (rp_int*)malloc(nump_max*sizeof(rp_int));
      toutnodes->nrefe  = (rp_int*)malloc(toutnodes->nelem*sizeof(rp_int));
      toutnodes->coord  = (double**)malloc(toutnodes->ndim*sizeof(double*));
      for (i=0;i<toutnodes->ndim;i++) toutnodes->coord[i]=(double*)malloc(nump_max*sizeof(double));
      
      
      toutnodebord->ndim   = maillnodebord.ndim;
      toutnodebord->ndiele = maillnodebord.ndiele;
      toutnodebord->nelem  = maillnodebord.nelem;
      toutnodebord->ndmat  = maillnodebord.ndmat;

      /* au cas ou il n'y a pas d'elt de bord dans la premiere part */
      nb=1; if (maillnodebord.nelem>0) nb=maillnodebord.nelem;

      toutnodebord->node   = (rp_int**)malloc(toutnodebord->ndmat*sizeof(rp_int*));
      for (i=0;i<toutnodebord->ndmat;i++) toutnodebord->node[i]=(rp_int*)malloc(nb*sizeof(rp_int));
      toutnodebord->nrefe  = (rp_int*)malloc(nb*sizeof(rp_int));


    }
  else /* ajustement des tailles pour les partitions suivantes */
    {

      toutnodebord->nelem += maillnodebord.nelem;
      toutnodes->nelem += maillnodes.nelem;

      if (maillnodebord.nelem>0)
	toutnodebord->nrefe=(rp_int*)realloc(toutnodebord->nrefe,toutnodebord->nelem*sizeof(rp_int));
      
      toutnodes->nrefe=(rp_int*)realloc(toutnodes->nrefe,toutnodes->nelem*sizeof(rp_int));
      
      if (maillnodebord.nelem>0){
	for (i=0;i<toutnodebord->ndmat;i++) 
	  toutnodebord->node[i]=(rp_int*)realloc(toutnodebord->node[i],toutnodebord->nelem*sizeof(rp_int));
      }

      for (i=0;i<toutnodes->ndmat;i++) 
	toutnodes->node[i]= (rp_int*)realloc(toutnodes->node[i],toutnodes->nelem*sizeof(rp_int));
      
      if (nump_max>toutnodes->npoin)
	{
	  toutnodes->nref=(rp_int*)realloc(toutnodes->nref,nump_max*sizeof(rp_int));
	  for (i=0;i<toutnodes->ndim;i++) toutnodes->coord[i]=(double*)realloc(toutnodes->coord[i],nump_max*sizeof(double));
	}
      
      if (nump_max>toutnodes->npoin) toutnodes->npoin =nump_max;
      
    }
}
  
/*|======================================================================|
  | SYRTHES PARALLELE                                           MAI 2008 |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | Liberation des tableaux                                              |
  |                                                                      |
  |                                                                      |
  |======================================================================|*/
void post_libere_tabmaill(struct Maillage *maillnodes,
			  struct Maillage *maillnodebord)
{
  rp_int i;

  for (i=0;i<maillnodes->ndmat;i++) free(maillnodes->node[i]);
  free(maillnodes->node);
  
  free(maillnodes->nref);
  free(maillnodes->nrefe);
  
for (i=0;i<maillnodes->ndim;i++) free(maillnodes->coord[i]);
  free(maillnodes->coord);
  
  if (maillnodebord->nelem>0){
    for (i=0;i<maillnodebord->ndmat;i++) free(maillnodebord->node[i]);
    free(maillnodebord->node);
    free(maillnodebord->nrefe);
  }

}
  

/*|======================================================================|
  | SYRTHES PARALLELE                                           MAI 2008 |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | lecture d'un fichier resultat                                        |
  |                                                                      |
  |                                                                      |
  |======================================================================|*/
void post_lire_entete_dt(FILE *fresu,
			 rp_int *ntsyr,double *rdtts,double *tempss,
			 rp_int nbligne)
{
  rp_int i;
  char ch[200],chrien[100];

  /* lecture de l'entete (2 ou 3 lignes suivant que la premiere ligne a deja ete lue ou non */
  for (i=0;i<nbligne;i++) 
    fgets(ch,200,fresu);

  fscanf(fresu,"%s%d%s%lf%s%lf\n",chrien,ntsyr, chrien,tempss, chrien,rdtts);
  fgets(ch,200,fresu);

}

/*|======================================================================|
  | SYRTHES PARALLELE                                           MAI 2008 |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | lecture d'un fichier resultat                                        |
  |                                                                      |
  |                                                                      |
  |======================================================================|*/
int post_lire_entete_var(FILE *fresu,char *nomvar,rp_int *typlu)
{
  rp_int i,nbplu;
  char ch[200],chrien[100];

  if (feof(fresu))
    return 0;
  else{
    fgets(ch,200,fresu);
    if (!strncmp(ch,"***VAR=",7)){
      sscanf(ch,"%s%s%s%d%s%d\n",chrien,nomvar, chrien,typlu, chrien,&nbplu);
      return 1;
    }
    else
      return 0;
  }

}
/*|======================================================================|
  | SYRTHES PARALLELE                                           MAI 2008 |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | lecture d'un fichier resultat                                        |
  |                                                                      |
  |                                                                      |
  |======================================================================|*/
void post_lire_resu(FILE *fresu,rp_int npoin,double *tmps,
		    rp_int *ntsyr,double *rdtts,double *tempss)
{
  rp_int i,nv;
  char ch[200],chrien[100];
  rp_int typlu,nbplu;

  /* lecture de l'entete (2 ou 3 lignes suivant que la premiere ligne a deja ete lue ou non */
  for (i=0;i<3;i++) 
    fgets(ch,200,fresu);

  fscanf(fresu,"%s%d%s%lf%s%lf\n",chrien,ntsyr, chrien,tempss, chrien,rdtts);

  fgets(ch,200,fresu);
  fscanf(fresu,"%s%s%s%d%s%d\n",chrien,chrien, chrien,&typlu, chrien,&nbplu);


  nv=npoin/6;
  for (i=0;i<nv*6;i+=6) fscanf(fresu,"%16lf %16lf %16lf %16lf %16lf %16lf\n",
			       tmps+i,tmps+i+1,tmps+i+2,tmps+i+3,tmps+i+4,tmps+i+5);

  for (i=nv*6;i<npoin;i++) fscanf(fresu,"%16lf ",tmps+i);

  

}
/*|======================================================================|
  | SYRTHES PARALLELE                                           MAI 2008 |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | lecture d'un fichier resultat                                        |
  |                                                                      |
  |                                                                      |
  |======================================================================|*/
void post_lire_var(FILE *fresu,rp_int npoin,double *var)
{
  rp_int i,nv;

  nv=npoin/6;
  for (i=0;i<nv*6;i+=6) fscanf(fresu,"%16lf %16lf %16lf %16lf %16lf %16lf\n",
			       var+i,var+i+1,var+i+2,var+i+3,var+i+4,var+i+5);

  for (i=nv*6;i<npoin;i++) fscanf(fresu,"%16lf ",var+i);
}

/*|======================================================================|
  | SYRTHES PARALLELE                                           MAI 2008 |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | Allocation des tableaux                                              |
  |                                                                      |
  |                                                                      |
  |======================================================================|*/
void post_concat(rp_int geom,rp_int resu,
		 rp_int num_ele_deb,rp_int num_eleb_deb,
		 struct Maillage maillnodes,struct Maillage maillnodebord,
		 struct Maillage toutnodes,struct Maillage toutnodebord,
		 struct SDparall sdparall)
{
  rp_int i,j,ng;

  if (geom)
    {

      for (i=0;i<maillnodes.npoin;i++)
	{
	  ng=sdparall.npglob[i];
	  for (j=0;j<maillnodes.ndim;j++) toutnodes.coord[j][ng]=maillnodes.coord[j][i];
	  toutnodes.nref[ng]=maillnodes.nref[i];
	}
      
      for (i=0;i<maillnodes.nelem;i++)
	{
	  for (j=0;j<maillnodes.ndmat;j++) toutnodes.node[j][num_ele_deb+i]=sdparall.npglob[maillnodes.node[j][i]];
	  toutnodes.nrefe[num_ele_deb+i]=maillnodes.nrefe[i];
	}
      
      if (maillnodebord.nelem>0){
	for (i=0;i<maillnodebord.nelem;i++)
	  {
	    for (j=0;j<maillnodebord.ndmat;j++) toutnodebord.node[j][num_eleb_deb+i]=sdparall.npglob[maillnodebord.node[j][i]];
	    toutnodebord.nrefe[num_eleb_deb+i]=maillnodebord.nrefe[i];
	  }
      }
    }


}
/*|======================================================================|
  | SYRTHES PARALLELE                                           MAI 2008 |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | Traitement d 'une variable sur les noeuds                            |
  |                                                                      |
  |                                                                      |
  |======================================================================|*/
void post_concat_var3(rp_int npoin,double *temp,double *touttemp,struct SDparall sdparall)
{
  rp_int i,ng;

  for (i=0;i<npoin;i++)
    touttemp[sdparall.npglob[i]]=temp[i];
}
/*|======================================================================|
  | SYRTHES PARALLELE                                           MAI 2008 |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | Traitement d 'une variable sur les elements                          |
  |                                                                      |
  |                                                                      |
  |======================================================================|*/
void post_concat_var2(rp_int npoin,double *temp,double *touttemp,struct SDparall sdparall,rp_int num_ele_deb)
{
  rp_int i,ng;

  for (i=0;i<npoin;i++)
    touttemp[num_ele_deb+i]=temp[i];
}

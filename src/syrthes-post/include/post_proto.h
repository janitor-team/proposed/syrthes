#include "post_bd.h"
rp_int post_cherche_max(rp_int,struct SDparall);
rp_int post_cherche_min(rp_int,struct SDparall);
void post_alloue_tab(rp_int,rp_int,rp_int,rp_int,
		     struct Maillage,struct Maillage,struct Maillage*,struct Maillage*);
void post_libere_tabmaill(struct Maillage*, struct Maillage*);

int post_lire_entete_var(FILE *,char *,rp_int *);
void post_lire_entete_dt(FILE*,rp_int*,double*,double*,rp_int);
void post_lire_resu(FILE*,rp_int,double *,rp_int*,double*,double*);
void post_lire_var(FILE*,rp_int,double*);

void post_concat(rp_int,rp_int,rp_int,rp_int,
		 struct Maillage,struct Maillage, struct Maillage,struct Maillage,
		 struct SDparall);
void post_concat_var2(rp_int,double*,double*,struct SDparall,rp_int);
void post_concat_var3(rp_int,double*,double*,struct SDparall);


void lire_syrthes(FILE*,struct Maillage*,struct Maillage*,struct SDparall*);

void ecrire_geom(char*,struct Maillage,struct Maillage);
void ecrire_entete(FILE *,rp_int,double,double);
void ecrire_resu(FILE *,rp_int,double*,char*,rp_int);

void imprime_maillage(struct Maillage);
void verif_alloue_int1d(char*,rp_int*);
void verif_alloue_int2d(rp_int,char*,rp_int**);
void verif_alloue_double1d(char*,double*);
void verif_alloue_double2d(rp_int,char*,double**);
void verif_alloue_char(char*,char*);
void fic_bin_f__endswap(void *,size_t,size_t);

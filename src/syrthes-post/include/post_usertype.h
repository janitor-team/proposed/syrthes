#ifndef _USERTYPE_H
#define _USERTYPE_H


/*  type of langage */
/*  0 = french      */
/*  1 = english     */

#define SYRTHES_LANG 1

#define POST_TYPEWIDTH 32


/* ------------------------------------------- */

#define FR 0
#define EN 1

#if POST_TYPEWIDTH == 32
  typedef int rp_int;
#elif POST_TYPEWIDTH == 64
  typedef long rp_int;
#else
  #error "Incorrect user-supplied value fo POST_TYPEWIDTH"
#endif


#endif

/*   typedef int rp_int; */
/*   typedef int32_t rp_int; */
/*   typedef int64_t rp_int; */

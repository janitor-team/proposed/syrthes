# -*- coding: iso-8859-1 -*-

###
### This file is generated automatically by SALOME v6.5.0 with dump python functionality
###

import sys
import salome

salome.salome_init()
theStudy = salome.myStudy

import salome_notebook
notebook = salome_notebook.notebook

mypath="/home/E02847/ether/syrthes4/syrthes4/src/tests/cas_plates3d_salome/salome"

#sys.path.insert( 0, r'/home/E02847/ether/syrthes4/syrthes4/src/tests/cas_plates3d_salome/salome')
sys.path.insert( 0, r mypath)

###
### GEOM component
###

import GEOM
import geompy
import math
import SALOMEDS


geompy.init_geom(theStudy)

Cone_1 = geompy.MakeConeR1R2H(0.16, 0.21, 0.05)
Cylinder_central = geompy.MakeCylinderRH(0.05, 1.5)
Cylinder_11 = geompy.MakeCylinderRH(0.35, 0.1)
Cylinder_12 = geompy.MakeCylinderRH(0.4, 0.1)
Cylinder_13 = geompy.MakeCylinderRH(0.45, 0.1)
geompy.TranslateDXDYDZ(Cylinder_central, 0, 0, 0.25)
geompy.TranslateDXDYDZ(Cylinder_central, 0, 0, -0.5)
geompy.TranslateDXDYDZ(Cone_1, 0, 0, 0.25)
geompy.TranslateDXDYDZ(Cylinder_11, 0, 0, 0.39)
geompy.TranslateDXDYDZ(Cylinder_12, 0, 0, 0.39)
geompy.TranslateDXDYDZ(Cylinder_13, 0, 0, 0.39)
cyl_2_ext = geompy.MakeCut(Cylinder_13, Cylinder_12)
cyl_2_int = geompy.MakeCut(Cylinder_11, Cylinder_central)
Cylinder_32 = geompy.MakeCylinderRH(0.15, 0.05)
Cylinder_33 = geompy.MakeCylinderRH(0.2, 0.05)
Cylinder_34 = geompy.MakeCylinderRH(0.4, 0.05)
Cyl_3_int = geompy.MakeCut(Cylinder_32, Cylinder_central)
Cyl_3_ext = geompy.MakeCut(Cylinder_34, Cylinder_33)
geompy.TranslateDXDYDZ(Cyl_3_int, 0, 0, 0.4)
geompy.TranslateDXDYDZ(Cyl_3_ext, 0, 0, 0.4)
geompy.TranslateDXDYDZ(Cyl_3_int, 0, 0, 0.14)
geompy.TranslateDXDYDZ(Cyl_3_ext, 0, 0, 0.14)
Cylinder_1 = geompy.MakeCylinderRH(0.25, 0.1)
Cylinder_2 = geompy.MakeCylinderRH(0.3, 0.1)
Cylinder_3 = geompy.MakeCylinderRH(0.45, 0.1)
Cyl_4_int = geompy.MakeCut(Cylinder_1, Cylinder_central)
Cyl_4_ext = geompy.MakeCut(Cylinder_3, Cylinder_2)
geompy.TranslateDXDYDZ(Cyl_4_int, 0, 0, 0.66)
geompy.TranslateDXDYDZ(Cyl_4_ext, 0, 0, 0.66)
Cylinder_4 = geompy.MakeCylinderRH(0.15, 0.2)
geompy.TranslateDXDYDZ(Cylinder_4, 0, 0, -0.1)
Cone_2 = geompy.MakeConeR1R2H(0.15, 0.55, 0.2)
Cone_3 = geompy.MakeConeR1R2H(0.05, 0.5, 0.2)
geompy.TranslateDXDYDZ(Cone_2, 0, 0, 0.1)
geompy.TranslateDXDYDZ(Cone_3, 0, 0, 0.1)
Cylinder_petitcentre = geompy.MakeCylinderRH(0.05, 0.3)
geompy.TranslateDXDYDZ(Cylinder_4, 0, 0, -0.05)
geompy.TranslateDXDYDZ(Cylinder_4, 0, 0, 0.05)
geompy.TranslateDXDYDZ(Cylinder_petitcentre, 0, 0, -0.05)
Cut_1_cyl_base = geompy.MakeCut(Cylinder_4, Cylinder_petitcentre)
Cut_cone_inf = geompy.MakeCut(Cone_2, Cone_3)
Cylinder_5 = geompy.MakeCylinderRH(0.55, 0.65)
Cylinder_6 = geompy.MakeCylinderRH(0.5, 0.65)
Cut_paroi_cyl = geompy.MakeCut(Cylinder_5, Cylinder_6)
geompy.TranslateDXDYDZ(Cut_paroi_cyl, 0, 0, 0.3)
Cylinder_dessus = geompy.MakeCylinderRH(0.55, 0.05)
geompy.TranslateDXDYDZ(Cylinder_dessus, 0, 0, 0.9)
geompy.TranslateDXDYDZ(Cylinder_dessus, 0, 0, 0.9)
geompy.TranslateDXDYDZ(Cylinder_dessus, 0, 0, -0.9)
Cylinder_8 = geompy.MakeCylinderRH(0.15, 0.25)
cyl_sup = geompy.MakeTranslation(Cylinder_8, 0, 0, 0.95)
Cylinder_9 = geompy.MakeCylinderRH(0.05, 0.25)
geompy.TranslateDXDYDZ(Cylinder_9, 0, 0, 0.9)
Partition_tour = geompy.MakePartition([Cut_1_cyl_base, Cut_cone_inf, Cut_paroi_cyl, Cylinder_dessus, cyl_sup], [], [], [], geompy.ShapeType["SOLID"], 0, [], 0)
geomObj_1 = geompy.MakeCut(Partition_tour, Cylinder_9)
O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)
Box_1 = geompy.MakeBoxDXDYDZ(2, 2, 2)
geompy.TranslateDXDYDZ(Box_1, -1, -2, -0.5)
Compound_four_plaques = geompy.MakeCompound([Cone_1, cyl_2_ext, cyl_2_int, Cyl_3_int, Cyl_3_ext, Cyl_4_int, Cyl_4_ext, geomObj_1])
demi_four_plaques = geompy.MakeCut(Compound_four_plaques, Box_1)
geompy.TranslateDXDYDZ(Cylinder_petitcentre, 0, 0, -0.1)
Partition_1 = geompy.MakePartition([demi_four_plaques], [Cylinder_petitcentre], [], [], geompy.ShapeType["SOLID"], 0, [], 0)
geompy.TranslateDXDYDZ(Cylinder_petitcentre, 0, 0, 0.9)
geompy.TranslateDXDYDZ(Cylinder_petitcentre, 0, 0, 0.2)
Partition_2 = geompy.MakePartition([Partition_1], [Cylinder_petitcentre], [], [], geompy.ShapeType["SOLID"], 0, [], 0)
Cylinder_7 = geompy.MakeCylinderRH(0.15, 0.06)
geompy.TranslateDXDYDZ(Cylinder_7, 0, 0, 0.9)
geompy.TranslateDXDYDZ(Cylinder_7, 0, 0, -0.01)
Partition_3 = geompy.MakePartition([Partition_2], [Cylinder_7], [], [], geompy.ShapeType["SOLID"], 0, [], 0)
[geomObj_2,geomObj_3,geomObj_4,geomObj_5,geomObj_6,geomObj_7,geomObj_8,geomObj_9,geomObj_10,geomObj_11,geomObj_12,geomObj_13,geomObj_14,geomObj_15,geomObj_16,geomObj_17] = geompy.ExtractShapes(Partition_3, geompy.ShapeType["SOLID"], True)
[geomObj_18,geomObj_19,geomObj_20,geomObj_21,geomObj_22,geomObj_23,geomObj_24,geomObj_25,geomObj_26] = geompy.SubShapes(Partition_3, [275, 423, 299, 387, 351, 375, 434, 323, 265])
geomObj_27 = geompy.GetSubShape(Partition_3, [428])
geomObj_28 = geompy.GetSubShape(Partition_3, [397])
geomObj_29 = geompy.GetSubShape(Partition_3, [373])
geomObj_30 = geompy.GetSubShape(Partition_3, [349])
geomObj_31 = geompy.GetSubShape(Partition_3, [325])
geomObj_32 = geompy.GetSubShape(Partition_3, [301])
geomObj_33 = geompy.GetSubShape(Partition_3, [277])
geomObj_34 = geompy.GetSubShape(Partition_3, [2])
disques = geompy.CreateGroup(Partition_3, geompy.ShapeType["SOLID"])
geompy.UnionIDs(disques, [56, 158, 2, 22, 90, 192, 124])
interne = geompy.CreateGroup(Partition_3, geompy.ShapeType["FACE"])
geompy.UnionIDs(interne, [323, 275, 299, 387, 375, 265, 351, 423, 434])
ss_couvercle = geompy.CreateGroup(Partition_3, geompy.ShapeType["FACE"])
geompy.UnionIDs(ss_couvercle, [375, 351])
bords_d4 = geompy.CreateGroup(Partition_3, geompy.ShapeType["FACE"])
geompy.UnionIDs(bords_d4, [211, 187, 216, 182, 177, 204, 221, 170])
geompy.DifferenceIDs(bords_d4, [211, 187, 216, 182, 177, 204, 221, 170])
geompy.UnionIDs(bords_d4, [211, 187, 216, 182, 177, 204, 221, 170, 190, 224, 160, 194])
geompy.DifferenceIDs(bords_d4, [211, 187, 216, 182, 177, 204, 221, 170, 190, 224, 160, 194])
geompy.UnionIDs(bords_d4, [216, 182, 204, 170, 190, 224, 160, 194])
bord_d2 = geompy.CreateGroup(Partition_3, geompy.ShapeType["FACE"])
geompy.UnionIDs(bord_d2, [75, 41, 46, 88, 85, 68, 34, 51])
geompy.DifferenceIDs(bord_d2, [75, 41, 46, 88, 85, 68, 34, 51])
geompy.UnionIDs(bord_d2, [75, 41, 46, 88, 85, 68, 34, 51, 58, 24, 54])
geompy.DifferenceIDs(bord_d2, [75, 41, 46, 88, 85, 68, 34, 51, 58, 24, 54])
geompy.UnionIDs(bord_d2, [46, 88, 68, 34, 58, 24, 54])
geompy.DifferenceIDs(bord_d2, [46, 88, 68, 34, 58, 24, 54])
geompy.UnionIDs(bord_d2, [46, 88, 68, 34, 58, 24, 54, 80])
bord_d3 = geompy.CreateGroup(Partition_3, geompy.ShapeType["FACE"])
geompy.UnionIDs(bord_d3, [119, 122, 114, 109, 148, 143, 153, 102, 136, 156, 126])
geompy.DifferenceIDs(bord_d3, [119, 122, 114, 109, 148, 143, 153, 102, 136, 156, 126])
geompy.UnionIDs(bord_d3, [119, 122, 114, 109, 148, 143, 153, 102, 136, 156, 126, 92])
geompy.DifferenceIDs(bord_d3, [119, 122, 114, 109, 148, 143, 153, 102, 136, 156, 126, 92])
geompy.UnionIDs(bord_d3, [122, 114, 148, 102, 136, 156, 126, 92])
bord_d1 = geompy.CreateGroup(Partition_3, geompy.ShapeType["FACE"])
geompy.UnionIDs(bord_d1, [17, 20, 14, 4])
geompy.DifferenceIDs(bord_d1, [17, 20, 14, 4])
geompy.UnionIDs(bord_d1, [20, 14, 4])
four = geompy.CreateGroup(Partition_3, geompy.ShapeType["SOLID"])
geompy.UnionIDs(four, [226, 373, 267, 397, 325, 301, 277, 349])
geompy.DifferenceIDs(four, [226, 373, 267, 397, 325, 301, 277, 349])
geompy.UnionIDs(four, [226, 373, 267, 397, 325, 301, 277, 349, 428])
geomObj_35 = geompy.MakeCompound([interne, bords_d4, bord_d2, bord_d3, bord_d1])
geomObj_36 = geompy.GetSubShape(geomObj_35, [58])
geomObj_37 = geompy.GetSubShape(geomObj_35, [13])
Compound_surf_ray = geompy.MakeCompound([interne, bords_d4, bord_d2, bord_d3, bord_d1])
listSubShapeIDs = geompy.SubShapeAllIDs(Compound_surf_ray, geompy.ShapeType["SHAPE"])
listSubShapeIDs = geompy.SubShapeAllIDs(Compound_surf_ray, geompy.ShapeType["FACE"])
geomObj_38 = geompy.GetSubShape(Compound_surf_ray, [58])
geomObj_39 = geompy.GetSubShape(Compound_surf_ray, [13])
geomObj_40 = geompy.GetSubShape(Partition_3, [56])
geomObj_41 = geompy.GetSubShape(Partition_3, [323])
geomObj_42 = geompy.GetSubShape(Partition_3, [375])
geomObj_43 = geompy.GetSubShape(Partition_3, [216])
geomObj_44 = geompy.GetSubShape(Partition_3, [46])
geomObj_45 = geompy.GetSubShape(Partition_3, [122])
geomObj_46 = geompy.GetSubShape(Partition_3, [20])
geomObj_47 = geompy.GetSubShape(Partition_3, [226])
dessous = geompy.CreateGroup(Partition_3, geompy.ShapeType["FACE"])
geompy.UnionIDs(dessous, [238, 269])
geomObj_48 = geompy.GetSubShape(Partition_3, [238])
geomObj_49 = geompy.GetSubShape(Compound_surf_ray, [2])
listSubShapeIDs = geompy.SubShapeAllIDs(geomObj_49, geompy.ShapeType["SHAPE"])
geomObj_50 = geompy.GetSubShape(geomObj_49, [2])
dessus = geompy.CreateGroup(Partition_3, geompy.ShapeType["FACE"])
geompy.UnionIDs(dessus, [363])
couronne = geompy.CreateGroup(Partition_3, geompy.ShapeType["FACE"])
geompy.UnionIDs(couronne, [339])
Disk1 = geompy.CreateGroup(Partition_3, geompy.ShapeType["SOLID"])
geompy.UnionIDs(Disk1, [2])
Disk2_int = geompy.CreateGroup(Partition_3, geompy.ShapeType["SOLID"])
geompy.UnionIDs(Disk2_int, [56])
Disk2_ext = geompy.CreateGroup(Partition_3, geompy.ShapeType["SOLID"])
geompy.UnionIDs(Disk2_ext, [22])
Disk3_int = geompy.CreateGroup(Partition_3, geompy.ShapeType["SOLID"])
geompy.UnionIDs(Disk3_int, [90])
Disk3_ext = geompy.CreateGroup(Partition_3, geompy.ShapeType["SOLID"])
geompy.UnionIDs(Disk3_ext, [124])
Disk4_int = geompy.CreateGroup(Partition_3, geompy.ShapeType["SOLID"])
geompy.UnionIDs(Disk4_int, [158])
Disk4_ext = geompy.CreateGroup(Partition_3, geompy.ShapeType["SOLID"])
geompy.UnionIDs(Disk4_ext, [192])
geomObj_51 = geompy.GetSubShape(Partition_3, [226])
steel_copper = geompy.CreateGroup(Compound_surf_ray, geompy.ShapeType["FACE"])
geompy.UnionIDs(steel_copper, [3, 13, 19, 26, 36, 43, 47, 51, 58, 62, 82, 106, 114, 119, 146, 163, 167, 193, 210, 220, 224])
granite = geompy.CreateGroup(Compound_surf_ray, geompy.ShapeType["FACE"])
geompy.UnionIDs(granite, [110, 129, 139, 156, 171, 176, 186, 203, 228, 233, 239, 245, 72, 92])
geomObj_52 = geompy.GetSubShape(Compound_surf_ray, [3])
geomObj_53 = geompy.GetSubShape(Compound_surf_ray, [110])
Disk1.SetColor(SALOMEDS.Color(0,1,0))
Disk2_int.SetColor(SALOMEDS.Color(0.666667,0.333333,0))
Disk2_ext.SetColor(SALOMEDS.Color(0.968627,0.192157,0.0901961))
Disk3_int.SetColor(SALOMEDS.Color(1,1,0))
Disk3_ext.SetColor(SALOMEDS.Color(1,0.666667,0))
Disk4_int.SetColor(SALOMEDS.Color(0.666667,0,1))
Disk4_ext.SetColor(SALOMEDS.Color(0.709804,0.172549,0.458824))
four.SetColor(SALOMEDS.Color(0,0.666667,1))
geompy.addToStudy( Cone_1, 'Cone_1' )
geompy.addToStudy( Cylinder_central, 'Cylinder_central' )
geompy.addToStudy( Cylinder_11, 'Cylinder_11' )
geompy.addToStudy( Cylinder_12, 'Cylinder_12' )
geompy.addToStudy( Cylinder_13, 'Cylinder_13' )
geompy.addToStudy( cyl_2_ext, 'cyl_2_ext' )
geompy.addToStudy( cyl_2_int, 'cyl_2_int' )
geompy.addToStudy( Cylinder_32, 'Cylinder_32' )
geompy.addToStudy( Cylinder_33, 'Cylinder_33' )
geompy.addToStudy( Cylinder_34, 'Cylinder_34' )
geompy.addToStudy( Cyl_3_int, 'Cyl_3_int' )
geompy.addToStudy( Cyl_3_ext, 'Cyl_3_ext' )
geompy.addToStudy( Cylinder_1, 'Cylinder_1' )
geompy.addToStudy( Cylinder_2, 'Cylinder_2' )
geompy.addToStudy( Cylinder_3, 'Cylinder_3' )
geompy.addToStudy( Cyl_4_int, 'Cyl_4_int' )
geompy.addToStudy( Cyl_4_ext, 'Cyl_4_ext' )
geompy.addToStudy( Cylinder_4, 'Cylinder_4' )
geompy.addToStudy( Cone_2, 'Cone_2' )
geompy.addToStudy( Cone_3, 'Cone_3' )
geompy.addToStudy( Cylinder_petitcentre, 'Cylinder-petitcentre' )
geompy.addToStudy( Cut_1_cyl_base, 'Cut_1-cyl-base' )
geompy.addToStudy( Cut_cone_inf, 'Cut_cone_inf' )
geompy.addToStudy( Cylinder_5, 'Cylinder_5' )
geompy.addToStudy( Cylinder_6, 'Cylinder_6' )
geompy.addToStudy( Cut_paroi_cyl, 'Cut-paroi-cyl' )
geompy.addToStudy( Cylinder_dessus, 'Cylinder_dessus' )
geompy.addToStudy( Cylinder_8, 'Cylinder_8' )
geompy.addToStudy( cyl_sup, 'cyl-sup' )
geompy.addToStudy( Cylinder_9, 'Cylinder_9' )
geompy.addToStudy( Partition_tour, 'Partition_tour' )
geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( Box_1, 'Box_1' )
geompy.addToStudy( Compound_four_plaques, 'Compound_four_plaques' )
geompy.addToStudy( demi_four_plaques, 'demi-four-plaques' )
geompy.addToStudy( Partition_1, 'Partition_1' )
geompy.addToStudy( Partition_2, 'Partition_2' )
geompy.addToStudy( Cylinder_7, 'Cylinder_7' )
geompy.addToStudy( Partition_3, 'Partition_3' )
geompy.addToStudyInFather( Partition_3, disques, 'disques' )
geompy.addToStudyInFather( Partition_3, interne, 'interne' )
geompy.addToStudyInFather( Partition_3, ss_couvercle, 'ss_couvercle' )
geompy.addToStudyInFather( Partition_3, bords_d4, 'bords-d4' )
geompy.addToStudyInFather( Partition_3, bord_d2, 'bord-d2' )
geompy.addToStudyInFather( Partition_3, bord_d3, 'bord-d3' )
geompy.addToStudyInFather( Partition_3, bord_d1, 'bord-d1' )
geompy.addToStudy( four, 'four' )
geompy.addToStudy( Compound_surf_ray, 'Compound_surf-ray' )
geompy.addToStudyInFather( Partition_3, dessous, 'dessous' )
geompy.addToStudyInFather( Partition_3, dessus, 'dessus' )
geompy.addToStudyInFather( Partition_3, couronne, 'couronne' )
geompy.addToStudyInFather( Partition_3, Disk1, 'Disk1' )
geompy.addToStudyInFather( Partition_3, Disk2_int, 'Disk2-int' )
geompy.addToStudyInFather( Partition_3, Disk2_ext, 'Disk2-ext' )
geompy.addToStudyInFather( Partition_3, Disk3_int, 'Disk3-int' )
geompy.addToStudyInFather( Partition_3, Disk3_ext, 'Disk3-ext' )
geompy.addToStudyInFather( Partition_3, Disk4_int, 'Disk4-int' )
geompy.addToStudyInFather( Partition_3, Disk4_ext, 'Disk4-ext' )
geompy.addToStudyInFather( Compound_surf_ray, steel_copper, 'steel-copper' )
geompy.addToStudyInFather( Compound_surf_ray, granite, 'granite' )

###
### SMESH component
###

import smesh, SMESH, SALOMEDS

smesh.SetCurrentStudy(theStudy)
import BLSURFPlugin
import GHS3DPlugin
import StdMeshers
import NETGENPlugin
Mesh_1 = smesh.Mesh(Partition_3)
BLSURF_1 = Mesh_1.Triangle(algo=smesh.BLSURF)
BLSURF_Parameters = BLSURF_1.Parameters()
GHS3D_3D_2 = Mesh_1.Tetrahedron(algo=smesh.GHS3D)
Mesh_2 = smesh.Mesh(Compound_surf_ray)
NETGEN_2D_ONLY_3 = Mesh_2.Triangle(algo=smesh.NETGEN_2D)
NETGEN_Parameters_2D_ONLY = NETGEN_2D_ONLY_3.Parameters()
NETGEN_Parameters_2D_ONLY.SetOptimize( 1 )
NETGEN_Parameters_2D_ONLY.SetFineness( 2 )
NETGEN_Parameters_2D_ONLY.SetMinSize( 0.0335893 )
NETGEN_Parameters_2D_ONLY.SetQuadAllowed( 0 )
Regular_1D_4 = Mesh_2.Segment()
LocalLength_0_1_1e_07 = Regular_1D_4.LocalLength(0.08,[],1e-07)
interne_1 = Mesh_1.GroupOnGeom(interne,'interne',SMESH.FACE)
ss_couvercle_1 = Mesh_1.GroupOnGeom(ss_couvercle,'ss_couvercle',SMESH.FACE)
bords_d4_1 = Mesh_1.GroupOnGeom(bords_d4,'bords-d4',SMESH.FACE)
bord_d2_1 = Mesh_1.GroupOnGeom(bord_d2,'bord-d2',SMESH.FACE)
bord_d3_1 = Mesh_1.GroupOnGeom(bord_d3,'bord-d3',SMESH.FACE)
bord_d1_1 = Mesh_1.GroupOnGeom(bord_d1,'bord-d1',SMESH.FACE)
dessous_1 = Mesh_1.GroupOnGeom(dessous,'dessous',SMESH.FACE)
dessus_1 = Mesh_1.GroupOnGeom(dessus,'dessus',SMESH.FACE)
couronne_1 = Mesh_1.GroupOnGeom(couronne,'couronne',SMESH.FACE)
dessus_1.SetColor( SALOMEDS.Color( 0, 0.666667, 1 ))
couronne_1.SetColor( SALOMEDS.Color( 0, 0.666667, 1 ))
BLSURF_Parameters.SetPhySize( 0.02 )
isDone = Mesh_1.Compute()
NETGEN_Parameters_2D_ONLY.SetMaxSize( 0.04 )
NETGEN_Parameters_2D_ONLY.SetSecondOrder( 0 )
isDone = Mesh_2.Compute()
smesh.SetName(Mesh_2, 'Mesh_2')
Mesh_2.ExportMED( r'/home/E02847/ether/syrthes4/syrthes4/src/tests/cas_plates3d_salome/salome/plates3d-rad.med', 0, SMESH.MED_V2_2, 1 )
Disk1_1 = Mesh_1.GroupOnGeom(Disk1,'Disk1',SMESH.VOLUME)
Disk2_int_1 = Mesh_1.GroupOnGeom(Disk2_int,'Disk2-int',SMESH.VOLUME)
Disk2_ext_1 = Mesh_1.GroupOnGeom(Disk2_ext,'Disk2-ext',SMESH.VOLUME)
Disk3_int_1 = Mesh_1.GroupOnGeom(Disk3_int,'Disk3-int',SMESH.VOLUME)
Disk3_ext_1 = Mesh_1.GroupOnGeom(Disk3_ext,'Disk3-ext',SMESH.VOLUME)
Disk4_int_1 = Mesh_1.GroupOnGeom(Disk4_int,'Disk4-int',SMESH.VOLUME)
Disk4_ext_1 = Mesh_1.GroupOnGeom(Disk4_ext,'Disk4-ext',SMESH.VOLUME)
Disk1_1.SetColor( SALOMEDS.Color( 0, 1, 0 ))
Disk2_int_1.SetColor( SALOMEDS.Color( 0.666667, 0.333333, 0 ))
Disk2_ext_1.SetColor( SALOMEDS.Color( 0.666667, 0, 0 ))
Disk3_int_1.SetColor( SALOMEDS.Color( 1, 1, 0 ))
Disk3_ext_1.SetColor( SALOMEDS.Color( 1, 0.666667, 0 ))
Disk4_int_1.SetColor( SALOMEDS.Color( 0.666667, 0.333333, 1 ))
Disk4_ext_1.SetColor( SALOMEDS.Color( 0.666667, 0, 0.498039 ))
four_1 = Mesh_1.GroupOnGeom(four,'four',SMESH.VOLUME)
four_1.SetColor( SALOMEDS.Color( 0, 0.666667, 1 ))
smesh.SetName(Mesh_1, 'Mesh_1')
Mesh_1.ExportMED( r'/home/E02847/ether/syrthes4/syrthes4/src/tests/cas_plates3d_salome/salome/plates3d.med', 0, SMESH.MED_V2_2, 1 )
interne_1.SetColor( SALOMEDS.Color( 1, 0, 0.666667 ))
ss_couvercle_1.SetColor( SALOMEDS.Color( 1, 0, 0.666667 ))
bords_d4_1.SetColor( SALOMEDS.Color( 1, 0, 0.666667 ))
bord_d2_1.SetColor( SALOMEDS.Color( 1, 0, 0.666667 ))
bord_d3_1.SetColor( SALOMEDS.Color( 1, 0, 0.666667 ))
bord_d1_1.SetColor( SALOMEDS.Color( 1, 0, 0.666667 ))
dessous_1.SetColor( SALOMEDS.Color( 1, 0, 0.666667 ))
[ smeshObj_1 ] = Mesh_2.GetGroups()
[ Regular_1D ] = Mesh_2.GetMesh().GetSubMeshes()
[ interne_1, ss_couvercle_1, bords_d4_1, bord_d2_1, bord_d3_1, bord_d1_1, dessous_1, dessus_1, couronne_1, Disk1_1, Disk2_int_1, Disk2_ext_1, Disk3_int_1, Disk3_ext_1, Disk4_int_1, Disk4_ext_1, four_1 ] = Mesh_1.GetGroups()
dessous_1.SetColor( SALOMEDS.Color( 0.666667, 1, 0 ))
couronne_1.SetColor( SALOMEDS.Color( 0, 0, 1 ))
interne_1.SetColor( SALOMEDS.Color( 1, 0.666667, 0 ))
dessous_1.SetColor( SALOMEDS.Color( 1, 0.666667, 1 ))
dessous_1.SetColor( SALOMEDS.Color( 0.666667, 1, 1 ))
ss_couvercle_1.SetColor( SALOMEDS.Color( 1, 0, 0 ))
ss_couvercle_1.SetColor( SALOMEDS.Color( 0.666667, 1, 0 ))
bord_d1_1.SetColor( SALOMEDS.Color( 1, 0, 0 ))
bord_d1_1.SetColor( SALOMEDS.Color( 1, 0, 1 ))
bord_d3_1.SetColor( SALOMEDS.Color( 1, 0.333333, 1 ))
bords_d4_1.SetColor( SALOMEDS.Color( 1, 0.666667, 1 ))
Regular_1D = Mesh_2.GetSubMesh( Compound_surf_ray, 'Regular_1D' )
steel_copper_1 = Mesh_2.GroupOnGeom(steel_copper,'steel-copper',SMESH.FACE)
granite_1 = Mesh_2.GroupOnGeom(granite,'granite',SMESH.FACE)
steel_copper_1.SetColor( SALOMEDS.Color( 0, 0.666667, 1 ))
steel_copper_1.SetColor( SALOMEDS.Color( 1, 0.666667, 0 ))
granite_1.SetColor( SALOMEDS.Color( 0, 0.666667, 1 ))
smesh.SetName(Mesh_2, 'Mesh_2')
Mesh_2.ExportMED( r'/home/E02847/ether/syrthes4/syrthes4/src/tests/cas_plates3d_salome/salome/plates3d-rad.med', 0, SMESH.MED_V2_2, 1 )
Mesh_2.RemoveGroup( smeshObj_1 )
smesh.SetName(Mesh_2, 'Mesh_2')
Mesh_2.ExportMED( r'/home/E02847/ether/syrthes4/syrthes4/src/tests/cas_plates3d_salome/salome/plates3d-rad.med', 0, SMESH.MED_V2_2, 1 )
Regular_1D = Mesh_2.GetSubMesh( Compound_surf_ray, 'NETGEN_2D_ONLY' )

## some objects were removed
aStudyBuilder = theStudy.NewBuilder()
SO = theStudy.FindObjectIOR(theStudy.ConvertObjectToIOR(smeshObj_1))
if SO is not None: aStudyBuilder.RemoveObjectWithChildren(SO)
## set object names
smesh.SetName(Mesh_1.GetMesh(), 'Mesh_1')
smesh.SetName(BLSURF_1.GetAlgorithm(), 'BLSURF_1')
smesh.SetName(BLSURF_Parameters, 'BLSURF_Parameters')
smesh.SetName(GHS3D_3D_2.GetAlgorithm(), 'GHS3D_3D_2')
smesh.SetName(Mesh_2.GetMesh(), 'Mesh_2')
smesh.SetName(NETGEN_2D_ONLY_3.GetAlgorithm(), 'NETGEN_2D_ONLY_3')
smesh.SetName(NETGEN_Parameters_2D_ONLY, 'NETGEN_Parameters_2D_ONLY')
smesh.SetName(Regular_1D_4.GetAlgorithm(), 'Regular_1D_4')
smesh.SetName(LocalLength_0_1_1e_07, 'LocalLength=0.1,1e-07')
smesh.SetName(interne_1, 'interne')
smesh.SetName(ss_couvercle_1, 'ss_couvercle')
smesh.SetName(bords_d4_1, 'bords-d4')
smesh.SetName(bord_d2_1, 'bord-d2')
smesh.SetName(bord_d3_1, 'bord-d3')
smesh.SetName(bord_d1_1, 'bord-d1')
smesh.SetName(dessous_1, 'dessous')
smesh.SetName(dessus_1, 'dessus')
smesh.SetName(couronne_1, 'couronne')
smesh.SetName(Disk1_1, 'Disk1')
smesh.SetName(Disk2_int_1, 'Disk2-int')
smesh.SetName(Disk2_ext_1, 'Disk2-ext')
smesh.SetName(Disk3_int_1, 'Disk3-int')
smesh.SetName(Disk3_ext_1, 'Disk3-ext')
smesh.SetName(Disk4_int_1, 'Disk4-int')
smesh.SetName(Disk4_ext_1, 'Disk4-ext')
smesh.SetName(four_1, 'four')
smesh.SetName(Regular_1D, 'Regular_1D')
smesh.SetName(steel_copper_1, 'steel-copper')
smesh.SetName(granite_1, 'granite')

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser(1)

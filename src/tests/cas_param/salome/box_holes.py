# -*- coding: iso-8859-1 -*-

###
### This file is generated automatically by SALOME v6.6.0 with dump python functionality
###

import sys
import salome

salome.salome_init()
theStudy = salome.myStudy

import salome_notebook
notebook = salome_notebook.notebook
sys.path.insert( 0, r'/netdata/ether/syrthes4/SPECIFIQ_EDF/syrthes4_tutorial/cas_param/salome')

###
### GEOM component
###

import GEOM
import geompy
import math
import SALOMEDS


geompy.init_geom(theStudy)

O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)
Box_1 = geompy.MakeBoxDXDYDZ(1, 1, 1)
Cylinder_1 = geompy.MakeCylinderRH(0.1, 1)
geompy.TranslateDXDYDZ(Cylinder_1, 0.2, 0.2, 0)
Translation_1 = geompy.MakeTranslation(Cylinder_1, 0.3, 0, 0)
Translation_2 = geompy.MakeTranslation(Translation_1, 0.3, 0, 0)
Translation_3 = geompy.MakeTranslation(Cylinder_1, 0, 0.3, 0)
Translation_4 = geompy.MakeTranslation(Translation_3, 0.3, 0, 0)
Translation_5 = geompy.MakeTranslation(Translation_4, 0.3, 0, 0)
Translation_6 = geompy.MakeTranslation(Translation_3, 0, 0.3, 0)
Translation_7 = geompy.MakeTranslation(Translation_6, 0.3, 0, 0)
Translation_8 = geompy.MakeTranslation(Translation_7, 0.3, 0, 0)
Partition_1 = geompy.MakePartition([Cylinder_1, Translation_1, Translation_2, Translation_3, Translation_4, Translation_5, Translation_6, Translation_7, Translation_8], [], [], [], geompy.ShapeType["SOLID"], 0, [], 0)
box_holes = geompy.MakeCut(Box_1, Partition_1)
listSubShapeIDs = geompy.SubShapeAllIDs(box_holes, geompy.ShapeType["SOLID"])
Group_vol = geompy.CreateGroup(box_holes, geompy.ShapeType["SOLID"])
geompy.UnionIDs(Group_vol, [1])
Group_faces_box = geompy.CreateGroup(box_holes, geompy.ShapeType["FACE"])
geompy.UnionIDs(Group_faces_box, [3, 87, 52, 13])
Group_faces_holes = geompy.CreateGroup(box_holes, geompy.ShapeType["FACE"])
geompy.UnionIDs(Group_faces_holes, [98, 89, 107, 110, 104, 95, 92, 113, 101])
geomObj_1 = geompy.GetSubShape(box_holes, [3])
geomObj_2 = geompy.GetSubShape(box_holes, [98])
in_out = geompy.CreateGroup(box_holes, geompy.ShapeType["FACE"])
geompy.UnionIDs(in_out, [57, 20])
geomObj_3 = geompy.GetSubShape(box_holes, [57])
geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( Box_1, 'Box_1' )
geompy.addToStudy( Cylinder_1, 'Cylinder_1' )
geompy.addToStudy( Translation_1, 'Translation_1' )
geompy.addToStudy( Translation_2, 'Translation_2' )
geompy.addToStudy( Translation_3, 'Translation_3' )
geompy.addToStudy( Translation_4, 'Translation_4' )
geompy.addToStudy( Translation_5, 'Translation_5' )
geompy.addToStudy( Translation_6, 'Translation_6' )
geompy.addToStudy( Translation_7, 'Translation_7' )
geompy.addToStudy( Translation_8, 'Translation_8' )
geompy.addToStudy( Partition_1, 'Partition_1' )
geompy.addToStudy( box_holes, 'box_holes' )
geompy.addToStudyInFather( box_holes, Group_vol, 'Group_vol' )
geompy.addToStudyInFather( box_holes, Group_faces_box, 'Group_faces_box' )
geompy.addToStudyInFather( box_holes, Group_faces_holes, 'Group_faces_holes' )
geompy.addToStudyInFather( box_holes, in_out, 'in_out' )

###
### SMESH component
###

import smesh, SMESH, SALOMEDS

smesh.SetCurrentStudy(theStudy)
import BLSURFPlugin
import NETGENPlugin
Mesh_1 = smesh.Mesh(box_holes)
BLSURF = Mesh_1.Triangle(algo=smesh.BLSURF)
BLSURF_Parameters_1 = BLSURF.Parameters()
BLSURF_Parameters_1.SetMinSize( 0.00173214 )
BLSURF_Parameters_1.SetMaxSize( 0.346428 )
NETGEN_3D = Mesh_1.Tetrahedron()
BLSURF_Parameters_1.SetPhySize( 0.04 )
isDone = Mesh_1.Compute()
Group_vol_1 = Mesh_1.GroupOnGeom(Group_vol,'Group_vol',SMESH.VOLUME)
Group_faces_box_1 = Mesh_1.GroupOnGeom(Group_faces_box,'Group_faces_box',SMESH.FACE)
Group_faces_holes_1 = Mesh_1.GroupOnGeom(Group_faces_holes,'Group_faces_holes',SMESH.FACE)
Group_faces_box_1.SetColor( SALOMEDS.Color( 0, 0.666667, 1 ))
Group_faces_holes_1.SetColor( SALOMEDS.Color( 1, 0, 0 ))
in_out_1 = Mesh_1.GroupOnGeom(in_out,'in_out',SMESH.FACE)
in_out_1.SetColor( SALOMEDS.Color( 0.666667, 1, 0 ))

## set object names
smesh.SetName(Mesh_1.GetMesh(), 'Mesh_1')
smesh.SetName(BLSURF.GetAlgorithm(), 'BLSURF')
smesh.SetName(BLSURF_Parameters_1, 'BLSURF Parameters_1')
smesh.SetName(NETGEN_3D.GetAlgorithm(), 'NETGEN_3D')
smesh.SetName(Group_vol_1, 'Group_vol')
smesh.SetName(Group_faces_box_1, 'Group_faces_box')
smesh.SetName(Group_faces_holes_1, 'Group_faces_holes')
smesh.SetName(in_out_1, 'in_out')

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser(1)

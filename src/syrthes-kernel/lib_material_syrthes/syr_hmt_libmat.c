/*-----------------------------------------------------------------------

                         SYRTHES version 4.1
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the Code_Saturne Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

/*|======================================================================|
  | SYRTHES 4.1                                       COPYRIGHT EDF 2008 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | Transferts couples : proprietes des materiaux                        |
  |======================================================================| */



/*|====== Beton =========================================================| */
#include "syr_hmt_lib_beton.c"

/*|====== Pin ===========================================================| */
#include "syr_hmt_lib_pin.c"

/*|====== pse_normal ====================================================| */
#include "syr_hmt_lib_pse_normal.c"

/*|====== Laine de roche ================================================| */
#include "syr_hmt_lib_laine_verre.c"

/*|====== bois_agglomere ================================================| */
#include "syr_hmt_lib_bois_agglo.c"

/*|====== polyurethane ==================================================| */
#include "syr_hmt_lib_polyurethane.c"

/*|====== mdf ===========================================================| */
#include "syr_hmt_lib_mdf.c"

/*|====== fibre_bois ====================================================| */
#include "syr_hmt_lib_fibre_bois.c"

/*|====== air ===========================================================| */
#include "syr_hmt_lib_air.c"

/*|====== air ===========================================================| */
#include "syr_hmt_lib_acier.c"

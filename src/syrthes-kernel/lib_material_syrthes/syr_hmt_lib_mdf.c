/*-----------------------------------------------------------------------

                         SYRTHES version 4.1
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the Code_Saturne Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

/*|======================================================================|
  | SYRTHES 4.1                                       COPYRIGHT EDF 2008 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |    Proprietes du mdf                                               |
  |======================================================================| */

/* Initialisations des constantes */
double fmat_const_mdf(struct ConstMateriaux *constmat,
			struct ConstPhyhmt constphyhmt)
{
  constmat->eps0=0.549;
  constmat->rhos=589;
  constmat->cs=1551;
  constmat->xknv=0;
  constmat->xk=3.9e-13;
/*   constmat->xk=3.e-19; commente pour test */
  constmat->taumax=545.;
  return 1;
}


/* taux d'humidite volumique */
/* ---------------------------------------------------------------------- */
double fmat_ftauv_mdf(struct ConstPhyhmt constphyhmt,struct ConstMateriaux constmat,
			double xpv,double psat,double t)
{
  double x,a,b,tauv;

  x=xpv/psat;

  tauv=(137.85732*x)/((1-0.9699*x)*(1+7.1061*x));

  return tauv;
}

/* pente de l'isotherme de sorption */
/* ---------------------------------------------------------------------- */
double fmat_falpha_mdf(struct ConstPhyhmt constphyhmt,struct ConstMateriaux constmat,
			double pv,double psat,double t)
{
  double x,a,alpha;
  
  x=pv/psat;
  
  alpha = (137.85732 
    + 950.141102*x*x)
    /((1.-0.9699*x)*(1.+7.1061*x)*(1.-0.9699*x)*(1.+7.1061*x));
  
    return alpha/psat;
}

/* permeabilite relative au gaz */
/* ---------------------------------------------------------------------- */
double fmat_fkrg_mdf(struct ConstMateriaux constmat,double tauv)
{

  double x;

  x=2.485e-8-1.929e-11*tauv-4.955e-14*tauv*tauv;

  return  x*1.80991e-5/(1.2*constmat.xk);
}

/* permeabilite relative au liquide */
/* ---------------------------------------------------------------------- */
double fmat_fkrl_mdf(struct ConstMateriaux constmat,double tauv)
{
  double x;

  x=1.17e-16*(0.2449*exp(1.339*tauv/517.75)-0.2441*exp(-93.79*tauv/517.75));

/* on n'a pas acces aux grandeurs liees au liquide : viscosite dynamique et masse volumique.
   on prend donc une valeur de la viscosité dynamique a 20°C */
  return x*8.7e-04/(1000*constmat.xk);
}

/* conductivite thermique du materiau humide */
/* ---------------------------------------------------------------------- */
double fmat_fklambt_mdf(double t,double tauv)
{
  /* Attention cette valeur semble varier suivant les sources */
/*   return 1.4; cp pour test*/
  return tauv/1000*0.4747+0.107;
}

/* coefficient de diffusion de la vapeur dans le materiau */
/* ---------------------------------------------------------------------- */
  double fmat_fpiv_mdf(double xpv, double psat, double t)
{
  double x,y,z;
/* x = HR ; y = tauv ; z = coeff diffusion m2/s  */

/*calcul de tauv*/
  x=xpv/psat;

  y=(137.85732*x)/((1-0.9699*x)*(1+7.1061*x));

/*calcul du coeff diffusion en m2/s*/

  z=2.61e-5*(-18.14e-4*y+1);

  return z*101300/(461.889*t);
}

/* Fonction utilisateur hm : chaleur latente complementaire */
/* ---------------------------------------------------------------------- */
double fmat_fhm_mdf(double tauv)
{
  return 0.;
}

/* derivee par rapport a tauv de la fonction hm */
/* ---------------------------------------------------------------------- */
double fmat_fdhmdtauv_mdf(double tauv)
{
  return 0.;
}

/* Fonction utilisateur fbetap */
/* ---------------------------------------------------------------------- */
double fmat_fbetap_mdf(struct ConstPhyhmt constphyhmt,
			 struct ConstMateriaux constmat,
			 double xpv,double psat,double tauv,double t)
{
  return -fmat_falpha_mdf(constphyhmt,constmat,xpv,psat,t)*xpv
    *(fphyhmt_fxl(constphyhmt,t)+fmat_fhm_mdf(tauv))/constphyhmt.Rv/(t*t);
}

/* fonction supplementaire dhp - betap * d hm/d tauv */
/* ---------------------------------------------------------------------- */
double fmat_fdhp_mdf(double betap, double tauv)
{
  return 0.;
}

/* fonction supplementaire dht - alpha * d hm/d tauv */
/* ---------------------------------------------------------------------- */
double fmat_fdht_mdf(double alphat, double tauv)
{
  return 0.;
}



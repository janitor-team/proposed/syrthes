/*-----------------------------------------------------------------------

                         SYRTHES version 4.1
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the Code_Saturne Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

/*|======================================================================|
  | SYRTHES 4.1                                       COPYRIGHT EDF 2008 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |    Proprietes du fibre_bois                                          |
  |======================================================================| */

/* Initialisations des constantes */
double fmat_const_fibre_bois(struct ConstMateriaux *constmat,
			struct ConstPhyhmt constphyhmt)
{
  constmat->eps0=0.9;
  constmat->rhos=146;
  constmat->cs=1272.165;
  constmat->xknv=0;
  constmat->xk=4.16e-13;
/*   constmat->xk=3.e-19; commente pour test */
  constmat->taumax=32.4749147;
  return 1;
}


/* taux d'humidite volumique */
/* ---------------------------------------------------------------------- */
double fmat_ftauv_fibre_bois(struct ConstPhyhmt constphyhmt,struct ConstMateriaux constmat,
			double xpv,double psat,double t)
{
  double x,a,b,tauv;

  x=xpv/psat;

  tauv=1.46*(45.9999*x*x*x-53.0031*x*x+29.246*x+2.92263e-4);

  return tauv;
}

/* pente de l'isotherme de sorption */
/* ---------------------------------------------------------------------- */
double fmat_falpha_fibre_bois(struct ConstPhyhmt constphyhmt,struct ConstMateriaux constmat,
			double pv,double psat,double t)
{
  double x,a,alpha;
  
  x=pv/psat;
  
  alpha = 201.479562*x*x-154.769052*x+42.69916;
  
    return alpha/psat;
}

/* permeabilite relative au gaz */
/* ---------------------------------------------------------------------- */
double fmat_fkrg_fibre_bois(struct ConstMateriaux constmat,double tauv)
{

  double x;

  x=1;

  return  x;
}

/* permeabilite relative au liquide */
/* ---------------------------------------------------------------------- */
double fmat_fkrl_fibre_bois(struct ConstMateriaux constmat,double tauv)
{
  double x;

  x=0;

  return x;
}

/* conductivite thermique du materiau humide */
/* ---------------------------------------------------------------------- */
double fmat_fklambt_fibre_bois(double t,double tauv)
{
  double x;

  x = 4.088e-4*tauv+1.08e-4*t+3.8e-2 ;

  return x;
}

/* coefficient de diffusion de la vapeur dans le materiau */
/* ---------------------------------------------------------------------- */
  double fmat_fpiv_fibre_bois(double xpv, double psat, double t)
{
  double xhr,y;

  xhr=xpv/psat;
  y = (1.9e-12*pow((t+273.15),0.8)*101300)/(4.8-2.07*xhr); 

  return y;
}

/* Fonction utilisateur hm : chaleur latente complementaire */
/* ---------------------------------------------------------------------- */
double fmat_fhm_fibre_bois(double tauv)
{
  return 0.;
}

/* derivee par rapport a tauv de la fonction hm */
/* ---------------------------------------------------------------------- */
double fmat_fdhmdtauv_fibre_bois(double tauv)
{
  return 0.;
}

/* Fonction utilisateur fbetap */
/* ---------------------------------------------------------------------- */
double fmat_fbetap_fibre_bois(struct ConstPhyhmt constphyhmt,
			 struct ConstMateriaux constmat,
			 double xpv,double psat,double tauv,double t)
{
  return -fmat_falpha_fibre_bois(constphyhmt,constmat,xpv,psat,t)*xpv
    *(fphyhmt_fxl(constphyhmt,t)+fmat_fhm_fibre_bois(tauv))/constphyhmt.Rv/(t*t);
}

/* fonction supplementaire dhp - betap * d hm/d tauv */
/* ---------------------------------------------------------------------- */
double fmat_fdhp_fibre_bois(double betap, double tauv)
{
  return 0.;
}

/* fonction supplementaire dht - alpha * d hm/d tauv */
/* ---------------------------------------------------------------------- */
double fmat_fdht_fibre_bois(double alphat, double tauv)
{
  return 0.;
}



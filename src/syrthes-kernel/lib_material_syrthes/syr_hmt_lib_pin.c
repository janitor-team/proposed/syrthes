/*-----------------------------------------------------------------------

                         SYRTHES version 4.1
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the Code_Saturne Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

/*|======================================================================|
  | SYRTHES 4.1                                       COPYRIGHT EDF 2008 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |    Proprietes du pin  (bois avec les references KKH)                 |
  |======================================================================| */

/* Initialisations des constantes */
double fmat_const_pin(struct ConstMateriaux *constmat,
		      struct ConstPhyhmt constphyhmt)

{
  constmat->eps0=0.25;
  constmat->rhos=500;
  constmat->cs=1600;
  constmat->xknv=0;
  constmat->xk=8.57e-16;
  constmat->taumax=250;
  return 1;
}


/* taux d'humidite volumique */
/* ---------------------------------------------------------------------- */
double fmat_ftauv_pin(struct ConstPhyhmt constphyhmt,struct ConstMateriaux constmat,
			double xpv,double psat,double t)
{
  double x,tauv;
  x=xpv/psat;

  if (x < 0.205e0)
    tauv= 0.1418048780e3 * x;
  else if ( x < 0.433e0)
    tauv=  0.1439631579e2 + 0.7157894737e2 * x;
  else if (  x < 0.656e0)
    tauv=  0.1469165919e2 + 0.7089686099e2 * x;
  else if (  x < 0.858e0)
     tauv= -0.3320554455e2 + 0.1439108911e3 * x;
  else if (  x < 0.954e0)
    tauv=  -0.2014500000e3 + 0.3400000000e3 * x;
  else if (  x < 0.982e0)
    tauv=  -0.7632878571e3 + 0.9289285714e3 * x;
  else if (x<=1.)
    tauv=  -0.5365555556e4 + 0.5615555556e4 * x;
  else 
    {
/*       printf("error pin tauv : pv/psat>1 (= %f psat=%f)\n",x,psat); */
/*       exit(1); */
      tauv=constmat.taumax;
    }
  return tauv;
}

/* pente de l'isotherme de sorption */
/* ---------------------------------------------------------------------- */
double fmat_falpha_pin(struct ConstPhyhmt constphyhmt,struct ConstMateriaux constmat,
			double pv,double psat,double t)
{
  double x,alpha;

  x=pv/psat;

  if (x < 0.433e0)
    alpha= 0.2049466139e3 - 0.3080084677e3 * x;
  else if ( x < 0.656e0)
    alpha=  0.7290335722e2 - 0.3058683318e1 * x;
  else if (x < 0.858e0)
    alpha=  -0.1662180091e3 + 0.3614555946e3 * x;
  else if ( x < 0.954e0)
    alpha=  -0.1608635520e4 + 0.2042594884e4 * x;
  else if (x<=1.)
    alpha=  -0.1090704348e6 + 0.1146859903e6 * x;
  else
    {
/*       printf("error pin tauv : pv/psat>1 (= %f psat=%f)\n",x,psat); */
/*       exit(1); */
      alpha=-0.1090704348e6 + 0.1146859903e6 * 1.;
    }

  return alpha/psat;
}

/* permeabilite relative au gaz */
/* ---------------------------------------------------------------------- */
double fmat_fkrg_pin(struct ConstMateriaux constmat,double tauv)
{
  return 1-tauv/constmat.taumax;
}

/* permeabilite relative au liquide */
/* ---------------------------------------------------------------------- */
double fmat_fkrl_pin(struct ConstMateriaux constmat,double tauv)
{
  double x,fkrl;
  x=tauv/constmat.taumax;

  if (x<=0.138) 
    fkrl= x*1e-7;
  else if (x<=0.207) 
    fkrl= (x-0.1)*1e-6;
  else if (x<=0.243) 
    fkrl= (2*x-0.3)*1e-6;
  else if (x<=0.366) 
    fkrl= (x-0.2)*1e-6;
  else if (x<=0.5) 
    fkrl= (2.537*x+0.7315)*1e-6;
  else if (x<0.66) 
    fkrl= (6.25*x-1.125)*1e-6;
  else if (x<0.75) 
    fkrl= 0.444*x-293037;
  else if (x<0.85) 
    fkrl= 1.4*x-1.01;
  else if (x<0.9) 
    fkrl= 3*x-2.37;
  else if (x<0.95) 
    fkrl= 5.4*x-4.53;
  else if (x<=1.) 
    fkrl= 8*x-7;
  else{
/*     printf("probleme krl : tauv=%f taumax=%f\n",tauv,constmat.taumax); */
/*     return 0; */
    fkrl=8*1.-7;
    }
  return fkrl;
}


/* conductivite thermique du materiau humide */
/* ---------------------------------------------------------------------- */
double fmat_fklambt_pin(double t,double tauv)
{
  return 0.15;
}


/* coefficient de diffusion de la vapeur dans le materiau */
/* ---------------------------------------------------------------------- */
double fmat_fpiv_pin(double xpv,double psat,double t)
{
  double x,piv;
  x = xpv/psat;
    if (x < 0.262e0)
      piv=0.2069771429e-7 + 0.7428571429e-8 * x;
    else if ( x < 0.492e0)
      piv=0.9627156522e-8 + 0.4968260870e-7 * x;
    else if ( x < 0.646e0)
      piv=0.1784458442e-7 + 0.3298051948e-7 * x;
    else if ( x<=1.)
      piv=-0.3945851000e-7 + 0.1216850000e-6 * x;
    else
      {
/* 	printf("error pin tauv : pv/psat>1 (= %f psat=%f)\n",x,psat); */
/* 	exit(1); */
	piv=-0.3945851000e-7 + 0.1216850000e-6 * 1.;
      }
  return piv;

}

/* Fonction utilisateur hm : chaleur latente complementaire */
/* ---------------------------------------------------------------------- */
double fmat_fhm_pin(double tauv)
{
  double hm,tauv_sup,tauv_inf;

  tauv_sup=2.017e+7/1.922e+5;
  tauv_inf=20.;

  if (tauv< tauv_inf)
    hm=2.017e+7/tauv_inf-1.922e+5;
  else if (tauv<tauv_sup)
    hm=2.017e+7/tauv-1.922e+5;
  else
      hm=0.;
  return hm;
}

/* derivee par rapport a tauv de la fonction hm */
/* ---------------------------------------------------------------------- */
double fmat_fdhmdtauv_pin(double tauv)
{
  /* peut-etre a revoir car je n'aime pas la discontinuite - christophe */
  double dhmdtauv,tauv_sup,tauv_inf;
  
  tauv_sup=2.017e+7/1.922e+5;
  tauv_inf=20.;
  
  if (tauv< tauv_inf)
    dhmdtauv=-2.017e+7/(tauv_inf*tauv_inf);
  else if (tauv<tauv_sup)
    dhmdtauv=-2.017e+7/(tauv*tauv);
  else
    dhmdtauv=0.;
  return dhmdtauv;
}

/* Fonction utilisateur fbetap */
/* ---------------------------------------------------------------------- */
double fmat_fbetap_pin(struct ConstPhyhmt constphyhmt,
		       struct ConstMateriaux constmat,
		       double xpv,double psat,double tauv,double t)
{
  /* Sans doute a revoir - christophe */

  return -fmat_falpha_pin(constphyhmt,constmat,xpv,psat,t)*xpv
    *(fphyhmt_fxl(constphyhmt,t)+fmat_fhm_pin(tauv))/constphyhmt.Rv/(t*t);
}

/* fonction supplementaire dhp - betap * d hm/d tauv */
/* ---------------------------------------------------------------------- */
double fmat_fdhp_pin(double betap, double tauv)
{
  /* A revoir - christophe - pour passer dhmdtauv en argument egalement dans 
   coefequa */

  return 0.;
}

/* fonction supplementaire dht - alpha * d hm/d tauv */
/* ---------------------------------------------------------------------- */
double fmat_fdht_pin(double alphat, double tauv)
{
  /* A revoir - christophe - pour passer dhmdtauv en argument egalement dans 
   coefequa */

  return 0.;
}

/*-----------------------------------------------------------------------

                         SYRTHES version 4.1
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the Code_Saturne Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#ifndef _HMT_BIBMAT_H
#define _HMT_BIBMAT_H

#include "syr_hmt_bd.h"


/*=====================================================*/
/*======== Bibliotheque de materiaux           ========*/
/*=====================================================*/

/* nombre de materiaux disponibles dans la bibliotheque */
#define NB_MAT                    10
 

#define MAT_BETON                  0
#define MAT_BOIS_PIN               1
#define MAT_PSE_NORMAL             2
#define MAT_LAINE_VERRE            3
#define MAT_BOIS_AGGLO             4
#define MAT_POLYURETHANE           5
#define MAT_MDF                    6
#define MAT_FIBRE_BOIS             7
#define MAT_AIR                    8
#define MAT_ACIER                  9

/* liste des materiaux disponibles */
/* ------------------------------- */
#ifdef LISTE_MAT
char *liste_mat[NB_MAT]= {
"BETON",             "BOIS_PIN",             "PSE_NORMAL",
"LAINE_VERRE",       "BOIS_AGGLO",           "POLYURETHANE", 
"MDF",               "FIBRE_BOIS",           "AIR",
"ACIER"         };
#else
extern char *liste_mat[NB_MAT];
#endif

/* functions protoype */
/* ------------------ */

#include "syr_hmt_lib_beton.h"
#include "syr_hmt_lib_pin.h"
#include "syr_hmt_lib_pse_normal.h"
#include "syr_hmt_lib_laine_verre.h"
#include "syr_hmt_lib_bois_agglo.h"
#include "syr_hmt_lib_polyurethane.h"
#include "syr_hmt_lib_mdf.h"
#include "syr_hmt_lib_fibre_bois.h"
#include "syr_hmt_lib_air.h"
#include "syr_hmt_lib_acier.h"


static double (*fmat_const[])(struct ConstMateriaux*,struct ConstPhyhmt)=
{fmat_const_beton,            fmat_const_pin,           fmat_const_pse_normal,   
 fmat_const_laine_verre,      fmat_const_bois_agglo,    fmat_const_polyurethane, 
 fmat_const_mdf,              fmat_const_fibre_bois,    fmat_const_air,
 fmat_const_acier};
 

static double (*fmat_ftauv[])(struct ConstPhyhmt,struct ConstMateriaux,double,double,double)=
{fmat_ftauv_beton,            fmat_ftauv_pin,           fmat_ftauv_pse_normal,   
 fmat_ftauv_laine_verre,      fmat_ftauv_bois_agglo,    fmat_ftauv_polyurethane, 
 fmat_ftauv_mdf,              fmat_ftauv_fibre_bois,    fmat_ftauv_air,
 fmat_ftauv_acier };

static double (*fmat_falpha[])(struct ConstPhyhmt,struct ConstMateriaux,double,double,double)=
{fmat_falpha_beton,            fmat_falpha_pin,           fmat_falpha_pse_normal,   
 fmat_falpha_laine_verre,      fmat_falpha_bois_agglo,    fmat_falpha_polyurethane, 
 fmat_falpha_mdf,              fmat_falpha_fibre_bois,    fmat_falpha_air,
 fmat_falpha_acier};

static double (*fmat_fkrg[])(struct ConstMateriaux,double)=
{fmat_fkrg_beton,            fmat_fkrg_pin,           fmat_fkrg_pse_normal,   
 fmat_fkrg_laine_verre,      fmat_fkrg_bois_agglo,    fmat_fkrg_polyurethane, 
 fmat_fkrg_mdf,              fmat_fkrg_fibre_bois,    fmat_fkrg_air,
 fmat_fkrg_acier};

static double (*fmat_fkrl[])(struct ConstMateriaux,double)=
{fmat_fkrl_beton,            fmat_fkrl_pin,           fmat_fkrl_pse_normal,   
 fmat_fkrl_laine_verre,      fmat_fkrl_bois_agglo,    fmat_fkrl_polyurethane, 
 fmat_fkrl_mdf,              fmat_fkrl_fibre_bois,    fmat_fkrl_air,
 fmat_fkrl_acier};

static double (*fmat_fklambt[])(double,double)=
{fmat_fklambt_beton,            fmat_fklambt_pin,           fmat_fklambt_pse_normal,   
 fmat_fklambt_laine_verre,      fmat_fklambt_bois_agglo,    fmat_fklambt_polyurethane, 
 fmat_fklambt_mdf,              fmat_fklambt_fibre_bois,    fmat_fklambt_air,
 fmat_fklambt_acier};

static double (*fmat_fpiv[])(double,double,double)=
{fmat_fpiv_beton,            fmat_fpiv_pin,           fmat_fpiv_pse_normal,   
 fmat_fpiv_laine_verre,      fmat_fpiv_bois_agglo,    fmat_fpiv_polyurethane, 
 fmat_fpiv_mdf,              fmat_fpiv_fibre_bois,    fmat_fpiv_air,
 fmat_fpiv_acier};


static double (*fmat_fhm[])(double)=
{fmat_fhm_beton,            fmat_fhm_pin,           fmat_fhm_pse_normal,   
 fmat_fhm_laine_verre,      fmat_fhm_bois_agglo,    fmat_fhm_polyurethane, 
 fmat_fhm_mdf,              fmat_fhm_fibre_bois,    fmat_fhm_air,
 fmat_fhm_acier};


static double (*fmat_fdhmdtauv[])(double)=
{fmat_fdhmdtauv_beton,            fmat_fdhmdtauv_pin,           fmat_fdhmdtauv_pse_normal,   
 fmat_fdhmdtauv_laine_verre,      fmat_fdhmdtauv_bois_agglo,    fmat_fdhmdtauv_polyurethane, 
 fmat_fdhmdtauv_mdf,              fmat_fdhmdtauv_fibre_bois,    fmat_fdhmdtauv_air,
 fmat_fdhmdtauv_acier};


static double (*fmat_fbetap[])(struct ConstPhyhmt,struct ConstMateriaux,
				double,double,double,double)=
{fmat_fbetap_beton,            fmat_fbetap_pin,           fmat_fbetap_pse_normal,   
 fmat_fbetap_laine_verre,      fmat_fbetap_bois_agglo,    fmat_fbetap_polyurethane, 
 fmat_fbetap_mdf,              fmat_fbetap_fibre_bois,    fmat_fbetap_air,
 fmat_fbetap_acier};


static double (*fmat_fdhp[])(double,double)=
{fmat_fdhp_beton,            fmat_fdhp_pin,           fmat_fdhp_pse_normal,   
 fmat_fdhp_laine_verre,      fmat_fdhp_bois_agglo,    fmat_fdhp_polyurethane, 
 fmat_fdhp_mdf,              fmat_fdhp_fibre_bois,     fmat_fdhp_air,
 fmat_fdhp_acier};


static double (*fmat_fdht[])(double,double)=
{fmat_fdht_beton,            fmat_fdht_pin,           fmat_fdht_pse_normal,   
 fmat_fdht_laine_verre,      fmat_fdht_bois_agglo,    fmat_fdht_polyurethane, 
 fmat_fdht_mdf,              fmat_fdht_fibre_bois,    fmat_fdht_air,
 fmat_fdht_acier};


#endif

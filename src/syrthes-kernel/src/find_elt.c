/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <math.h>
# include <stdlib.h>
# include <string.h>

# include "syr_usertype.h"
# include "syr_tree.h"
# include "syr_abs.h"
# include "syr_bd.h"
# include "syr_proto.h"

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  | findel                                                               |
  |         Dans quel element se trouve x,y,(z)                          |
  |        (sans utilisation des equations des aretes ou faces)          |
  |======================================================================| */
void findel(struct node *arbre ,int ndim,double **coolag,int **nodlag, 
	    double x,double y, double z,int *numel,double toler,int *err)
{
  struct node *noeud;
  struct element *el1;
  int na,nb,nc,nd;
  double xa,ya,za,xb,yb,zb,xc,yc,zc,xd,yd,zd;
  
  
  noeud = arbre;
  *err=0;
  
  if (ndim==2)
    {
      find_node_2d(&noeud,x,y);
      if (noeud->lelement==NULL)
	{
	  /* printf("  %% ATTENTION FINDEL : point x=%f, y=%f\n",x,y);*/
	  /* printf("                         il n'y a pas d'element dans ce noeud !\n"); */
	  /* exit(0); */
	  *err=1;
	}
      else
	{
	  el1 = noeud->lelement;
	  *err=0;
	  while (el1!=NULL)
	    {
	      *numel = el1->num;
	      na=nodlag[0][*numel]; nb=nodlag[1][*numel]; nc=nodlag[2][*numel];
	      xa=coolag[0][na] ; ya=coolag[1][na];
	      xb=coolag[0][nb] ; yb=coolag[1][nb];
	      xc=coolag[0][nc] ; yc=coolag[1][nc];
	      if (in_tria_2d(xa,ya,xb,yb,xc,yc,x,y,toler))
		return;
	      else
		el1 = el1->suivant;
	    }
	  /*   printf("  %% ERREUR FINDEL : le point x=%f, y=%f n'est dans aucun element !\n",x,y);*/
	  /*  exit(0);*/
	  *err=1;
	}
    }
  else  /* cas 3d */
    {
      find_node_3d(&noeud,x,y,z);
      if (noeud->lelement==NULL)
	{
	  /* printf("  %% ATTENTION FINDEL :  x=%f, y=%f, z=%f\n",x,y,z);*/
	  /* printf("                     il n'y a pas d'element dans ce noeud !\n"); */
	  /* exit(0); */
	  *err=1;
	}
      else
	{
	  el1 = noeud->lelement;
	  *err=0;
	  while (el1!=NULL)
	    {
	      *numel = el1->num;
	      na=nodlag[0][*numel]; nb=nodlag[1][*numel]; 
	      nc=nodlag[2][*numel];  nd=nodlag[3][*numel];
	      xa=coolag[0][na] ; ya=coolag[1][na] ; za=coolag[2][na];
	      xb=coolag[0][nb] ; yb=coolag[1][nb] ; zb=coolag[2][nb];
	      xc=coolag[0][nc] ; yc=coolag[1][nc] ; zc=coolag[2][nc]; 
	      xd=coolag[0][nd] ; yd=coolag[1][nd] ; zd=coolag[2][nd];
	      if (in_tetra2(xa,ya,za,xb,yb,zb,xc,yc,zc,xd,yd,zd,x,y,z,toler))
		return; 
	      else
		el1 = el1->suivant; 
	    }
	  /*	    printf("  %% ERREUR FINDEL : le point x=%f,y=%f,z=%f n'est dans aucun element !\n",x,y,z); */
	  /*  exit(0);*/
	  *err=1;
	}
    }
}


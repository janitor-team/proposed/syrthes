/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "syr_tree.h"
# include "syr_abs.h"
# include "syr_bd.h"
# include "syr_proto.h"

/* # include "mpi.h" */


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | ivoitj_2d                                                            |
  |         Detecter si deux point se voient                             |
  |======================================================================| */

void ivoitj_2d(struct node *arbre,struct node *noeud,struct node *noeud_arr,
	       double ro[],double rd[],double pt_arr[],int *intersect,double size_min,
	       int **nodray,double **cooray,int *arrivee,
	       double dim_boite[])
{
  struct element *fa1;
  int numel,i1;
  struct node  *n_en_cours;  /*    int n_en_cours;*/
  double xv,yv,epsi;
  

  fa1 = noeud->lelement;
  n_en_cours =  noeud;   /*    n_en_cours =  noeud->name;*/
  epsi=1.E-6;
  
  /*  printf(">>> ivoitj_2d : on est dans la boite %d\n",noeud->name); 
    printf(">>> ivoitj_2d : xmin,xmax,ymin,ymax,%f %f %f %f \n", 
	   noeud->xc-noeud->size,noeud->xc+noeud->size, 
	   noeud->yc-noeud->size,noeud->yc+noeud->size ); */

 /*   printf(" on est dans la boite %d \n", noeud->name); */
  while (fa1 != NULL)
	{
	  numel = fa1->num;
/*	    printf(" ro, rd %f %f %f %f   \n", ro[0],ro[1],rd[0]*1.e6,rd[1]*1.e6); */
	  *intersect = ray_inter_seg(arbre,n_en_cours,numel,ro,rd,
				     nodray,cooray,arrivee);
/*	    printf(" on teste la facette %d, intersect=%d\n",numel,*intersect); */

	  if (*intersect==-1)
	    {
	      fa1 = fa1->suivant;
	      continue;
	      }
	  else
	    {
	      /* printf(" >>> ivoitj_2d : intersection  avec l'element %d \n",*intersect); */
	      break;
	    }
	  
	} /* fin du while */




/* printf(" >> ivoitj_2d : test d'intersection : *intersect %d *arrivee %d\n", *intersect,*arrivee);*/
  if (!( *intersect>=0 || *arrivee ||(noeud==noeud_arr) ))
    {
      voxel_voisin_2d(&xv,&yv,noeud->xc,noeud->yc, noeud->sizx,noeud->sizy,
		      ro,rd,pt_arr,size_min);
      if (fabs(rd[0])<epsi && fabs(rd[1])<epsi)
	{
	  *arrivee=1;
	  *intersect=-1;
	  /*	printf(" >> ivoitj_2d : on sort par rd nul\n"); */
	}
      else
	{
	  noeud = arbre;
	  find_node_2d(&noeud,xv,yv);
	  if (in_rectan(xv,yv,dim_boite[0],dim_boite[1],dim_boite[2],dim_boite[3]))
	    ivoitj_2d(arbre,noeud,noeud_arr,ro,rd,pt_arr,intersect,size_min,
		      nodray,cooray,arrivee,dim_boite);
	  else
	    {
	      *intersect=-1;
	      /*	    printf(" >> ivoitj_2d : on est sorti de la boite noeud->name= %d\n",noeud->name); */
	    }
	}
    }
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | ivoitj_2d                                                            |
  |         Detecter si deux point se voient                             |
  |======================================================================| */

void ivoitj_2dST(struct node *arbre,struct node *noeud,struct node *noeud_arr,
		 double ro[],double rd[],double pt_arr[],int *intersect,double size_min,
		 int **nodray,double **cooray,double **xnfray,int *arrivee,
		 double dim_boite[],int *typfacray,double **transm,double *coeff_transp,
		 int nbande)
{
  struct element *fa1;
  int numel,i1,i;
  struct node  *n_en_cours;  /*    int n_en_cours;*/
  double xv,yv,epsi;
  

  fa1 = noeud->lelement;
  n_en_cours =  noeud;   /*    n_en_cours =  noeud->name;*/
  epsi=1.E-6;
  

  /*  printf(">>> ivoitj_2d : on est dans la boite %d\n",noeud->name); 
    printf(">>> ivoitj_2d : xmin,xmax,ymin,ymax,%f %f %f %f \n", 
	   noeud->xc-noeud->size,noeud->xc+noeud->size, 
	   noeud->yc-noeud->size,noeud->yc+noeud->size ); */

  while (fa1 != NULL)
	{
	  numel = fa1->num;
/*	    printf(" ro, rd %f %f %f %f   \n", ro[0],ro[1],rd[0]*1.e6,rd[1]*1.e6); */
	  *intersect = ray_inter_seg(arbre,n_en_cours,numel,ro,rd,
				     nodray,cooray,arrivee);
/*	    printf(" on teste la facette %d, intersect=%d\n",numel,*intersect); */

	  if (*intersect==-1)
	    {
	      fa1 = fa1->suivant;
	      continue;
	    }
	  else if (typfacray[numel]==TYPRVITRE)
	    {
	      if (rd[0]*xnfray[0][numel]+rd[1]*xnfray[1][numel]<0.)
		for (i=0;i<nbande;i++) coeff_transp[i]*=transm[i][numel];
	      *intersect=-1;
	      fa1 = fa1->suivant;
	      continue;
	    }
	  else
	    {
	      /* printf(" >>> ivoitj_2d : intersection  avec l'element %d \n",*intersect); */
	      for (i=0;i<nbande;i++) coeff_transp[i]=0;
	      break;
	    }
	  
	} /* fin du while */




/* printf(" >> ivoitj_2d : test d'intersection : *intersect %d *arrivee %d\n", *intersect,*arrivee);*/
  if (!( *intersect>=0 || *arrivee ||(noeud==noeud_arr) ))
    {
      voxel_voisin_2d(&xv,&yv,noeud->xc,noeud->yc, noeud->sizx,noeud->sizy,
		      ro,rd,pt_arr,size_min);
      if (fabs(rd[0])<epsi && fabs(rd[1])<epsi)
	{
	  *arrivee=1;
	  *intersect=-1;
	  for (i=0;i<nbande;i++) coeff_transp[i]=1;
	  /*	printf(" >> ivoitj_2d : on sort par rd nul\n"); */
	}
      else
	{
	  noeud = arbre;
	  find_node_2d(&noeud,xv,yv);
	  if (in_rectan(xv,yv,dim_boite[0],dim_boite[1],dim_boite[2],dim_boite[3]))
	    ivoitj_2dST(arbre,noeud,noeud_arr,ro,rd,pt_arr,intersect,size_min,
			nodray,cooray,xnfray,arrivee,dim_boite,typfacray,
			transm,coeff_transp,nbande);
	  else
	    {
	      *intersect=-1;
	      for (i=0;i<nbande;i++) coeff_transp[i]=1;
	      /* printf(" >> ivoitj_2d : on est sorti de la boite %d\n"); */
	    }
	}
    }
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | ray_inter_seg                                                        |
  |         detection de l'intersection entre un rayon et un segment     |
  |======================================================================| */
int ray_inter_seg(struct node *arbre,struct node *n_en_cours,
		  int numel,double ro[],double rd[],
		  int **nodray,double **cooray,int *arrivee)
{
  int na,nb,i,j,k;
  double xa,ya,xb,yb,xc,yc,xab,yab,xac,yac;
  double a,b,c,d,t,den,epsi,xp,yp,zp;
  struct node *noeud;

  epsi=1.E-6;    

  na=nodray[0][numel]; nb=nodray[1][numel];
  xa=cooray[0][na]; ya=cooray[1][na];
  xb=cooray[0][nb]; yb=cooray[1][nb];

  a = ya-yb; b = xb-xa;  c = -(a*xa+b*ya);

  den = a*rd[0]+b*rd[1];


 /*  printf( " den %f \n",den);*/

  if (fabs(den)<epsi)
      return(-1);
  
  t = - (c  + a*ro[0]+b*ro[1] ) / den; 

  if (t<epsi)     
      return(-1);

  else if (t>(1+epsi)) 
      return(-1);



  xp = ro[0]+t*rd[0];  yp = ro[1]+t*rd[1]; 
  if (in_seg(xa,ya,xb,yb,xp,yp))
    {
      if (fabs(t-1.)<epsi) 
	{      
	  noeud = arbre;
	  find_node_2d(&noeud,xp,yp);
	  /*	  printf("recherche preliminaire : %d\n",noeud->name); */
	  for (i=-1;i<2;i=i+2)
	    for (j=-1;j<2;j=j+2)
	      {
		  noeud = arbre;
		  find_node_2d(&noeud,xp+epsi*i,yp+epsi*j);
/*		  printf("i,j,k,noeud->name n_en_cours %d %d %d %d  xp yp %f %f  \n",
			 i,j,k,noeud->name,n_en_cours,xp,yp); */

		  if (noeud == n_en_cours) 
		    {*arrivee = 1;}
		}

	  return(-1); 
	}
      else
	return(numel);
    }
  else
    return(-1);
}



/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | voxel_voisin_2d                                                      |
  |         Recherche du voxel voisin                                    |
  |======================================================================| */
void voxel_voisin_2d (double *xv,double *yv,double xc, double yc, 
		      double dx,double dy,double ro[], double rd[], 
		      double pt_arr[], double size_min)
{
  int it;
  double t,tt[2],orient[2],xpi,ypi,ddx,ddy,size,epsv,xn;
  
  size = size_min/4.;
  epsv=1.E-6;
  
  if (fabs(rd[0])<epsv)
    tt[0] = 1.E6;  
  else
    {
      t=(xc+dx-ro[0])/rd[0]; tt[0]=t; orient[0] = 1.;                  /* plan x=xc+dx */
      t=(xc-dx-ro[0])/rd[0]; if(t>tt[0]) {tt[0]=t; orient[0] = -1.;}   /* plan x=xc-dx */
    }
  
  if (fabs(rd[1])<epsv)
    tt[1] = 1.E6;  
  else
    {
      t=(yc+dy-ro[1])/rd[1]; tt[1]=t; orient[1] = 1.;                  /* plan y=yc+dy */
      t=(yc-dy-ro[1])/rd[1]; if(t>tt[1]) {tt[1]=t; orient[1] = -1.;}   /* plan y=yc-dy */
    }
  
  
  it = 0; t=tt[0];
  if (tt[1]<t) {it=1; t= tt[1];}
  
  xpi = ro[0] + t*rd[0]; ypi = ro[1] + t*rd[1];
  
  if (fabs(tt[0]-tt[1])<epsv)
    {ddx=orient[0]*size; ddy=orient[1]*size;}
  
  else if (it==0)
    {ddx=orient[0]*size; ddy=0. ;}
  
  else if (it==1)
    {ddx=0.; ddy=orient[1]*size;}
  
  if (fabs(xpi-ro[0])<epsv && fabs(ypi-ro[1])<epsv )
    {
      xn = sqrt(rd[0]*rd[0]+rd[1]*rd[1]);
      *xv = xpi + rd[0]/xn*size;        
      *yv = ypi + rd[1]/xn*size;    
    }
  else
    {
      *xv = xpi + ddx;         
      *yv = ypi + ddy;
    }
  
  ro[0] = xpi;
  ro[1] = ypi;
  
  rd[0] = pt_arr[0] - xpi;
  rd[1] = pt_arr[1] - ypi;
  /*    printf(">>> ivoitj_2d : intersection avec boite %f %f %f \n",xpi,ypi,zpi);  */
}









/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | ivoitj_3d                                                            |
  |         Detecter si deux point se voient                             |
  |======================================================================| */

void ivoitj_3d(struct node *arbre,struct node *noeud,struct node *noeud_arr,
	       double ro[],double rd[],double pt_arr[],int *intersect,
	       double size_min,int **nodray,double **cooray,int *arrivee,
	       double dim_boite[])
{
  struct element *fa1;
  int numel,i1;
  struct node  *n_en_cours;  /*    int n_en_cours;*/
  double xv,yv,zv,epsi;
  
  fa1 = noeud->lelement;
  n_en_cours =  noeud;   /*    n_en_cours =  noeud->name;*/
  epsi=1.E-6;
  
  /*  printf(">>> ivoitj_3d : on est dans la boite %d\n", noeud->name); 
    printf(">>> ivoitj_3d : xmin,xmax,ymin,ymax,zmin,zmax %f %f %f %f %f %f \n", 
	   noeud->xc-noeud->size,noeud->xc+noeud->size, 
	   noeud->yc-noeud->size,noeud->yc+noeud->size, 
	   noeud->zc-noeud->size,noeud->zc+noeud->size ); */

 /*   printf(" on est dans la boite %d \n", noeud->name); */
  if (fabs(rd[0])<epsi && fabs(rd[1])<epsi && fabs(rd[2])<epsi)
    *arrivee = 1;
  else
    while (fa1 != NULL)
      {
	numel = fa1->num;
	/*	    printf(" on teste la facette %d\n",numel); */
	
	/*      printf(" ro, rd %f %f %f %f %f %f  \n", ro[0],ro[1],ro[2],rd[0]*1.e6,rd[1]*1.e6,rd[2]*1.e6); */
	*intersect = ray_inter_triangle(arbre,n_en_cours,numel,ro,rd,
					nodray,cooray,arrivee);
	
	if (*intersect==-1)
	  {
	    fa1 = fa1->suivant;
	    continue;
	  }
	else
	  {
	    /* printf(" >>> ivoitj_3d : intersection  avec l'element %d \n",*intersect); */
	    break;
	  }
	
      } /* fin du while */
  
  
  
  
  /* printf(" >> ivoitj_3d : test d'intersection : *intersect %d *arrivee %d\n", *intersect,*arrivee);*/
  /*    if (!( *intersect || *arrivee ||(noeud->name==noeud_arr->name) )) */
  if (!( *intersect>=0 || *arrivee ||(noeud==noeud_arr) ))
    {
      voxel_voisin_3d(&xv,&yv,&zv,noeud->xc,noeud->yc,noeud->zc,
		      noeud->sizx,noeud->sizy,noeud->sizz,
		      ro,rd,pt_arr,size_min);
      if (fabs(rd[0])<epsi && fabs(rd[1])<epsi && fabs(rd[2])<epsi)
	{
	  *arrivee=1;
		*intersect=-1;
	  /*	printf(" >> ivoitj_3d : on sort par rd nul\n"); */
	}
      else
	{
	  noeud = arbre;
	  find_node_3d(&noeud,xv,yv,zv);
	  if (in_boite(xv,yv,zv,dim_boite[0],dim_boite[1],
		       dim_boite[2],dim_boite[3],
		       dim_boite[4],dim_boite[5]))
	    ivoitj_3d(arbre,noeud,noeud_arr,ro,rd,pt_arr,intersect,size_min,
		      nodray,cooray,arrivee,dim_boite);
	  else
	    {
	      *intersect=-1;
	      /*	    printf(" >> ivoitj_3d : on est sorti de la boite\n"); */
	    }
	}
    }
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | ivoitj_3d                                                            |
  |         Detecter si deux point se voient                             |
  |======================================================================| */

void ivoitj_3dST(struct node *arbre,struct node *noeud,struct node *noeud_arr,
		 double ro[],double rd[],double pt_arr[],int *intersect,
		 double size_min,int **nodray,double **cooray,double **xnfray,
		 int *arrivee,double dim_boite[],int *typfacray,
		 double **transm,double *coeff_transp,int nbande)
{
  struct element *fa1;
  int numel,i1,i;
  struct node  *n_en_cours;  /*    int n_en_cours;*/
  double xv,yv,zv,epsi;
  
  fa1 = noeud->lelement;
  n_en_cours =  noeud;   /*    n_en_cours =  noeud->name;*/
  epsi=1.E-6;
  
  /*  printf(">>> ivoitj_3d : on est dans la boite %d\n", noeud->name); 
    printf(">>> ivoitj_3d : xmin,xmax,ymin,ymax,zmin,zmax %f %f %f %f %f %f \n", 
	   noeud->xc-noeud->size,noeud->xc+noeud->size, 
	   noeud->yc-noeud->size,noeud->yc+noeud->size, 
	   noeud->zc-noeud->size,noeud->zc+noeud->size ); */

 /*   printf(" on est dans la boite %d \n", noeud->name); */
  if (fabs(rd[0])<epsi && fabs(rd[1])<epsi && fabs(rd[2])<epsi)
    *arrivee = 1;
  else
    while (fa1 != NULL)
      {
	numel = fa1->num;
	/*	    printf(" on teste la facette %d\n",numel); */
	
	/*      printf(" ro, rd %f %f %f %f %f %f  \n", ro[0],ro[1],ro[2],rd[0]*1.e6,rd[1]*1.e6,rd[2]*1.e6); */
	*intersect = ray_inter_triangle(arbre,n_en_cours,numel,ro,rd,
					nodray,cooray,arrivee);
	
	if (*intersect==-1)
	  {
	    fa1 = fa1->suivant;
	    continue;
	  }
	  else if (typfacray[numel]==TYPRVITRE)
	    {
	      if (rd[0]*xnfray[0][numel]+rd[1]*xnfray[1][numel]+rd[2]*xnfray[2][numel]<0.)
		for (i=0;i<nbande;i++) coeff_transp[i]*=transm[i][numel]; 
	      *intersect=-1;
	      fa1 = fa1->suivant;
	      continue;
	    }
	  else
	    {
	      /* printf(" >>> ivoitj_2d : intersection  avec l'element %d \n",*intersect); */
	      for (i=0;i<nbande;i++) coeff_transp[i]=0;
	      break;
	    }
      } /* fin du while */
  
  
  
  
  /* printf(" >> ivoitj_3d : test d'intersection : *intersect %d *arrivee %d\n", *intersect,*arrivee);*/
  /*    if (!( *intersect || *arrivee ||(noeud->name==noeud_arr->name) )) */
  if (!( *intersect>=0 || *arrivee ||(noeud==noeud_arr) ))
    {
      voxel_voisin_3d(&xv,&yv,&zv,noeud->xc,noeud->yc,noeud->zc,
		      noeud->sizx,noeud->sizy,noeud->sizz,
		      ro,rd,pt_arr,size_min);
      if (fabs(rd[0])<epsi && fabs(rd[1])<epsi && fabs(rd[2])<epsi)
	{
	  *arrivee=1;
	  *intersect=-1;
	  for (i=0;i<nbande;i++) coeff_transp[i]=1;
	  /*	printf(" >> ivoitj_3d : on sort par rd nul\n"); */
	}
      else
	{
	  noeud = arbre;
	  find_node_3d(&noeud,xv,yv,zv);
	  if (in_boite(xv,yv,zv,dim_boite[0],dim_boite[1],
		       dim_boite[2],dim_boite[3],
		       dim_boite[4],dim_boite[5]))
	    ivoitj_3dST(arbre,noeud,noeud_arr,ro,rd,pt_arr,intersect,size_min,
			nodray,cooray,xnfray,arrivee,dim_boite,typfacray,
			transm,coeff_transp,nbande);
	  else
	    {
	      *intersect=-1;
	      for (i=0;i<nbande;i++) coeff_transp[i]=1;
	      /*	    printf(" >> ivoitj_3d : on est sorti de la boite\n"); */
	    }
	}
    }
}






/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | ray_inter_triangle                                                   |
  |         detection de l'intersection entre un rayon et un triangle    |
  |======================================================================| */
int ray_inter_triangle(struct node *arbre,struct node  *n_en_cours,
		       int numel,double ro[],double rd[],
		       int **nodray,double **cooray,int *arrivee)
{
  int na,nb,nc,i,j,k;
  double xa,ya,za,xb,yb,zb,xc,yc,zc,xab,yab,zab,xac,yac,zac;
  double a,b,c,d,t,den,epsi,xp,yp,zp,xn,den1;
  struct node *noeud;

  epsi=1.E-4;    

  na=nodray[0][numel]; nb=nodray[1][numel]; nc=nodray[2][numel]; 
  xa=cooray[0][na]; ya=cooray[1][na];       za=cooray[2][na];
  xb=cooray[0][nb]; yb=cooray[1][nb];       zb=cooray[2][nb];
  xc=cooray[0][nc]; yc=cooray[1][nc];       zc=cooray[2][nc];

  /* equation du plan du triangle */
  xab = xb-xa; yab = yb-ya; zab = zb-za;
  xac = xc-xa; yac = yc-ya; zac = zc-za;
  a = yab*zac-zab*yac;
  b = zab*xac-xab*zac;
  c = xab*yac-yab*xac;
  xn=sqrt(a*a+b*b+c*c);
  a /=xn; b /=xn; c /=xn;
  d = -(a*xa+b*ya+c*za);

  den = a*rd[0]+b*rd[1]+c*rd[2];
  den1=den/(sqrt(rd[0]*rd[0]+rd[1]*rd[1]+rd[2]*rd[2]));



 /*  printf( " den %f \n",den);*/

  if (fabs(den1)<epsi)
      return(-1);
  
  t = - (d  + a*ro[0]+b*ro[1]+c*ro[2] ) / den;   
  
/*  printf(" >> ray_inter_tria t = %f \n", t);   */

  if (t<epsi)     /* ==> le plan du triangle est derriere => pas d'intersection */
      return(-1);

  else if (t>(1+epsi)) /* ==> le plan du triangle est trop loin  => pas d'intersection */
      return(-1);


  xp = ro[0]+t*rd[0];  yp = ro[1]+t*rd[1]; zp = ro[2]+t*rd[2]; /* intersection */
  if (in_triangle(a,b,c,d,xa,ya,za,xab,yab,zab,xac,yac,zac,xp,yp,zp))
    {
      if (fabs(t-1.)<epsi) 
	{      
	  noeud = arbre;
	  find_node_3d(&noeud,xp,yp,zp);
/*	  printf("recherche preliminaire : %d\n",noeud->name); */
	  for (i=-1;i<2;i=i+2)
	    for (j=-1;j<2;j=j+2)
	      for (k=-1;k<2;k=k+2)
		{
		  noeud = arbre;
		  find_node_3d(&noeud,xp+epsi*i,yp+epsi*j,zp+epsi*k);
/*		  printf("i,j,k,noeud->name n_en_cours %d %d %d %d %d xp yp zp %f %f %f \n",
			 i,j,k,noeud->name,n_en_cours,xp,yp,zp); */

		  if (noeud == n_en_cours) 
		    {*arrivee = 1;}
		}

	  return(-1); 
	}
      else
	return(numel);
    }
  else
    return(-1);
}



/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | voxel_voisin_3d                                                      |
  |         Recherche du voxel voisin                                    |
  |======================================================================| */
void voxel_voisin_3d (double *xv,double *yv, double *zv, 
		      double xc, double yc, double zc,
		      double dx,double dy,double dz,
		      double ro[], double rd[], double pt_arr[],
		      double size_min)
{
  int it;
  double t,tt[3],orient[3],xpi,ypi,zpi,ddx,ddy,ddz,size,epsv,xn;
  
  size = size_min/4.;
  epsv=1.E-10;
  
  if (fabs(rd[0])<epsv)
    tt[0] = 1.E6;  
  else
    {
      t=(xc+dx-ro[0])/rd[0]; tt[0]=t; orient[0] = 1.;                  /* plan x=xc+dx */
      t=(xc-dx-ro[0])/rd[0]; if(t>tt[0]) {tt[0]=t; orient[0] = -1.;}   /* plan x=xc-dx */
    }
  
  if (fabs(rd[1])<epsv)
    tt[1] = 1.E6;  
  else
    {
      t=(yc+dy-ro[1])/rd[1]; tt[1]=t; orient[1] = 1.;                  /* plan y=yc+dy */
      t=(yc-dy-ro[1])/rd[1]; if(t>tt[1]) {tt[1]=t; orient[1] = -1.;}   /* plan y=yc-dy */
    }
  
  if (fabs(rd[2])<epsv)
    tt[2] = 1.E6;  
  else
    {
      t=(zc+dz-ro[2])/rd[2]; tt[2]=t; orient[2] = 1.;                  /* plan z=zc+dz */
      t=(zc-dz-ro[2])/rd[2]; if(t>tt[2]) {tt[2]=t; orient[2] = -1.;}   /* plan z=zc-dz */
    }
  
  
  it = 0; t=tt[0];
  if (tt[1]<t) {it=1; t= tt[1];}
  if (tt[2]<t) {it=2; t= tt[2];}
  
  xpi = ro[0] + t*rd[0]; ypi = ro[1] + t*rd[1]; zpi = ro[2] + t*rd[2];
  
  if (fabs(tt[0]-tt[1])<epsv && fabs(tt[2]-tt[1])<epsv)
    {ddx=orient[0]*size; ddy=orient[1]*size; ddz=orient[2]*size;}
  
  else if (fabs(tt[0]-tt[1])<epsv && fabs(tt[0]-t)<epsv)
    {ddx=orient[0]*size; ddy=orient[1]*size; ddz=0.;}
  
  else if (fabs(tt[2]-tt[1])<epsv && fabs(tt[1]-t)<epsv)
    {ddx=0.; ddy=orient[1]*size; ddz=orient[2]*size;}
  
  else if (fabs(tt[2]-tt[0])<epsv && fabs(tt[0]-t)<epsv)
    {ddx=orient[0]*size; ddy=0. ; ddz=orient[2]*size;}
  
  else if (it==0)
    {ddx=orient[0]*size; ddy=0. ; ddz=0.;}
  
  else if (it==1)
    {ddx=0.; ddy=orient[1]*size; ddz=0.;}
  
  else if (it==2)
    {ddx=0.; ddy=0. ; ddz=orient[2]*size;}
  
  
  if (fabs(xpi-ro[0])<epsv && fabs(ypi-ro[1])<epsv  && fabs(zpi-ro[2])<epsv )
    {
      xn = sqrt(rd[0]*rd[0]+rd[1]*rd[1]+rd[2]*rd[2]);
      *xv = xpi + rd[0]/xn*size;        
      *yv = ypi + rd[1]/xn*size;    
      *zv = zpi + rd[2]/xn*size;    
    }
  else
    {
      *xv = xpi + ddx;         
      *yv = ypi + ddy;
      *zv = zpi + ddz;
    }
  
  ro[0] = xpi;
  ro[1] = ypi;
  ro[2] = zpi;
  
  rd[0] = pt_arr[0] - xpi;
  rd[1] = pt_arr[1] - ypi;
  rd[2] = pt_arr[2] - zpi;   
  /*    printf(">>> ivoitj_3d : intersection avec boite %f %f %f \n",xpi,ypi,zpi);  */
}


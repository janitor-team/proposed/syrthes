/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

#include "syr_usertype.h"
#include "syr_bd.h"
#include "syr_tree.h"
#include "syr_option.h"
#include "syr_abs.h"
#include "syr_proto.h"
#include "syr_const.h"
#include "syr_parall.h"

/* # include <mpi.h> */

extern struct Affichages affich;
extern struct Performances perfo;

double c2=1.4388e-2, tsoleil=5777.;

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |  initialisation pour le solaire                                      |
  |======================================================================| */
void initsolaire(struct Soleil *soleil,struct Lieu lieu,struct node **arbre,
		 struct Maillage maillnodray,struct ProphyRay phyray,
		 double *size_min,double *dim_boite,
		 struct SDparall sdparall)
{
  int i,j;
  double tcpu1,tcpu2;
  int nbeltmax_tree=30;


  tcpu1=cpusyrt();

  soleil->emissi=(double**)malloc(phyray.bandespec.nb*sizeof(double*));
  soleil->transm=(double**)malloc(phyray.bandespec.nb*sizeof(double*));
  soleil->reflec=(double**)malloc(phyray.bandespec.nb*sizeof(double*));
  soleil->absorb=(double**)malloc(phyray.bandespec.nb*sizeof(double*));
  
  for (j=0;j<phyray.bandespec.nb;j++)
    { 
      soleil->emissi[j]=(double*)malloc(maillnodray.nelem*sizeof(double));
      soleil->transm[j]=(double*)malloc(maillnodray.nelem*sizeof(double));
      soleil->reflec[j]=(double*)malloc(maillnodray.nelem*sizeof(double));
      soleil->absorb[j]=(double*)malloc(maillnodray.nelem*sizeof(double));
    }
  verif_alloue_double2d(phyray.bandespec.nb,"alloueray",soleil->emissi);
  verif_alloue_double2d(phyray.bandespec.nb,"alloueray",soleil->transm);
  verif_alloue_double2d(phyray.bandespec.nb,"alloueray",soleil->reflec);
  verif_alloue_double2d(phyray.bandespec.nb,"alloueray",soleil->absorb);
  
  perfo.mem_ray+=phyray.bandespec.nb*maillnodray.nelem*4*sizeof(double);


  constantes_soleil(soleil);

  (*arbre)= (struct node *)malloc(sizeof(struct node));
  if ((*arbre)==NULL) {
    if (SYRTHES_LANG == FR)
	      printf(" ERREUR main : probleme d'allocation memoire\n");
    else if (SYRTHES_LANG == EN)
      printf(" ERROR main : memory allocation problem\n");
    syrthes_exit(1);
  }

  soleil->diffus=(double**)malloc(phyray.bandespec.nb*sizeof(double*));
  soleil->direct=(double**)malloc(phyray.bandespec.nb*sizeof(double*));

  for (i=0;i<phyray.bandespec.nb;i++)  
    { 
      soleil->direct[i]=(double*)malloc(maillnodray.nelem*sizeof(double));
      soleil->diffus[i]=(double*)malloc(maillnodray.nelem*sizeof(double)); 
    }
  verif_alloue_double2d(phyray.bandespec.nb,"syrthes",soleil->direct);
  verif_alloue_double2d(phyray.bandespec.nb,"syrthes",soleil->diffus);
  
  if (maillnodray.ndim==2)
    { build_quadtree_1d(*arbre,maillnodray.npoin,maillnodray.nelem,
			maillnodray.node,maillnodray.coord,size_min,dim_boite,nbeltmax_tree);
      cnor_2d(maillnodray);
    }
  else
    { boite(maillnodray.npoin,maillnodray.coord,dim_boite);
      build_octree(*arbre,maillnodray.npoin,maillnodray.nelem,
		   maillnodray.node,maillnodray.coord,size_min,dim_boite,nbeltmax_tree);
      cnor_3d(maillnodray);
    }

  horizon(maillnodray,soleil,lieu);
  
  tcpu2=cpusyrt();
  perfo.cpu_init_soleil=tcpu2-tcpu1;
  if (syrglob_nparts==1 ||syrglob_rang==0)
    if (SYRTHES_LANG == FR)
      printf("\n       >>>> Temps des initialisations pour le rayonnement solaire: %f <<<<\n",
	     perfo.cpu_init_soleil); 
    else if (SYRTHES_LANG == EN)
      printf("\n       >>>> Time for solar radiation initialisations : %f <<<<\n",
	     perfo.cpu_init_soleil); 
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |  constantes_soleil                                                   |
  |======================================================================| */
void resol_solaire(struct Soleil *soleil,struct Lieu *lieu,struct node *arbre,
		   struct Maillage maillnodray,struct ProphyRay phyray,
		   struct PasDeTemps pasdetemps,struct Vitre vitre,
		   struct Meteo meteo,struct Myfile myfile,struct PropInfini propinf,
		   double size_min,double *dim_boite,struct SDparall sdparall)

{
  double tcpu1,tcpu2;

  char *nommois_FR[12]={"janvier","fevrier","mars","avril","mai","juin",
	      "juillet","aout","septembre","octobre","novembre","decembre"};

  char *nommois_EN[12]={"january","february","marsh","april","mai","june",
	      "july","august","september","october","november","december"};

  tcpu1=cpusyrt();
  if (soleil->anglect_actif)
    {
      if (maillnodray.ndim==2)
	flux2d_soleil_anglect(*soleil,maillnodray,
			      pasdetemps.tempss,vitre,phyray,
			      arbre,size_min,dim_boite);
      else
	flux3d_soleil_anglect(*soleil,maillnodray,
			      pasdetemps.tempss,vitre,phyray,
			      arbre,size_min,dim_boite);
    }
  else
    {
      mise_a_jour_date(lieu,pasdetemps.rdtts);
      if (syrglob_nparts==1 ||syrglob_rang==0)
	if (SYRTHES_LANG == FR)
	  printf("\n Date initiale + %e secondes : le %d %s a %d h %d\n\n",
		 pasdetemps.tempss,lieu->jour,nommois_FR[lieu->mois-1],
		 lieu->heure,lieu->minute);
	else if (SYRTHES_LANG == EN)
	  printf("\n Initial time + %e seconds : the %d %s a %d h %d\n\n",
		 pasdetemps.tempss,lieu->jour,nommois_EN[lieu->mois-1],
		 lieu->heure,lieu->minute);
      
      hauteur_soleil(*lieu,soleil);
      if (maillnodray.ndim==2)
	flux2d_soleil(*lieu,*soleil,meteo,maillnodray,propinf.fdfnp1,
		      pasdetemps,vitre,phyray,arbre,size_min,dim_boite);
      else
	flux3d_soleil(*lieu,*soleil,meteo,maillnodray,propinf.fdfnp1,
		      pasdetemps,vitre,phyray,arbre,size_min,dim_boite);
    }

  tcpu2=cpusyrt();
  perfo.cpu_1iter_soleil=tcpu2-tcpu1;
  perfo.cpu_iter_soleil+=perfo.cpu_1iter_soleil;
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |  constantes_soleil                                                   |
  |======================================================================| */
void constantes_soleil(struct Soleil *soleil)
{
  soleil->I0=1380;
  soleil->Rterre=6378140.;
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |  horizon                                                             |
  |======================================================================| */
void horizon(struct Maillage maillnodray,
	     struct Soleil *soleil,struct Lieu lieu)
{
  int i,iz;
  double z=0;

  if (maillnodray.ndim==2) iz=1; else iz=2;

  if (maillnodray.ndim==2 && !lieu.vertical) z=lieu.hauteur;
  else
    for (i=0;i<maillnodray.nelem;i++)
      if (maillnodray.coord[1][iz]>z) z=maillnodray.coord[1][iz];

  if (z<1.)
    soleil->sphere=3500.;
  else
    {
      soleil->sphere=2*soleil->Rterre*sqrt(1.-soleil->Rterre/(soleil->Rterre+z));
    }

  if (syrglob_nparts==1 ||syrglob_rang==0)
    if (SYRTHES_LANG == FR)
      printf("\n *** horizon : rayon de la sphere =%f\n",soleil->sphere);
    else if (SYRTHES_LANG == EN)
      printf("\n *** horizon : sphere radius =%f\n",soleil->sphere);
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |   mise_a_jour_date                                                   |
  |======================================================================| */
void mise_a_jour_date(struct Lieu *lieu,double dt)
{
  int hh,mm,ss,jj,j,i,dd;
  int jmois[12]={31,28,31,30,31,30,31,31,30,31,30,31};

  s_en_h(dt,&hh,&mm,&ss); 
  jj=(int)(hh/24); hh-=jj*24;
  /* lieu->day+=jj;   if(lieu->day>365) lieu->day-=365; */

  lieu->seconde+=ss; if (lieu->seconde>59) {mm++; lieu->seconde-=60;}
  lieu->minute+=mm; if (lieu->minute>59) {hh++; lieu->minute-=60;}
  lieu->heure+=hh; if (lieu->heure>23) {jj++; lieu->heure-=24;}
  lieu->jour+=jj;
  while (lieu->jour>jmois[lieu->mois-1])
    {
      lieu->jour-=jmois[lieu->mois-1];
      lieu->mois++; if (lieu->mois>12) lieu->mois=1;
    }
  for (i=dd=0;i<lieu->mois-1;i++) dd+=jmois[i];
  lieu->day=dd+lieu->jour;   if(lieu->day>365) lieu->day-=365;
}
 
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |   hauteur_soleil                                                     |
  |======================================================================| */
void hauteur_soleil(struct Lieu lieu,struct Soleil *soleil)
{
  double lat,lon,tucivil,tsm,wd,et,tsv,tsvastro;
  double ah,x,decl,sinh,h,sina,a,ttest,decal;
  int hh,mm,ss,hhh;

  /* longitude : comptee positive vers l'ouest */
  /* latitude  : comptee positive vers le nord */
  lat=(lieu.latituded+lieu.latitudem/60.0)*Pi/180.; 
  lon=(lieu.longituded+lieu.longitudem/60.0)*Pi/180. ;

  /* que pour la france */
  if (lieu.mois>=4 || lieu.mois<=10) decal=7200; else decal=3600;

  tucivil=lieu.heure*3600.+lieu.minute*60.+lieu.seconde;
  /*  tucivil+=lieu.fuseau*3600.;
      tucivil+=lieu.decalage*3600.; */
  tucivil-=decal;  /* ??????????? attention : et si il est minuit ? passer au jour precedent ? */
  s_en_h(tucivil,&hh,&mm,&ss);
  if (syrglob_nparts==1 ||syrglob_rang==0)
    if (SYRTHES_LANG == FR)
      printf(">>> hauteur_soleil : tucivil=%f (h): %d %d %d",tucivil,hh,mm,ss);
    else if (SYRTHES_LANG == EN)
      printf(">>> hauteur_soleil : tucivil=%f (h): %d %d %d",tucivil,hh,mm,ss);

  /*correction de longitude (positive si la longitude est negative) */
  tsm=-24.*3600.*lon/(2*Pi);
  s_en_h(abs(tsm),&hh,&mm,&ss);
  if (syrglob_nparts==1 ||syrglob_rang==0){
    if (tsm>=0){
      if (SYRTHES_LANG == FR)
	printf(">>> hauteur_soleil : correction longitude (h): %dh %dmn  %ds \n",hh,mm,ss);
      else if (SYRTHES_LANG == EN)
	printf(">>> hauteur_soleil : longitudinal correction (h): %dh %dmn %ds \n",hh,mm,ss);
    }
    else{
      if (SYRTHES_LANG == FR)
	printf(">>> hauteur_soleil : correction longitude (h): - %dh %dmn %ds \n",hh,mm,ss);
      else if (SYRTHES_LANG == EN)
	printf(">>> hauteur_soleil : longitudinal correction (h): - %dh %dmn %ds \n",hh,mm,ss);
    }
  }


  if (syrglob_nparts==1 ||syrglob_rang==0)
    if (SYRTHES_LANG == FR)
      printf(">>> hauteur_soleil - numero de jour %d",lieu.day);
    else if (SYRTHES_LANG == EN)
      printf(">>> hauteur_soleil - day number %d",lieu.day);

  wd=2*Pi/366.*lieu.day;
  et=-( 0.0002-0.4197*cos(wd)+3.2265*cos(2*wd)+0.0903*cos(3*wd)
       + 7.3509*sin(wd)+9.3912*sin(2*wd)+0.3361*sin(3*wd) );
  et*=60;
  s_en_h(et,&hh,&mm,&ss);

  if (syrglob_nparts==1 ||syrglob_rang==0)
    if (SYRTHES_LANG == FR)
      printf(">>> equation du temps et (h) %d %d %d\n",hh,mm,ss);
    else if (SYRTHES_LANG == EN)
      printf(">>> time equation and (h) %d %d %d\n",hh,mm,ss);

  ttest=tucivil+tsm-et; s_en_h(ttest,&hhh,&mm,&ss);

  tsvastro=tucivil+tsm-et-12*3600.;
  /*  if (tsvastro<0.) tsvastro+=24*3600; */
  s_en_h(tsvastro,&hh,&mm,&ss);

  if (syrglob_nparts==1 ||syrglob_rang==0)
    if (SYRTHES_LANG == FR)
      printf(">>> hauteur_soleil : temps - tsvastro (h): %d %d %d",hh,mm,ss);
    else if (SYRTHES_LANG == EN)
      printf(">>> hauteur_soleil - time -tsvastro (h): %d %d %d",hh,mm,ss);

  /*  ah=15.*tsvastro/3600.; */
  ah=15.*tsvastro/3600.; 
  if (SYRTHES_LANG == FR)      printf("  angle horaire=%f  ",ah);
  else if (SYRTHES_LANG == EN) printf("  hour angle=%f  ",ah);

  ah*=Pi/180.;

  x=30*lieu.mois+lieu.jour-202;
  decl=23.45*cos(x*Pi/180.);
/*   if (syrglob_nparts==1 ||syrglob_rang==0) printf("declinaison=%f\n",decl); */
  decl*=Pi/180.;
 
  sinh=sin(lat)*sin(decl)+cos(lat)*cos(decl)*cos(ah);
  soleil->h=h=asin(sinh);
  sina=cos(decl)*sin(ah)/cos(h);
  soleil->a=a=asin(sina); 

  if (syrglob_nparts==1 ||syrglob_rang==0)
    if (SYRTHES_LANG == FR){
      printf(">>> hauteur_soleil - hauteur=%f",h*180/Pi);
      printf(" azimut=%f \n",soleil->a*180/Pi);
    }
    else if (SYRTHES_LANG == EN){
      printf(">>> hauteur_soleil - height=%f",h*180/Pi);
      printf(" azimuthal position=%f \n",soleil->a*180/Pi);
    }

  soleil->x=-(cos(decl)*sin(ah));
  soleil->y=-(cos(decl)*sin(lat)*cos(ah)-sin(decl)*cos(lat));
  soleil->z=sin(lat)*sin(decl)+cos(lat)*cos(decl)*cos(ah);

  if (syrglob_nparts==1 ||syrglob_rang==0)
    if (SYRTHES_LANG == FR)
      printf(">>> hauteur_soleil - position soleil : %f %f %f\n",soleil->x,soleil->y,soleil->z);
    else if (SYRTHES_LANG == EN)
      printf(">>> hauteur_soleil - solar position : %f %f %f\n",soleil->x,soleil->y,soleil->z);
  

  /* correction de distance Terre/soleil */
  /*  soleil->C=1.+0.034*cos(30*(lieu.mois-1)+lieu.jour); */
  soleil->C=1.+0.034*cos(lieu.day);
  /* printf(">>> hauteur_soleil - distance Terre/soleil=%f\n",soleil->C); */


}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |   s_en_h                                                             |
  |======================================================================| */
void s_en_h(double sec,int *h,int *m, int *s)
{
  *h=(int)(sec/3600);
  sec-=(*h*3600);
  *m=(int)(sec/60);
  *s=sec-(*m*60);
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |   proj2d_soleil                                                      |
  |======================================================================| */
void proj2d_soleil(struct Lieu lieu,struct Soleil soleil,
		   double *xs,double *ys)
{
  double x,y,xn;

  if (lieu.vertical)
    {
      x=soleil.x*cos(lieu.gama)+soleil.y*sin(lieu.gama); y=soleil.z;
      xn=sqrt(x*x+y*y); /* x/=xn; y/=xn; */
      *xs=soleil.sphere*x;
      *ys=soleil.sphere*y;
    }
  else
    {
      x=soleil.x; y=soleil.y;
      xn=sqrt(x*x+y*y);  /* x/=xn; y/=xn; */
      *xs=soleil.sphere*x;
      *ys=soleil.sphere*y;
    }

  if (syrglob_nparts==1 ||syrglob_rang==0)
    printf(" >>>proj2d_soleil  3D= %f  %f %f 2D= %f %f\n",
	   soleil.x,soleil.y,soleil.z,*xs/soleil.sphere,*ys/soleil.sphere);
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |   proj2d_soleil                                                      |
  |======================================================================| */
void proj3d_soleil(struct Lieu lieu,struct Soleil soleil,
		   double *xs,double *ys,double *zs)
{
  *xs=soleil.sphere*soleil.x;
  *ys=soleil.sphere*soleil.y;
  *zs=soleil.sphere*soleil.z;

  if (syrglob_nparts==1 ||syrglob_rang==0)
    if (SYRTHES_LANG == FR)
      printf(" >>> proj3d_soleil : soleil projete : %f %f %f\n",*xs+lieu.xc,*ys+lieu.yc,*zs+lieu.zc);
    else if (SYRTHES_LANG == EN)
      printf(" >>> proj3d_soleil : solar projection : %f %f %f\n",*xs+lieu.xc,*ys+lieu.yc,*zs+lieu.zc);
}


  
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | derriere_soleil2d                                                    |
  |======================================================================| */
int derriere_soleil2d (double xn,double yn,double us,double vs)
{
  if (xn*us+yn*vs<0.) return 0;  /* les faces  se voient */
  else 
    {
      /* printf(" >>> derriere_soleil2d : le soleil est derriere \n"); */
      return 1;
    }
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | derriere_soleil2d                                                    |
  |======================================================================| */
int derriere_soleil3d (double xn,double yn,double zn,
		       double us,double vs,double ws)
{
  if (xn*us+yn*vs+zn*ws<0.) return 0;  /* les faces  se voient */
  else 
    {
      if (syrglob_nparts==1 ||syrglob_rang==0)
	if (SYRTHES_LANG == FR)
	  printf(" >>> derriere_soleil3d : le soleil est derriere \n");
	else if (SYRTHES_LANG == EN)
	  printf(" >>> derriere_soleil3d : the sun is behind\n");
      return 1;
    }
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |  flux2d_soleil                                                       |
  |======================================================================| */
void flux2d_soleil(struct Lieu lieu,struct Soleil soleil,struct Meteo meteo,
		   struct Maillage maillnodray,
		   double *fdfnp1,struct PasDeTemps pasdetemps,
		   struct Vitre vitre,struct ProphyRay phyray,
		   struct node *arbre,double size_min,double dim_boite[])
{
  int i,n,nn,na,nb,nv;
  int intersect,arrivee;
  double a,b,c,x,y,xa,ya,xb,yb,xn,yn,sinh,xs,ys,us,vs,xx,xm,ym,r,xi,ybas;
  double xss,yss,zss,rx,ry,teta;
  double *fdirecth,*fdiffush,*coeff_transp,cc,w1,w2;
  double ro[2],rd[2],pt_arr[2],d,teta_lim;
  double eps=1.e-6;
  struct node *noeud_dep,*noeud_arr;
  double **cooray,**xnf;
  int *typfacray,**nodray;

  cooray=maillnodray.coord;
  xnf=maillnodray.xnf;
  typfacray=maillnodray.type;
  nodray=maillnodray.node;

  fdirecth=(double*)malloc(phyray.bandespec.nb*sizeof(double));     verif_alloue_double1d("flux2d_soleil",fdirecth);
  fdiffush=(double*)malloc(phyray.bandespec.nb*sizeof(double));     verif_alloue_double1d("flux2d_soleil",fdiffush);
  coeff_transp=(double*)malloc(phyray.bandespec.nb*sizeof(double)); verif_alloue_double1d("flux2d_soleil",coeff_transp);
  sinh=sin(soleil.h);

  for (nn=0;nn<phyray.bandespec.nb;nn++)
    for (n=0;n<maillnodray.nelem;n++) soleil.direct[nn][n]=soleil.diffus[nn][n]=0;

  if (!lieu.vertical)
    {
      teta_lim=0.5*acos(soleil.Rterre/(soleil.Rterre+lieu.hauteur));
      if (sinh<0.1 || -teta_lim>soleil.h || soleil.h>Pi+teta_lim) 
	{
	  if (syrglob_nparts==1 ||syrglob_rang==0)
	    if (SYRTHES_LANG == FR){
	      if (meteo.actif) printf("flux2d_soleil : fluxdirectmeteo= 0 0 vrai= 0 0  nuit \n");
	      else printf("flux2d_soleil fluxdirectformule= 0 0 vrai= 0 0  nuit \n");
	    }
	    else if (SYRTHES_LANG == EN){
	      if (meteo.actif) printf("flux2d_soleil : fluxdirectmeteo= 0 0 right= 0 0  night \n");
	      else printf("flux2d_soleil fluxdirectformule= 0 0 right= 0 0  night \n");
	    }
	  return;
	}
    }

  proj2d_soleil(lieu,soleil,&xs,&ys);
  us=-xs; vs=-ys; xx=sqrt(us*us+vs*vs); us/=xx; vs/=xx;
  if (meteo.actif) 
    {
      interp_meteo(pasdetemps.tempss,meteo,fdirecth,fdiffush,phyray.bandespec.nb);
        if (syrglob_nparts==1 ||syrglob_rang==0)
	  printf("flux2d_soleil fluxdirectmeteo= %f %f",fdirecth[0],fdirecth[1]);
      if (sinh>0.) for (i=0;i<phyray.bandespec.nb;i++)  fdirecth[i]=min(1350.,fdirecth[i]/sinh); 
      if (syrglob_nparts==1 ||syrglob_rang==0) printf(" vrai= %f %f\n",fdirecth[0],fdirecth[1]);
    }
  else
    if (phyray.bandespec.nb==1)
      {
	fdirecth[0]=soleil.I0*soleil.C*soleil.A*exp(-soleil.B/sinh);
	fdiffush[0]=soleil.I0*soleil.C*sinh*(0.271-0.2939*soleil.A*exp(-soleil.B/sinh));
      }
    else
      {
	for (nn=0;nn<phyray.bandespec.nb;nn++)
	  {
	    w1=wiebel(c2/(phyray.bandespec.borneinf[nn]*tsoleil));
	    w2=wiebel(c2/(phyray.bandespec.bornesup[nn]*tsoleil));
	    /* if (nn==0) {w1=0; w2=0.56;} else {w1=0; w2=0.44;} */ /* specific cas TD ???????????????????? */
	    fdirecth[nn]=soleil.I0*(w2-w1)*soleil.C*soleil.A*exp(-soleil.B/sinh);
	    fdiffush[nn]=soleil.I0*(w2-w1)*soleil.C*sinh*(0.271-0.2939*soleil.A*exp(-soleil.B/sinh));
	  }
	if (syrglob_nparts==1 ||syrglob_rang==0)
	  printf("flux2d_soleil fluxdirectformule= %f %f %f %f\n",
		 fdirecth[0],fdirecth[1],sinh,soleil.C);
      }

  user_solaire(fdirecth,fdiffush,phyray.bandespec.nb,pasdetemps);


  /* calcul des proprietes en fn de l'angle d'incidence */
  for (nn=0;nn<phyray.bandespec.nb;nn++)
    for (i=0;i<maillnodray.nelem;i++)
      {
	soleil.emissi[nn][i]=phyray.emissi[nn][i];
	soleil.reflec[nn][i]=phyray.reflec[nn][i];
	soleil.absorb[nn][i]=phyray.absorb[nn][i];
	soleil.transm[nn][i]=phyray.transm[nn][i];
      }
  for (i=0;i<maillnodray.nelem;i++)
    {
 	  xn=xnf[0][i]; yn=xnf[1][i];
	  na=nodray[0][i]; nb=nodray[1][i];
	  xa=cooray[0][na]; ya=cooray[1][na];
	  xb=cooray[0][nb]; yb=cooray[1][nb];
	  xm=0.5*(xa+xb); ym=0.5*(ya+yb); 
	  rx=xs-xm; ry=ys-ym;
	  d=sqrt(rx*rx+ry*ry); rx/=d; ry/=d;
	  teta=acos(rx*xn+ry*yn);
	  user_propincidence(i,phyray.bandespec.nb,teta,
			     soleil.emissi,soleil.reflec,soleil.absorb,soleil.transm,
			     phyray.emissi,phyray.reflec,phyray.absorb,phyray.transm);
    }

  for (n=0;n<maillnodray.nelem;n++)
    if (typfacray[n]!=TYPRTI && typfacray[n]!=TYPRFI)
      {
	/*	  printf(">>> flux2d_soleil facette %d \n",n); */
	xn=xnf[0][n]; yn=xnf[1][n];
	na=nodray[0][n]; nb=nodray[1][n];
	xa=cooray[0][na]; ya=cooray[1][na];
	xb=cooray[0][nb]; yb=cooray[1][nb];
	if (lieu.vertical)
	  ym=0.5*(ya+yb);
	else
	  ym=lieu.hauteur;
	teta_lim=0.5*acos(soleil.Rterre/(soleil.Rterre+ym));
	if (-teta_lim<soleil.h && soleil.h<Pi+teta_lim)
	  {
	    sinh=sin(soleil.h+teta_lim);
	    if (!derriere_soleil2d(xn,yn,us,vs))
	      {
		ro[0]=0.5*(xa+xb);     ro[1]=0.5*(ya+yb);
		d=(xs-ro[0])*(xs-ro[0])+(ys-ro[1])*(ys-ro[1]);
		/* pt_arr -> intersection du ray avec la boite */
		a=-vs; b=us; c=-(a*ro[0]+b*ro[1]); /* eq droite du ray */ 
		if (fabs(xs)<eps)   /* ray // y -> inter avec y=cte */
		  {
		    pt_arr[0]=ro[0]; 
		    if (ys>0) pt_arr[1]=dim_boite[3]*0.995; 
		    else pt_arr[1]=dim_boite[2]*0.995;
		  }
		else 
		  {
		    x=dim_boite[0]*0.995;  y=(-c-a*x)/b;
		    if (lieu.vertical) ybas=0.; else ybas=dim_boite[2];
		    if (y<ybas || y>dim_boite[3]*0.995 || (xs-x)*(xs-x)+(ys-y)*(ys-y)>d ||
			xs*(x-ro[0])+ys*(y-ro[1])<0.)
		      {
			x=dim_boite[1]*0.995; y=(-c-a*x)/b;
			if (y<ybas || y>dim_boite[3]*0.995 || (xs-x)*(xs-x)+(ys-y)*(ys-y)>d ||
			    xs*(x-ro[0])+ys*(y-ro[1])<0)
			  {
			    y=dim_boite[3]*0.995; x=(-c-b*y)/a;
			    if (x<dim_boite[0] || x>dim_boite[1]*0.995 || (xs-x)*(xs-x)+(ys-y)*(ys-y)>d ||
				xs*(x-ro[0])+ys*(y-ro[1])<0)
			      y=dim_boite[2]*0.995; x=(-c-b*y)/a;
			  }
		      }
		    pt_arr[0]=x; pt_arr[1]=y;
		  }
		
		rd[0]=pt_arr[0]-ro[0];  rd[1]=pt_arr[1]-ro[1];
		noeud_dep=arbre; noeud_arr=arbre;
		find_node_2d (&noeud_dep,ro[0],ro[1]);
		find_node_2d (&noeud_arr,pt_arr[0],pt_arr[1]);
		arrivee=0; intersect=-1; 
		for (nn=0;nn<phyray.bandespec.nb;nn++) coeff_transp[nn]=1.;
		if (vitre.nelem)
		  ivoitj_2dST(arbre,noeud_dep,noeud_arr,ro,rd,pt_arr,
			      &intersect,size_min,nodray,cooray,xnf,&arrivee,dim_boite,
			      typfacray,soleil.transm,coeff_transp,phyray.bandespec.nb);
		else
		  ivoitj_2d(arbre,noeud_dep,noeud_arr,ro,rd,pt_arr,
			    &intersect,size_min,nodray,cooray,&arrivee,dim_boite);
		if (intersect==-1)
		  {
		    xi=acos(yn); if (yn<0) xi=-xi; 
		    /* cc=cos(soleil.h)*sin(xi)*cos(soleil.a-lieu.gama)+sinh*cos(xi); */
		    /* cc=-us*xn - vs*yn ; */
		    if (lieu.vertical)
		      {
			cc= xn*cos(lieu.gama)*soleil.x+xn*sin(lieu.gama)*soleil.y+yn*soleil.z;
			/* printf("normale 2D %f %f    3D %f %f %f\n",xn,yn,xn*cos(lieu.gama),xn*sin(lieu.gama),yn);*/
		      }
		    else
		      {
			cc=xn*soleil.x+yn*soleil.y;
			/* printf("normale 2D %f %f    3D %f %f %f\n",xn,yn,xn,yn,0.);*/
		      }
		    for (nn=0;nn<phyray.bandespec.nb;nn++) soleil.direct[nn][n]=fdirecth[nn]*coeff_transp[nn]*cc;
		  }
	      }
	    for (nn=0;nn<phyray.bandespec.nb;nn++) soleil.diffus[nn][n]=fdiffush[nn];
	      
	  }
      }

	  
	  
  
  if (affich.ray_soleil && (syrglob_nparts==1 ||syrglob_rang==0))
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("\n *** flux2d_soleil : FLUX SOLAIRE \n");
	  for (n=0;n<maillnodray.nelem;n++)
	    {
	      nn=0;
	      printf("      Facette %4d  Bande %d    Direct=%9.4f  Diffus=%9.4f  Total=%9.4f\n",
		     n+1,nn+1,soleil.direct[nn][n],soleil.diffus[nn][n]*fdfnp1[n]/maillnodray.volume[n],
		     soleil.direct[nn][n]+soleil.diffus[nn][n]*fdfnp1[n]/maillnodray.volume[n]);
	      for (nn=1;nn<phyray.bandespec.nb;nn++)
		printf("                    Bande %d    Direct=%9.4f  Diffus=%9.4f  Total=%9.4f\n",
		       nn+1,soleil.direct[nn][n],soleil.diffus[nn][n]*fdfnp1[n]/maillnodray.volume[n],
		       soleil.direct[nn][n]+soleil.diffus[nn][n]*fdfnp1[n]/maillnodray.volume[n]);
	    }
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n *** flux2d_soleil :SOLAR FLUX \n");
	  for (n=0;n<maillnodray.nelem;n++)
	    {
	      nn=0;
	      printf("      Face %4d  Band %d    Direct=%9.4f  Diffuse=%9.4f  Total=%9.4f\n",
		     n+1,nn+1,soleil.direct[nn][n],soleil.diffus[nn][n]*fdfnp1[n]/maillnodray.volume[n],
		     soleil.direct[nn][n]+soleil.diffus[nn][n]*fdfnp1[n]/maillnodray.volume[n]);
	      for (nn=1;nn<phyray.bandespec.nb;nn++)
		printf("                    Band %d    Direct=%9.4f  Diffuse=%9.4f  Total=%9.4f\n",
		       nn+1,soleil.direct[nn][n],soleil.diffus[nn][n]*fdfnp1[n]/maillnodray.volume[n],
		       soleil.direct[nn][n]+soleil.diffus[nn][n]*fdfnp1[n]/maillnodray.volume[n]);
	    }
	}
    }

  free(fdirecth); free(fdiffush); free(coeff_transp);
  
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |  flux2d_soleil                                                       |
  |======================================================================| */
void flux2d_soleil_anglect(struct Soleil soleil,
			   struct Maillage maillnodray,
			   double tempss,
			   struct Vitre vitre,struct ProphyRay phyray,
			   struct node *arbre,double size_min,double dim_boite[])
{
  int i,n,nn,na,nb,nv;
  int intersect,arrivee;
  double a,b,c,x,y,xa,ya,xb,yb,xn,yn,sinh,xs,ys,us,vs,xx,xm,ym,r,ah,xi,az;
  double rx,ry,teta,ybas;
  double *coeff_transp,cc;
  double ro[2],rd[2],pt_arr[2],d,teta_lim;
  double eps=1.e-6;
  struct node *noeud_dep,*noeud_arr;
  double **cooray,**xnf;
  int *typfacray,**nodray;

  cooray=maillnodray.coord;
  xnf=maillnodray.xnf;
  typfacray=maillnodray.type;
  nodray=maillnodray.node;
  
  coeff_transp=(double*)malloc(phyray.bandespec.nb*sizeof(double));
  verif_alloue_double1d("flux2d_soleil_anglect",coeff_transp);
  sinh=sin(soleil.h);

  ah=soleil.anglect;
  az=soleil.azict;
  xs=soleil.sphere*cos(ah);
  ys=soleil.sphere*sin(ah);


  us=-xs; vs=-ys; xx=sqrt(us*us+vs*vs); us/=xx; vs/=xx;

  for (nn=0;nn<phyray.bandespec.nb;nn++)
    for (n=0;n<maillnodray.nelem;n++) soleil.direct[nn][n]=soleil.diffus[nn][n]=0;

  /* calcul des proprietes en fn de l'angle d'incidence */
  for (nn=0;nn<phyray.bandespec.nb;nn++)
    for (i=0;i<maillnodray.nelem;i++)
      {
	soleil.emissi[nn][i]=phyray.emissi[nn][i];
	soleil.reflec[nn][i]=phyray.reflec[nn][i];
	soleil.absorb[nn][i]=phyray.absorb[nn][i];
	soleil.transm[nn][i]=phyray.transm[nn][i];
      }
  for (i=0;i<maillnodray.nelem;i++)
    {
 	  xn=xnf[0][i]; yn=xnf[1][i];
	  na=nodray[0][i]; nb=nodray[1][i];
	  xa=cooray[0][na]; ya=cooray[1][na];
	  xb=cooray[0][nb]; yb=cooray[1][nb];
	  xm=0.5*(xa+xb); ym=0.5*(ya+yb); 
	  rx=xs-xm; ry=ys-ym;
	  d=sqrt(rx*rx+ry*ry); rx/=d; ry/=d;
	  teta=acos(rx*xn+ry*yn);
	  user_propincidence(i,phyray.bandespec.nb,teta,
			     soleil.emissi,soleil.reflec,soleil.absorb,soleil.transm,
			     phyray.emissi,phyray.reflec,phyray.absorb,phyray.transm);
    }

  for (n=0;i<maillnodray.nelem;n++)
    if (typfacray[n]!=TYPRTI && typfacray[n]!=TYPRFI)
      {
	if (syrglob_nparts==1 ||syrglob_rang==0) printf(">>> flux2d_soleil_anglect face %d\n ",n);
	xn=xnf[0][n]; yn=xnf[1][n];
	na=nodray[0][n]; nb=nodray[1][n];
	xa=cooray[0][na]; ya=cooray[1][na];
	xb=cooray[0][nb]; yb=cooray[1][nb];
	if (!derriere_soleil2d(xn,yn,us,vs))
	  {
	    ro[0]=0.5*(xa+xb);     ro[1]=0.5*(ya+yb);
	    d=(xs-ro[0])*(xs-ro[0])+(ys-ro[1])*(ys-ro[1]);
	    /* pt_arr -> intersection du ray avec la boite */
	    a=-vs; b=us; c=-(a*ro[0]+b*ro[1]); /* eq droite du ray */ 
	    if (fabs(xs)<eps)   /* ray // y -> inter avec y=cte */
	      {
		pt_arr[0]=ro[0]; 
		if (ys>0) pt_arr[1]=dim_boite[3]*0.995; 
		else pt_arr[1]=dim_boite[2]*0.995;
	      }
	    else 
	      {
		x=dim_boite[0]*0.995;  y=(-c-a*x)/b;
		ybas=dim_boite[2];
		if (y<ybas || y>dim_boite[3]*0.995 || (xs-x)*(xs-x)+(ys-y)*(ys-y)>d ||
		    xs*(x-ro[0])+ys*(y-ro[1])<0.)
		  {
		    x=dim_boite[1]*0.995; y=(-c-a*x)/b;
		    if (y<ybas || y>dim_boite[3]*0.995 || (xs-x)*(xs-x)+(ys-y)*(ys-y)>d ||
			xs*(x-ro[0])+ys*(y-ro[1])<0)
		      {
			y=dim_boite[3]*0.995; x=(-c-b*y)/a;
			if (x<dim_boite[0] || x>dim_boite[1]*0.995 || (xs-x)*(xs-x)+(ys-y)*(ys-y)>d ||
			    xs*(x-ro[0])+ys*(y-ro[1])<0)
			  y=dim_boite[2]*0.995; x=(-c-b*y)/a;
		      }
		  }
		pt_arr[0]=x; pt_arr[1]=y;
	      }
	    rd[0]=pt_arr[0]-ro[0];  rd[1]=pt_arr[1]-ro[1];
	    noeud_dep=arbre; noeud_arr=arbre;
	    find_node_2d (&noeud_dep,ro[0],ro[1]);
	    find_node_2d (&noeud_arr,pt_arr[0],pt_arr[1]);
	    arrivee=0; intersect=-1; 
	    for (nn=0;nn<phyray.bandespec.nb;nn++) coeff_transp[nn]=1.;
	    if (vitre.nelem)
	      ivoitj_2dST(arbre,noeud_dep,noeud_arr,ro,rd,pt_arr,
			  &intersect,size_min,nodray,cooray,xnf,&arrivee,dim_boite,
			  typfacray,soleil.transm,coeff_transp,phyray.bandespec.nb);
	    else
	      ivoitj_2d(arbre,noeud_dep,noeud_arr,ro,rd,pt_arr,
			&intersect,size_min,nodray,cooray,&arrivee,dim_boite);
	    if (intersect==-1)
	      {
		/* xi=acos(yn); if (yn<0) xi=-xi; 
		   cc=cos(ah)*sin(xi)*cos(az)+sin(ah)*cos(xi); */
		cc=-us*xn - vs*yn ;
		for (nn=0;nn<phyray.bandespec.nb;nn++)  
		  soleil.direct[nn][n]=coeff_transp[nn]*soleil.fluxct[nn]*cc;
	      }
	  }
      }
  

  if (affich.ray_soleil && (syrglob_nparts==1 ||syrglob_rang==0))
    {
      
      if (SYRTHES_LANG == FR)
	{
	  printf("\n *** flux2d_soleil_anglect : FLUX SOLAIRE ET ANGLES CONSTANTS \n");
	  for (n=0;i<maillnodray.nelem;n++)
	    {
	      printf("      Facette %d  Flux solaire : ",n+1);
	      for (nn=0;nn<phyray.bandespec.nb;nn++) printf("Bande %d : %9.4f ",nn+1,soleil.direct[nn][n]);
	      printf("\n");
	    }
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n *** flux2d_soleil_anglect : SOLAR FLUX AND CONSTANT ANGLES \n");
	  for (n=0;i<maillnodray.nelem;n++)
	    {
	      printf("      Face %d  Solar flux : ",n+1);
	      for (nn=0;nn<phyray.bandespec.nb;nn++) printf("Band %d : %9.4f ",nn+1,soleil.direct[nn][n]);
	      printf("\n");
	    }
	}
    }

  free(coeff_transp);

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |  flux3d_soleil                                                       |
  |======================================================================| */
void flux3d_soleil(struct Lieu lieu,struct Soleil soleil,struct Meteo meteo,
		   struct Maillage maillnodray,
		   double *fdfnp1,struct PasDeTemps pasdetemps,
		   struct Vitre vitre,struct ProphyRay phyray,
		   struct node *arbre,double size_min,double dim_boite[])
{
  int i,n,nn,na,nb,nc,nv;
  int intersect,arrivee;
  double a,b,c,x,y,z,xa,ya,za,xb,yb,zb,xc,yc,zc,xn,yn,zn;
  double rx,ry,rz,teta;
  double sinh,xs,ys,zs,us,vs,ws,xm,ym,zm,r,xi,t,gama,w1,w2;
  double *fdirecth,*fdiffush,*coeff_transp,cc;
  double ro[3],rd[3],pt_arr[3],d,centre_boite[3],xxs,yys,zzs,xx,yy,zz;
  double dx,dy,dz,eps=1.e-6, tiers=1./3.,teta_lim;
  struct node *noeud_dep,*noeud_arr;
  double **cooray,**xnf;
  int *typfacray,**nodray;

  cooray=maillnodray.coord;
  xnf=maillnodray.xnf;
  typfacray=maillnodray.type;
  nodray=maillnodray.node;

  centre_boite[0]=(dim_boite[0]+dim_boite[1])*0.5;
  centre_boite[1]=(dim_boite[2]+dim_boite[3])*0.5;
  centre_boite[2]=(dim_boite[4]+dim_boite[5])*0.5;
  dx=(-dim_boite[0]+dim_boite[1])*0.5;
  dy=(-dim_boite[2]+dim_boite[3])*0.5;
  dz=(-dim_boite[4]+dim_boite[5])*0.5;

  fdirecth=(double*)malloc(phyray.bandespec.nb*sizeof(double));     verif_alloue_double1d("flux3d_soleil",fdirecth);
  fdiffush=(double*)malloc(phyray.bandespec.nb*sizeof(double));     verif_alloue_double1d("flux3d_soleil",fdiffush);
  coeff_transp=(double*)malloc(phyray.bandespec.nb*sizeof(double)); verif_alloue_double1d("flux3d_soleil",coeff_transp);
  sinh=sin(soleil.h);

  proj3d_soleil(lieu,soleil,&xs,&ys,&zs);
  us=-xs; vs=-ys; ws=-zs; xx=sqrt(us*us+vs*vs+ws*ws); us/=xx; vs/=xx; ws/=xx;
  xxs=xs-centre_boite[0]; yys=ys-centre_boite[1]; zzs=zs-centre_boite[2];

  if (meteo.actif) 
    {
      interp_meteo(pasdetemps.tempss,meteo,fdirecth,fdiffush,phyray.bandespec.nb);
      printf("flux3d_soleil fluxdirectmeteo= %f %f",fdirecth[0],fdirecth[1]);
      if (sinh>0.) for (i=0;i<phyray.bandespec.nb;i++)  fdirecth[i]=min(1350.,fdirecth[i]/sinh); 
      printf(" vrai= %f %f\n",fdirecth[0],fdirecth[1]);
    }
  else
    if (phyray.bandespec.nb==1)
      {
	fdirecth[0]=soleil.I0*soleil.C*soleil.A*exp(-soleil.B/sinh);
	fdiffush[0]=soleil.I0*soleil.C*sinh*(0.271-0.2939*soleil.A*exp(-soleil.B/sinh));
      }
    else
      for (nn=0;nn<phyray.bandespec.nb;nn++)
	{
	  w1=wiebel(c2/(phyray.bandespec.borneinf[nn]*tsoleil));
	  w2=wiebel(c2/(phyray.bandespec.bornesup[nn]*tsoleil));
	  fdirecth[nn]=soleil.I0*(w2-w1)*soleil.C*soleil.A*exp(-soleil.B/sinh);
	  fdiffush[nn]=soleil.I0*(w2-w1)*soleil.C*sinh*(0.271-0.2939*soleil.A*exp(-soleil.B/sinh));
	}

  user_solaire(fdirecth,fdiffush,phyray.bandespec.nb,pasdetemps);

  for (nn=0;nn<phyray.bandespec.nb;nn++)
    for (n=0;n<maillnodray.nelem;n++) soleil.direct[nn][n]=soleil.diffus[nn][n]=0;

  /* calcul des proprietes en fn de l'angle d'incidence */
  for (nn=0;nn<phyray.bandespec.nb;nn++)
    for (i=0;i<maillnodray.nelem;i++)
      {
	soleil.emissi[nn][i]=phyray.emissi[nn][i];
	soleil.reflec[nn][i]=phyray.reflec[nn][i];
	soleil.absorb[nn][i]=phyray.absorb[nn][i];
	soleil.transm[nn][i]=phyray.transm[nn][i];
      }
  for (i=0;i<maillnodray.nelem;i++)
    {
      xn=xnf[0][i]; yn=xnf[1][i];
      na=nodray[0][i]; nb=nodray[1][i]; nc=nodray[2][i];
      xa=cooray[0][na]; ya=cooray[1][na]; za=cooray[2][na];
      xb=cooray[0][nb]; yb=cooray[1][nb]; zb=cooray[2][nb];
      xc=cooray[0][nc]; yc=cooray[1][nc]; zc=cooray[2][nc];
      xm=tiers*(xa+xb+xc); ym=tiers*(ya+yb+yc); zm=tiers*(za+zb+zc);
      rx=xs-xm; ry=ys-ym; rz=zs-zm;
      d=sqrt(rx*rx+ry*ry+rz*rz); rx/=d; ry/=d; rz/=d;
      teta=acos(rx*xn+ry*yn+rz*zn);
      user_propincidence(i,phyray.bandespec.nb,teta,
			 soleil.emissi,soleil.reflec,soleil.absorb,soleil.transm,
			 phyray.emissi,phyray.reflec,phyray.absorb,phyray.transm);
    }

  for (n=0;n<maillnodray.nelem;n++)
    if (typfacray[n]!=TYPRTI && typfacray[n]!=TYPRFI)
      {
	printf(">>> flux3d_soleil face %d \n",n);
/* 	if (n==164) */
/* 	  printf(">>> on y est\n" );*/
	xn=xnf[0][n]; yn=xnf[1][n]; zn=xnf[2][n];
	na=nodray[0][n]; nb=nodray[1][n];  nc=nodray[2][n];
	xa=cooray[0][na]; ya=cooray[1][na]; za=cooray[2][na];
	xb=cooray[0][nb]; yb=cooray[1][nb]; zb=cooray[2][nb];
	xc=cooray[0][nc]; yc=cooray[1][nc]; zc=cooray[2][nc];
	ym=tiers*(za+zb+zc); 
	teta_lim=0.5*acos(soleil.Rterre/(soleil.Rterre+ym));
	if (-teta_lim<soleil.h && soleil.h<Pi+teta_lim)
	  {
	    sinh=sin(soleil.h+teta_lim);
	    if (!derriere_soleil3d(xn,yn,zn,us,vs,ws))
	      {
		ro[0]=tiers*(xa+xb+xc);  ro[1]=tiers*(ya+yb+yc); ro[2]=tiers*(za+zb+zc);
		xx=ro[0]-centre_boite[0]; yy=ro[1]-centre_boite[1]; zz=ro[2]-centre_boite[2];
		if (seg_cubex(dx,dy,dz,xx,xxs,yy,yys,zz,zzs))
		  {d=xs-ro[0]; t=(dim_boite[1]-ro[0])/d; 
		    x=dim_boite[1]; y=ro[1]+t*(ys-ro[1]); z=ro[2]+t*(zs-ro[2]);}
		else if (seg_cubex(-dx,dy,dz,xx,xxs,yy,yys,zz,zzs))
		  {d=xs-ro[0]; t=(dim_boite[0]-ro[0])/d; 
		    x=dim_boite[0]; y=ro[1]+t*(ys-ro[1]); z=ro[2]+t*(zs-ro[2]);}
		else if (seg_cubey(dx,dy,dz,xx,xxs,yy,yys,zz,zzs))
		  {d=ys-ro[1]; t=(dim_boite[3]-ro[1])/d; 
		    x=ro[0]+t*(xs-ro[0]); y=dim_boite[3]; z=ro[2]+t*(zs-ro[2]);}
		else if (seg_cubey(dx,-dy,dz,xx,xxs,yy,yys,zz,zzs))
		  {d=ys-ro[1]; t=(dim_boite[2]-ro[1])/d; 
		    x=ro[0]+t*(xs-ro[0]); y=dim_boite[2]; z=ro[2]+t*(zs-ro[2]);}
		else if (seg_cubez(dx,dy,dz,xx,xxs,yy,yys,zz,zzs))
		  {d=zs-ro[2]; t=(dim_boite[5]-ro[2])/d; 
		    y=ro[1]+t*(ys-ro[1]); x=ro[0]+t*(xs-ro[0]); z=dim_boite[5];}
		
		pt_arr[0]=x*0.995; pt_arr[1]=y*0.995; pt_arr[2]=z*0.995; 
		
		rd[0]=pt_arr[0]-ro[0];  rd[1]=pt_arr[1]-ro[1]; rd[2]=pt_arr[2]-ro[2];
		noeud_dep=arbre; noeud_arr=arbre;
		find_node_3d (&noeud_dep,ro[0],ro[1],ro[2]);
		find_node_3d (&noeud_arr,pt_arr[0],pt_arr[1],pt_arr[2]);
		arrivee=0; intersect=-1;
		for (nn=0;nn<phyray.bandespec.nb;nn++) coeff_transp[nn]=1.;
		if (vitre.nelem)
		  ivoitj_3dST(arbre,noeud_dep,noeud_arr,ro,rd,pt_arr,
			      &intersect,size_min,nodray,cooray,xnf,&arrivee,dim_boite,
			      typfacray,soleil.transm,coeff_transp,phyray.bandespec.nb);
		else
		  ivoitj_3d(arbre,noeud_dep,noeud_arr,ro,rd,pt_arr,
			    &intersect,size_min,nodray,cooray,&arrivee,dim_boite);
		if (intersect==-1)
		  {
		    xi=fabs(acos(zn)); if (zn<0) xi=-xi;
		    gama=fabs(acos(xn)); if (yn<0) gama+=Pi;
		    /* cc=cos(soleil.h)*sin(xi)*cos(soleil.a-gama)+sinh*cos(xi); */
		    cc=-us*xn - vs*yn - ws*zn ;
		    for (nn=0;nn<phyray.bandespec.nb;nn++) soleil.direct[nn][n]=fdirecth[nn]*coeff_transp[nn]*cc;
		  }
	      }
	    for (nn=0;nn<phyray.bandespec.nb;nn++) soleil.diffus[nn][n]=fdiffush[nn];
	  }
	
      }

  
  if (affich.ray_soleil && (syrglob_nparts==1 ||syrglob_rang==0))
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("\n *** flux3d_soleil : FLUX SOLAIRE \n");
	  for (n=0;n<maillnodray.nelem;n++)
	    {
	      nn=0;
	      printf("      Facette %4d  Bande %d    Direct=%9.4f  Diffus=%9.4f  Total=%9.4f\n",
		     n+1,nn+1,soleil.direct[nn][n],soleil.diffus[nn][n]*fdfnp1[n]/maillnodray.volume[n],
		     soleil.direct[nn][n]+soleil.diffus[nn][n]*fdfnp1[n]/maillnodray.volume[n]);
	      for (nn=1;nn<phyray.bandespec.nb;nn++)
		printf("                    Bande %d    Direct=%9.4f  Diffus=%9.4f  Total=%9.4f\n",
		       nn+1,soleil.direct[nn][n],soleil.diffus[nn][n]*fdfnp1[n]/maillnodray.volume[n],
		       soleil.direct[nn][n]+soleil.diffus[nn][n]*fdfnp1[n]/maillnodray.volume[n]);
	    }
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n *** flux3d_soleil : SOLAR FLUX \n");
	  for (n=0;n<maillnodray.nelem;n++)
	    {
	      nn=0;
	      printf("      Face %4d  Band %d    Direct=%9.4f  Diffuse=%9.4f  Total=%9.4f\n",
		     n+1,nn+1,soleil.direct[nn][n],soleil.diffus[nn][n]*fdfnp1[n]/maillnodray.volume[n],
		     soleil.direct[nn][n]+soleil.diffus[nn][n]*fdfnp1[n]/maillnodray.volume[n]);
	      for (nn=1;nn<phyray.bandespec.nb;nn++)
		printf("                    Band %d    Direct=%9.4f  Diffuse=%9.4f  Total=%9.4f\n",
		       nn+1,soleil.direct[nn][n],soleil.diffus[nn][n]*fdfnp1[n]/maillnodray.volume[n],
		       soleil.direct[nn][n]+soleil.diffus[nn][n]*fdfnp1[n]/maillnodray.volume[n]);
	    }
	}
    }

  free(fdirecth);   free(fdiffush);   free(coeff_transp);

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |  flux2d_soleil                                                       |
  |======================================================================| */
void flux3d_soleil_anglect(struct Soleil soleil,
			   struct Maillage maillnodray,
			   double tempss,
			   struct Vitre vitre,struct ProphyRay phyray,
			   struct node *arbre,double size_min,double dim_boite[])
{
  int i,n,nn,na,nb,nc,nv;
  int intersect,arrivee;
  double a,b,c,x,y,z,xa,ya,za,xb,yb,zb,xc,yc,zc,xn,yn,zn;
  double sinh,xs,ys,zs,us,vs,ws,xm,ym,zm,r,xi,t,ah,az,gama;
  double rx,ry,rz,teta;
  double *coeff_transp,cc;
  double ro[3],rd[3],pt_arr[3],d,centre_boite[3],xxs,yys,zzs,xx,yy,zz;
  double dx,dy,dz,eps=1.e-6, tiers=1./3.,teta_lim;
  struct node *noeud_dep,*noeud_arr;
  double **cooray,**xnf;
  int *typfacray,**nodray;

  cooray=maillnodray.coord;
  xnf=maillnodray.xnf;
  typfacray=maillnodray.type;
  nodray=maillnodray.node;

  coeff_transp=(double*)malloc(phyray.bandespec.nb*sizeof(double));
  verif_alloue_double1d("flux3d_soleil_anglect",coeff_transp);
  centre_boite[0]=(dim_boite[0]+dim_boite[1])*0.5;
  centre_boite[1]=(dim_boite[2]+dim_boite[3])*0.5;
  centre_boite[2]=(dim_boite[4]+dim_boite[5])*0.5;
  dx=(-dim_boite[0]+dim_boite[1])*0.5;
  dy=(-dim_boite[2]+dim_boite[3])*0.5;
  dz=(-dim_boite[4]+dim_boite[5])*0.5;


  sinh=sin(soleil.h);
  ah=soleil.anglect;
  az=soleil.azict;
  xs=soleil.sphere*cos(ah)*cos(az);
  ys=soleil.sphere*cos(ah)*sin(az);
  zs=soleil.sphere*sin(ah);

  us=-xs; vs=-ys; ws=-zs; xx=sqrt(us*us+vs*vs+ws*ws); us/=xx; vs/=xx; ws/=xx;
  xxs=xs-centre_boite[0]; yys=ys-centre_boite[1]; zzs=zs-centre_boite[2];


  for (nn=0;nn<phyray.bandespec.nb;nn++)
    for (n=0;n<maillnodray.nelem;n++) soleil.direct[nn][n]=soleil.diffus[nn][n]=0;

  /* calcul des proprietes en fn de l'angle d'incidence */
  for (nn=0;nn<phyray.bandespec.nb;nn++)
    for (i=0;i<maillnodray.nelem;i++)
      {
	soleil.emissi[nn][i]=phyray.emissi[nn][i];
	soleil.reflec[nn][i]=phyray.reflec[nn][i];
	soleil.absorb[nn][i]=phyray.absorb[nn][i];
	soleil.transm[nn][i]=phyray.transm[nn][i];
      }
  for (i=0;i<maillnodray.nelem;i++)
    {
      xn=xnf[0][i]; yn=xnf[1][i];
      na=nodray[0][i]; nb=nodray[1][i]; nc=nodray[2][i];
      xa=cooray[0][na]; ya=cooray[1][na]; za=cooray[2][na];
      xb=cooray[0][nb]; yb=cooray[1][nb]; zb=cooray[2][nb];
      xc=cooray[0][nc]; yc=cooray[1][nc]; zc=cooray[2][nc];
      xm=tiers*(xa+xb+xc); ym=tiers*(ya+yb+yc); zm=tiers*(za+zb+zc);
      rx=xs-xm; ry=ys-ym; rz=zs-zm;
      d=sqrt(rx*rx+ry*ry+rz*rz); rx/=d; ry/=d; rz/=d;
      teta=acos(rx*xn+ry*yn+rz*zn);
      user_propincidence(i,phyray.bandespec.nb,teta,
			 soleil.emissi,soleil.reflec,soleil.absorb,soleil.transm,
			 phyray.emissi,phyray.reflec,phyray.absorb,phyray.transm);
    }

  for (n=0;n<maillnodray.nelem;n++)
    if (typfacray[n]!=TYPRTI && typfacray[n]!=TYPRFI)
      {
	printf(">>> flux3d_soleil_ct facette %d \n",n);
	if (n==259)
	  printf(">>> on y est\n");
	xn=xnf[0][n]; yn=xnf[1][n]; zn=xnf[2][n];
	na=nodray[0][n]; nb=nodray[1][n];  nc=nodray[2][n];
	xa=cooray[0][na]; ya=cooray[1][na]; za=cooray[2][na];
	xb=cooray[0][nb]; yb=cooray[1][nb]; zb=cooray[2][nb];
	xc=cooray[0][nc]; yc=cooray[1][nc]; zc=cooray[2][nc];
	ym=tiers*(za+zb+zc); 
	teta_lim=0.5*acos(soleil.Rterre/(soleil.Rterre+ym));
	if (-teta_lim<ah && ah<Pi+teta_lim)
	  {
	    sinh=sin(ah+teta_lim);
	    if (!derriere_soleil3d(xn,yn,zn,us,vs,ws))
	      {
		ro[0]=tiers*(xa+xb+xc);  ro[1]=tiers*(ya+yb+yc); ro[2]=tiers*(za+zb+zc);
		xx=ro[0]-centre_boite[0]; yy=ro[1]-centre_boite[1]; zz=ro[2]-centre_boite[2];
		if (seg_cubex(dx,dy,dz,xx,xxs,yy,yys,zz,zzs))
		  {d=xs-ro[0]; t=(dim_boite[1]-ro[0])/d; 
		    x=dim_boite[1]; y=ro[1]+t*(ys-ro[1]); z=ro[2]+t*(zs-ro[2]);}
		else if (seg_cubex(-dx,dy,dz,xx,xxs,yy,yys,zz,zzs))
		  {d=xs-ro[0]; t=(dim_boite[0]-ro[0])/d; 
		    x=dim_boite[0]; y=ro[1]+t*(ys-ro[1]); z=ro[2]+t*(zs-ro[2]);}
		else if (seg_cubey(dx,dy,dz,xx,xxs,yy,yys,zz,zzs))
		  {d=ys-ro[1]; t=(dim_boite[3]-ro[1])/d; 
		    x=ro[0]+t*(xs-ro[0]); y=dim_boite[3]; z=ro[2]+t*(zs-ro[2]);}
		else if (seg_cubey(dx,-dy,dz,xx,xxs,yy,yys,zz,zzs))
		  {d=ys-ro[1]; t=(dim_boite[2]-ro[1])/d; 
		    x=ro[0]+t*(xs-ro[0]); y=dim_boite[2]; z=ro[2]+t*(zs-ro[2]);}
		else if (seg_cubez(dx,dy,dz,xx,xxs,yy,yys,zz,zzs))
		  {d=zs-ro[2]; t=(dim_boite[5]-ro[2])/d; 
		    y=ro[1]+t*(ys-ro[1]); x=ro[0]+t*(xs-ro[0]); z=dim_boite[5];}
		
		pt_arr[0]=x*0.995; pt_arr[1]=y*0.995; pt_arr[2]=z*0.995; 
		
		rd[0]=pt_arr[0]-ro[0];  rd[1]=pt_arr[1]-ro[1]; rd[2]=pt_arr[2]-ro[2];
		noeud_dep=arbre; noeud_arr=arbre;
		find_node_3d (&noeud_dep,ro[0],ro[1],ro[2]);
		find_node_3d (&noeud_arr,pt_arr[0],pt_arr[1],pt_arr[2]);
		arrivee=0; intersect=-1; 
		for (nn=0;nn<phyray.bandespec.nb;nn++) coeff_transp[nn]=1.;
		if (vitre.nelem)
		  ivoitj_3dST(arbre,noeud_dep,noeud_arr,ro,rd,pt_arr,
			      &intersect,size_min,nodray,cooray,xnf,&arrivee,dim_boite,
			      typfacray,soleil.transm,coeff_transp,phyray.bandespec.nb);
		else
		  ivoitj_3d(arbre,noeud_dep,noeud_arr,ro,rd,pt_arr,
			    &intersect,size_min,nodray,cooray,&arrivee,dim_boite);
		if (intersect==-1)
		  {
		    /* xi=fabs(acos(zn)); if (zn<0) xi=-xi; 
		       gama=fabs(acos(xn)); if (yn<0) gama=-gama;
		       cc=cos(ah)*sin(xi)*cos(az-gama)+sin(ah)*cos(xi); */
		    cc=-us*xn - vs*yn - ws*zn ;
		    for (nn=0;nn<phyray.bandespec.nb;nn++) soleil.direct[nn][n]=coeff_transp[nn]*soleil.fluxct[nn]*cc;
		  }
	      }
	  }
	
      }


  if (affich.ray_soleil && (syrglob_nparts==1 ||syrglob_rang==0))
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("\n *** flux3d_soleil_anglect : FLUX SOLAIRE ET ANGLES CONSTANTS \n");
	  for (n=0;i<maillnodray.nelem;n++)
	    {
	      printf("      Facette %d  Flux solaire : ",n+1);
	      for (nn=0;nn<phyray.bandespec.nb;nn++) printf("Bande %d : %9.4f ",nn+1,soleil.direct[nn][n]);
	      printf("\n");
	    }
	}  
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n *** flux3d_soleil_anglect : SOLAR FLUX AND CONSTANT ANGLES \n");
	  for (n=0;i<maillnodray.nelem;n++)
	    {
	      printf("      Face %d  Solar Flux : ",n+1);
	      for (nn=0;nn<phyray.bandespec.nb;nn++) printf("Band %d : %9.4f ",nn+1,soleil.direct[nn][n]);
	      printf("\n");
	    }
	}  
    }
  
  free(coeff_transp);

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |  interp_meteo                                                        |
  |======================================================================| */
void interp_meteo(double tempss,struct Meteo meteo,
		  double *fdirecth,double *fdiffush,int nbande)
{
  int i,n,numvar_fdif,numvar_fdir;
  double a;
  
  /* numero de la variable flux_diffu et flux_direct dans le fichier meteo */
  /* on suppose que dans le fichier meteo, on a dabord le flux direct des n bandes 
     puis le flux diffus des n bandes */
  numvar_fdir=1;
  numvar_fdif=numvar_fdir+nbande;

  if (tempss<meteo.var[0][0])
    {
      for (n=0;n<nbande;n++)
	{
	  fdirecth[n]=meteo.var[numvar_fdir+n][0];
	  fdiffush[n]=meteo.var[numvar_fdif+n][0];
	}
    }
  else if (tempss>=meteo.var[0][meteo.nelem-1])
    {
      for (n=0;n<nbande;n++)
	{
	  fdirecth[n]=meteo.var[numvar_fdir+n][meteo.nelem-1];
	  fdiffush[n]=meteo.var[numvar_fdif+n][meteo.nelem-1];
	}
    }
  else
    {
      i=0;
      while (tempss>meteo.var[0][i+1]) i++;
      a=(tempss-meteo.var[0][i])/(meteo.var[0][i+1]-meteo.var[0][i]);
      for (n=0;n<nbande;n++)
	{
	  fdirecth[n]=a*meteo.var[numvar_fdir+n][i+1]+(1-a)*meteo.var[numvar_fdir+n][i];
	  fdiffush[n]=a*meteo.var[numvar_fdif+n][i+1]+(1-a)*meteo.var[numvar_fdif+n][i];
	}
    }

  if (affich.ray_soleil && (syrglob_nparts==1 ||syrglob_rang==0))
    {
      if (SYRTHES_LANG == FR)
	{
	  printf(">>> fichier meteo : au temps  t=%f\n",tempss);
	  for (n=0;n<nbande;n++)
	    printf("               bande %d : direct=%f diffus=%f\n",n+1,fdirecth[n],fdiffush[n]);
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf(">>> weather file : at time  t=%f\n",tempss);
	  for (n=0;n<nbande;n++)
	    printf("               band %d : direct=%f diffuse=%f\n",n+1,fdirecth[n],fdiffush[n]);
	}
    }
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | tciel_meteo                                                          |
  |======================================================================| */
void tciel_meteo(struct Meteo meteo,int nbande,double tempss,double *tciel)
{
  /* ??????????????? a appeler quelque part ! */
  int i,n;
  double text,humr,fdir,fdif,trosee;
  static double anebul=0.5;
  double tref0=273.15,eciel;
  int numvar_text=4;
  int numvar_humr=5;

  /*  Ceci est un exemple. Il faut l'adapter au fichier meteo a traiter */

  /* numvar_text= numero de la variable qui contient la temperature du ciel */
  /* numvar_humr= numero de la variable qui contient l'humidite relative */

  text = interpol_table1D(tempss,meteo.nelem,meteo.var[0],meteo.var[numvar_text]);
  humr = interpol_table1D(tempss,meteo.nelem,meteo.var[0],meteo.var[numvar_humr]);
  humr*=0.01;

  /* modelisation simple de la temperature de ciel */
  *tciel=text-10.;

  /* modelisation plus complexe de la temperature de ciel (en commentaire pour le moment) */
  /*
  for (fdir=0,n=0;n<nbande;n++) fdir+ = interpol_table1D(tempss,meteo.nelem,meteo.time,meteo.meteo.var[?][n]);
  for (fdif=0,n=0;n<nbande;n++) fdif+ = interpol_table1D(tempss,meteo.nelem,meteo.time,meteo.meteo.var[?][n]);
  
  if (fdir+fdif>0.01) 
    if (fdif/(fdif+fdir)>0.3) 
      anebul=sqrt((fdif/(fdif+fdir)-0.3)/0.7);  
	
  trosee=(17.4*tref0-33.16*(log(humr)+17.4*text/(text+240.)))/
         (17.4-log(humr)-17.4*text/(text+240.))-273.15;

  eciel=(0.0028*trosee+0.787)*(1-0.56*anebul)+0.56*anebul; 
  *tciel=pow(eciel,0.25)*(text+273.15)-273.15; 
  
  printf(" tciel_meteo : trosee=%f, anebul=%f, eciel=%f, tciel=%f\n",trosee,anebul,eciel,*tciel);
  */
} 


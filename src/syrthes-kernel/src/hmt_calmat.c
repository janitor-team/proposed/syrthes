/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

#include "syr_usertype.h"
#include "syr_bd.h"
#include "syr_option.h"
#include "syr_abs.h"
#include "syr_tree.h"
#include "syr_const.h"
#include "syr_hmt_proto.h"
#include "syr_proto.h"



/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | madif_hmt                                                            |
  |======================================================================| */

void hmt_madif(struct Maillage maillnodes,
	       double *coeff,double *res,double *xdms)
{
  int i,nca;
  double r1,r2,r3;
  int ne0,ne1,ne2,ne3;
  double **coords;
  double s4=0.25, s12=1./12., s36=1./36.;
  double x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4;
  double xgrad1,ygrad1,zgrad1,xgrad2,ygrad2,zgrad2;
  double xgrad3,ygrad3,zgrad3,xgrad4,ygrad4,zgrad4;
  double *c0,*c1,*c2;
  double dx0,dx1,dx2,dx3,dx02,dx12,dx22,dx32;
  double dy0,dy1,dy2,dy3,dy02,dy12,dy22,dy32;
  double dz0,dz1,dz2,dz3,dz02,dz12,dz22,dz32;
  double x01,x02,x03,x12,x13,x23;
  double y01,y02,y03,y12,y13,y23;
  double z01,z02,z03,z12,z13,z23;
  double cc;
  int **nar;

  if (maillnodes.iaxisy==1) nca=1; else nca=0;
  nar=maillnodes.eltarete;
  coords=maillnodes.coord;

  if (maillnodes.ndim==2 && maillnodes.iaxisy==0)
    {
      for (i=0; i<maillnodes.nelem; i++)
	{
	  ne0=maillnodes.node[0][i];ne1=maillnodes.node[1][i];ne2=maillnodes.node[2][i];

	  c0=*coords; c1=*(coords+1);
	  dx0=-(*(c1+ne2)-*(c1+ne1)); dy0=*(c0+ne2)-*(c0+ne1);
	  dx1=-(*(c1+ne0)-*(c1+ne2)); dy1=*(c0+ne0)-*(c0+ne2);
	  dx2=-(*(c1+ne1)-*(c1+ne0)); dy2=*(c0+ne1)-*(c0+ne0);

	  cc=s4/maillnodes.volume[i]*coeff[i];
	  
	  dx02=dx0*dx0; dx12=dx1*dx1; dx22=dx2*dx2;
	  dy02=dy0*dy0; dy12=dy1*dy1; dy22=dy2*dy2;

	  res[ne0]+= cc*(dx02+dy02);
	  res[ne1]+= cc*(dx12+dy12);
	  res[ne2]+= cc*(dx22+dy22);
	  xdms[nar[0][i]]+=cc*(dx0*dx1+dy0*dy1);   
	  xdms[nar[2][i]]+=cc*(dx0*dx2+dy0*dy2);   
	  xdms[nar[1][i]]+=cc*(dx1*dx2+dy1*dy2); 
	}
    }
  else if (maillnodes.ndim==2)
    {
      for (i=0; i<maillnodes.nelem; i++)
	{
	  ne0=maillnodes.node[0][i];ne1=maillnodes.node[1][i];ne2=maillnodes.node[2][i];

	  r1 = fabs(*(*(coords+nca)+ne0));
	  r2 = fabs(*(*(coords+nca)+ne1));
	  r3 = fabs(*(*(coords+nca)+ne2));

	  c0=*coords; c1=*(coords+1);
	  dx0=-(*(c1+ne2)-*(c1+ne1)); dy0=*(c0+ne2)-*(c0+ne1);
	  dx1=-(*(c1+ne0)-*(c1+ne2)); dy1=*(c0+ne0)-*(c0+ne2);
	  dx2=-(*(c1+ne1)-*(c1+ne0)); dy2=*(c0+ne1)-*(c0+ne0);

	  cc=s12/maillnodes.volume[i]*coeff[i]*(r1+r2+r3);
	  
	  dx02=dx0*dx0; dx12=dx1*dx1; dx22=dx2*dx2;
	  dy02=dy0*dy0; dy12=dy1*dy1; dy22=dy2*dy2;

	  res[ne0]+= cc*(dx02+dy02); 
	  res[ne1]+= cc*(dx12+dy12); 
	  res[ne2]+= cc*(dx22+dy22); 
	  xdms[nar[0][i]]+=cc*(dx0*dx1+dy0*dy1);  
	  xdms[nar[2][i]]+=cc*(dx0*dx2+dy0*dy2);    
	  xdms[nar[1][i]]+=cc*(dx1*dx2+dy1*dy2);    	 
	}
    }     
  else
    {

      for (i=0; i<maillnodes.nelem; i++)
	{
	  ne0=maillnodes.node[0][i]; ne1=maillnodes.node[1][i];
	  ne2=maillnodes.node[2][i]; ne3=maillnodes.node[3][i];

	  c0=*coords; c1=*(coords+1); c2=*(coords+2);
	  x01 =*(c0+ne1)-*(c0+ne0); y01 =*(c1+ne1)-*(c1+ne0); z01 =*(c2+ne1)-*(c2+ne0);
	  x02 =*(c0+ne2)-*(c0+ne0); y02 =*(c1+ne2)-*(c1+ne0); z02 =*(c2+ne2)-*(c2+ne0);
	  x03 =*(c0+ne3)-*(c0+ne0); y03 =*(c1+ne3)-*(c1+ne0); z03 =*(c2+ne3)-*(c2+ne0);
	  x12 =*(c0+ne2)-*(c0+ne1); y12 =*(c1+ne2)-*(c1+ne1); z12 =*(c2+ne2)-*(c2+ne1);
          x13 =*(c0+ne3)-*(c0+ne1); y13 =*(c1+ne3)-*(c1+ne1); z13 =*(c2+ne3)-*(c2+ne1);
	  x23 =*(c0+ne3)-*(c0+ne2); y23 =*(c1+ne3)-*(c1+ne2); z23 =*(c2+ne3)-*(c2+ne2);

	  dx0=y13*z12-z13*y12; dy0=-x13*z12+z13*x12; dz0=x13*y12-y13*x12;
	  dx1=-y23*z02+z23*y02; dy1=x23*z02-z23*x02; dz1=-x23*y02+y23*x02;
	  dx2=y13*z03-z13*y03; dy2=-x13*z03+z13*x03; dz2=x13*y03-y13*x03;
	  dx3=y01*z02-z01*y02; dy3=-x01*z02+z01*x02; dz3=x01*y02-y01*x02;

	  dx02=dx0*dx0; dx12=dx1*dx1; dx22=dx2*dx2; dx32=dx3*dx3;
	  dy02=dy0*dy0; dy12=dy1*dy1; dy22=dy2*dy2; dy32=dy3*dy3;
	  dz02=dz0*dz0; dz12=dz1*dz1; dz22=dz2*dz2; dz32=dz3*dz3;

	  cc=s36/maillnodes.volume[i]*coeff[i];

	  res[ne0]+=cc*(dx02+dy02+dz02);
	  res[ne1]+=cc*(dx12+dy12+dz12);    
	  res[ne2]+=cc*(dx22+dy22+dz22); 
	  res[ne3]+=cc*(dx32+dy32+dz32);  

	  xdms[nar[0][i]]+=cc*(dx0*dx1+dy0*dy1+dz0*dz1);
	  xdms[nar[2][i]]+=cc*(dx0*dx2+dy0*dy2+dz0*dz2);
	  xdms[nar[3][i]]+=cc*(dx0*dx3+dy0*dy3+dz0*dz3);
	  xdms[nar[1][i]]+=cc*(dx1*dx2+dy1*dy2+dz1*dz2);
	  xdms[nar[4][i]]+=cc*(dx1*dx3+dy1*dy3+dz1*dz3);
	  xdms[nar[5][i]]+=cc*(dx2*dx3+dy2*dy3+dz2*dz3);
	}
    }
}


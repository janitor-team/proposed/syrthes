/*============================================================================
 * Main structure for a mesh representation
 *============================================================================*/

/*
  This file is part of the "Parallel Location and Exchange" library,
  intended to provide mesh or particle-based code coupling services.

  Copyright (C) 2005-2010  EDF

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/*===========================================================================
 * Main structure for a mesh representation
 * AUTHORS  : J. Bonelle, Y Fournier
 *
 * Library: Syrthes                                   Copyright EDF 2008-2010
 *===========================================================================*/

#if defined(_SYRTHES_CFD_)

/*----------------------------------------------------------------------------
 * Standard C library headers
 *----------------------------------------------------------------------------*/

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*----------------------------------------------------------------------------
 *  Local headers
 *----------------------------------------------------------------------------*/

#include <ple_defs.h>

/*----------------------------------------------------------------------------
 *  Header for the current file
 *----------------------------------------------------------------------------*/

#include "syr_cfd_mesh.h"
#include "syr_cfd_mesh_priv.h"

/*============================================================================
 * Local macro definitions
 *============================================================================*/

/*
 * Macros for future internationalization via gettext() or a similar
 * function (to mark translatable character strings)
 */

#if defined(ENABLE_NLS)

#include <libintl.h>
#define _(String) gettext(String)
#define gettext_noop(String) String
#define N_(String) gettext_noop(String)

#else

#define _(String) String
#define N_(String) String
#define textdomain(Domain)
#define bindtextdomain(Package, Directory)

#endif

/*============================================================================
 * Static global variables
 *============================================================================*/

/* Number of vertices associated with each "mesh" element type */

const int  syr_cfd_mesh_n_vertices_element[] = {2,   /* Edge */
                                                3,   /* Triangle */
                                                4};  /* Tetrahedron */

/*============================================================================
 * Private function definitions
 *============================================================================*/

/*============================================================================
 * Public function definitions
 *============================================================================*/

/*----------------------------------------------------------------------------
 * Creation of a mesh representation structure.
 *
 * Ownership of the given coordinates and connectivity arrays is transferred
 * to the mesh representation structure.
 *
 * parameters:
 *   dim           <-- spatial dimension
 *   n_vertices    <-- number of vertices
 *   n_elements    <-- number of elements
 *   type          <-- type of elements
 *   vertex_coords <-- coordinates of parent vertices (interlaced)
 *   vertex_num    <-- element -> vertex connectivity
 *
 * returns:
 *  pointer to created mesh representation structure
 *----------------------------------------------------------------------------*/

syr_cfd_mesh_t *
syr_cfd_mesh_create(int                 dim,
                    int                 n_vertices,
                    int                 n_elements,
                    syr_cfd_element_t   type,
                    double              vertex_coords[],
                    int                 vertex_num[])
{
  syr_cfd_mesh_t  *this_mesh;

  PLE_MALLOC(this_mesh, 1, syr_cfd_mesh_t);

  this_mesh->dim = dim;

  this_mesh->n_vertices = n_vertices;
  this_mesh->n_elements = n_elements;
  this_mesh->element_type = type;

  this_mesh->vertex_coords = vertex_coords;
  this_mesh->vertex_num = vertex_num;

  return (this_mesh);
}

/*----------------------------------------------------------------------------
 * Destruction of a mesh representation structure.
 *
 * parameters:
 *   this_mesh  <-> pointer to pointer to structure that should be destroyed
 *----------------------------------------------------------------------------*/

void
syr_cfd_mesh_destroy(syr_cfd_mesh_t  **this_mesh)
{
  syr_cfd_mesh_t  *_this_mesh = *this_mesh;

  /* Local structures */

  if (_this_mesh->vertex_coords != NULL)
    PLE_FREE(_this_mesh->vertex_coords);

  if (_this_mesh->vertex_num != NULL)
    PLE_FREE(_this_mesh->vertex_num);

  /* Main structure destroyed and NULL returned */

  PLE_FREE(*this_mesh);
}

/*----------------------------------------------------------------------------
 * Copy element centers to an array.
 *
 * parameters:
 *   this_mesh    <-- pointer to mesh structure
 *   cell_centers --> cell centers coordinates (pre-allocated)
 *----------------------------------------------------------------------------*/

void
syr_cfd_mesh_get_element_centers(const syr_cfd_mesh_t  *mesh,
                                 ple_coord_t           *cell_centers)
{
  int i, j, k, v_id;

  const int dim = mesh->dim;
  const int stride = syr_cfd_mesh_n_vertices_element[mesh->element_type];
  const double mult = 1.0 / stride;
  const ple_coord_t *coords = mesh->vertex_coords;

  for (i = 0; i < mesh->n_elements; i++) {

    v_id = mesh->vertex_num[(i*stride)] - 1;
    for (k = 0; k < dim; k++)
      cell_centers[i*dim + k] = coords[v_id*dim + k];

    for (j = 1; j < stride; j++) {
      v_id = mesh->vertex_num[(i*stride) + j] - 1;
      for (k = 0; k < dim; k++)
        cell_centers[i*dim + k] += coords[v_id*dim + k];
    }

    for (k = 0; k < dim; k++)
      cell_centers[i*dim + k] *= mult;
  }
}

/*----------------------------------------------------------------------------
 * Dump printout of a mesh representation structure.
 *
 * parameters:
 *   this_mesh <-- pointer to structure that should be dumped
 *----------------------------------------------------------------------------*/

void
syr_cfd_mesh_dump(const syr_cfd_mesh_t  *this_mesh)
{
  int  i;
  int  num_vertex = 1;
  const int     *num = this_mesh->vertex_num;
  const double  *coord = this_mesh->vertex_coords;

  /* Names of (single) "nodal" element types */

  const char  *elt_type_name[] = {N_("edge"),
                                  N_("triangle"),
                                  N_("tetrahedron")};

  /* Global indicators */
  /*--------------------*/

  ple_printf("\n"
             "Spatial dimension:             %d\n"
             "Element type:                  %s\n",
             this_mesh->dim,
             elt_type_name[this_mesh->element_type]);

  ple_printf("\n"
             "Number of elements:            %d\n"
             "Number of vertices:            %d\n",
            this_mesh->n_elements,
            this_mesh->n_vertices);

  if (this_mesh->n_vertices > 0) {

    /* Output coordinates depending on parent numbering */

    ple_printf("\nVertex coordinates:\n\n");
    switch(this_mesh->dim) {
    case 1:
      for (i = 0; i < this_mesh->n_vertices; i++)
        ple_printf("%10d : %12.5f\n",
                   num_vertex++, coord[i]);
      break;
    case 2:
      for (i = 0; i < this_mesh->n_vertices; i++)
        ple_printf("%10d : %12.5f %12.5f\n",
                   num_vertex++, coord[i*2], coord[i*2+1]);
      break;
    case 3:
      for (i = 0; i < this_mesh->n_vertices; i++)
        ple_printf("%10d : %12.5f %12.5f %12.5f\n",
                   num_vertex++, coord[i*3],
                   coord[i*3+1], coord[i*3+2]);
      break;
    default:
      ple_printf("coordinates not output\n"
                 "dimension = %d unsupported\n", this_mesh->dim);
    }

  }

  /* Global indicators */
  /*--------------------*/

  ple_printf("\nElements -> vertices connectivity:\n\n");

  switch(syr_cfd_mesh_n_vertices_element[this_mesh->element_type]) {
  case 2:
    for (i = 0; i < this_mesh->n_elements; i++)
        ple_printf("%10d : %10d %10d\n",
                   i+1, num[i*2], num[i*2+1]);
    break;
  case 3:
    for (i = 0; i < this_mesh->n_elements; i++)
      ple_printf("%10d : %10d %10d %10d\n",
                 i+1, num[i*3], num[i*3+1], num[i*3+2]);
    break;
  case 4:
    for (i = 0; i < this_mesh->n_elements; i++)
      ple_printf("%10d : %10d %10d %10d %10d\n",
                 i+1, num[i*4], num[i*4+1], num[i*4+2], num[i*4+3]);
    break;
  default:
    break;
  }
}

/*===========================================================================*/

#endif /* defined(_SYRTHES_CFD_) */

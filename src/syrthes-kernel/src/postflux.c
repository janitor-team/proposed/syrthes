/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

#include "syr_usertype.h"
#include "syr_bd.h"
#include "syr_option.h"
#include "syr_abs.h"
#include "syr_tree.h"
#include "syr_const.h"
#include "syr_proto.h"

/* #include "mpi.h" */


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | postflux_isotro                                                      |
  |======================================================================| */

void postflux_isotro(struct Maillage maillnodes,
		     struct Iso kiso,double *t,double **fluxT)
{
  int i,nca,nume;
  int np,nel;
  int ne0,ne1,ne2,ne3,**nodes;
  double s2=0.5,  s6=1./6.;
  double xk;
  double *c0,*c1,*c2,**coords;
  double dx0,dx1,dx2,dx3,dx02,dx12,dx22,dx32;
  double dy0,dy1,dy2,dy3,dy02,dy12,dy22,dy32;
  double dz0,dz1,dz2,dz3,dz02,dz12,dz22,dz32;
  double x01,x02,x03,x12,x13,x23;
  double y01,y02,y03,y12,y13,y23;
  double z01,z02,z03,z12,z13,z23;
  double coeff;
  int **nar;



  if (maillnodes.iaxisy==1) nca=1; else nca=0;
  np=maillnodes.npoin;
  nel=maillnodes.nelem;
  nodes=maillnodes.node;
  coords=maillnodes.coord;
  nar=maillnodes.eltarete;
  
  if (maillnodes.ndim==2 && maillnodes.iaxisy==0)
    {
      for (i=0; i<kiso.nelem; i++)
	{
	  nume=kiso.ele[i];
	  ne0=nodes[0][nume];ne1=nodes[1][nume];ne2=nodes[2][nume];
	  xk=kiso.k[i];

	  c0=*coords; c1=*(coords+1);
	  dx0=-(*(c1+ne2)-*(c1+ne1)); dy0=*(c0+ne2)-*(c0+ne1);
	  dx1=-(*(c1+ne0)-*(c1+ne2)); dy1=*(c0+ne0)-*(c0+ne2);
	  dx2=-(*(c1+ne1)-*(c1+ne0)); dy2=*(c0+ne1)-*(c0+ne0);

	  coeff=s2/maillnodes.volume[nume]*xk;
	  
	  fluxT[0][nume]=-coeff*(t[ne0]*dx0+t[ne1]*dx1+t[ne2]*dx2);
	  fluxT[1][nume]=-coeff*(t[ne0]*dy0+t[ne1]*dy1+t[ne2]*dy2);
	}
    }
  else if (maillnodes.ndim==2)
    {
      for (i=0; i<kiso.nelem; i++)
	{
	  nume=kiso.ele[i];
	  ne0=nodes[0][nume];ne1=nodes[1][nume];ne2=nodes[2][nume];
	  xk=kiso.k[i];

	  c0=*coords; c1=*(coords+1);
	  dx0=-(*(c1+ne2)-*(c1+ne1)); dy0=*(c0+ne2)-*(c0+ne1);
	  dx1=-(*(c1+ne0)-*(c1+ne2)); dy1=*(c0+ne0)-*(c0+ne2);
	  dx2=-(*(c1+ne1)-*(c1+ne0)); dy2=*(c0+ne1)-*(c0+ne0);

	  coeff=s2/maillnodes.volume[nume]*xk;
	 
	  /* a verifier mais a priori meme expression qu'en cartesien */
	  fluxT[0][nume]=-coeff*(t[ne0]*dx0+t[ne1]*dx1+t[ne2]*dx2);
	  fluxT[1][nume]=-coeff*(t[ne0]*dy0+t[ne1]*dy1+t[ne2]*dy2);
   	 
	}
    }     
  else
    {

      for (i=0; i<kiso.nelem; i++)
	{

	  nume=kiso.ele[i];
	  ne0=nodes[0][nume];ne1=nodes[1][nume];ne2=nodes[2][nume];ne3=nodes[3][nume];
	  xk=kiso.k[i];

	  ne0=nodes[0][nume];ne1=nodes[1][nume];ne2=nodes[2][nume];ne3=nodes[3][nume];
	  c0=*coords; c1=*(coords+1); c2=*(coords+2);
	  x01 =*(c0+ne1)-*(c0+ne0); y01 =*(c1+ne1)-*(c1+ne0); z01 =*(c2+ne1)-*(c2+ne0);
	  x02 =*(c0+ne2)-*(c0+ne0); y02 =*(c1+ne2)-*(c1+ne0); z02 =*(c2+ne2)-*(c2+ne0);
	  x03 =*(c0+ne3)-*(c0+ne0); y03 =*(c1+ne3)-*(c1+ne0); z03 =*(c2+ne3)-*(c2+ne0);
	  x12 =*(c0+ne2)-*(c0+ne1); y12 =*(c1+ne2)-*(c1+ne1); z12 =*(c2+ne2)-*(c2+ne1);
          x13 =*(c0+ne3)-*(c0+ne1); y13 =*(c1+ne3)-*(c1+ne1); z13 =*(c2+ne3)-*(c2+ne1);
	  x23 =*(c0+ne3)-*(c0+ne2); y23 =*(c1+ne3)-*(c1+ne2); z23 =*(c2+ne3)-*(c2+ne2);

	  dx0=y13*z12-z13*y12; dy0=-x13*z12+z13*x12; dz0=x13*y12-y13*x12;
	  dx1=-y23*z02+z23*y02; dy1=x23*z02-z23*x02; dz1=-x23*y02+y23*x02;
	  dx2=y13*z03-z13*y03; dy2=-x13*z03+z13*x03; dz2=x13*y03-y13*x03;
	  dx3=y01*z02-z01*y02; dy3=-x01*z02+z01*x02; dz3=x01*y02-y01*x02;

	  coeff=s6/maillnodes.volume[nume]*xk;
	  
	  fluxT[0][nume]=-coeff*(t[ne0]*dx0+t[ne1]*dx1+t[ne2]*dx2+t[ne3]*dx3);
	  fluxT[1][nume]=-coeff*(t[ne0]*dy0+t[ne1]*dy1+t[ne2]*dy2+t[ne3]*dy3);
	  fluxT[2][nume]=-coeff*(t[ne0]*dz0+t[ne1]*dz1+t[ne2]*dz2+t[ne3]*dz3);
	}
    }
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | postflux_orthotro                                                    |
  |======================================================================| */

void postflux_orthotro(struct Maillage maillnodes,
		       struct Ortho kortho,double *t,double **fluxT)
{
  int i,nca,nume;
  double s2=0.5,s6=1./6.,coeff;
  double r1,r2,r3;
  int np,nel,**nodes;
  int ne0,ne1,ne2,ne3;
  double xk,yk,zk;
  double dx0,dx1,dx2,dx3,dx02,dx12,dx22,dx32;
  double dy0,dy1,dy2,dy3,dy02,dy12,dy22,dy32;
  double dz0,dz1,dz2,dz3,dz02,dz12,dz22,dz32;
  double x01,x02,x03,x12,x13,x23;
  double y01,y02,y03,y12,y13,y23;
  double z01,z02,z03,z12,z13,z23;
  double *c0,*c1,*c2,**coords;
  int **nar;

  if (maillnodes.iaxisy==1) nca=1; else nca=0;
  np=maillnodes.npoin;
  nel=maillnodes.nelem;
  nodes=maillnodes.node;
  coords=maillnodes.coord;
  nar=maillnodes.eltarete;

  if (maillnodes.ndim==2 && maillnodes.iaxisy==0)
    {
      for (i=0; i<kortho.nelem; i++)
	{

	  nume=kortho.ele[i];
	  xk=kortho.k11[i]; yk=kortho.k22[i];

	  ne0=nodes[0][nume];ne1=nodes[1][nume];ne2=nodes[2][nume];
	  c0=*coords; c1=*(coords+1);
	  dx0=-(*(c1+ne2)-*(c1+ne1)); dy0=*(c0+ne2)-*(c0+ne1);
	  dx1=-(*(c1+ne0)-*(c1+ne2)); dy1=*(c0+ne0)-*(c0+ne2);
	  dx2=-(*(c1+ne1)-*(c1+ne0)); dy2=*(c0+ne1)-*(c0+ne0);

	  coeff=s2/maillnodes.volume[nume];

 	  fluxT[0][nume]=-xk*coeff*(t[ne0]*dx0+t[ne1]*dx1+t[ne2]*dx2);
	  fluxT[1][nume]=-yk*coeff*(t[ne0]*dy0+t[ne1]*dy1+t[ne2]*dy2);
   
	}
    }
  else if (maillnodes.ndim==2)
    {
      for (i=0; i<kortho.nelem; i++)
	{	  
	  nume=kortho.ele[i];
	  xk=kortho.k11[i]; yk=kortho.k22[i];


	  ne0=nodes[0][nume];ne1=nodes[1][nume];ne2=nodes[2][nume];
	  r1=fabs(*(*(coords+nca)+ne0)) * coeff;
	  r2=fabs(*(*(coords+nca)+ne1)) * coeff;
	  r3=fabs(*(*(coords+nca)+ne2)) * coeff;

	  c0=*coords; c1=*(coords+1);
	  dx0=-(*(c1+ne2)-*(c1+ne1)); dy0=*(c0+ne2)-*(c0+ne1);
	  dx1=-(*(c1+ne0)-*(c1+ne2)); dy1=*(c0+ne0)-*(c0+ne2);
	  dx2=-(*(c1+ne1)-*(c1+ne0)); dy2=*(c0+ne1)-*(c0+ne0);

	  /* chris : a verifier ulterieurement en axy */
	  coeff=s2/maillnodes.volume[nume];

 	  fluxT[0][nume]=-xk*coeff*(t[ne0]*dx0+t[ne1]*dx1+t[ne2]*dx2);
	  fluxT[1][nume]=-yk*coeff*(t[ne0]*dy0+t[ne1]*dy1+t[ne2]*dy2);

	}
    }     
  else
    {

      for (i=0; i<kortho.nelem; i++)
	{
	  xk=kortho.k11[i]; yk=kortho.k22[i]; zk=kortho.k33[i];

	  nume=kortho.ele[i];
	  coeff=s6/maillnodes.volume[nume];

	  ne0=nodes[0][nume];ne1=nodes[1][nume];ne2=nodes[2][nume];ne3=nodes[3][nume];
	  c0=*coords; c1=*(coords+1); c2=*(coords+2);
	  x01 =*(c0+ne1)-*(c0+ne0); y01 =*(c1+ne1)-*(c1+ne0); z01 =*(c2+ne1)-*(c2+ne0);
	  x02 =*(c0+ne2)-*(c0+ne0); y02 =*(c1+ne2)-*(c1+ne0); z02 =*(c2+ne2)-*(c2+ne0);
	  x03 =*(c0+ne3)-*(c0+ne0); y03 =*(c1+ne3)-*(c1+ne0); z03 =*(c2+ne3)-*(c2+ne0);
	  x12 =*(c0+ne2)-*(c0+ne1); y12 =*(c1+ne2)-*(c1+ne1); z12 =*(c2+ne2)-*(c2+ne1);
          x13 =*(c0+ne3)-*(c0+ne1); y13 =*(c1+ne3)-*(c1+ne1); z13 =*(c2+ne3)-*(c2+ne1);
	  x23 =*(c0+ne3)-*(c0+ne2); y23 =*(c1+ne3)-*(c1+ne2); z23 =*(c2+ne3)-*(c2+ne2);

	  dx0=y13*z12-z13*y12; dy0=-x13*z12+z13*x12; dz0=x13*y12-y13*x12;
	  dx1=-y23*z02+z23*y02; dy1=x23*z02-z23*x02; dz1=-x23*y02+y23*x02;
	  dx2=y13*z03-z13*y03; dy2=-x13*z03+z13*x03; dz2=x13*y03-y13*x03;
	  dx3=y01*z02-z01*y02; dy3=-x01*z02+z01*x02; dz3=x01*y02-y01*x02;

	  fluxT[0][nume]=-xk*coeff*(t[ne0]*dx0+t[ne1]*dx1+t[ne2]*dx2+t[ne3]*dx3);
	  fluxT[1][nume]=-yk*coeff*(t[ne0]*dy0+t[ne1]*dy1+t[ne2]*dy2+t[ne3]*dy3);
	  fluxT[2][nume]=-zk*coeff*(t[ne0]*dz0+t[ne1]*dz1+t[ne2]*dz2+t[ne3]*dz3);
  
	}
    }
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | postflux_anisotro                                                    |
  |======================================================================| */

void postflux_anisotro(struct Maillage maillnodes,
		       struct Aniso kaniso,double *t,double **fluxT)
{
  int i,nca,nume;
  double s2=0.5, s6=1./6.,coeff;
  double r1,r2,r3;
  int np,nel;
  int ne0,ne1,ne2,ne3,**nodes;
  double xk,yk,zk,xyk,xzk,yzk;
  double dx0,dx1,dx2,dx3,dx02,dx12,dx22,dx32;
  double dy0,dy1,dy2,dy3,dy02,dy12,dy22,dy32;
  double dz0,dz1,dz2,dz3,dz02,dz12,dz22,dz32;
  double x01,y01,z01,x02,y02,z02,x03,y03,z03;
  double x12,y12,z12,x13,y13,z13,x23,y23,z23;
  double kxx,kxy,kyy;
  double *c0,*c1,*c2,**coords;
  double dtdx,dtdy,dtdz;
  int **nar;


  if (maillnodes.iaxisy==1) nca=1; else nca=0;
  np=maillnodes.npoin;   
  nel=maillnodes.nelem;
  nodes=maillnodes.node;
  coords=maillnodes.coord;
  nar=maillnodes.eltarete;

  if (maillnodes.ndim==2 && maillnodes.iaxisy==0)
    {
      for (i=0; i<kaniso.nelem; i++)
	{
	  nume=kaniso.ele[i];
	  xk=kaniso.k11[i]; yk=kaniso.k22[i]; xyk=kaniso.k12[i];

	  ne0=nodes[0][nume];ne1=nodes[1][nume];ne2=nodes[2][nume];
	  c0=*coords; c1=*(coords+1);
	  dx0=-(*(c1+ne2)-*(c1+ne1)); dy0=*(c0+ne2)-*(c0+ne1);
	  dx1=-(*(c1+ne0)-*(c1+ne2)); dy1=*(c0+ne0)-*(c0+ne2);
	  dx2=-(*(c1+ne1)-*(c1+ne0)); dy2=*(c0+ne1)-*(c0+ne0);

	  coeff=s2/maillnodes.volume[nume];
	  
 	  fluxT[0][nume]=-coeff*(xk*(t[ne0]*dx0+t[ne1]*dx1+t[ne2]*dx2)
			       +xyk*(t[ne0]*dy0+t[ne1]*dy1+t[ne2]*dy2));
	  fluxT[1][nume]=-coeff*(xyk*(t[ne0]*dx0+t[ne1]*dx1+t[ne2]*dx2)
			       +yk*(t[ne0]*dy0+t[ne1]*dy1+t[ne2]*dy2));

	}
    }
  else if (maillnodes.ndim==2)
    {
      for (i=0; i<kaniso.nelem; i++)
	{

	  nume=kaniso.ele[i];
	  xk=kaniso.k11[i]; yk=kaniso.k22[i]; xyk=kaniso.k12[i];

	  ne0=nodes[0][nume];ne1=nodes[1][nume];ne2=nodes[2][nume];
	  r1=fabs(*(*(coords+nca)+ne0)) * coeff;
	  r2=fabs(*(*(coords+nca)+ne1)) * coeff;
	  r3=fabs(*(*(coords+nca)+ne2)) * coeff;
	  
/* 	  kxx=  xk* (r1+r2+r3); */
/* 	  kxy=  xyk*(r1+r2+r3); */
/* 	  kyy=  yk* (r1+r2+r3); */

	  c0=*coords; c1=*(coords+1);
	  dx0=-(*(c1+ne2)-*(c1+ne1)); dy0=*(c0+ne2)-*(c0+ne1);
	  dx1=-(*(c1+ne0)-*(c1+ne2)); dy1=*(c0+ne0)-*(c0+ne2);
	  dx2=-(*(c1+ne1)-*(c1+ne0)); dy2=*(c0+ne1)-*(c0+ne0);

	  coeff=s2/maillnodes.volume[nume];
	  
	  
 	  /* chris : a verifier ulterieurement en axy */
	  fluxT[0][nume]=-coeff*(xk*(t[ne0]*dx0+t[ne1]*dx1+t[ne2]*dx2)
			       +xyk*(t[ne0]*dy0+t[ne1]*dy1+t[ne2]*dy2));
	  fluxT[1][nume]=-coeff*(xyk*(t[ne0]*dx0+t[ne1]*dx1+t[ne2]*dx2)
			       +yk*(t[ne0]*dy0+t[ne1]*dy1+t[ne2]*dy2));


	}
    }     
  else
    {
      for (i=0; i<kaniso.nelem; i++)
	{
	  nume=kaniso.ele[i];
	  xk=kaniso.k11[i];   yk=kaniso.k22[i];   zk=kaniso.k33[i];
	  xyk=kaniso.k12[i];  xzk=kaniso.k13[i];  yzk=kaniso.k23[i];
	      
	  ne0=nodes[0][nume];ne1=nodes[1][nume];ne2=nodes[2][nume];ne3=nodes[3][nume];
	  c0=*coords; c1=*(coords+1); c2=*(coords+2);
	  x01 =*(c0+ne1)-*(c0+ne0); y01 =*(c1+ne1)-*(c1+ne0); z01 =*(c2+ne1)-*(c2+ne0);
	  x02 =*(c0+ne2)-*(c0+ne0); y02 =*(c1+ne2)-*(c1+ne0); z02 =*(c2+ne2)-*(c2+ne0);
	  x03 =*(c0+ne3)-*(c0+ne0); y03 =*(c1+ne3)-*(c1+ne0); z03 =*(c2+ne3)-*(c2+ne0);
	  x12 =*(c0+ne2)-*(c0+ne1); y12 =*(c1+ne2)-*(c1+ne1); z12 =*(c2+ne2)-*(c2+ne1);
          x13 =*(c0+ne3)-*(c0+ne1); y13 =*(c1+ne3)-*(c1+ne1); z13 =*(c2+ne3)-*(c2+ne1);
	  x23 =*(c0+ne3)-*(c0+ne2); y23 =*(c1+ne3)-*(c1+ne2); z23 =*(c2+ne3)-*(c2+ne2);

	  dx0=y13*z12-z13*y12; dy0=-x13*z12+z13*x12; dz0=x13*y12-y13*x12;
	  dx1=-y23*z02+z23*y02; dy1=x23*z02-z23*x02; dz1=-x23*y02+y23*x02;
	  dx2=y13*z03-z13*y03; dy2=-x13*z03+z13*x03; dz2=x13*y03-y13*x03;
	  dx3=y01*z02-z01*y02; dy3=-x01*z02+z01*x02; dz3=x01*y02-y01*x02;

	  coeff=s6/maillnodes.volume[nume];
	  
	  dtdx=t[ne0]*dx0+t[ne1]*dx1+t[ne2]*dx2+t[ne3]*dx3;
	  dtdy=t[ne0]*dy0+t[ne1]*dy1+t[ne2]*dy2+t[ne3]*dy3;
	  dtdz=t[ne0]*dz0+t[ne1]*dz1+t[ne2]*dz2+t[ne3]*dz3;

 	  fluxT[0][nume]=-coeff*(xk*dtdx+xyk*dtdy+xzk*dtdz);
	  fluxT[1][nume]=-coeff*(xyk*dtdx+yk*dtdy+yzk*dtdz);
	  fluxT[2][nume]=-coeff*(xzk*dtdx+yzk*dtdy+zk*dtdz);
	}
    }
}

/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <math.h>
# include <stdlib.h>
# include <string.h>

# include "syr_usertype.h"
# include "syr_tree.h"
# include "syr_abs.h"
# include "syr_bd.h"
# include "syr_proto.h"

/* # include "mpi.h" */


extern struct Performances perfo;
extern struct Affichages affich;
extern int nelvoip;
extern int nsp;

double conso=0;

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | build_quadtree                                                       |
  |         Construction du quadtree                                     |
  |======================================================================| */
void build_quadtree_1d (struct node *arbre,int npoinr,int nelray,
			int **nodray,double **cooray, double *size_min,
			double dim_boite[],int nbeltmax_tree)
{
  struct child *p1;
  struct element *f1,*f2;
  
  
  int i,nbelt;
  double dx,dy,dz,dd,ddp;
  double xmin,xmax,ymin,ymax;
  
  
  xmin =  1.e10; ymin=  1.e6 ;
  xmax = -1.e10; ymax= -1.e6 ;
    
  for (i=0;i<npoinr;i++)
    {
      xmin = min(cooray[0][i],xmin); ymin = min(cooray[1][i],ymin);
      xmax = max(cooray[0][i],xmax); ymax = max(cooray[1][i],ymax);
    }


  dx = xmax-xmin; dy=ymax-ymin;
  xmin -= (dx*0.01); ymin -= (dy*0.01); 
  xmax += (dx*0.01); ymax += (dy*0.01); 
  dx = xmax-xmin; dy=ymax-ymin;
     

  dim_boite[0]=xmin; dim_boite[1]=xmax; 
  dim_boite[2]=ymin; dim_boite[3]=ymax; 

  strncpy(arbre->name,"A\0",2);
  arbre->xc = (xmin+xmax)*0.5;   arbre->yc = (ymin+ymax)*0.5;
  arbre->sizx = dx*0.5; arbre->sizy = dy*0.5;    
  arbre->lelement = NULL;
  arbre->lfils = NULL;
  *size_min = min(dx,dy);
  
  f1 = (struct element *)malloc(sizeof(struct element)); conso+=sizeof(struct element);
  if (f1==NULL) 
    {
      if (SYRTHES_LANG == FR)
	printf(" ERREUR build_quadtree_1d : probleme d'allocation memoire\n");
      else if (SYRTHES_LANG == EN)
	printf(" ERROR build_quadtree_1d : Memory allocation problem\n");
      syrthes_exit(1);
    }
  f1->num = 0;
  f1->suivant=NULL;
  arbre->lelement=f1;
  
  for (i=1;i<nelray;i++) 
    {
	f2 = (struct element *)malloc(sizeof(struct element)); conso+=sizeof(struct element);
	if (f2==NULL) 
	  {
	    if (SYRTHES_LANG == FR)
	      printf(" ERREUR build_quadtree_1d : probleme d'allocation memoire\n");
	    else if (SYRTHES_LANG == EN)
	      printf(" ERROR build_quadtree_1d : Memory allocation problem\n");
	    syrthes_exit(1);
	  }
	f2->num = i;
	f2->suivant=NULL;
	f1->suivant = f2;
	f1 = f2;
    }
    nbelt = nelray;

    decoupe1d(arbre,nodray,cooray,nelray,npoinr,nbelt,size_min,nbeltmax_tree);

    if (affich.ray_mem_devel) 
      if (SYRTHES_LANG == FR)
	printf("   ---> build_quadtree_1d : memoire pour la construction de l'arbre %.3f Mo\n",conso/1.e6);
      else if (SYRTHES_LANG == EN)
	printf("   ---> build_quadtree_1d : Memory used for building the quadtree %.3f Mo\n",conso/1.e6);

    perfo.mem_ray+=conso;
    if (perfo.mem_ray>perfo.mem_ray_max) perfo.mem_ray_max=perfo.mem_ray;

    elague_tree(arbre);   

/*   printf("\n\n Arbre apres elaguage\n");
    affiche_tree(arbre,4);   */
     
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | decoupe2d                                                            |
  |         Construction du quadtree                                     |
  |======================================================================| */
void decoupe1d(struct node *noeud,int **nodray,double **cooray,
	       int nelray,int npoinr,int nbelt,double *size_min,int nbeltmax_tree)
{
  double xmin[4],xmax[4],ymin[4],ymax[4];
  double x,y,dx,dy ;
  int i,nbfac,nbelt_max;
  struct node *n1,*n2,*noeudi;
  struct child *f1,*f2;
  struct element *elt1;
  char *c[]={"A","B","C","D","E","F","G","H"};

  
/*   nbelt_max=max(nelvoip*nsp+10,30); */
  
  if (nbelt>nbeltmax_tree)
    {
      
      x = noeud->xc; y = noeud->yc;
      dx = noeud->sizx; dy = noeud->sizy;
      
      xmax[0]=xmax[3]= x;
      xmin[1]=xmin[2]= x;
      xmin[0]=xmin[3]= x - dx;
      xmax[1]=xmax[2]= x + dx;

      ymax[2]=ymax[3]= y;
      ymin[0]=ymin[1]= y;
      ymin[2]=ymin[3]= y - dy;
      ymax[0]=ymax[1]= y + dy;


      f1= (struct child *)malloc(sizeof(struct child)); conso+=sizeof(struct child);
      n1= (struct node *) malloc(sizeof(struct node )); conso+=sizeof(struct node);
      if (f1==NULL || n1==NULL) 
	{
	  if (SYRTHES_LANG == FR)
	    printf(" ERREUR decoupe1d : probleme d'allocation memoire\n");
	  else if (SYRTHES_LANG == EN)
	    printf(" ERROR decoupe1d : Memory allocation problem\n");
	  syrthes_exit(1);
	}

      noeud->lfils = f1;
      strcpy(f1->name,noeud->name); strcat(f1->name,"A"); 
      f1->fils = n1;
      f1->suivant = NULL;

      for (i=1;i<4;i++)
	  {
	    f2= (struct child *)malloc(sizeof(struct child)); conso+=sizeof(struct child);
	    n2= (struct node *) malloc(sizeof(struct node )); conso+=sizeof(struct node);
	    if (f2==NULL || n2==NULL) 
	      {
		if (SYRTHES_LANG == FR)
		  printf(" ERREUR decoupe1d : probleme d'allocation memoire\n");
		else if (SYRTHES_LANG == EN)
		  printf(" ERROR decoupe1d : Memory allocation problem\n");
		syrthes_exit(1);
	      }
	    f1->suivant = f2;
	    strcpy(f2->name,noeud->name); strcat(f2->name,c[i]); 
	    f2->fils = n2;
	    f2->suivant = NULL;
	    f1 = f2;
	  }
      

      f1 = noeud->lfils;

      for (i=0;i<4;i++)
	  {
	    noeudi = f1->fils;
	    strcpy(noeudi->name,noeud->name); strcat(noeudi->name,c[i]); 
	    noeudi->xc = (xmin[i]+xmax[i])*0.5;
	    noeudi->yc = (ymin[i]+ymax[i])*0.5;
	    noeudi->sizx = (xmax[i]-xmin[i])*0.5;
	    noeudi->sizy = (ymax[i]-ymin[i])*0.5;
	    *size_min = min(*size_min,noeudi->sizx);
	    *size_min = min(*size_min,noeudi->sizy);
	    noeudi->lfils = NULL;
	    elt1= (struct element *)malloc(sizeof(struct element)); conso+=sizeof(struct element);
	    if (elt1==NULL) 
	      {
		if (SYRTHES_LANG == FR)
		  printf(" ERREUR decoupe1d : probleme d'allocation memoire\n");
		else if (SYRTHES_LANG == EN)
		  printf(" ERROR decoupe1d : Memory allocation problem\n");
		syrthes_exit(1);
	      }
	    noeudi->lelement = elt1;
	    
	    triseg(noeud->lelement,noeudi->lelement,
		   &nbfac,nelray,npoinr,nodray,cooray,
		   noeudi->xc,noeudi->yc,noeudi->sizx,noeudi->sizy);
	    
	    if (nbfac != 0)
	      decoupe1d(noeudi,nodray,cooray,nelray,npoinr,nbfac,size_min,nbeltmax_tree);
	    else
	      {
		noeudi->lelement = NULL;
		free(elt1);
	      }
	    
	    f1 = f1->suivant;
	  }
      
    }
  
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | triseg                                                               |
  |         Tri des segments pour les placer dans le quadtree            |
  |======================================================================| */
void triseg( struct element *face_pere, struct element *face_fils, 
	     int *nbfac,int nelray,int npoinr,int **nodray,double **cooray,
	     double xcc,double ycc,double dx,double dy)
{
  int i,n,prem ;
  double xa,ya,xb,yb;
  struct element *fp1,*ff1,*ff2;
  
  prem = 1;
  fp1 = face_pere;
  ff1 = face_fils;
  *nbfac = 0;

   do
     {
       n=nodray[0][fp1->num];  xa=cooray[0][n];  ya=cooray[1][n];
       n=nodray[1][fp1->num];  xb=cooray[0][n];  yb=cooray[1][n];
       
       
       if (seg_in_rectan(xa,ya,xb,yb,xcc,ycc,dx,dy))
	 {
	   if (prem)
	     {
	       prem = 0;
	       ff1->num = fp1->num;
	       ff1->suivant = NULL;
	     }
	   else
	     {
	       ff2= (struct element *)malloc(sizeof(struct element)); conso+=sizeof(struct element);
	       if (ff2==NULL) 
		 {
		   if (SYRTHES_LANG == FR)
		     printf(" ERREUR triseg : probleme d'allocation memoire\n");
		   else if (SYRTHES_LANG == EN)
		     printf(" ERROR triseg : Memory allocation problem\n");
		   syrthes_exit(1);
		 }
	       ff2->num = fp1->num;
	       ff2->suivant = NULL;
	       ff1->suivant = ff2;
	       ff1 = ff2;
	     }
	   *nbfac += 1;
	   
	 }
       
       fp1 = fp1->suivant;
       
     }while (fp1 != NULL);

   
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | tria_in_rectan                                                       |
  |         Tri des triangles pour les placer dans le quadtree           |
  |======================================================================| */
int seg_in_rectan(double xa,double ya,double xb,double yb,
		  double xcc,double ycc,double dx,double dy)
{
  double xmin,xmax,ymin,ymax;
  double d2;
  double epsi;
  
  epsi=1.E-5;
  d2=dx+dy; 

  /*   xmin = xcc-dx-epsi; xmax = xcc+dx+epsi; */
  /*   ymin = ycc-dy-epsi; ymax = ycc+dy+epsi; */

  /* au lieu d'un eps, on rajoute un pourcentage de la boite */
  xmin = xcc-1.05*dx; xmax = xcc+1.05*dx;
  ymin = ycc-1.05*dy; ymax = ycc+1.05*dy;


    if (in_rectan (xa,ya,xmin,xmax,ymin,ymax))
      return(1);
    else if (in_rectan(xb,yb,xmin,xmax,ymin,ymax))
      return(1);
  
    else if (xa>xmax && xb>xmax)
      return(0);
    else if (xa<xmin && xb<xmin)
      return(0);
    else if (ya>ymax && yb>ymax)
      return(0);
    else if (ya<ymin && yb<ymin)
      return(0);
  
    else
      {
	xa -= xcc; ya -= ycc;
	xb -= xcc; yb -= ycc;
	
	if (xa+ya<-d2 && xb+yb<-d2)
	    return(0);
	else if (xa+ya>d2  && xb+yb>d2)
	    return(0);
	else if (xa-ya>-d2  && xb-yb>-d2)
	    return(0);
	else if (ya-ya<d2 && yb-yb<d2)
	    return(0);


	else if (seg_rectanx( dx, dy,xa,xb,ya,yb))
	    return(1);
	else if (seg_rectanx(-dx, dy,xa,xb,ya,yb))
	    return(1);
	else if (seg_rectany( dx, dy,xa,xb,ya,yb))
	    return(1);
	else if (seg_rectany( dx,-dy,xa,xb,ya,yb))
	    return(1);
      }
    return(2);
}

    


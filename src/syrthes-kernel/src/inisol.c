/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>

#include "syr_usertype.h"
#include "syr_bd.h"
#include "syr_tree.h"
#include "syr_option.h"
#include "syr_proto.h"
#include "syr_hmt_proto.h"

/* #include "mpi.h" */

extern struct Performances perfo;
extern struct Affichages affich;

extern int nbVar;

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Initialisation des structures de donnees                             |
  |======================================================================| */

void init_clim(struct Maillage *maillnodes,struct MaillageBord *maillnodebord,  
	       struct MaillageCL *maillnodeus,struct MaillageCL *maillnoderc,
	       struct Clim *diric,struct Cperio *perio,
	       struct Clim *flux,struct Clim *echang,struct Contact *rescon,
	       struct Clim *rayinf,struct Cvol *fluxvol,
	       struct Humid humid,struct HmtClimhhh *hmtclimhhh,
	       struct Variable variable,
	       struct SDparall sdparall)
{
  int i,j,n,nb;
  double *p,*p1,*p2;
  int cv;

  /* conditions aux limites */
  /* ---------------------- */

  if (!humid.actif) {
    decode_clim_alloue(maillnodebord);
    decode_clim_cond(maillnodebord);
    decode_clim_all(maillnodebord);
  }
  else {
    decode_clim_alloue(maillnodebord);
    hmt_decode_clim(maillnodebord);
    decode_clim_all(maillnodebord);
  }

  
  xmaill(*maillnodes,*maillnodebord,maillnodeus,sdparall);


  if (!humid.actif) {
    cree_liste_clim_cond(*maillnodes,*maillnodebord,*maillnodeus,diric,flux,echang);
    alloue_val_clim_cond(diric,flux,echang);
    cree_liste_clim_all(*maillnodes,*maillnodebord,*maillnodeus,rescon,rayinf,sdparall);
    alloue_val_clim_all(rescon,rayinf);
  }
  else {
    hmt_cree_liste_clim(*maillnodeus,hmtclimhhh);
    hmt_alloue_val_clim(hmtclimhhh);
    cree_liste_clim_all(*maillnodes,*maillnodebord,*maillnodeus,rescon,rayinf,sdparall);
    alloue_val_clim_all(rescon,rayinf); 
  }


  if (!humid.actif) {
    lire_limite_cond(flux,diric,echang,*maillnodes,*maillnodebord,*maillnodeus);
    lire_limite_all(rescon,rayinf,*maillnodes,*maillnodebord,*maillnodeus);
  }
  else {
    hmt_lire_limite(hmtclimhhh,*maillnodes,*maillnodebord,*maillnodeus);
    lire_limite_all(rescon,rayinf,*maillnodes,*maillnodebord,*maillnodeus);
  }

  if (humid.actif)
    hmt_cree_liste_clim(*maillnodeus,hmtclimhhh);


  /* conditions volumiques */
  /* --------------------- */

  decode_cvol(*maillnodes,fluxvol,variable.nomvar);
  alloue_val_cvol(fluxvol);
  lire_condvol(fluxvol,maillnodes->nrefe);

  for (n=0;n<nbVar;n++)
    {
      /* indicateur de presence de flux volumiques sur au moins 1 domaine */
      if (fluxvol[n].nelem)  fluxvol[n].existglob=1;  else fluxvol[n].existglob=0;
      
#ifdef _SYRTHES_MPI_
      if (syrglob_nparts>1) {
	cv=fluxvol[n].existglob;
	MPI_Allreduce(&cv,&(fluxvol[n].existglob),1,MPI_INT,MPI_SUM,sdparall.syrthes_comm_world);
      }
#endif
    }


  /* periodicite */
  /* ----------- */

  perio->existglob=existe_perio();

  if (perio->existglob)
    {
      if (sdparall.nparts==1) 
	lire_perio(perio,*maillnodes,*maillnodebord,sdparall);
      else {
	perio->npoin=0;
	for (n=0;n<sdparall.nparts;n++) perio->npoin+=sdparall.nbcommunperio[n];
	perio->trav=(double*)malloc(perio->npoin*sizeof(double));
      }
    }
  else
    {perio->npoin=perio->nelem=0;}


  /* resistances de contact */
  /* ---------------------- */

  if (rescon->nelem) 
    {
      if (sdparall.nparts==1) 
	lire_pairesrc(rescon,*maillnodes,*maillnodebord,sdparall); 
      else 
	{
	  rescon->npoin=0;
	  for (n=0;n<sdparall.nparts;n++) rescon->npoin+=sdparall.nbcommunrc[n];
	}
      alloue_rc(rescon);
    }
  else
    rescon->npoin=0;

  /* volumes et surfaces */
  /* ------------------- */

  svolum(maillnodes,maillnodeus);

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Allocation des structures de donnees                                 |
  |======================================================================| */
void alloue_val_clim_cond(struct Clim *diric,struct Clim *flux,struct Clim *echang)
{
  int i,j;
  double *p,*p1,*p2;


  if (diric->npoin>0) 
    {
      diric->val1=(double**)malloc(diric->ndmat*sizeof(double*));
      for (j=0;j<diric->ndmat;j++) diric->val1[j]=(double*)malloc(diric->nelem*sizeof(double));
      verif_alloue_double2d(diric->ndmat,"alloue",diric->val1);
      for (j=0;j<diric->ndmat;j++) 
	for (i=0;i<diric->nelem;i++) diric->val1[j][i]=0;
      perfo.mem_cond+=(diric->ndmat*diric->nelem) * sizeof(double);
    }
  
  if (flux->nelem>0) 
    {
      flux->val1=(double**)malloc(flux->ndmat*sizeof(double*));
      for (i=0;i<flux->ndmat;i++) flux->val1[i]=(double*)malloc(flux->nelem*sizeof(double));
      verif_alloue_double2d(flux->ndmat,"alloue",flux->val1);
      for (j=0;j<flux->ndmat;j++) 
	for (i=0;i<flux->nelem;i++) flux->val1[j][i]=0;
      perfo.mem_cond+=(flux->ndmat*flux->nelem) * sizeof(double);
    }
  
  if (echang->nelem>0) 
    {
      echang->val1=(double**)malloc(echang->ndmat*sizeof(double*));
      for (i=0;i<echang->ndmat;i++) echang->val1[i]=(double*)malloc(echang->nelem*sizeof(double));
      echang->val2=(double**)malloc(echang->ndmat*sizeof(double*));
      for (i=0;i<echang->ndmat;i++) echang->val2[i]=(double*)malloc(echang->nelem*sizeof(double));
      verif_alloue_double2d(echang->ndmat,"alloue",echang->val1);
      verif_alloue_double2d(echang->ndmat,"alloue",echang->val2);
      for (j=0;j<echang->ndmat;j++) 
	for (i=0;i<echang->nelem;i++) echang->val1[j][i]=echang->val2[j][i]=0;
      perfo.mem_cond+=(echang->ndmat*echang->nelem) * 2 * sizeof(double);
    }
  
  if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Allocation des structures de donnees                                 |
  |======================================================================| */
void alloue_val_clim_all(struct Contact *rescon,struct Clim *rayinf)
{
  int i,j,n;
  double *p,*p1,*p2;


  rescon->g=(double***)malloc(nbVar*sizeof(double**));

  if (rescon->nelem>0) 
    {
      for (n=0;n<nbVar;n++)
	{
	  rescon->g[n]=(double**)malloc(rescon->ndmat*sizeof(double*));
	  for (i=0;i<rescon->ndmat;i++) rescon->g[n][i]=(double*)malloc(rescon->nelem*sizeof(double));
	  verif_alloue_double2d(rescon->ndmat,"alloue",rescon->g[n]);
	  for (j=0;j<rescon->ndmat;j++) 
	    for (i=0;i<rescon->nelem;i++) rescon->g[n][j][i]=0;
	  perfo.mem_cond+=(rescon->ndmat*rescon->nelem) * sizeof(double);
	}
    }


  if (rayinf->nelem>0) 
    {
      rayinf->val1=(double**)malloc(rayinf->ndmat*sizeof(double*));
      for (i=0;i<rayinf->ndmat;i++) rayinf->val1[i]=(double*)malloc(rayinf->nelem*sizeof(double));
      rayinf->val2=(double**)malloc(rayinf->ndmat*sizeof(double*));
      for (i=0;i<rayinf->ndmat;i++) rayinf->val2[i]=(double*)malloc(rayinf->nelem*sizeof(double));
      verif_alloue_double2d(rayinf->ndmat,"alloue",rayinf->val1);
      verif_alloue_double2d(rayinf->ndmat,"alloue",rayinf->val2);
      for (j=0;j<rayinf->ndmat;j++) 
	for (i=0;i<rayinf->nelem;i++) rayinf->val1[j][i]=rayinf->val2[j][i]=0;
      perfo.mem_cond+=(rayinf->ndmat*rayinf->nelem) * 2 * sizeof(double);
    }


  if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Allocation des structures de donnees                                 |
  |======================================================================| */
void alloue_val_cvol(struct Cvol *fluxvol)
{
  int n,i;
  double *p,*q;

  for (n=0;n<nbVar;n++)
    if (fluxvol[n].nelem>0) 
      {
	fluxvol[n].nbval=2;

	fluxvol[n].val1=(double*)malloc(fluxvol[n].nelem*sizeof(double));
	verif_alloue_double1d("alloue",fluxvol[n].val1);

	fluxvol[n].val2=(double*)malloc(fluxvol[n].nelem*sizeof(double));
	verif_alloue_double1d("alloue",fluxvol[n].val2);

	for (i=0,p=fluxvol[n].val1,q=fluxvol[n].val2;i<fluxvol[n].nelem;i++,p++,q++) 
	  {*p=0.; *q=0.;}

	perfo.mem_cond+=(fluxvol[n].nelem) * sizeof(double);
      }
  
  if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;
}


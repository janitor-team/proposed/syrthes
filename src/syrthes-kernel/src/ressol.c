/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

#include "syr_usertype.h"
#include "syr_bd.h"
#include "syr_option.h"
#include "syr_abs.h"
#include "syr_tree.h"
#include "syr_const.h"
#include "syr_parall.h"

#include "syr_proto.h"

#ifdef _SYRTHES_MPI_
#include "mpi.h"
MPI_Status status;
#endif

extern struct Performances perfo;
extern struct Affichages affich;
extern int optim,lmst;

#ifdef _SYRTHES_CFD_
extern int nbCFD;
#endif

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |   Gestion de la resolution de la conduction                          |
  |======================================================================| */
void ressol(struct PasDeTemps *pasdetemps,
	    struct Maillage maillnodes, struct MaillageCL maillnodeus,  
	    struct MaillageCL maillnoderc,struct MaillageBord maillnodebord,  
	    struct Clim diric,struct Cperio perio,
	    struct Climcfd scoupf,struct Climcfd scouvf,
	    struct Clim flux,struct Clim echang,
	    struct Contact rescon,struct Couple scoupr,struct Clim rayinf,
	    struct Cvol fluxvol,struct Mst mst,
	    struct Variable variable,
	    struct Prophy physol,struct Meteo meteo,struct Myfile myfile,
	    struct Matrice matr,
	    struct Travail trav,struct Travail trave,
	    struct SDparall sdparall)
{
  int i;
  double s;
  int numvar=0;
  double *tmpsa,*tmps;

  if (syrglob_nparts==1 ||syrglob_rang==0)
    {
      if (SYRTHES_LANG == FR)        printf("\n ---RESOLUTION DE LA TEMPERATURE----\n");
      else if (SYRTHES_LANG == EN)   printf("\n ---TEMPERATURE SOLVER---\n");
      
    }

  tmps  = variable.var[variable.adr_t];
  tmpsa = variable.var[variable.adr_t_m1];

  /* sauvegarde de l'etape n */
  for (i=0;i<maillnodes.npoin;i++) {*(tmpsa+i)=*(tmps+i);}


  user_cphyso_fct(maillnodes,tmpsa,physol,pasdetemps->tempss);
  user_cphyso    (maillnodes,tmpsa,physol,pasdetemps,meteo,myfile);

  user_limfso_fct(maillnodes,maillnodebord,maillnodeus,tmpsa,diric,flux,echang,rayinf,pasdetemps->tempss);
  user_limfso    (maillnodes,maillnodebord,maillnodeus,tmpsa,diric,flux,echang,rayinf,pasdetemps,meteo,myfile);


  if (fluxvol.nelem) user_cfluvs_fct(maillnodes,tmpsa,fluxvol,pasdetemps->tempss);
  user_cfluvs    (maillnodes,tmpsa,fluxvol,pasdetemps,meteo,myfile);

  if (rescon.nelem) user_rescon_fct(maillnodes,maillnodeus,tmpsa,trav.tab[0],rescon,pasdetemps->tempss,sdparall);
  user_rescon(maillnodes,maillnodeus,tmpsa,trav.tab[0],rescon,pasdetemps,sdparall);

  difsol(*pasdetemps,
         maillnodes,maillnodeus,maillnodebord,
	 tmps,tmpsa,physol,
	 diric,echang,rescon,scoupr,flux,rayinf,scoupf,scouvf,fluxvol,
	 perio,mst,matr,trav,trave,
	 sdparall);

}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |        Resolution de la diffusion                                    |
  |======================================================================| */

void difsol(struct PasDeTemps pasdetemps,
	    struct Maillage maillnodes,
	    struct MaillageCL maillnodeus,struct MaillageBord maillnodebord,
	    double *tmps,double *tmpsa,struct Prophy physol,
	    struct Clim diric,struct Clim echang,struct Contact rescon,
	    struct Couple scoupr,struct Clim flux,struct Clim rayinf,
	    struct Climcfd scoupf,struct Climcfd scouvf,
	    struct Cvol fluxvol,struct Cperio perio,
	    struct Mst mst,struct Matrice matr,struct Travail trav,struct Travail trave,
	    struct SDparall sdparall )
{
  int i,j,nn;
  double rindts,*trav0,*trav1,*trav2,*trav3,*trav5,*coeff;
  int lseg=1;
  static int prem=1;


  trav0=trav.tab[0];
  trav1=trav.tab[1];
  trav2=trav.tab[2];
  trav3=trav.tab[3];
  trav5=trav.tab[5];
  
  coeff=trave.tab[0];

  /* calcul de la matrice de masse */
  /* ----------------------------- */
  azero1D(maillnodes.npoin,trav1);
  for (i=0;i<maillnodes.nelem;i++) coeff[i]=physol.rho[i]*physol.cp[i];
  mamass(pasdetemps,maillnodes,coeff,trav1);
  if (sdparall.nparts>1) assemb_complt(trav1,maillnodes.npoin,trav0,sdparall);
  
  /* ----> ici trav1 contient la matrice de masse finale */
  if (affich.cond_mat) 
    {
      if (SYRTHES_LANG == FR)
	{
	  if (sdparall.nparts > 1)
	    for (i=0;i<maillnodes.npoin;i++) 
	      printf("matrice de masse : rang=%d nglob=%d trav1[%d]=%25.18e\n",
		     sdparall.rang,sdparall.npglob[i],i,trav1[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) 
	      printf("matrice de masse : trav1[%d]=%25.18e\n",i,trav1[i]);
	}
      else if (SYRTHES_LANG == EN)
	{
	  if (sdparall.nparts > 1)
	    for (i=0;i<maillnodes.npoin;i++) 
	      printf("mass matrix : rank=%d nglob=%d trav1[%d]=%25.18e\n",
		     sdparall.rang,sdparall.npglob[i],i,trav1[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) printf("mass matrix : trav1[%d]=%25.18e\n",i,trav1[i]);
	}
    }


  /* matrice de diffusion  */
  /* --------------------- */
  if (prem ||  physol.kvar)
    {
      azero1D(maillnodes.npoin,trav2);
      azero1D(maillnodes.nbarete,matr.xdms);
      if (physol.kiso.nelem)   madif_isotro(maillnodes,physol.kiso,trav2,matr.xdms);
      if (physol.kortho.nelem) madif_orthotro(maillnodes,physol.kortho,trav2,matr.xdms);
      if (physol.kaniso.nelem) madif_anisotro(maillnodes,physol.kaniso,trav2,matr.xdms);
/*       M_mat(maillnodes,trav2,matr.xdms); */
      if (sdparall.nparts>1) assemb_complt(trav2,maillnodes.npoin,trav0,sdparall);
      
      /* ----> ici trav2 contient la diagonale de la matrice de diffusion */
      if (affich.cond_mat) 
	{
	  if (SYRTHES_LANG == FR)
	    {
	      if (sdparall.nparts > 1)
		for (i=0;i<maillnodes.npoin;i++) printf("rang=%d, nglob=%d trav2[%d]=%25.18e\n",
							sdparall.rang,sdparall.npglob[i],i,trav2[i]);
	      else
		for (i=0;i<maillnodes.npoin;i++) printf("tableau trav2[%d]=%25.18e\n",i,trav2[i]);
	    }
	  else if (SYRTHES_LANG == EN)
	    {
	      if (sdparall.nparts > 1)
		for (i=0;i<maillnodes.npoin;i++) printf("rank=%d, nglob=%d trav2[%d]=%25.18e\n",
							sdparall.rang,sdparall.npglob[i],i,trav2[i]);
	      else
		for (i=0;i<maillnodes.npoin;i++) printf("table trav2[%d]=%25.18e\n",i,trav2[i]);
	    }
	}

      if (!physol.kvar) 
	{
	  free(maillnodes.eltarete);
	  for (i=0;i<maillnodes.npoin;i++) *(matr.ddiff+i)=*(trav2+i);
	  for (i=0;i<diric.nbarete;i++) diric.xdms[i]=matr.xdms[diric.arete[i]];
	}
      else
	{
	  if (prem) free(matr.ddiff);
	  for (i=0;i<diric.nbarete;i++) diric.xdms[i]=matr.xdms[diric.arete[i]];
	}

      prem=0;
    }

  

  /* contribution des CL surfaciques implicites */
  /* ------------------------------------------ */
  azero1D(maillnodes.npoin,trav3);
  if (maillnodeus.nelem)
    {
      mafcli(echang,scoupr,rayinf,scoupf,maillnodes,maillnodeus,
	     tmpsa,trav3,trav0);

      if (sdparall.nparts>1)      
	assemb_complt(trav3,maillnodes.npoin,trav0,sdparall);


      if (affich.cond_mat) {
	if (sdparall.nparts > 1)	
	  for (i=0;i<maillnodes.npoin;i++) printf("after mafcli : rang=%d nglob=%d trav3[%d]=%25.18e\n",
						  sdparall.rang,sdparall.npglob[i],i,trav3[i]);
	else
	  for (i=0;i<maillnodes.npoin;i++) printf("after mafcli : trav3[%d]=%25.18e\n",i,trav3[i]);
      }
    }
  else if (sdparall.nparts>1) 
    assemb_complt(trav3,maillnodes.npoin,trav0,sdparall);


  /* diagonale de la matrice (masse+diff+CL) */
  /* --------------------------------------- */
  if (physol.kvar)
    for (i=0;i<maillnodes.npoin;i++) *(matr.dmat+i)=*(trav1+i)+*(trav2+i)+*(trav3+i);
  else
    for (i=0;i<maillnodes.npoin;i++) *(matr.dmat+i)=*(trav1+i)+*(matr.ddiff+i)+*(trav3+i);

  if (affich.cond_mat) 
    {
      if (SYRTHES_LANG == FR)
	{
	  if (sdparall.nparts > 1)	
	    for (i=0;i<maillnodes.npoin;i++) printf("rang=%d nglob=%d matr.dmat[%d]=%25.18e\n",
						    sdparall.rang,sdparall.npglob[i],i,matr.dmat[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) printf("matr.dmat[%d]=%25.18e\n",i,matr.dmat[i]);
	}
      else if (SYRTHES_LANG == EN)
	{
	  if (sdparall.nparts > 1)	
	    for (i=0;i<maillnodes.npoin;i++) printf("rank=%d nglob=%d matr.dmat[%d]=%25.18e\n",
						    sdparall.rang,sdparall.npglob[i],i,matr.dmat[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) printf("matr.dmat[%d]=%25.18e\n",i,matr.dmat[i]);
	}
    }

  /* Contribution implicite du couplage volumique cfd s'il existe */
  /* ------------------------------------------------------------ */
#ifdef _SYRTHES_CFD_
  if (scouvf.existglob)
    {
      azero1D(maillnodes.npoin,trav2);
      if (scouvf.exist)
	cfd_vol_impl(maillnodes,scouvf,trav2);
      else if (sdparall.nparts>1)
	assemb_complt(trav2,maillnodes.npoin,trav0,sdparall);
      
      for (i=0;i<maillnodes.npoin;i++) *(matr.dmat+i)+=*(trav2+i);
    }
#endif

  /* Contribution implicite du couplage volumique SYRTHES  
     - on a alors mis sous la forme A*(B-T) : A et B calcule a partir de a et b 
     Rq: si A= alors on est purement en explicite */
  /* ------------------------------------------------------------ */
  if (fluxvol.existglob)
    {
      azero1D(maillnodes.npoin,trav2);
      if (fluxvol.nelem) smfvos_impl(maillnodes,fluxvol,trav2);
      if (sdparall.nparts>1) assemb_complt(trav2,maillnodes.npoin,trav0,sdparall);
      
      for (i=0;i<maillnodes.npoin;i++) *(matr.dmat+i)+=*(trav2+i);
    }


  /* contribution des Resistances de contact */
  /* --------------------------------------- */
  if (rescon.existglob)
    {
      if (rescon.npoin)
	{
	  for (i=0;i<maillnodes.npoin;i++) *(trav5+i)=0;
	  mafclc(rescon,maillnodes,maillnodeus,tmpsa,trav5,trav0);
	  
	  if (sdparall.nparts>1) 
	    for (j=nn=0;nn<sdparall.nparts;nn++)
	      for (i=0;i<sdparall.nbcommunrc[nn];i++)
		{rescon.gnonassc[j]=trav5[sdparall.tcommunrc[sdparall.adrcommunrc[nn]+2*i]]; j++;}
	  else
	    {
	      /* !!!!!! Essai chris d'integration dans la matrice en sequentiel */
/* ------------- isa inversion ici */
	       	    for (i=0;i<rescon.npoin;i++)
	       	      rescon.gnonassc[i]=rescon.gassc[i]=trav5[rescon.nump[i]];

/* 	      for (i=0;i<rescon.npoin;i++) */
/* 		{ */
/* 		  matr.dmat[rescon.nump[i]]+=trav5[rescon.nump[i]]; */
		  /* chris - pour l'instant je ne sais pas si c'est gassc ou gnonassc en scalaire c'est le meme */
/* 		  rescon.gnonassc[i]=trav5[rescon.nump[i]]; */
/* 		} */
	      /* Fin Essai chris d'integration dans la matrice en sequentiel */	    
	    }
	  
	  /* on fait maintenant l'assemblage complementaire */
 	  if (sdparall.nparts>1)       
	    {
	      assemb_complt(trav5,maillnodes.npoin,trav0,sdparall); 
	      for (j=nn=0;nn<sdparall.nparts;nn++)
		for (i=0;i<sdparall.nbcommunrc[nn];i++)
		  {rescon.gassc[j]=trav5[sdparall.tcommunrc[sdparall.adrcommunrc[nn]+2*i]]; j++;}
	    }
	  if (affich.cond_mat) 
	    {
	      if (SYRTHES_LANG == FR)
		{
		  if (sdparall.nparts > 1)	
		    for (i=0;i<maillnodes.npoin;i++) printf("apres mafclc : rang=%d nglob=%d trav5[%d]=%25.18e\n",
							    sdparall.rang,sdparall.npglob[i],i,trav5[i]);
		  else
		    for (i=0;i<maillnodes.npoin;i++) printf("apres mafclc : trav5[%d]=%25.18e\n",i,trav5[i]);
		}
	      else if (SYRTHES_LANG == EN)
		{
		  if (sdparall.nparts > 1)	
		    for (i=0;i<maillnodes.npoin;i++) printf("after mafclc : rank=%d nglob=%d trav5[%d]=%25.18e\n",
							    sdparall.rang,sdparall.npglob[i],i,trav5[i]);
		  else
		    for (i=0;i<maillnodes.npoin;i++) printf("after mafclc : trav5[%d]=%25.18e\n",i,trav5[i]);
		}
	    }
	}
      else if (sdparall.nparts>1) 
	assemb_complt(trav5,maillnodes.npoin,trav0,sdparall); /* erreur_chris : ici je ne comprends pas */
      
    }

  /* second membre */
  /* ------------- */
  for (i=0;i<maillnodes.npoin;i++) *(matr.b+i)=*(trav1+i)* *(tmpsa+i);

  if (affich.cond_mat) 
    {
      if (SYRTHES_LANG == FR)
	{
	  if (sdparall.nparts > 1)	
	    for (i=0;i<maillnodes.npoin;i++) printf("rang=%d nglob=%d matr.b[%d]=%25.18e\n",
						    sdparall.rang,sdparall.npglob[i]-1,i,matr.b[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) printf("matr.b[%d]=%25.18e\n",i,matr.b[i]);
	}
      else if (SYRTHES_LANG == EN)
	{
	  if (sdparall.nparts > 1)	
	    for (i=0;i<maillnodes.npoin;i++) printf("rank=%d nglob=%d matr.b[%d]=%25.18e\n",
						    sdparall.rang,sdparall.npglob[i]-1,i,matr.b[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) printf("matr.b[%d]=%25.18e\n",i,matr.b[i]);
	}
    }

  if (fluxvol.existglob)
    {
      azero1D(maillnodes.npoin,trav1);
      if (fluxvol.nelem) smfvos_expl (maillnodes,fluxvol,trav1);
      if (sdparall.nparts>1) assemb_complt(trav1,maillnodes.npoin,trav0,sdparall);
      for (i=0;i<maillnodes.npoin;i++) *(matr.b+i)+=*(trav1+i);

      if (affich.cond_mat) 
	if (SYRTHES_LANG == FR)
	  for (i=0;i<maillnodes.npoin;i++) printf("apres fluxvol : matr.b[%d]=%f\n",i,matr.b[i]);
	else if (SYRTHES_LANG == EN)
	  for (i=0;i<maillnodes.npoin;i++) printf("after fluxvol : matr.b[%d]=%f\n",i,matr.b[i]);
    }


  /* Contribution explicite du couplage volumique cfd s'il existe */
  /* ------------------------------------------------------------ */
#ifdef _SYRTHES_CFD_
  if (scouvf.existglob)
    {
      azero1D(maillnodes.npoin,trav2);
      if (scouvf.exist)
	cfd_vol_expl(maillnodes,scouvf,trav2);
      else if (sdparall.nparts>1)
	assemb_complt(trav2,maillnodes.npoin,trav0,sdparall);
      
      for (i=0;i<maillnodes.npoin;i++) *(matr.b+i)+=*(trav2+i);
    }
#endif


  if (lmst)
    {/* ????????????????? smfvol pas mis a jour - a voir par chris ulterieurement*/
/*       smfvol(maillnodes,mst.nelem,mst.nume,mst.divflux,matr.b,matr.wct); */
/*       azero1D(maillnodes.npoin,trav1); */
/*       assemb(maillnodes,trav1,matr.wct); */
/*       for (i=0;i<maillnodes.npoin;i++) *(matr.b+i)+=*(trav1+i); */
    }

  if (perio.existglob)
    if (sdparall.nparts==1) 
      {
	period(maillnodes,matr.dmat,trav1,perio);
	period(maillnodes,matr.b,trav1,perio);
      }
    else
      {
	period_parall(maillnodes,matr.dmat,trav1,perio,sdparall);
	period_parall(maillnodes,matr.b,trav1,perio,sdparall);
      }


  azero1D(maillnodes.npoin,trav1);
  for (i=0;i<maillnodeus.nelem*maillnodeus.ndmat;i++) {*(trav0+i)=0.;}
  if (maillnodeus.nelem)
    {
      smfflu(echang,scoupr,rayinf,scoupf,flux,maillnodes,maillnodeus,tmpsa,trav0);
      matbord(maillnodes,maillnodeus,trav0,trav1);

      if (affich.cond_mat) 
	{
	  printf("flux contribution\n");
	  if (sdparall.nparts > 1)	
	    for (i=0;i<maillnodes.npoin;i++) printf("rank=%d nglob=%d trav1[%d]=%25.18e\n",
						    sdparall.rang,sdparall.npglob[i],i,trav1[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) printf("trav1[%d]=%25.18e\n",i,trav1[i]);
	}

      if (sdparall.nparts>1)      
	assemb_complt(trav1,maillnodes.npoin,trav0,sdparall);

    }
  else if (sdparall.nparts>1) /* pas de cond de flux mais du parallelisme : il faut assurer les communications */
    {
      assemb_complt(trav1,maillnodes.npoin,trav0,sdparall);
    }

  for (i=0;i<maillnodes.npoin;i++) *(matr.b+i)+=*(trav1+i); 


  /* if (diric.nelem) */
  /* pour le parall, on rentre toujours dans smdirs */
  /* voir isa - peut etre a modifier avec un indicateur global */
  smdirs(diric,maillnodes,maillnodebord,tmpsa,perio,matr,trav,sdparall); 





  if (sdparall.nparts==1)           
    grconj(maillnodes,tmps,rescon,perio,trav,matr,epsgcs_t,nitmx_t);
  else
    grconj_parall(maillnodes,tmps,rescon,perio,trav,matr,epsgcs_t,nitmx_t,sdparall);

/*   if (sdparall.nparts > 1)	 */
/*     for (i=0;i<maillnodes.npoin;i++) printf("rang=%d nglob=%d tmps[%d]=%25.18e\n", */
/* 					    sdparall.rang,sdparall.npglob[i],i,tmps[i]); */
/*   else */
/*     for (i=0;i<maillnodes.npoin;i++) printf("tmps[%d]=%25.18e\n",i,tmps[i]); */
  

}




/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Prise en compte des flux volumiques explicite                        |
  |======================================================================| */

void smfvos_expl(struct Maillage maillnodes,
            struct Cvol fluxvol,double *res)
{
  int i,j,iv,np,nca,nel,**nodes;
  int *ne0,*ne1,*ne2,*ne3;
  double s12=1./12.,s4=1./4.,s3=1./3.,s20=1./20.,sv3,sv4,sv12,sv20;
  double r1,r2,r3;
  double *w0,*w1,*w2,*w3;
  double **coords;
  double xx;

  coords=maillnodes.coord;
  nodes=maillnodes.node;
  np=maillnodes.npoin;
  nel=fluxvol.nelem;
 
  if (maillnodes.iaxisy==1) nca=1; else nca=0;

  if (maillnodes.ndim==2 && maillnodes.iaxisy==0)
    for (iv=0;iv<nel;iv++)
      {
	i=fluxvol.nume[iv];
	xx=fluxvol.val2[iv]*s3*maillnodes.volume[i];
	res[nodes[0][i]]+=xx;  res[nodes[1][i]]+=xx;  res[nodes[2][i]]+=xx;
      }    
  else if (maillnodes.ndim==2)
    for (iv=0;iv<nel;iv++)
      {
	i=fluxvol.nume[iv];
	sv12=s12*maillnodes.volume[i]*fluxvol.val2[iv];
	r1=fabs(*(*(coords+nca)+nodes[0][i])); 
	r2=fabs(*(*(coords+nca)+nodes[1][i])); 
	r3=fabs(*(*(coords+nca)+nodes[2][i]));   
	res[nodes[0][i]]+= sv12*(2*r1+r2+r3);
	res[nodes[1][i]]+= sv12*(r1+2*r2+r3);
	res[nodes[2][i]]+= sv12*(r1+r2+2*r3);
      }
  else
    for (iv=0;iv<fluxvol.nelem;iv++)
      {
	i=fluxvol.nume[iv];
	xx=fluxvol.val2[iv]*s4*maillnodes.volume[i];
	res[nodes[0][i]]+=xx;  res[nodes[1][i]]+=xx;  
	res[nodes[2][i]]+=xx;  res[nodes[3][i]]+=xx;
      }
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Prise en compte des flux volumiques implicite                        |
  |======================================================================| */

void smfvos_impl(struct Maillage maillnodes,
            struct Cvol fluxvol,double *res)
{
  int i,j,iv,np,nca,nel,**nodes;
  int *ne0,*ne1,*ne2,*ne3;
  double s12=1./12.,s4=1./4.,s3=1./3.,s20=1./20.,sv3,sv4,sv12,sv20;
  double r1,r2,r3;
  double *w0,*w1,*w2,*w3;
  double **coords;
  double xx;

  coords=maillnodes.coord;
  nodes=maillnodes.node;
  np=maillnodes.npoin;
  nel=fluxvol.nelem;
 
  if (maillnodes.iaxisy==1) nca=1; else nca=0;

  if (maillnodes.ndim==2 && maillnodes.iaxisy==0)
    for (iv=0;iv<nel;iv++)
      {
	i=fluxvol.nume[iv];
	xx=-fluxvol.val1[iv]*s3*maillnodes.volume[i];
	res[nodes[0][i]]+=xx;  res[nodes[1][i]]+=xx;  res[nodes[2][i]]+=xx;
      }    
  else if (maillnodes.ndim==2)
    for (iv=0;iv<nel;iv++)
      {
	i=fluxvol.nume[iv];
	sv12=-s12*maillnodes.volume[i]*fluxvol.val1[iv];
	r1=fabs(*(*(coords+nca)+nodes[0][i])); 
	r2=fabs(*(*(coords+nca)+nodes[1][i])); 
	r3=fabs(*(*(coords+nca)+nodes[2][i]));   
	res[nodes[0][i]]+= sv12*(2*r1+r2+r3);
	res[nodes[1][i]]+= sv12*(r1+2*r2+r3);
	res[nodes[2][i]]+= sv12*(r1+r2+2*r3);
      }
  else
    for (iv=0;iv<fluxvol.nelem;iv++)
      {
	i=fluxvol.nume[iv];
	xx=-fluxvol.val1[iv]*s4*maillnodes.volume[i];
	res[nodes[0][i]]+=xx;  res[nodes[1][i]]+=xx;  
	res[nodes[2][i]]+=xx;  res[nodes[3][i]]+=xx;
      }
}
         

#ifdef _SYRTHES_CFD_
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Prise en compte des flux volumiques implicit cfd                     |
  |======================================================================| */
void cfd_vol_impl(struct Maillage maillnodes,struct Climcfd scouvf,double *res)
{
  int i,j,iv,np,nca,n,**nodes;
  int *ne0,*ne1,*ne2,*ne3;
  double s12=1./12.,s4=1./4.,s3=1./3.,s20=1./20.,sv3,sv4,sv12,sv20;
  double r1,r2,r3;
  double *w0,*w1,*w2,*w3;
  double **coords;
  double xx;

  coords=maillnodes.coord;
  nodes=maillnodes.node;
  np=maillnodes.npoin;

  if (maillnodes.iaxisy==1) nca=1; else nca=0;

  for (n=0;n<nbCFD;n++)
    if (scouvf.nelem[n])
      {
	if (maillnodes.ndim==2 && maillnodes.iaxisy==0)
	  for (iv=0;iv<scouvf.nelem[n];iv++)
	    {
	      i=scouvf.nume[n][iv];
	      xx=scouvf.hfluid[n][iv]*s3*maillnodes.volume[i];
	      res[nodes[0][i]]+=xx;res[nodes[1][i]]+=xx;res[nodes[2][i]]+=xx;
	    }    
	else if (maillnodes.ndim==2)
	  for (iv=0;iv<scouvf.nelem[n];iv++)
	    {
	      i=scouvf.nume[n][iv];
	      sv12=s12*maillnodes.volume[i]*scouvf.hfluid[n][iv];
	      r1=fabs(*(*(coords+nca)+nodes[0][i])); 
	      r2=fabs(*(*(coords+nca)+nodes[1][i])); 
	      r3=fabs(*(*(coords+nca)+nodes[2][i]));   
	      res[nodes[0][i]]+= sv12*(2*r1+r2+r3);
	      res[nodes[1][i]]+= sv12*(r1+2*r2+r3);
	      res[nodes[2][i]]+= sv12*(r1+r2+2*r3);
	    }
	else
	  for (iv=0;iv<scouvf.nelem[n];iv++)
	    {
	      i=scouvf.nume[n][iv];
	      xx=scouvf.hfluid[n][iv]*s4*maillnodes.volume[i];
	      res[nodes[0][i]]+=xx;res[nodes[1][i]]+=xx;res[nodes[2][i]]+=xx;res[nodes[3][i]]+=xx;
	    }
      }
}
#endif
#ifdef _SYRTHES_CFD_
  /*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Prise en compte des flux volumiques explicit cfd                     |
  |======================================================================| */
void cfd_vol_expl(struct Maillage maillnodes,struct Climcfd scouvf,double *res)
{
  int i,j,iv,np,nca,n,nel,**nodes;
  int *ne0,*ne1,*ne2,*ne3;
  double s12=1./12.,s4=1./4.,s3=1./3.,s20=1./20.,sv3,sv4,sv12,sv20;
  double r1,r2,r3;
  double *w0,*w1,*w2,*w3;
  double **coords;
  double xx;

  coords=maillnodes.coord;
  nodes=maillnodes.node;
  np=maillnodes.npoin;
 
  if (maillnodes.iaxisy==1) nca=1; else nca=0;

  for (n=0;n<nbCFD;n++)
    if (scouvf.nelem[n])
      {
	if (maillnodes.ndim==2 && maillnodes.iaxisy==0)
	  for (iv=0;iv<scouvf.nelem[n];iv++)
	    {
	      i=scouvf.nume[n][iv];
	      xx=scouvf.hfluid[n][iv]*scouvf.tfluid[n][iv]*s3*maillnodes.volume[i];
	      res[nodes[0][i]]+=xx;res[nodes[1][i]]+=xx;res[nodes[2][i]]+=xx;
	    }    
	else if (maillnodes.ndim==2)
	  for (iv=0;iv<scouvf.nelem[n];iv++)
	    {
	      i=scouvf.nume[n][iv];
	      sv12=s12*maillnodes.volume[i]*scouvf.hfluid[n][iv]*scouvf.tfluid[n][iv];
	      r1=fabs(*(*(coords+nca)+nodes[0][i])); 
	      r2=fabs(*(*(coords+nca)+nodes[1][i])); 
	      r3=fabs(*(*(coords+nca)+nodes[2][i]));   
	      res[nodes[0][i]]+= sv12*(2*r1+r2+r3);
	      res[nodes[1][i]]+= sv12*(r1+2*r2+r3);
	      res[nodes[2][i]]+= sv12*(r1+r2+2*r3);
	    }
	else
	  for (iv=0;iv<scouvf.nelem[n];iv++)
	    {
	      i=scouvf.nume[n][iv];
	      xx=scouvf.hfluid[n][iv]*scouvf.tfluid[n][iv]*s4*maillnodes.volume[i];
	      res[nodes[0][i]]+=xx;res[nodes[1][i]]+=xx;res[nodes[2][i]]+=xx;res[nodes[3][i]]+=xx;
	    }
      }
  
}
#endif
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Prise en compte des conditions de type flux au 2nd membre            |
  |                                                                      |
  | trav0 est de dimension                                               |
  |======================================================================| */

void smfflu(struct Clim echang,struct Couple scoupr,
            struct Clim rayinf,struct Climcfd scoupf,struct Clim flux,
	    struct Maillage maillnodes,struct MaillageCL maillnodeus,
	    double *tmpsa,double *trav0)
{
  int i,j,nf,ng,n;
  int nel,**nodeus;
  double hray,tr;


  nel=maillnodeus.nelem; 
  nodeus=maillnodeus.node;


#ifdef _SYRTHES_CFD_
  /*2.1 Termes de couplage thermique avec le fluide */
  if (scoupf.exist > 0) {
    for (n=0;n<nbCFD;n++)
      if (scoupf.nelem[n])
        for (j=0;j<scoupf.ndmat;j++)
          for (i=0;i<scoupf.nelem[n];i++)
            trav0[scoupf.nume[n][i]+nel*j]+=scoupf.hfluid[n][i]*scoupf.tfluid[n][i];
  }
#endif


  /*2.2 Termes du au coefficient d'echange */
  for (j=0;j<echang.ndmat;j++)
    for (i=0;i<echang.nelem;i++)
      trav0[echang.numf[i]+nel*j]+=echang.val2[j][i]*echang.val1[j][i];

  /*2.3 Termes du au rayonnement infini */
  for (j=0;j<rayinf.ndmat;j++)
    for (i=0;i<rayinf.nelem;i++)
      {
	nf=rayinf.numf[i]; ng=*(*(nodeus+j)+nf);
	tr=rayinf.val1[j][i];
	hray=rayinf.val2[j][i]*sigma*(tmpsa[ng]+tr+2*tkel)* 
	      ((tmpsa[ng]+tkel)*(tmpsa[ng]+tkel)+(tr+tkel)*(tr+tkel));
	trav0[nf+nel*j]+=rayinf.val1[j][i]*hray;
      }

  /*2.4 Termes du au rayonnement  */
  if (scoupr.nelem)
    for (j=0;j<maillnodeus.ndmat;j++)
      for (i=0;i<scoupr.nelem;i++)
	{
	  nf=scoupr.numf[i]; ng=*(*(nodeus+j)+nf);
	  tr=scoupr.t[j][i];
	  hray=scoupr.h[j][i]*sigma*(tmpsa[ng]+tr+2*tkel)* 
	    ((tmpsa[ng]+tkel)*(tmpsa[ng]+tkel)+(tr+tkel)*(tr+tkel));
	  trav0[nf+nel*j]+=scoupr.t[j][i]*hray;
	}

  /*2.5 Termes du aux flux */
  for (j=0;j<flux.ndmat;j++)
    for (i=0;i<flux.nelem;i++)
       trav0[flux.numf[i]+nel*j]+=flux.val1[j][i];



}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Taitement des dirichlet                                              |
  |======================================================================| */
void smdirs(struct Clim diric,
	    struct Maillage maillnodes,struct MaillageBord maillnodebord,
	    double *tmpsa,struct Cperio perio,struct Matrice matr,
	    struct Travail trav,
	    struct SDparall sdparall )
{
  int i,j,np;
  double vind1,vind2,vind3,vind4;
  double *trav1,*trav2,*dirval;
  double *xdms;
  
  trav1=trav.tab[0];
  trav2=trav.tab[1];
  dirval=trav.tab[2];
  xdms=matr.xdms;
  

  /* Rq : pour le parall, on passe toujours dans smdirs, meme s'il n'y pas de diric locaux */
  /* afin de faire ensuite l'assemblage complementaire */

  for (i=0;i<maillnodes.npoin;i++) 
    {*(trav1+i)=*(trav2+i)=*(dirval+i)=0.;}

  for (i=0;i<diric.nelem;i++) 
    for (j=0;j<diric.ndmat;j++) 
    {
      np=maillnodebord.node[j][diric.numf[i]];   
      *(dirval+np)=diric.val1[j][i];
    }


  /* modification du second membre pour noeud autre que dirichlet */
  if (diric.nbarete) omvdir(maillnodes,diric,trav1,dirval,matr);
/*   for (i=0;i<maillnodes.npoin;i++) printf(" omvdir 1 :nglob=%d trav1[%d]=%25.18e \n",sdparall.npglob[i]-1,i,trav1[i]); */

  if (sdparall.nparts>1) assemb_complt(trav1,maillnodes.npoin,trav2,sdparall);

  /* pas encore fait.... */
  /*  if (perio.nelem) contrib_perio(maillnodes,perio,trav1,trav2,matr);*/    /* ????? pas regarde */
/*   for (i=0;i<maillnodes.npoin;i++) printf(" omvdir 2 :nglob=%d trav1[%d]=%25.18e \n",sdparall.npglob[i]-1,i,trav1[i]); */

  for (i=0;i<maillnodes.npoin;i++) *(matr.b+i)-= *(trav1+i);

  /* fin du traitement pour parall, s'il n'y a pas de diric on ressort */  
  if (!diric.nbarete) return;


  /* for (i=0;i<maillnodes.npoin;i++) printf(">>> smdirs 1111111 i=%d matr.b=%f\n",i,matr.b[i]); */

  /* modification du second membre pour noeud dirichlet */
  for (i=0;i<diric.nelem;i++) 
    for (j=0;j<diric.ndmat;j++) 
    {
      np=maillnodebord.node[j][diric.numf[i]];   
      *(matr.b+np)=diric.val1[j][i];
    }

  /* Modif matrice */
  for (i=0;i<diric.npoin;i++) *(matr.dmat+diric.nump[i])=1.;
  for (i=0;i<diric.nbarete;i++) xdms[diric.arete[i]]=0.;

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Resolution par gradient conjugue                                     |
  |======================================================================| */

void grconj(struct Maillage maillnodes,
	    double *x,struct Contact rescon,struct Cperio perio,
	    struct Travail trav,struct Matrice matr,double epsgcs,int nitmx)
{
  int i,np,n=0;
  double x0,resnor,sl,rgrg,ro,prsca1,prsca2, alp,epsis;
  double *xx;
  double *res,*dd,*gd,*z,*travdiag,*travper;

  double test1,test2,test3;

  res=trav.tab[0];
  dd=trav.tab[1];
  gd=trav.tab[2];
  z=trav.tab[3];
  travper=trav.tab[4];
  travdiag=trav.tab[5];

  for (i=0;i<maillnodes.npoin;i++) travdiag[i]=1./matr.dmat[i]; 

  /* Essai chris - integration des resistances de contact dans la matrice */
/* ------------- isa decommente ici */
  if (rescon.nelem)
    for (i=0;i<rescon.npoin;i++) travdiag[rescon.nump[i]]=1./(matr.dmat[rescon.nump[i]]+rescon.gassc[i]);
  /* Fin essai chris - integration des resistances de contact dans la matrice */
  

  np=maillnodes.npoin;
/*   for (i=0;i<np;i++) printf(" grconj_seq : travdiag[%d]=%25.18e matr.b[%d]=%25.18e \n",i,travdiag[i],i,matr.b[i]); */
/*   for (i=0;i<maillnodes.nbarete;i++) printf("grconj_seq : xdms[%d]=%25.18e \n",i,matr.xdms[i]); */


  prsca1=syr_prosca(np,x,x);
  x0=sqrt(prsca1);
  if (x0<1.e-20) x0=1.e-4; epsis=1.e-4*x0;

  azero1D(np,res);
  if (rescon.nelem)
    {
      /* !!!! Essai chris - integration resistance de contact dans la matrice */
      /* En fait, la boucle  pourrait etre mis dans omv pour une certaine logique */
/* ------------- isa inversion ici */
             for (i=0;i<rescon.npoin;i++)
      	res[rescon.nump[i]]+=rescon.gnonassc[i]*(x[rescon.nump[i]]-x[rescon.numpc[i]]);
/*       for (i=0;i<rescon.npoin;i++) */
/* 	res[rescon.nump[i]]-=rescon.gnonassc[i]*x[rescon.numpc[i]]; */
      /* !!!! Fin Essai chris - integration resistance de contact dans la matrice */

/*       for (i=0;i<rescon.npoin;i++)  */
/* 	printf(" grconj_seq : res[%d]=%25.18e gnonassc[%d]=%25.18e x[%d]=%25.18e xc[%d]=%25.18e \n", */
/* 	       i,res[rescon.nump[i]],i,rescon.gnonassc[i],i,x[rescon.nump[i]],i,x[rescon.numpc[i]]); */

    }

  omv(maillnodes,res,x,matr,perio,travper);


/*   for (i=0;i<np;i++) printf(" grconj_seq : res[%d]=%25.18e matr.b[%d]=%25.18e \n",i,res[i],i,matr.b[i]); */


  syr_axpy(np,-1,matr.b,res);        /* res[i]-=matr.b[i]; */

/*   for (i=0;i<np;i++) printf(" grconj_seq : res[%d]=%25.18e \n",i,res[i]); */
  prsca1=syr_prosca(np,res,res);
  resnor=sqrt(prsca1);

/*   printf(" seq  resnor=%20.15e \n",resnor); */
  /* chris debug*/

  if (resnor<=epsis && resnor<=epsgcs*sqrt((double)(np)))
    {
      if (SYRTHES_LANG == FR)
	printf(" *** GRCONJ: ITERATION   PRECISION RELATIVE  PRECISION ABSOLUE\n");
      else if (SYRTHES_LANG == EN)
	printf(" *** GRCONJ: ITERATION   RELATIVE PRECISION  ABSOLUTE PRECISION\n");
      printf("                %4d         %12.5e                %12.5e\n",
	     n,resnor/x0,resnor/sqrt((double)(np)));
    }
  else
    {
      if (SYRTHES_LANG == FR)
	printf(" *** GRCONJ: ITERATION   PRECISION RELATIVE  PRECISION ABSOLUE\n");
      else if (SYRTHES_LANG == EN)
	printf(" *** GRCONJ: ITERATION   RELATIVE PRECISION  ABSOLUTE PRECISION\n");
      
      do
	{
	  n++;
 	  for (i=0;i<np;i++) gd[i]=res[i]*travdiag[i]; 
	  
	  sl=syr_prosca(np,res,gd); 

 	  if (n==1) syr_copy(np,gd,dd);   /* dd[i]=gd[i] */
 	  else {alp=sl/rgrg; for (i=0;i<np;i++) dd[i]=gd[i]+dd[i]*alp;}

	  rgrg=sl;
	  
	  for (i=0;i<np;i++) z[i]=0;
	  if (rescon.nelem)
	    {
	      /* !!!! Essai chris - integration resistance de contact dans la matrice */
	      /* En fait, la boucle  pourrait etre mis dans omv pour une certaine logique */
/* ------------- isa inversion ici */
	      for (i=0;i<rescon.npoin;i++)
		z[rescon.nump[i]]+=rescon.gnonassc[i]*(dd[rescon.nump[i]]-dd[rescon.numpc[i]]);
/* 	      for (i=0;i<rescon.npoin;i++) */
/* 		z[rescon.nump[i]]-=rescon.gnonassc[i]*dd[rescon.numpc[i]]; */
	      /* !!!! Fin Essai chris - integration resistance de contact dans la matrice */
	    }


	  omv(maillnodes,z,dd,matr,perio,travper);

	  prsca1=syr_prosca(np,res,dd);
	  prsca2=syr_prosca(np,dd,z);

	  ro=-prsca1/prsca2;

 	  syr_axpy(np,ro,dd,x);          /* x[i]+=ro*dd[i]; */
 	  syr_axpy(np,ro,z,res);         /* res[i]+=ro*z[i]; */ 

	  prsca1=syr_prosca(np,res,res);
	  resnor=sqrt(prsca1);

	  /* >>>> pour bien faire il faudrait creer un affich special */
/* 	  printf(" seq  n=%d resnor=%20.15e \n",n,resnor); */

	  if (n%25==0) 
	    printf("                %4d         %12.5e        %12.5e\n",
		   n,resnor/x0,resnor/sqrt((double)(np)));
	}
      while(!( (resnor<=epsis &&  resnor<=epsgcs*sqrt((double)(np))) || n>=nitmx ));
  
      if ((n%25)!=0)
	printf("                %4d         %12.5e        %12.5e\n",
	       n,resnor/x0,resnor/sqrt((double)(np)));
    }
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Prise en compte de la periodicite                                    |
  |======================================================================| */
void period(struct Maillage maillnodes,double *x,double *trav,
	    struct Cperio perio)
{
  int i;
  

  for (i=0;i<maillnodes.npoin;i++) *(trav+i)=0;

  for (i=0;i<perio.npoin;i++) trav[perio.nump[i]]+=x[perio.numpc[i]];

  for (i=0;i<maillnodes.npoin;i++) x[i] += trav[i];
/*   for (i=0;i<maillnodes.npoin;i++) {printf(">>> perio : trav[%d]=%f \n",i,trav[i]);} */

}


  
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Allocation des matrices pour la resolution                           |
  |======================================================================| */
void alloue_matrice(struct Maillage maillnodes, struct Prophy physol,
		    struct Matrice *matr)
{
  int i;

  matr->b=(double*)malloc(maillnodes.npoin*sizeof(double));  
  verif_alloue_double1d("alloue_matrice",matr->b);      

  matr->dmat=(double*)malloc(maillnodes.npoin*sizeof(double));    
  verif_alloue_double1d("alloue_matrice",matr->dmat); 

  matr->ddiff=(double*)malloc(maillnodes.npoin*sizeof(double));    
  verif_alloue_double1d("alloue_matrice",matr->ddiff); 
     
  perfo.mem_cond+=3*maillnodes.npoin*sizeof(double);  

  matr->xdms=(double*)malloc(maillnodes.nbarete*sizeof(double));
  verif_alloue_double1d("alloue_matrice",matr->xdms); 
  perfo.mem_cond+=maillnodes.nbarete*sizeof(double);

  if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        M-matricisation generalisee                                   |
  |        ATTENTION pas parallelisee rigoureusement pour l'instant      |
  |======================================================================| */
void M_mat(struct Maillage maillnodes,double *trav,double *xdms)
{
  int i,nb;
  int nbs,**nar;
  int *nar0,*nar1;
  double xseg;

  nbs=maillnodes.nbarete;
  nar=maillnodes.arete;
  nb=0;

  for (i=0,nar0=*(nar),nar1=*(nar+1);i<nbs;i++,nar0++,nar1++)
    {
      xseg=xdms[i];
      if (xseg>0.)
	{
	  trav[*nar0]+=xseg; trav[*nar1]+=xseg; xdms[i]=0.;
	  nb++;
	}
    }

  if (affich.cond_mat) 
    if (SYRTHES_LANG == FR)
      printf("M_mat : Nombre de termes affectes par une M-matricisation %d \n",nb);
    else if (SYRTHES_LANG == EN)
      printf("M_mat : Number of terms affected by M-matrix transformation %d \n",nb);
}



/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Prise en compte de la periodicite                                    |
  |======================================================================| */
void period_parall(struct Maillage maillnodes,double *x,double *trav,
		   struct Cperio perio,struct SDparall sdparall)
{
#ifdef _SYRTHES_MPI_
  int i;

  valeur_copain_parall(maillnodes.npoin,x,trav,
		       sdparall.nparts,sdparall.rang,
		       sdparall.nbcommunperio,sdparall.adrcommunperio,
		       sdparall.tcommunperio,
		       perio.trav,sdparall);
 
  for (i=0;i<maillnodes.npoin;i++) x[i] += trav[i];
/*   for (i=0;i<maillnodes.npoin;i++) {printf(">>> perio_par : rang=%d npglob=%d trav[%d]=%f \n", */
/* 					   sdparall.rang,sdparall.npglob[i],i,trav[i]);} */

#endif
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Recuperation des valeur des correspondants sur les autres domaines   |
  | pour le traitement de la periodicite et des Resist de contact        |
  | le vecteur x contient les grandeurs a echanger                       |
  | les valeurs de x sur les autres procs sont recuperees dans trav      |
  |                                                                      |
  | pour memoire :                                                       |
  |   nparts        = sdparall.nparts                                    |
  |   rang          = sdparall.rang                                      |
  |   nbcommunX     = sdparall.nbcommunrc   ou sdparall.nbcommunperio    |
  |   adrcommunX    = sdparall.adrcommunrc  ou sdparall.adrcommunperio   |
  |   tcommunX      = sdparall.tcommunrc    ou sdparall.tcommunperio     |
  |   loctrav       =  rescon.trav          ou perio.trav                |
  |                                                                      |
  |======================================================================| */ 
void valeur_copain_parall(int npoin,double *x,double *trav,
			  int nparts,int rang,
			  int *nbcommunX, int *adrcommunX, int *tcommunX,
			  double *loctrav,struct SDparall sdparall)
{
#ifdef _SYRTHES_MPI_
  int nn,i;


  azero1D(npoin,trav);

  for (nn=0;nn<nparts;nn++)
    {
      if (nn!=rang)
	{
	  for (i=0;i<nbcommunX[nn];i++) 
	    {
/* 	      printf("valcopain-- part nn=%d  i=%d tcommunX[adrcommunX[nn]+2*i]=%d\n",nn,i,tcommunX[adrcommunX[nn]+2*i]); */
	    loctrav[i]=x[tcommunX[adrcommunX[nn]+2*i]];
	    }
	  
	  MPI_Sendrecv_replace(loctrav,nbcommunX[nn],MPI_DOUBLE,nn,0,nn,0,
			       sdparall.syrthes_comm_world,&status);
	  
	  for (i=0;i<nbcommunX[nn];i++)
	    trav[tcommunX[adrcommunX[nn]+2*i]]=loctrav[i];
	}
      else /* RC au sein de la meme partition */
	{
	  for (i=0;i<nbcommunX[nn];i++)
	    trav[tcommunX[adrcommunX[nn]+2*i]]=x[tcommunX[adrcommunX[nn]+2*i+1]];
	}
    }
#endif
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |  Gradient conjugue en parallele                                      |
  |======================================================================| */

void grconj_parall(struct Maillage maillnodes,
		   double *x,struct Contact rescon,struct Cperio perio,
		   struct Travail trav,struct Matrice matr,double epsgcs,int nitmx,
		   struct SDparall sdparall )
{
#ifdef _SYRTHES_MPI_
  int i,j,np,n=0,rang,grstop,npg,nn,num;
  double x0,resnor,sl,rgrg,ro,prsca1,prsca2, alp,epsis,resuloc;
  double tabres[2];
  double *xx;
  double *res,*dd,*gd,*z,*trav0,*travdiag;

  res=trav.tab[0];
  dd=trav.tab[1];
  gd=trav.tab[2];
  z=trav.tab[3];
  trav0=trav.tab[4];
  travdiag=trav.tab[5];

  rang=sdparall.rang;
  np=maillnodes.npoin;
  npg=sdparall.nbno_global;

  
  syr_prosca_parall(&prsca1,x,x,sdparall);
  x0=sqrt(prsca1);
  if (x0<1.e-20) x0=1.e-4; epsis=1.e-4*x0;


  azero1D(np,res);


  if (rescon.npoin)
    {
      for (i=0;i<maillnodes.npoin;i++) travdiag[i]=1./matr.dmat[i];
      for (j=nn=0;nn<sdparall.nparts;nn++)
	for (i=0;i<sdparall.nbcommunrc[nn];i++) 
	  {
	    num=sdparall.tcommunrc[sdparall.adrcommunrc[nn]+2*i];
	    travdiag[num]=1./(matr.dmat[num]+rescon.gassc[j]);
	    j++;
	  }
    }
  else    
    for (i=0;i<maillnodes.npoin;i++) travdiag[i]=1./matr.dmat[i];

/*   for (i=0;i<np;i++) printf(" grconj : n=%d rang=%d nglob=%d travdiag[%d]=%25.18e matr.b[%d]=%25.18e  \n", */
/* 			    n,sdparall.rang,sdparall.npglob[i],i,travdiag[i],i,matr.b[i]); */
/*   for (i=0;i<maillnodes.nbarete;i++) printf("grconj : rang=%d xdms[%d]=%25.18e \n", */
/* 					    sdparall.rang,i,matr.xdms[i]); */

  /* s'il y a quelque part des RC */
  /* il faut recuperer les x des corresp sur les autres proc */
  if (rescon.existglob)
    {
      
      valeur_copain_parall(maillnodes.npoin,x,trav0,
			   sdparall.nparts,sdparall.rang,
			   sdparall.nbcommunrc,sdparall.adrcommunrc,sdparall.tcommunrc,
			   rescon.trav,sdparall);
      
      for (j=nn=0;nn<sdparall.nparts;nn++)
	for (i=0;i<sdparall.nbcommunrc[nn];i++) 
	  {
	    num =sdparall.tcommunrc[sdparall.adrcommunrc[nn]+2*i];
	    res[num] += rescon.gnonassc[j]*(x[num]-trav0[num])/sdparall.nfoisrc[sdparall.adrcommunrc[nn]/2+i];
	    j++;
	  }
    }


  omv_parall(maillnodes,res,x,matr,perio,trav0,sdparall);
/*   for (i=0;i<np;i++) printf(" grconj :  rang=%d nglob=%d res[%d]=%25.18e  matr.b[%d]=%25.18e\n", */
/* 			    sdparall.rang,sdparall.npglob[i],i,res[i],i,matr.b[i]); */

  for (i=0;i<np;i++) res[i]-=matr.b[i];

/*   for (i=0;i<np;i++) printf(" grconj :  rang=%d nglob=%d res[%d]=%25.18e  \n", */
/* 			    sdparall.rang,sdparall.npglob[i],i,res[i]); */

  syr_prosca_parall(&prsca1,res,res,sdparall);
  resnor=sqrt(prsca1);
/*   printf(" paral  resnor=%20.15e \n",resnor); */
  /* chris debug*/

  if (rang==0)
    {
      if (resnor<=epsis && resnor<=epsgcs*sqrt((double)(npg)))
	{
	  if (SYRTHES_LANG == FR)
	    printf(" *** GRCONJ: ITERATION   PRECISION RELATIVE  PRECISION ABSOLUE\n");
	  else if (SYRTHES_LANG == EN)
	    printf(" *** GRCONJ: ITERATION   RELATIVE PRECISION  ABSOLUTE PRECISION\n");
	  printf("                %4d         %12.5e                %12.5e\n",
		 n,resnor/x0,resnor/sqrt((double)(npg)));
	  grstop=1;
	  MPI_Bcast(&grstop,1,MPI_INT,0,sdparall.syrthes_comm_world);
	  return;
	}
      else
	{
	  grstop=0;
	  MPI_Bcast(&grstop,1,MPI_INT,0,sdparall.syrthes_comm_world);
	}
    }
  else
    {
      MPI_Bcast(&grstop,1,MPI_INT,0,sdparall.syrthes_comm_world);
      if (grstop==1) return;
    }


  /* PROCESSUS ITERATIF */
  /* ------------------ */
  
  if (rang==0) 
    if (SYRTHES_LANG == FR)
      printf(" *** GRCONJ: ITERATION   PRECISION RELATIVE  PRECISION ABSOLUE\n");
    else if (SYRTHES_LANG == EN)
      printf(" *** GRCONJ: ITERATION   RELATIVE PRECISION  ABSOLUTE PRECISION\n");
      
 boucle : n++;
  
  for (i=0;i<np;i++) gd[i]=res[i]*travdiag[i];
  
  syr_prosca_parall(&sl,res,gd,sdparall);
  
  if (n==1) for (i=0;i<np;i++) dd[i]=gd[i];
  else {alp=sl/rgrg; for (i=0;i<np;i++) dd[i]=gd[i]+dd[i]*alp;}

  rgrg=sl;
  

  for (i=0;i<np;i++) z[i]=0;

  if (rescon.existglob)
    {
      valeur_copain_parall(maillnodes.npoin,dd,trav0,
			   sdparall.nparts,sdparall.rang,
			   sdparall.nbcommunrc,sdparall.adrcommunrc,sdparall.tcommunrc,
			   rescon.trav,sdparall);

      for (j=nn=0;nn<sdparall.nparts;nn++)
	for (i=0;i<sdparall.nbcommunrc[nn];i++) 
	  {
	    num =sdparall.tcommunrc[sdparall.adrcommunrc[nn]+2*i];
	    z[num] += rescon.gnonassc[j]*(dd[num]-trav0[num])/sdparall.nfoisrc[sdparall.adrcommunrc[nn]/2+i];
	    j++;
	  }
    }
/*   for (i=0;i<np;i++) printf(" grconj : n=%d rang=%d nglob=%d z[%d]=%25.18e  \n", */
/* 			    n,sdparall.rang,sdparall.npglob[i],i,z[i]); */


  omv_parall(maillnodes,z,dd,matr,perio,trav0,sdparall);
    

/*   syr_prosca_parall(&prsca1,res,dd,sdparall); */
/*   syr_prosca_parall(&prsca2,z,dd,sdparall); */
  syr_2prosca_parall(tabres,res,dd, z,dd,sdparall);
  prsca1=tabres[0];
  prsca2=tabres[1];

  ro=-prsca1/prsca2;
  
  for (i=0;i<np;i++) x[i]+=ro*dd[i];
  for (i=0;i<np;i++) res[i]+=ro*z[i];
  syr_prosca_parall(&prsca1,res,res,sdparall);
  resnor=sqrt(prsca1);

  if (rang==0 && n%25==0) 
    printf("                 %4d         %12.5e        %12.5e\n",
	   n,resnor/x0,resnor/sqrt((double)(npg)));
  
  
  if (rang==0)  
    {
      if ( ! ( (resnor<epsis &&  resnor<epsgcs*sqrt((double)npg)) 
	       ||  n>=nitmx ) ) 
	{
    	  grstop=0;
	  MPI_Bcast(&grstop,1,MPI_INT,0,sdparall.syrthes_comm_world);
	  goto boucle;
	}
      else
	{
    	  grstop=1;
	  MPI_Bcast(&grstop,1,MPI_INT,0,sdparall.syrthes_comm_world);
	  if ((n%25) != 0) 
	    printf("                 %4d         %12.5e        %12.5e\n",
		   n,resnor/x0,resnor/sqrt((double)npg));
  	}
    }
  else
    {
      MPI_Bcast(&grstop,1,MPI_INT,0,sdparall.syrthes_comm_world);
      if (grstop==0) goto boucle;
    }
#endif
}

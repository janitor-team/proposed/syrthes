/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "syr_usertype.h"
#include "syr_tree.h"
#include "syr_bd.h"
#include "syr_option.h"
#include "syr_proto.h"
#include "syr_const.h"

#ifdef _SYRTHES_MPI_
#include "mpi.h"
#endif

extern struct Performances perfo;
extern struct Affichages affich;
extern char nomdata[CHLONG];
extern FILE *fdata;

extern int nbVar;

static char ch[CHLONG],motcle[CHLONG],repc[CHLONG],formule[CHLONG];
static double dlist[100];
static int ilist[MAX_REF];

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | decryptage des flux volumiques                                       |
  |======================================================================| */
void decode_cvol(struct Maillage maillnodes,struct Cvol *fluxvol,char **nomvar)
{
  int n,i,j,nr,nb,netot;
  int i1,i2,id,ii,ityp;
  int **ref;  /* [numvar][max_ref] */


  /* Allocations */
  /* ----------- */

  ref=(int**)malloc(nbVar*sizeof(int*));
  for (n=0;n<nbVar;n++) 
    {
      ref[n]=(int*)malloc(MAX_REF*sizeof(int));
      for (j=0;j<MAX_REF;j++) ref[n][j]=0;
    }

  /* 1ere passe : decodage des references pour les cond vol */
  /* ====================================================== */
    fseek(fdata,0,SEEK_SET);

  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);
	  id=i2+1;

	  ityp=-1;
	  if      (!strcmp(motcle,"CVOL_T")      || !strcmp(motcle,"CVOL_PV")      || !strcmp(motcle,"CVOL_PT"))      ityp=0;
	  else if (!strcmp(motcle,"CVOL_T_FCT")  || !strcmp(motcle,"CVOL_PV_FCT")  || !strcmp(motcle,"CVOL_PT_FCT"))  ityp=1;
	  else if (!strcmp(motcle,"CVOL_T_PROG") || !strcmp(motcle,"CVOL_PV_PROG") || !strcmp(motcle,"CVOL_PT_PROG")) ityp=2;


	  if (ityp>=0)
	    {		  
	      switch(ityp){
	      case 0: rep_ndbl(2,dlist,&ii,ch+id); id+=ii; break;
	      case 1: ii=rep_ch(formule,ch+id); id+=ii;
  	              ii=rep_ch(formule,ch+id); id+=ii;
		      break;
	      }
	      rep_listint(ilist,&nb,ch+id);

	      if (strstr(motcle,"_T")) /* flux volumique sur T */ 
		{
		  if (ilist[0]==-1)
		    for (j=0;j<MAX_REF;j++) ref[0][j]=1;
		  else
		    for (n=0;n<nb;n++) ref[0][ilist[n]]=1;
		}
	      else if (strstr(motcle,"_PV")) 
		{
		  if (ilist[0]==-1)
		    for (j=0;j<MAX_REF;j++) ref[1][j]=1;
		  else
		    for (n=0;n<nb;n++) ref[1][ilist[n]]=1;
		}
	      else if (strstr(motcle,"_PT")) 
		{
		  if (ilist[0]==-1)
		    for (j=0;j<MAX_REF;j++) ref[2][j]=1;
		  else
		    for (n=0;n<nb;n++) ref[2][ilist[n]]=1;
		}
	    }
	}
    }


  /* compte du nombre de flux vol pour chaque variable */
  /* ------------------------------------------------- */
  for (n=0;n<nbVar;n++) fluxvol[n].nelem=0;

  for (i=0;i<maillnodes.nelem;i++)
    {
      nr=maillnodes.nrefe[i];
      for (n=0;n<nbVar;n++) if (ref[n][nr]) fluxvol[n].nelem++;
    }

  /* indicateur global de presence de flux volumiques */


  /* allocations et initialisations des elt avec flux vol */
  /* ---------------------------------------------------- */
  for (n=0;n<nbVar;n++) 
    {
      fluxvol[n].existglob=0;

      if(fluxvol[n].nelem)
	{
	  /* indicateur global de presence de flux volumiques */
	  fluxvol[n].existglob=1;

	  fluxvol[n].nume=(int*)malloc(fluxvol[n].nelem*sizeof(int));
	  verif_alloue_int1d("decode_cvol",fluxvol[n].nume);
	  perfo.mem_cond+=fluxvol[n].nelem*sizeof(int);
	  
	  for (i=nb=0;i<maillnodes.nelem;i++)
	    {
	      nr=maillnodes.nrefe[i];
	      if (ref[n][nr]) {fluxvol[n].nume[nb]=i;nb++;}
	    }
	}

      netot=fluxvol[n].nelem;

#ifdef _SYRTHES_MPI_
      if (syrglob_nparts>1) netot=somme_int_parall(fluxvol[n].nelem);
#endif


      if (netot &&(syrglob_nparts==1 || syrglob_rang==0))
	if (SYRTHES_LANG == FR)
	  printf("\n *** FLUX VOLUMIQUES pour la variable %s : nombre d'elements concernes : %d\n",nomvar[n],netot);
	else if (SYRTHES_LANG == EN)
	  printf("\n *** Volumic Flux for variable %s : Number of elements : %d\n",nomvar[n],netot);
    }

  if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;

  /* liberation memoire */
  for (n=0;n<nbVar;n++) free(ref[n]);
  free(ref);

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |        Lecture                                                       |
  |======================================================================| */
void lire_condvol(struct Cvol *fluxvol,int *nrefe)
{
  int i,j,k,indic=0;
  int i1,i2,id,ii,nb,nr,n;


  if (syrglob_nparts==1 ||syrglob_rang==0){
    if (SYRTHES_LANG == FR)
      printf("\n *** LECTURE DES CONDITIONS VOLUMIQUES DANS LE FICHIER DE DONNEES\n");
    else if (SYRTHES_LANG == EN)
      printf("\n *** READING THE VOLUMIC CONDITIONS IN THE DATA FILE\n");
  }

  fseek(fdata,0,SEEK_SET);


  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);
	  id=i2+1;

	  if (!strcmp(motcle,"CVOL_T")) 
	    {lire_fluxvol(&(fluxvol[ADR_T]),nrefe,ch,id," T"); indic=1;}
	  else if (!strcmp(motcle,"CVOL_PV")) 
	    {lire_fluxvol(&(fluxvol[ADR_PV]),nrefe,ch,id,"PV");indic=1;}
	    else if (!strcmp(motcle,"CVOL_PT")) 
	      {lire_fluxvol(&(fluxvol[ADR_PT]),nrefe,ch,id,"PT");indic=1;}
	}
    }
  
  if (!indic && syrglob_nparts==1) 
    if (SYRTHES_LANG == FR)
      printf("        --> Aucune condition volumique constante n'est presente dans le fichier de donnees\n");
    else if (SYRTHES_LANG == EN)
      printf("        --> No constant volumic source term is present in the data file\n");

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |        Lecture                                                       |
  |======================================================================| */
void lire_fluxvol(struct Cvol *fluxvol,int *nrefe,char *ch,int id,char *nomvar)
{
  int i,j,indic=0;
  int ii,nb,nr,n;
  int *tab;
  double val;

  indic=1;
  rep_ndbl(2,dlist,&ii,ch+id);
  rep_listint(ilist,&nb,ch+id+ii);
  
  if (fluxvol->nelem)
    {
      if (ilist[0]==-1)
	{
	  for (i=0;i<fluxvol->nelem;i++) 
	    {fluxvol->val1[i]=dlist[0]; fluxvol->val2[i]=dlist[1];}
	}
      else
	for (n=0;n<nb;n++)
	  {
	    nr=ilist[n];
	    for (i=0;i<fluxvol->nelem;i++) 
	      if (nrefe[fluxvol->nume[i]]==nr) 
		{fluxvol->val1[i]=dlist[0]; fluxvol->val2[i]=dlist[1];}
	  }
    }
  
  if (syrglob_nparts==1 ||syrglob_rang==0)
    {
      if (ilist[0]==-1)
	{
	  if (SYRTHES_LANG == FR)
	    printf("        --> %s - FLUX VOLUMIQUE de (%f * T + %f) impose sur tout le domaine\n",nomvar,dlist[0],dlist[1]);
	  else if (SYRTHES_LANG == EN)
	    printf("        --> %s - VOLUMIC SOURCE of (%f * T + %f) set on the whole domaine\n",nomvar,dlist[0],dlist[1]);
	}
      else
	{
	  if (SYRTHES_LANG == FR)
	    printf("        --> %s - FLUX VOLUMIQUE de (%f * T + %f) impose sur les elements de reference",nomvar,dlist[0],dlist[1]);
	  else if (SYRTHES_LANG == EN)
	    printf("        --> %s - VOLUMIC SOURCE of (%f * T + %f) set on the elements with reference",nomvar,dlist[0],dlist[1]);
	  for (n=0;n<nb;n++) printf(" %d",ilist[n]);
	  printf("\n");
	}
    }


}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |   Verification des proprietes physiques                              |
  |======================================================================| */
void verify_initial_cvol(struct Maillage maillnodes,struct Cvol *fluxvol)
{
  int i,n,ok=1,nbtrop;
  int *ele;
  char *name[3]={"T","PV","PT"};

  /* controle des flux volumiques */
  /* ---------------------------- */

  ele=(int*)malloc(maillnodes.nelem*sizeof(int));
  verif_alloue_int1d("verify_initial_cvol",ele);


  for (n=0;n<nbVar;n++)
    {
      for (i=0;i<maillnodes.nelem;i++) ele[i]=0; /* nombre de fois qu'un fluxvol est defini */ 
      
      for (i=0;i<fluxvol[n].nelem;i++) ele[fluxvol[n].nume[i]]++;
      
      /* y'a plus qu'a voir si tout est OK ! */
      for (nbtrop=i=0;i<maillnodes.nelem;i++) 
	if (ele[i]>1) nbtrop++;
      
      if (nbtrop){
	if (SYRTHES_LANG == FR) 
	  printf(" ERREUR : variable %s -> multiple definition de flux volumiques sur le meme element. Nombre d'elements : %d\n",
		 name[n],nbtrop);
	else if (SYRTHES_LANG == EN) 
	  printf(" ERROR : variable %s -> multiply definition of volumic source on the same element. Number of elements : %d\n",
		 name[n],nbtrop);
      }
    }

  free(ele);

  if (nbtrop) syrthes_exit(1);

}

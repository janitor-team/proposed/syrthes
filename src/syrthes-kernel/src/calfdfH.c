/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

#include "syr_usertype.h"
# include "syr_tree.h"
# include "syr_abs.h"
# include "syr_const.h"
# include "syr_option.h"
# include "syr_bd.h"
# include "syr_proto.h"

#ifdef _SYRTHES_MPI_
# include "mpi.h"
#endif

extern struct Performances perfo;
extern struct Affichages affich;
                     
extern struct Affichages affich;


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | cfdf2d                                                               |
  |         Gestion du calcul des facteurs de forme en dimension 2       |
  |======================================================================| */
void calfdfH(struct Maillage maillnodray,
	     struct FacForme ***listefdf,double **diagfdf,
	     struct Compconnexe volconnexe,
	     struct Mask mask,
	     struct Horizon *horiz,
	     struct SDparall_ray sdparall_ray)
{
      
  int n,nn,i,j,k,nelem,npoinr,nbelemax;
  int ligne_deb,ligne_fin,nelemloc;
  int faces_cachees,imult,nv;
  double xn1,yn1,zn1,x,y,z;
  int *grconv;
  double **coo2,**xnf2,xmult,**xnfH,**xn,*px,*py,*pz,*pland;
  struct Maillage maillnod2,maillnodH;
  double t1,t2;
  struct FacForme *pff; 
  double taille_boite,taille_seg;


  /* nombre de facteurs de forme a calculer */
  /* -------------------------------------- */
  if (sdparall_ray.npartsray>1)    {
    ligne_deb=sdparall_ray.ieledeb[sdparall_ray.rang];
    ligne_fin=sdparall_ray.ieledeb[sdparall_ray.rang+1];
    nelemloc=sdparall_ray.nelrayloc[sdparall_ray.rang];
  }
  else {
    ligne_deb=0;
    ligne_fin=maillnodray.nelem;
    nelemloc=maillnodray.nelem;
  }

  maillnodH=maillnodray;
  maillnodH.ndim=maillnodray.ndim;
  maillnodH.npoin=maillnodray.npoin;
  maillnodH.nelem=maillnodray.nelem+horiz->nelem;

  maillnodH.type=(int*)malloc(maillnodH.nelem*sizeof(int));
  verif_alloue_int1d("calfdfH",maillnodH.type);
  for (i=0;i<maillnodray.nelem;i++) {maillnodH.type[i]=maillnodray.type[i];}
  for (i=maillnodray.nelem;i<maillnodH.nelem;i++) {maillnodH.type[i]=20;}

  maillnodH.node=(int**)malloc(maillnodH.ndim*sizeof(int*));
  for (i=0;i<maillnodH.ndim;i++) maillnodH.node[i]=(int*)malloc(maillnodH.nelem*sizeof(int));
  verif_alloue_int2d(maillnodH.ndim,"calfdfH",maillnodH.node);
  for (i=0;i<maillnodray.nelem;i++) 
    {maillnodH.node[0][i]=maillnodray.node[0][i]; maillnodH.node[1][i]=maillnodray.node[1][i];}
  if (maillnodH.ndim==3) for (i=0;i<maillnodray.nelem;i++) maillnodH.node[2][i]=maillnodray.node[2][i];
  for (i=0;i<horiz->nelem;i++) 
    {maillnodH.node[0][maillnodray.nelem+i]=horiz->node[0][i]; 
     maillnodH.node[1][maillnodray.nelem+i]=horiz->node[1][i];}
  if (maillnodH.ndim==3) for (i=0;i<horiz->nelem;i++) maillnodH.node[2][maillnodray.nelem+i]=horiz->node[2][i];


  maillnodH.coord=(double**)malloc(maillnodH.ndim*sizeof(double*));
  for (i=0;i<maillnodray.ndim;i++) maillnodH.coord[i]=(double*)malloc(maillnodH.npoin*sizeof(double));
  verif_alloue_double2d(maillnodH.ndim,"calfdfH",maillnodH.coord);
  for (i=0;i<maillnodray.npoin;i++) {
    maillnodH.coord[0][i]=maillnodray.coord[0][i]; 
    maillnodH.coord[1][i]=maillnodray.coord[1][i];
    if (maillnodH.ndim==3 )maillnodH.coord[2][i]=maillnodray.coord[2][i];}




  if (maillnodray.ndim==2)
    dimension_2d(maillnodH,&taille_boite,&taille_seg);
  else
    dimension_3d(maillnodH,&taille_boite,&taille_seg);

  
  imult = 0;  xmult = 1.;
  
  if(taille_seg < 0.01 || taille_seg > 10)
    {
      imult = 1;
      if (taille_seg < 0.01) xmult = 0.1/taille_seg;
      if (taille_seg > 10  ) xmult =   1/taille_seg;
      if(affich.ray_fdf) 
	if (SYRTHES_LANG == FR) printf("Facteur multiplicatif interne xmult= %f \n",xmult);
	else if (SYRTHES_LANG == EN) printf("Internal multiplying factor xmult= %f \n",xmult);

      if (maillnodray.ndim==2)
	  for (i=0,px=maillnodH.coord[0],py=maillnodH.coord[1];i<maillnodH.npoin;i++,px++,py++)
	    {*px *= xmult; *py *= xmult;}
      else
	  for (i=0,px=*(maillnodH.coord),py=*(maillnodH.coord+1),pz=*(maillnodH.coord+2);
	       i<maillnodH.npoin;i++,px++,py++,pz++)
	    {*px *= xmult; *py *= xmult; *pz *= xmult;}
    }

  taille_boite *= xmult;
  taille_seg   *= xmult;
  
  for(i=0;i<volconnexe.nb;i++) {
    volconnexe.x[i]*=xmult; 
    volconnexe.y[i]*=xmult; 
    if (maillnodray.ndim==3) volconnexe.z[i]*=xmult;} 


  /* orientation des facettes - composantes connexes */
  /* ----------------------------------------------- */

  grconv = (int*)malloc(maillnodH.nelem*sizeof(int));
  verif_alloue_int1d("calfdfH",grconv);
  perfo.mem_ray+=maillnodray.nelem * sizeof(int);


  if (maillnodray.ndim==2)
    {
      orient2d(maillnodH,volconnexe,grconv);
      for (i=0;i<maillnodray.nelem;i++) 
	{maillnodray.node[0][i]=maillnodH.node[0][i];maillnodray.node[1][i]=maillnodH.node[1][i];}
    }
  else
    {
      orient3d(maillnodH,volconnexe,grconv);
      for (i=0;i<maillnodray.nelem;i++) 
	{maillnodray.node[0][i]=maillnodH.node[0][i];
	maillnodray.node[1][i]=maillnodH.node[1][i];
	maillnodray.node[2][i]=maillnodH.node[2][i];}
    }


  horiz->fdf=(double*)malloc(maillnodray.nelem*sizeof(double));
  verif_alloue_double1d("calfdfH",horiz->fdf);
  for (i=0;i<maillnodH.nelem;i++)horiz->fdf[i]=0;

  /* --------------------------------------------------------*/
  /*          calcul des fdf                                 */
  /* --------------------------------------------------------*/

	  
  /* allocation pour le stockage des fdf */
  for (n=0;n<sdparall_ray.npartsray;n++)
    listefdf[n]=(struct FacForme**)malloc(nelemloc*sizeof(struct FacForme*));
  *diagfdf=(double*)malloc(nelemloc*sizeof(double));

  for (n=0;n<sdparall_ray.npartsray;n++)
    for (i=0;i<nelemloc;i++)
      listefdf[n][i]=NULL;

  for (i=0;i<nelemloc;i++)
    (*diagfdf)[i]=0.;


  maillnodH.xnf=(double**)malloc(maillnodH.ndim*sizeof(double*));
  for (i=0;i<maillnodray.ndim;i++) maillnodH.xnf[i]=(double*)malloc(maillnodH.nelem*sizeof(double));
  verif_alloue_double2d(maillnodH.ndim,"calfdfH",maillnodH.xnf);
  
  if (maillnodray.ndim==2)
    {
      box_2d(maillnodH.npoin,maillnodH.coord); 
      cnor_2d(maillnodH);
    }
  else
    {
      box_3d(maillnodH.npoin,maillnodH.coord); 
      cnor_3d(maillnodH);
    }
  
  /* nombre max d'elements par composante connexe */
  pland=(double*)malloc(maillnodH.nelem*sizeof(double));
  verif_alloue_double1d("calfdfH",pland);
  perfo.mem_ray+=maillnodH.nelem*sizeof(double);



  /* --------------------------------------------------------*/
  /*          calcul des fdf                                 */
  /* --------------------------------------------------------*/

  if (maillnodH.ndim==2)
    {
      for (i=0;i<maillnodH.nelem;i++)
	{
	  xn1=maillnodH.xnf[0][i]; 
	  yn1=maillnodH.xnf[1][i]; 
	  x=maillnodH.coord[0][maillnodH.node[0][i]];  y=maillnodH.coord[1][maillnodH.node[0][i]];
	  pland[i]=-x*xn1-y*yn1;
	  
	}
      facecache_2d(maillnodH.nelem,maillnodH.node,maillnodH.coord,maillnodH.xnf,pland,grconv,&faces_cachees,
		   ligne_deb,ligne_fin);
      
      t1=cpusyrt();
  
      facforme_2d(maillnodray.nelem,maillnodray.npoin,maillnodH.nelem,
		  maillnodH.node,maillnodH.coord,pland,maillnodH.xnf,
		  listefdf,*diagfdf,grconv,faces_cachees,
		  mask,horiz,maillnodH.type,sdparall_ray);
    }
  else
    {
      for (i=0;i<maillnodH.nelem;i++)
	{
	  xn1=maillnodH.xnf[0][i]; 
	  yn1=maillnodH.xnf[1][i]; 
	  zn1=maillnodH.xnf[2][i]; 
	  x=maillnodH.coord[0][maillnodH.node[0][i]];  
	  y=maillnodH.coord[1][maillnodH.node[0][i]];
	  z=maillnodH.coord[2][maillnodH.node[0][i]];
	  pland[i]=-x*xn1-y*yn1-z*zn1;
	  
	}
      facecache_3d(maillnodH.nelem,maillnodH.node,maillnodH.coord,maillnodH.xnf,pland,grconv,&faces_cachees,
		   ligne_deb,ligne_fin);

      t1=cpusyrt();
      
      facforme_3d(maillnodray.nelem,maillnodray.npoin,maillnodH.nelem,
		  maillnodH.node,maillnodH.coord,pland,maillnodH.xnf,
		  listefdf,*diagfdf,grconv,faces_cachees,
		  mask,horiz,maillnodH.type,sdparall_ray);
    }

  t2=cpusyrt();

  if (SYRTHES_LANG == FR)
    printf("\n       >>>> calfdfH : temps de calcul des facteurs de forme = %e\n",t2-t1);
  else if (SYRTHES_LANG == EN)
    printf("\n       >>>> calfdfH : CPU time to calculate view factor = %e\n",t2-t1);
  

/* --------------------------------------------------------*/

  /* liberation des tableaux locaux */
  free(pland);
  free(grconv);

  free(maillnodH.type);

  for (i=0;i<maillnodH.ndim;i++) 
    {free(maillnodH.node[i]); 
      free(maillnodH.coord[i]); 
      free(maillnodH.xnf[i]);}

  free(maillnodH.node); free(maillnodH.xnf);  free(maillnodH.coord); 

  if(imult)
    {
      for(i=0;i<volconnexe.nb;i++) 
	{volconnexe.x[i]/=xmult; volconnexe.y[i]/=xmult; if (maillnodray.ndim==3) volconnexe.z[i]*=xmult;}
      for (i=0,px=maillnodray.coord[0],py=maillnodray.coord[1];i<maillnodray.npoin;i++,px++,py++)
	{*px /= xmult; *py /= xmult;}
      xmult *= xmult ;
      

      for (i=0;i< maillnodray.nelem ;i++) 
	{
	  maillnodray.volume[i] /=  xmult;
	  for (j=0;j<sdparall_ray.npartsray;j++)
	    {
	      pff=listefdf[j][i]; 
	      while (pff){pff->fdf/=xmult; pff=pff->suivant;}
	    }
	}
    }

}


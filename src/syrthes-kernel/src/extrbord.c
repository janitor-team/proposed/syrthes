/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>

#include "syr_usertype.h"
#include "syr_abs.h"
#include "syr_bd.h"
#include "syr_option.h"
#include "syr_proto.h"

extern struct Performances perfo;
extern struct Affichages affich;

int somfac[4][3];

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | Extraction du maillage de bord                                       |
  |======================================================================| */

void extrbord2(struct Maillage maillnodes,struct MaillageBord *maillnodebord,
	       int **nrefac)
{
  int i,j;
  int is1,is2,is3,isad,js1,js2,iso1,iso2,jso1,jso2;
  int nmemax,nelep1,ip,ip1,ipmax,ipmin;
  int nel,neli,nelj,ifac,ifaci,ifacj,ifauxi,ifauxj;
  int nfacbo,nr;
  int *iadr,*itrav;
  int nbface=3,**nvoisin;

  nvoisin=(int**)malloc(nbface*sizeof(int*));
  for (i=0; i<nbface; i++) nvoisin[i]=(int*)malloc(maillnodes.nelem*sizeof(int));
  verif_alloue_int2d(nbface,"extrbord2",nvoisin);

  nmemax =  maillnodes.nelem * 3 + 2 * maillnodes.npoin;
  nelep1 =  maillnodes.nelem + 1;
  ipmin  =  2 * maillnodes.npoin+1;
  ip     =  2 * maillnodes.npoin;
  

  somfac[0][0] = 0; somfac[0][1] = 1;
  somfac[1][0] = 1; somfac[1][1] = 2;
  somfac[2][0] = 2; somfac[2][1] = 0;


  iadr  = (int *)malloc(nmemax * sizeof(int));
  itrav = (int *)malloc(nmemax * sizeof(int));
  verif_alloue_int1d("extrbord2",iadr);
  verif_alloue_int1d("extrbord2",itrav);

  perfo.mem_cond+=(2*nmemax+maillnodes.nelem*nbface) * sizeof(int);
  if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;


  for (i=0; i < nmemax ; i++) *(iadr+i) = -1 ;
  for (i=0; i < nmemax ; i++) *(itrav+i) = 0 ;
  for (j=0; j < nbface; j++)
    for (i=0; i < maillnodes.nelem ; i++) nvoisin[j][i]=-1;


  for ( ifac=0;ifac<3;ifac++)
    for (i=0;i<maillnodes.nelem;i++)
      {
	is1 = maillnodes.node[somfac[ifac][0]][i];
	is2 = maillnodes.node[somfac[ifac][1]][i];
	
	isad = is1+is2;
	
	if(iadr[isad] == -1)
	  {
	    iadr[isad] =i+ifac*nelep1 ;
	    itrav[isad]=isad;
	  }
	else 
	  {
	    ip++; iadr[ip]=i+ifac*nelep1; itrav[ip]=itrav[isad];
	    itrav[isad] = ip;
	  }
      }

  ipmax = ip ;

  for ( i=ipmax;i>=ipmin;i--)
    {
      ifauxi  = iadr[i];
      ifaci   = ifauxi/nelep1;
      neli    = ifauxi-ifaci*nelep1;

      if (nvoisin[ifaci][neli] != -1) continue;

      is1 = maillnodes.node[somfac[ifaci][0]][neli];
      is2 = maillnodes.node[somfac[ifaci][1]][neli];

      if (is1<is2){iso1=is1; iso2=is2;}
      else {iso1=is2; iso2=is1;}

      ip1 = i;
      while (ip1 >= ipmin)
	{
	  ip1     = itrav[ip1];
	  ifauxj  = iadr[ip1];
	  ifacj   = ifauxj/nelep1;
	  nelj    = ifauxj-ifacj*nelep1;

	  js1 = maillnodes.node[somfac[ifacj][0]][nelj];
	  js2 = maillnodes.node[somfac[ifacj][1]][nelj];

	  if (js1<js2){jso1=js1; jso2=js2;}
	  else {jso1=js2; jso2=js1;}

	  if (iso1==jso1 && iso2==jso2)
	    {
	      nvoisin[ifaci][neli] = nelj;
	      nvoisin[ifacj][nelj] = neli;
	      continue ;
	    }
	}

    }

  free(iadr);
  free(itrav);
  perfo.mem_cond-=(2*nmemax) * sizeof(int);



  maillnodebord->nelem=0;
  nfacbo=0;

  for (ifac=0;ifac<nbface;ifac++)
    for (nel=0;nel<maillnodes.nelem;nel++)
      if ( nvoisin[ifac][nel] == -1 ) nfacbo++;
  
  maillnodebord->nelem=nfacbo;
  maillnodebord->ndim=2;
  maillnodebord->ndmat=2;
  maillnodebord->ndiele=1;
  maillnodebord->nrefe=(int*)malloc(maillnodebord->nelem*sizeof(int));
  maillnodebord->node=(int**)malloc((maillnodebord->ndmat+1)*sizeof(int*));
  for (i=0;i<maillnodebord->ndmat+1;i++) 
    maillnodebord->node[i]=(int*)malloc(maillnodebord->nelem*sizeof(int));
  verif_alloue_int1d("extrbord2",maillnodebord->nrefe);
  verif_alloue_int2d(maillnodebord->ndmat+1,"extrbord2",maillnodebord->node);

  perfo.mem_cond+=(maillnodebord->nelem*(maillnodebord->ndmat+3) * sizeof(int));
  if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;


  nfacbo=0;
  for (ifac=0;ifac<nbface;ifac++)
    for (i=0;i<maillnodes.nelem;i++)
      if ( nvoisin[ifac][i] == -1 )
	{
	  is1 = maillnodes.node[somfac[ifac][0]][i]; 
	  is2 = maillnodes.node[somfac[ifac][1]][i]; 
	  maillnodebord->node[0][nfacbo]=is1;
	  maillnodebord->node[1][nfacbo]=is2;
	  maillnodebord->node[2][nfacbo]=i;
	  nr=nrefac[ifac][i]; 
	  maillnodebord->nrefe[nfacbo]=nr;
	  nfacbo++;
	}

  if (SYRTHES_LANG == FR)
    printf("\n *** EXTRBORD2 : nombre d'elements du maillage de bord : %d",maillnodebord->nelem);
  else if (SYRTHES_LANG == EN)
    printf("\n *** EXTRBORD2 : element number of the boundary mesh : %d",maillnodebord->nelem);

/*   if (affich.cond_creemaill) */
/*     { */
/*       if (SYRTHES_LANG == FR) */
/* 	printf("\n *** EXTRBORD2 : impression de nodebord\n"); */
/*       else if (SYRTHES_LANG == EN) */
/* 	printf("\n *** EXTRBORD2 : printing of nodebord\n"); */
/*       imprime_connectivite(*maillnodebord);  */
/*     } */

  /*  if (affich.cond_creemaill)
      {
      printf("\n >>> extrbord2 : impression de nvoisin (ex- nfabor)\n");
      for (i=0;i<maillnodes.nelem;i++) 
	{
	printf("\n element %d voisin ",i);
	for (j=0;j<nbface;j++) printf(" %d ",nvoisin[j][i]);
	}
	} 
  */

  for (i=0; i<nbface; i++) free(nvoisin[i]);
  free(nvoisin);
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | Extraction du maillage de bord                                       |
  |======================================================================| */

void extrbord3(struct Maillage maillnodes,struct MaillageBord *maillnodebord,
	       int **nrefac)
{
  int i,j;
  int is1,is2,is3,is4,is5,is6,isad,js1,js2,js3;
  int iso1,iso2,iso3,jso1,jso2,jso3;
  int nmemax,nelep1,ip,ip1,nr;
  int ipmax,ipmin;
  int nel,neli,nelj;
  int ifac,ifaci,ifacj;
  int ifauxi,ifauxj;
  int nfacbo;
  int *iadr,*itrav;
  int nbface=4,**nvoisin;

  nvoisin=(int**)malloc(nbface*sizeof(int*));
  for (i=0; i<nbface; i++) nvoisin[i]=(int*)malloc(maillnodes.nelem*sizeof(int));
  verif_alloue_int2d(nbface,"extrbord3",nvoisin); 

  nmemax = maillnodes.nelem * 4 + 3 *maillnodes.npoin;
  nelep1 = maillnodes.nelem + 1;
  ipmin  = 3*maillnodes.npoin+1;
  ip     = 3*maillnodes.npoin;
  

  somfac[0][0]=0; somfac[0][1]=1; somfac[0][2]=2;
  somfac[1][0]=0; somfac[1][1]=3; somfac[1][2]=1;
  somfac[2][0]=0; somfac[2][1]=2; somfac[2][2]=3;
  somfac[3][0]=1; somfac[3][1]=3; somfac[3][2]=2;


  iadr  = (int *)malloc(nmemax * sizeof(int));
  itrav = (int *)malloc(nmemax * sizeof(int));
  verif_alloue_int1d("extrbord3",iadr);
  verif_alloue_int1d("extrbord3",itrav);

  perfo.mem_cond+=(2*nmemax+maillnodes.nelem*nbface) * sizeof(int);
  if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;

  for (i=0; i < nmemax ; i++) *(iadr+i) = -1 ;
  for (i=0; i < nmemax ; i++) *(itrav+i) = 0 ;
  for (j=0; j < nbface; j++)
    for (i=0; i < maillnodes.nelem ; i++) nvoisin[j][i]=-1;


  for ( ifac=0;ifac<4;ifac++)
    {
    
      for ( i=0;i<maillnodes.nelem;i++)
	{
	  is1 = maillnodes.node[somfac[ifac][0]][i]; 
	  is2 = maillnodes.node[somfac[ifac][1]][i]; 
	  is3 = maillnodes.node[somfac[ifac][2]][i]; 

	  isad = is1+is2+is3;

	  if(iadr[isad] == -1)
	    {
	      iadr[isad] = i + ifac*nelep1;
	      itrav[isad]= isad;
	    }
	  else 
	    {
	      ip++;  iadr[ip]=i+ifac*nelep1; itrav[ip]=itrav[isad];
	      itrav[isad] = ip;
	    }
	}
    }

  ipmax = ip ;

  for ( i=ipmax;i>=ipmin;i--)
    {
      ifauxi  = iadr[i];
      ifaci   = ifauxi/nelep1;
      neli    = ifauxi-ifaci*nelep1;


      if (nvoisin[ifaci][neli] != -1) continue;

      is1 = maillnodes.node[somfac[ifaci][0]][neli];
      is2 = maillnodes.node[somfac[ifaci][1]][neli];
      is3 = maillnodes.node[somfac[ifaci][2]][neli];

      if ( is1 <= is2 && is1 <= is3) 
	{
	  if (is2 <= is3) { iso1=is1; iso2=is2; iso3=is3;}
	  else            { iso1=is1; iso2=is3; iso3=is2;}
	}
      else if ( is2 <= is1 && is2 <= is3) 
	{
	  if (is1 <= is3) { iso1=is2; iso2=is1; iso3=is3;}
	  else            { iso1=is2; iso2=is3; iso3=is1;}
	}
      else
	{
	  if (is1 <= is2) { iso1=is3; iso2=is1; iso3=is2;}
	  else            { iso1=is3; iso2=is2; iso3=is1;}
	}

      ip1 = i;
      while ( ip1 >= ipmin )
	{
	  ip1     = itrav[ip1];
	  ifauxj  = iadr[ip1];
	  ifacj   = ifauxj/nelep1;
	  nelj    = ifauxj-ifacj*nelep1;

	  js1 = maillnodes.node[somfac[ifacj][0]][nelj];
	  js2 = maillnodes.node[somfac[ifacj][1]][nelj];
	  js3 = maillnodes.node[somfac[ifacj][2]][nelj];

	  if ( js1 <= js2 && js1 <= js3) 
	    {
	      if (js2 <= js3) { jso1=js1; jso2=js2; jso3=js3;}
	      else            { jso1=js1; jso2=js3; jso3=js2;}
	    }
	  else if ( js2 <= js1 && js2 <= js3) 
	    {
	      if (js1 <= js3) { jso1=js2; jso2=js1; jso3=js3;}
	      else            { jso1=js2; jso2=js3; jso3=js1;}
	    }
	  else
	    {
	      if (js1 <= js2) { jso1=js3; jso2=js1; jso3=js2;}
	      else            { jso1=js3; jso2=js2; jso3=js1;}
	    }

	  if ( iso1 == jso1 &&  iso2 == jso2 &&  iso3 == jso3)
	    {
	      nvoisin[ifaci][neli] = nelj;
	      nvoisin[ifacj][nelj] = neli;
	      continue ;
	    }
	}

    }

  free(iadr);
  free(itrav);
  perfo.mem_cond-=(2*nmemax) * sizeof(int);


  nfacbo=0;

  for (ifac=0;ifac<nbface;ifac++)
    for (nel=0;nel<maillnodes.nelem;nel++)
      if ( nvoisin[ifac][nel] == -1 ) nfacbo++;

  maillnodebord->nelem=nfacbo;
  maillnodebord->ndim=3;
  maillnodebord->ndmat=3;
  maillnodebord->ndiele=2;
  maillnodebord->nrefe=(int*)malloc(maillnodebord->nelem*sizeof(int));
  maillnodebord->node=(int**)malloc((maillnodebord->ndmat+1)*sizeof(int*));
  for (i=0;i<maillnodebord->ndmat+1;i++) maillnodebord->node[i]=(int*)malloc(maillnodebord->nelem*sizeof(int));
  verif_alloue_int1d("extrbord3",maillnodebord->nrefe);
  verif_alloue_int2d(maillnodebord->ndmat+1,"extrbord3",maillnodebord->node);

  perfo.mem_cond+=(maillnodebord->nelem*(maillnodebord->ndmat+3) * sizeof(int));
  if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;

  nfacbo=0;
  for (ifac=0;ifac<nbface;ifac++)
    for (i=0;i<maillnodes.nelem;i++)
      {
      if ( nvoisin[ifac][i] == -1 )
	{
	  is1 = maillnodes.node[somfac[ifac][0]][i]; 
	  is2 = maillnodes.node[somfac[ifac][1]][i]; 
	  is3 = maillnodes.node[somfac[ifac][2]][i];
	  maillnodebord->node[0][nfacbo]=is1;
	  maillnodebord->node[1][nfacbo]=is2;
	  maillnodebord->node[2][nfacbo]=is3;
	  maillnodebord->node[3][nfacbo]=i;
	  nr=nrefac[ifac][i]; maillnodebord->nrefe[nfacbo]=nr;
	  nfacbo++;
	}
      }

  for (i=0; i<nbface; i++) free(nvoisin[i]);
  free(nvoisin);

  printf("\n *** EXTRBORD3 : nombre d'elements du maillage de bord : %d",maillnodebord->nelem);
/*    imprime_connectivite(*maillnodebord); */

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | Extraction des voisins                                               |
  |======================================================================| */

void extrvois2(struct Maillage maillnodes,int **nvoisin)
{
  int i,j;
  int is1,is2,is3,isad,js1,js2,iso1,iso2,jso1,jso2;
  int nmemax,nelep1,ip,ip1,ipmax,ipmin;
  int nel,neli,nelj,ifac,ifaci,ifacj,ifauxi,ifauxj;
  int nfacbo,nr;
  int *iadr,*itrav,nbface=3;
  
  nvoisin=(int**)malloc(nbface*sizeof(int*));
  for (i=0; i<nbface; i++) nvoisin[i]=(int*)malloc(maillnodes.nelem*sizeof(int));
  verif_alloue_int2d(nbface,"xmaillbord2",nvoisin);

  nmemax =  maillnodes.nelem * 3 + 2 * maillnodes.npoin;
  nelep1 =  maillnodes.nelem + 1;
  ipmin  =  2 * maillnodes.npoin+1;
  ip     =  2 * maillnodes.npoin;
  

  somfac[0][0] = 0; somfac[0][1] = 1;
  somfac[1][0] = 1; somfac[1][1] = 2;
  somfac[2][0] = 2; somfac[2][1] = 0;


  iadr  = (int *)malloc(nmemax * sizeof(int));
  itrav = (int *)malloc(nmemax * sizeof(int));
  verif_alloue_int1d("xmaillbord2",iadr);
  verif_alloue_int1d("xmaillbord2",itrav);

  perfo.mem_cond+=(2*nmemax+maillnodes.nelem*nbface) * sizeof(int);
  if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;


  for (i=0; i < nmemax ; i++) *(iadr+i) = -1 ;
  for (i=0; i < nmemax ; i++) *(itrav+i) = 0 ;
  for (j=0; j < nbface; j++)
    for (i=0; i < maillnodes.nelem ; i++) nvoisin[j][i]=-1;


  for ( ifac=0;ifac<3;ifac++)
    for (i=0;i<maillnodes.nelem;i++)
      {
	is1 = maillnodes.node[somfac[ifac][0]][i];
	is2 = maillnodes.node[somfac[ifac][1]][i];
	
	isad = is1+is2;
	
	if(iadr[isad] == -1)
	  {
	    iadr[isad] =i+ifac*nelep1 ;
	    itrav[isad]=isad;
	  }
	else 
	  {
	    ip++; iadr[ip]=i+ifac*nelep1; itrav[ip]=itrav[isad];
	    itrav[isad] = ip;
	  }
      }

  ipmax = ip ;

  for ( i=ipmax;i>=ipmin;i--)
    {
      ifauxi  = iadr[i];
      ifaci   = ifauxi/nelep1;
      neli    = ifauxi-ifaci*nelep1;

      if (nvoisin[ifaci][neli] != -1) continue;

      is1 = maillnodes.node[somfac[ifaci][0]][neli];
      is2 = maillnodes.node[somfac[ifaci][1]][neli];

      if (is1<is2){iso1=is1; iso2=is2;}
      else {iso1=is2; iso2=is1;}

      ip1 = i;
      while (ip1 >= ipmin)
	{
	  ip1     = itrav[ip1];
	  ifauxj  = iadr[ip1];
	  ifacj   = ifauxj/nelep1;
	  nelj    = ifauxj-ifacj*nelep1;

	  js1 = maillnodes.node[somfac[ifacj][0]][nelj];
	  js2 = maillnodes.node[somfac[ifacj][1]][nelj];

	  if (js1<js2){jso1=js1; jso2=js2;}
	  else {jso1=js2; jso2=js1;}

	  if (iso1==jso1 && iso2==jso2)
	    {
	      nvoisin[ifaci][neli] = nelj;
	      nvoisin[ifacj][nelj] = neli;
	      continue ;
	    }
	}

    }

  free(iadr);
  free(itrav);
  perfo.mem_cond-=(2*nmemax) * sizeof(int);


  if (affich.ray_extrbord)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("\n >>> extrvois2 : impression de nvoisin (ex- nfabor)\n");
	  for (i=0;i<maillnodes.nelem;i++) 
	    {
	      printf("\n element %d voisin ",i);
	      for (j=0;j<nbface;j++) printf(" %d ",nvoisin[j][i]);
	    }
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n >>> extrvois2 : nvoisin table \n");
	  for (i=0;i<maillnodes.nelem;i++) 
	    {
	      printf("\n element %d nvoisin ",i);
	      for (j=0;j<nbface;j++) printf(" %d ",nvoisin[j][i]);
	    }
	}
    } 
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | Extraction des voisins                                               |
  |======================================================================| */
void extrvois3(struct Maillage maillnodes,int **nvoisin)
{
  int i,j;
  int is1,is2,is3,is4,is5,is6,isad,js1,js2,js3;
  int iso1,iso2,iso3,jso1,jso2,jso3;
  int nmemax,nelep1,ip,ip1,nr;
  int ipmax,ipmin;
  int nel,neli,nelj;
  int ifac,ifaci,ifacj;
  int ifauxi,ifauxj;
  int nfacbo; 
  int *iadr,*itrav,nbface=4;
  
  nvoisin=(int**)malloc(nbface*sizeof(int*));
  for (i=0; i<nbface; i++) nvoisin[i]=(int*)malloc(maillnodes.nelem*sizeof(int));
  verif_alloue_int2d(nbface,"xmaillbord3",nvoisin);

  nmemax = maillnodes.nelem * 4 + 3 *maillnodes.npoin;
  nelep1 = maillnodes.nelem + 1;
  ipmin  = 3*maillnodes.npoin+1;
  ip     = 3*maillnodes.npoin;
  

  somfac[0][0]=0; somfac[0][1]=1; somfac[0][2]=2;
  somfac[1][0]=0; somfac[1][1]=3; somfac[1][2]=1;
  somfac[2][0]=0; somfac[2][1]=2; somfac[2][2]=3;
  somfac[3][0]=1; somfac[3][1]=3; somfac[3][2]=2;


  iadr  = (int *)malloc(nmemax * sizeof(int));
  itrav = (int *)malloc(nmemax * sizeof(int));
  verif_alloue_int1d("xmaillbord3",iadr);
  verif_alloue_int1d("xmaillbord3",itrav);

  perfo.mem_cond+=(2*nmemax+maillnodes.nelem*nbface) * sizeof(int);
  if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;

  for (i=0; i < nmemax ; i++) *(iadr+i) = -1 ;
  for (i=0; i < nmemax ; i++) *(itrav+i) = 0 ;
  for (j=0; j < nbface; j++)
    for (i=0; i < maillnodes.nelem ; i++) nvoisin[j][i]=-1;


  for ( ifac=0;ifac<4;ifac++)
    {
    
      for ( i=0;i<maillnodes.nelem;i++)
	{
	  is1 = maillnodes.node[somfac[ifac][0]][i]; 
	  is2 = maillnodes.node[somfac[ifac][1]][i]; 
	  is3 = maillnodes.node[somfac[ifac][2]][i]; 

	  isad = is1+is2+is3;

	  if(iadr[isad] == -1)
	    {
	      iadr[isad] = i + ifac*nelep1;
	      itrav[isad]= isad;
	    }
	  else 
	    {
	      ip++;  iadr[ip]=i+ifac*nelep1; itrav[ip]=itrav[isad];
	      itrav[isad] = ip;
	    }
	}
    }

  ipmax = ip ;

  for ( i=ipmax;i>=ipmin;i--)
    {
      ifauxi  = iadr[i];
      ifaci   = ifauxi/nelep1;
      neli    = ifauxi-ifaci*nelep1;


      if (nvoisin[ifaci][neli] != -1) continue;

      is1 = maillnodes.node[somfac[ifaci][0]][neli];
      is2 = maillnodes.node[somfac[ifaci][1]][neli];
      is3 = maillnodes.node[somfac[ifaci][2]][neli];

      if ( is1 <= is2 && is1 <= is3) 
	{
	  if (is2 <= is3) { iso1=is1; iso2=is2; iso3=is3;}
	  else            { iso1=is1; iso2=is3; iso3=is2;}
	}
      else if ( is2 <= is1 && is2 <= is3) 
	{
	  if (is1 <= is3) { iso1=is2; iso2=is1; iso3=is3;}
	  else            { iso1=is2; iso2=is3; iso3=is1;}
	}
      else
	{
	  if (is1 <= is2) { iso1=is3; iso2=is1; iso3=is2;}
	  else            { iso1=is3; iso2=is2; iso3=is1;}
	}

      ip1 = i;
      while ( ip1 >= ipmin )
	{
	  ip1     = itrav[ip1];
	  ifauxj  = iadr[ip1];
	  ifacj   = ifauxj/nelep1;
	  nelj    = ifauxj-ifacj*nelep1;

	  js1 = maillnodes.node[somfac[ifacj][0]][nelj];
	  js2 = maillnodes.node[somfac[ifacj][1]][nelj];
	  js3 = maillnodes.node[somfac[ifacj][2]][nelj];

	  if ( js1 <= js2 && js1 <= js3) 
	    {
	      if (js2 <= js3) { jso1=js1; jso2=js2; jso3=js3;}
	      else            { jso1=js1; jso2=js3; jso3=js2;}
	    }
	  else if ( js2 <= js1 && js2 <= js3) 
	    {
	      if (js1 <= js3) { jso1=js2; jso2=js1; jso3=js3;}
	      else            { jso1=js2; jso2=js3; jso3=js1;}
	    }
	  else
	    {
	      if (js1 <= js2) { jso1=js3; jso2=js1; jso3=js2;}
	      else            { jso1=js3; jso2=js2; jso3=js1;}
	    }

	  if ( iso1 == jso1 &&  iso2 == jso2 &&  iso3 == jso3)
	    {
	      nvoisin[ifaci][neli] = nelj;
	      nvoisin[ifacj][nelj] = neli;
	      continue ;
	    }
	}

    }

  free(iadr);
  free(itrav);
  perfo.mem_cond-=(2*nmemax) * sizeof(int);


  if (affich.ray_extrbord)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("\n >>> extrvois3 : impression de nvoisin (ex- nfabor)\n");
	  for (i=0;i<maillnodes.nelem;i++) 
	    {
	      printf("\n element %d voisin ",i);
	      for (j=0;j<nbface;j++) printf(" %d ",nvoisin[j][i]);
	    }
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n >>> extrvois3 : nvoisin table printing \n");
	  for (i=0;i<maillnodes.nelem;i++) 
	    {
	      printf("\n element %d nvoisin ",i);
	      for (j=0;j<nbface;j++) printf(" %d ",nvoisin[j][i]);
	    }
	}
    } 

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL,                                     |
  |======================================================================|
  | Extraction de la table des aretes                                    |
  |======================================================================| */
void extrarete(struct Maillage *maillnodes)
{
  int n,n1,n2,m,nn,pastrouve;
  int nbareparele,nbarete;
  int **eltarete,**arete;
  struct BoutArete **tabarete,*p,*pp,*q;
  int somare[6][2];

  if (maillnodes->ndim==2)
    {
      nbareparele=3;
      somare[0][0] = 0; somare[0][1] = 1;
      somare[1][0] = 1; somare[1][1] = 2;
      somare[2][0] = 2; somare[2][1] = 0;
    }
  else
    {
      nbareparele=6;
      somare[0][0] = 0; somare[0][1] = 1;
      somare[1][0] = 1; somare[1][1] = 2;
      somare[2][0] = 2; somare[2][1] = 0;
      somare[3][0] = 0; somare[3][1] = 3;
      somare[4][0] = 3; somare[4][1] = 1;
      somare[5][0] = 2; somare[5][1] = 3;
    }


  maillnodes->eltarete=(int**)malloc(nbareparele*sizeof(int*));
  for (n=0;n<nbareparele;n++) maillnodes->eltarete[n]=(int*)malloc(maillnodes->nelem*sizeof(int));
  perfo.mem_cond+=nbareparele*maillnodes->nelem*sizeof(int);

  tabarete=(struct BoutArete**)malloc(maillnodes->npoin*sizeof(struct BoutArete*));
  for (n=0;n<maillnodes->npoin;n++) tabarete[n]=NULL;

  for (nbarete=n=0;n<maillnodes->nelem;n++)
    {
      for (m=0;m<nbareparele;m++)
	{
	  n1 = maillnodes->node[somare[m][0]][n];
	  n2 = maillnodes->node[somare[m][1]][n];
	  if (n1>n2) {nn=n1; n1=n2; n2=nn;}

	  if (!tabarete[n1])
	    {
	      q=(struct BoutArete*)malloc(sizeof(struct BoutArete));
	      q->num=n2;
	      q->numloc=nbarete;
	      q->suivant=NULL;
	      tabarete[n1]=q;
	      maillnodes->eltarete[m][n]=nbarete;
	      nbarete++;
	    }
	  else
	    {
	      pastrouve=1;
	      p=pp=tabarete[n1];
	      while(p && pastrouve)
		{
		  if (p->num==n2) {pastrouve=0; break;}
		  pp=p;
		  p=p->suivant;
		}
	      if (pastrouve)
		{
		  q=(struct BoutArete*)malloc(sizeof(struct BoutArete));
		  q->num=n2;
		  q->numloc=nbarete;
		  q->suivant=NULL;
		  pp->suivant=q;
		  maillnodes->eltarete[m][n]=nbarete;
		  nbarete++;
		}
	      else
		{
		  maillnodes->eltarete[m][n]=p->numloc;
		}
	    }
	}
    }

  /* construction de la table d'aretes */
  maillnodes->nbarete=nbarete;
  maillnodes->arete=(int**)malloc(2*sizeof(int*));
  for (n=0;n<2;n++) maillnodes->arete[n]=(int*)malloc(nbarete*sizeof(int));
  perfo.mem_cond+=nbarete*2*sizeof(int);

  for (n=0;n<maillnodes->npoin;n++)
    if (tabarete[n])
      {
	p=tabarete[n];
	while(p)
	  {
	    maillnodes->arete[0][p->numloc]=n;
	    maillnodes->arete[1][p->numloc]=p->num;
	    p=p->suivant;
	  }
      }

  /* destruction des listes intermediaires */
  for (n=0;n<maillnodes->npoin;n++)
    if (tabarete[n])
      {
	p=tabarete[n];
	while(p){q=p->suivant; free(p); p=q;}
      }
  free(tabarete);

  /* impression de controle */
  if (affich.cond_creemaill)
    {
      if (SYRTHES_LANG == FR)
	printf("\n >>> extrarete : nombre d'aretes = %d\n",maillnodes->nbarete);
      else if (SYRTHES_LANG == EN)
	printf("\n >>> extrarete : number of edges = %d\n",maillnodes->nbarete);

      if (SYRTHES_LANG == FR)
	{
	  printf("\n >>> extrarete : impression des tables d'aretes\n");
	  printf("                 nombre d'aretes = %d\n",maillnodes->nbarete);
	  for (n=0;n<maillnodes->nbarete;n++)
	    printf(" arete %d noeuds : %d %d\n",n,maillnodes->arete[0][n],maillnodes->arete[1][n]);
	  
	  printf("\n                 liste des aretes par element\n");
	  for (n=0;n<maillnodes->nelem;n++)
	    {
	      printf(" element %d aretes ",n);
	      for (m=0;m<nbareparele;m++) printf(" %d",maillnodes->eltarete[m][n]);
	      printf("\n");
	    }
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n >>> extrarete : Table of edges printing\n");
	  printf("                 number of edges = %d\n",maillnodes->nbarete);
	  for (n=0;n<maillnodes->nbarete;n++)
	    printf(" edge %d nodes : %d %d\n",n,maillnodes->arete[0][n],maillnodes->arete[1][n]);
	  
	  printf("\n                 edge list by element\n");
	  for (n=0;n<maillnodes->nelem;n++)
	    {
	      printf(" element %d edges ",n);
	      for (m=0;m<nbareparele;m++) printf(" %d",maillnodes->eltarete[m][n]);
	      printf("\n");
	    }
	}
      
      
    }
}


/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "syr_usertype.h"
# include "syr_tree.h"
# include "syr_abs.h"
# include "syr_option.h"
# include "syr_parall.h"
# include "syr_bd.h"
# include "syr_proto.h"
# include "syr_parall.h"
# include "syr_const.h"

#ifdef _SYRTHES_MPI_
# include "mpi.h"
#endif

extern struct Performances perfo;
extern struct Affichages affich;
                    


int nsp;
double taille_boite,taille_seg;

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP , C. PENIGUEL                                     |
  |======================================================================|
  |  Gestion du calcul des facteurs de forme                             |
  |======================================================================| */

void inifdf(struct GestionFichiers gestionfichiers,
	    struct Maillage *maillnodray,
	    struct SymPer raysymper,struct Compconnexe volconnexe,
	    struct PropInfini *propinf,
	    struct FacForme ****listefdf,double **diagfdf,
	    struct Mask mask,struct Vitre *vitre,
	    struct Horizon *horiz,
	    struct SDparall_ray *sdparall_ray)
{
  int i,j,nb,exist,rangcourant;
  int ligne_deb,ligne_fin,nelemloc;
  struct FacForme *pff;

  if (syrglob_nparts==1 || syrglob_rang==0)
    {
      if (SYRTHES_LANG == FR)
	printf("\n *** inifdf : facteurs de forme\n");
      else if (SYRTHES_LANG == EN)
	printf("\n *** inifdf : view factors\n");
    }

  /* allocation volume et normales */
  /* ----------------------------- */
  maillnodray->volume=(double*)malloc(maillnodray->nelem*sizeof(double));
  verif_alloue_double1d("alloueray",maillnodray->volume);
  perfo.mem_ray+=maillnodray->nelem*sizeof(double);

  maillnodray->xnf=(double**)malloc(maillnodray->ndim*sizeof(double));
  for (i=0;i<maillnodray->ndim;i++) 
    maillnodray->xnf[i]=(double*)malloc(maillnodray->nelem*sizeof(double));
  verif_alloue_double2d(maillnodray->ndim,"alloueray",maillnodray->xnf);
  perfo.mem_ray+=maillnodray->ndim*maillnodray->nelem*sizeof(double);

  /* calcul des surfaces */
  /* ------------------- */
  if (maillnodray->ndim==2 && maillnodray->iaxisy)    
    surface_anneau(*maillnodray);
  else if (maillnodray->ndim==2)
    surface_seg (*maillnodray);
  else  
    surface_tria(*maillnodray);


  /* lignes de fdf a calculer */
  /* ------------------------ */
  if (sdparall_ray->npartsray>1)    {
    ligne_deb=sdparall_ray->ieledeb[sdparall_ray->rangray];
    ligne_fin=sdparall_ray->ieledeb[sdparall_ray->rangray+1];
    nelemloc=sdparall_ray->nelrayloc[sdparall_ray->rangray];
    *listefdf=(struct FacForme ***)malloc(sdparall_ray->npartsray*sizeof(struct FacForme **));
    rangcourant=sdparall_ray->rangray;
    sdparall_ray->nelraytot=maillnodray->nelem;
  }
  else {
    ligne_deb=0;
    ligne_fin=maillnodray->nelem;
    nelemloc=maillnodray->nelem;
    *listefdf=(struct FacForme ***)malloc(sizeof(struct FacForme **));
    rangcourant=0;
    sdparall_ray->nelraytot=maillnodray->nelem;
  }
  

  if (gestionfichiers.lec_fdf)
    {
      lire_fdf(*listefdf,diagfdf,propinf,horiz,*sdparall_ray);
      if (affich.ray_fdf)  
	affiche_fdf(nelemloc,maillnodray->volume+ligne_deb,
		    *listefdf,*diagfdf,*propinf,*horiz,sdparall_ray->npartsray);
    }
  else
    {
      /* calul des fdf */
      /* ------------- */
      if (horiz->actif)
	calfdfH(*maillnodray,*listefdf,diagfdf,volconnexe,mask,horiz,*sdparall_ray);
      else
	calfdf(*maillnodray,*listefdf,diagfdf,raysymper,volconnexe,mask,horiz,*sdparall_ray);
      
      /* verifications et affichages */
      if (!horiz->actif && !propinf->actif)
	verif_fdf(nelemloc,maillnodray->volume+ligne_deb,*listefdf,*diagfdf,sdparall_ray->npartsray);
      if (affich.ray_fdf) 
	affiche_fdf(nelemloc,maillnodray->volume+ligne_deb,
		    *listefdf,*diagfdf,*propinf,*horiz,sdparall_ray->npartsray);
    }
  

   /* affichage pour debug pour IR/CP */ 
/*   printf("liste des fdf non nuls par ligne\n"); */
/*   for (i=0;i<nelemloc;i++) */
/*     { */
/*       printf("ligne %d\n",i); */
/*       for (j=0;j<sdparall_ray->npartsray;j++) */
/* 	{ */
/* 	  pff=(*listefdf)[j][i]; */
/* 	  while(pff) {printf("proc=%d j(=numenface)=%d fdf=%f\n",j,pff->numenface,pff->fdf); pff=pff->suivant;} */
/* 	} */
/*     } */
	  

 


  if (vitre->actif)
    couplevitre(vitre,*maillnodray,volconnexe);


  if (propinf->actif) 
    calfdfnp1(*maillnodray,*listefdf,*diagfdf,propinf,*horiz,sdparall_ray->npartsray);



  /* initialisations du tableau de presence des fdf avec les autres proc (parallelisme) */
  if (sdparall_ray->npartsray > 1)
    {
      sdparall_ray->fdfproc=(int*)malloc(sdparall_ray->npartsray*sizeof(int));
      
      for (j=0;j<sdparall_ray->npartsray;j++)
	{
	  if (j==sdparall_ray->rangray)
	    sdparall_ray->fdfproc[j]=1;
	  else
	    {
	      sdparall_ray->fdfproc[j]=0;
	      for (i=0;i<nelemloc;i++)
		if ((*listefdf)[j][i]) {sdparall_ray->fdfproc[j]=1; break;}
	    }
	}
    }

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP , C. PENIGUEL                                     |
  |======================================================================|
  |  Regularisation des facteurs de forme                                |
  |======================================================================| */
void regufdf(struct GestionFichiers gestionfichiers,
	     struct Maillage maillnodray,
	     struct PropInfini propinf,
	     struct FacForme ***listefdf,double *diagfdf,
	     struct Mask mask,
	     struct Horizon horiz,
	     struct SDparall_ray sdparall_ray)
{

  /* regularisation des fdf */
  if (!propinf.actif && !gestionfichiers.lec_fdf  && !mask.nelem)
    {
      if (sdparall_ray.npartsray>1)
	smoogc_parall(maillnodray.nelem,listefdf,diagfdf,maillnodray.volume,sdparall_ray);
      else
	{smoogc(maillnodray.nelem,listefdf[0],diagfdf,maillnodray.volume);}
      if (affich.ray_fdf) 
	affiche_fdf(maillnodray.nelem,maillnodray.volume,listefdf,diagfdf,
		    propinf,horiz,sdparall_ray.npartsray);
    }

  /* ecriture eventuelle sur fichier */
  if (gestionfichiers.stock_fdf) 
    ecrire_fdf(listefdf,diagfdf,propinf,horiz,maillnodray.nelem,sdparall_ray);


}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | calfdf                                                               |
  |         Gestion du calcul des facteurs de forme                      |
  |======================================================================| */
void calfdf(struct Maillage maillnodray,
	    struct FacForme ***listefdf,double **diagfdf,
	    struct SymPer raysymper,struct Compconnexe volconnexe,
	    struct Mask mask,struct Horizon *horiz,
	    struct SDparall_ray sdparall_ray)
{
  int n,nn,i,j,k,n2,nv;
  int ligne_deb,ligne_fin,nelemloc;
  int ok,noeud[6];
  int faces_cachees,imult,*typfbid;
  double xi[6],yi[6],zi[6],fforme;
  double dnx,dny,dnz,p,tiers,gx1,gy1,gz1,gx2,gy2,gz2,dir;  
  double xn1,yn1,zn1,xn2,yn2,zn2,x01,y01,z01,x02,y02,z02;
  double t1,t2 ;
  int *grcon,*grconv,*grconv2;
  double xmult,**xn,*px,*py,*pz,*pland;
  double x,y,z;
  struct SymPer defsymper;
  struct Maillage maillnod2,maill;
  struct FacForme *pff;



  /* nombre de facteurs de forme a calculer */
  /* -------------------------------------- */
  if (sdparall_ray.npartsray>1)    {
    ligne_deb=sdparall_ray.ieledeb[sdparall_ray.rangray];
    ligne_fin=sdparall_ray.ieledeb[sdparall_ray.rangray+1];
    nelemloc=sdparall_ray.nelrayloc[sdparall_ray.rangray];
  }
  else {
    ligne_deb=0;
    ligne_fin=maillnodray.nelem;
    nelemloc=maillnodray.nelem;
  }

  /* dimension du domaine */
  /* -------------------- */
  if (maillnodray.ndim==2)
    dimension_2d(maillnodray,&taille_boite,&taille_seg);
  else
    dimension_3d(maillnodray,&taille_boite,&taille_seg);

  imult = 0;  xmult = 1.;
  defsymper=raysymper;
  
  /* facteur d'echelle eventuel */
  /* -------------------------- */
  if(taille_seg < 0.01 || taille_seg > 10)
    {
      imult = 1;
      if (taille_seg < 0.01) xmult = 0.1/taille_seg;
      if (taille_seg > 10  ) xmult =   1/taille_seg;
      if(affich.ray_fdf) 
	if (SYRTHES_LANG == FR) printf("                  Facteur multiplicatif interne xmult= %f \n",xmult);
	else if (SYRTHES_LANG == EN) printf("                  Internal multiplying factor xmult= %f \n",xmult);

      if (maillnodray.ndim==2)
	{
	  for (i=0,px=maillnodray.coord[0],py=maillnodray.coord[1];i<maillnodray.npoin;i++,px++,py++)
	    {*px *= xmult; *py *= xmult;}
/* 	  for (i=0;i< maillnodray.nelem ;i++) maillnodray.volume[i] *=  xmult; */
	}
      else
	{
	  for (i=0,px=*(maillnodray.coord),py=*(maillnodray.coord+1),pz=*(maillnodray.coord+2);
	       i<maillnodray.npoin;i++,px++,py++,pz++)
	    {*px *= xmult; *py *= xmult; *pz *= xmult;}
/* 	  for (i=0;i< maillnodray.nelem ;i++) maillnodray.volume[i] *=  (xmult*xmult); */
	}
      
      taille_boite *= xmult;
      taille_seg   *= xmult;

      for(i=0;i<volconnexe.nb;i++) {
	volconnexe.x[i]*=xmult; 
	volconnexe.y[i]*=xmult; 
	if (maillnodray.ndim==3) volconnexe.z[i]*=xmult;} 

      if (defsymper.nbper && maillnodray.ndim==2) 
	{defsymper.per[0][0]*=xmult; defsymper.per[0][1]*=xmult; } /* uniqt sur les coord du point invariant */
      else if (defsymper.nbper && maillnodray.ndim==3)
	for(j=0;j<3;j++) defsymper.per[0][j]*=xmult;               /* uniqt sur les coord du point invariant */


      for(i=0;i<defsymper.nbsym;i++) 
	if (maillnodray.ndim==2) defsymper.sym[i][2]*=xmult;
	else defsymper.sym[i][3]*=xmult;
    }


  /* orientation des facettes - composantes connexes */
  /* ----------------------------------------------- */
  grconv = (int *)malloc( maillnodray.nelem * sizeof(int) );
  verif_alloue_int1d("calfdf",grconv);
  perfo.mem_ray+=maillnodray.nelem * sizeof(int);
  
  if (maillnodray.ndim==2 && maillnodray.iaxisy)
    {
      orient2d(maillnodray,volconnexe,grconv);
      gauss();
      /* si axe de symetrie = x alors inversion des axes x et y */
      if (maillnodray.iaxisy==1) alter_axi1(1,maillnodray,defsymper);
      cnor_2d(maillnodray); 
    }
  else if (maillnodray.ndim==2)
    {
      orient2d(maillnodray,volconnexe,grconv);
      cnor_2d(maillnodray);
    }
  else
    {
      orient3d(maillnodray,volconnexe,grconv);
      cnor_3d(maillnodray);
    }


  /* gestion des symetries et periodicites */
  /* ------------------------------------- */
 
  if (!defsymper.nbsym && !defsymper.nbper)
    {
      n2=1;
      dupliq_maill(maillnodray,&maillnod2,n2);
    }

  else
    {
      
      if (maillnodray.ndim==2 && maillnodray.iaxisy)
	{
	  if (defsymper.nbsym>1)
	    {
	      if (SYRTHES_LANG == FR)
		printf(">>> ERREUR calfdf : en rayonnement 2D-axi, on ne peut avoir qu'une seule symetrie\n\n");
	      else if (SYRTHES_LANG == EN)
		printf(">>> ERROR calfdf : in 2D-axi radiation, only one symetry is possible\n\n");
	      syrthes_exit(1);
	    }
	  if (defsymper.nbper)
	    {
	      if (SYRTHES_LANG == FR)
		printf(">>> ERREUR calfdf : en rayonnement 2D-axi, on ne peut pas avoir de periodicite\n\n");
	      else if (SYRTHES_LANG == EN)
		printf(">>> ERROR calfdf : in 2D-axi radiation, it is not possible to activate periodicity\n\n");
	      syrthes_exit(1);
	    }
	  
	  n2=2;
	  dupliq2d_sym(defsymper,n2,maillnodray,&maillnod2);    
	}
      
      else if (maillnodray.ndim==2)
	{
	  /* en 2D, on ne peut avoir que 1 periodicite OU 1 symetrie OU 2 symetries */
	  n2=1;
	  if (defsymper.nbper) n2=defsymper.nbper;
	  if (defsymper.nbsym==1)  n2*=2;  
	  else if (defsymper.nbsym==2)  n2*=4; 
      
	  if (defsymper.nbsym && defsymper.nbper)
	    {
	      if (SYRTHES_LANG == FR)
		printf(">>> ERREUR calfdf : en rayonnement 2D, on ne peut avoir a la fois symetrie et periodicite\n\n");
	      else if (SYRTHES_LANG == EN)
		printf(">>> ERROR calfdf : in 2D radiation, it is impossible  to have both symetry and periodicity\n\n");
	      syrthes_exit(1);
	    }

	  if (defsymper.nbsym)
	    dupliq2d_sym(defsymper,n2,maillnodray,&maillnod2);    
	  else if (defsymper.nbper)
	    dupliq2d_per(defsymper,n2,maillnodray,&maillnod2);  
 
	}
      else
	{
	  /* en 3D, on ne peut avoir que (1 per+1sym) OU 1 per OU (1,2 ou 3 symetries) */
	  n2=1;
	  if (defsymper.nbper) n2=defsymper.nbper;
	  switch (defsymper.nbsym)
	    {
	    case 1: n2*=2; break;
	    case 2: n2*=4; break;
	    case 3: n2*=8; break;
	    }

	  if (defsymper.nbsym && !defsymper.nbper) 
	    dupliq3d_sym(defsymper,n2,maillnodray,&maillnod2);   
	  else if (defsymper.nbper > 0)
	    dupliq3d_per(defsymper,n2,maillnodray,&maillnod2);    

	  /* isa deb */
/* 	  imprime_maillage(maillnod2); */
	  /* isa fin */
	}	  


      /* duplication des composantes connexes */
      grconv2=(int*)malloc(n2*maillnodray.nelem*sizeof(int));
      verif_alloue_int1d("calfdf",grconv2);
      for (n=0;n<n2;n++) 
	for (i=0;i<maillnodray.nelem;i++)
	  grconv2[n*maillnodray.nelem+i]=grconv[i];
    }


  if (maillnodray.ndim==2){
    if (!maillnodray.iaxisy) box_2d(maillnod2.npoin,maillnod2.coord);  
    cnor_2d(maillnod2);
  }
  else {
/*     box_3d(maillnod2.npoin,maillnod2.coord); ??????????????????????????????????????? */
    cnor_3d(maillnod2);
  }


  /* sur quel maillage on travaille */
  maill=maillnod2;
  grcon=grconv; 
  if (defsymper.nbsym || defsymper.nbper) grcon=grconv2; 

  if (affich.ray_fdf)
    ecrire_geom("maill_pour_fdf.syr",&maill,NULL);

  /* --------------------------------------------------------*/
  /*          calcul des fdf                                 */
  /* --------------------------------------------------------*/

	  
  /* allocation pour le stockage des fdf */
  for (n=0;n<sdparall_ray.npartsray;n++)
    listefdf[n]=(struct FacForme**)malloc(nelemloc*sizeof(struct FacForme*));
  *diagfdf=(double*)malloc(nelemloc*sizeof(double));

  for (n=0;n<sdparall_ray.npartsray;n++)
    for (i=0;i<nelemloc;i++)
      listefdf[n][i]=NULL;

  for (i=0;i<nelemloc;i++)
    (*diagfdf)[i]=0.;



  /* allocation des tableaux locaux */
  if (!maillnodray.iaxisy)
    {
      pland=(double*)malloc(maill.nelem*sizeof(double));
      verif_alloue_double1d("calfdf",pland);
      perfo.mem_ray+=maill.nelem*sizeof(double);
      if (perfo.mem_ray>perfo.mem_ray_max) perfo.mem_ray_max=perfo.mem_ray;
    }


  /* calcul des fdf */
  /* -------------- */
  if (maillnodray.ndim==2 && maillnodray.iaxisy)
    {
      t1=cpusyrt();
      
      facforme_2a(maillnodray.nelem,maillnodray.npoin,maill.nelem,maillnodray.iaxisy,
		  maill.node,maill.coord,maill.xnf,listefdf,*diagfdf,grcon,mask,
		  sdparall_ray);
      
      t2=cpusyrt();
      /* si axe de symetrie = x on remet dans le bon sens  x et y */
      if (maillnodray.iaxisy==1) alter_axi1(1,maillnodray,defsymper);
    }

  else if (maillnodray.ndim==2)
    {
      for (i=0;i<maill.nelem;i++)
	{
	  xn1=maill.xnf[0][i]; 
	  yn1=maill.xnf[1][i]; 
	  x=maill.coord[0][maill.node[0][i]];  y=maill.coord[1][maill.node[0][i]];
	  pland[i]=-x*xn1-y*yn1;
	}

      facecache_2d(maill.nelem,maill.node,maill.coord,maill.xnf,pland,grcon,&faces_cachees,
		   ligne_deb,ligne_fin);

      t1=cpusyrt();
      
      /* ??????????? attention a typfbid ??? ce n'est pas initialise... ca sert a quoi ??? */
      facforme_2d(maillnodray.nelem,maill.npoin,maill.nelem,maill.node,maill.coord,pland,maill.xnf,
		  listefdf,*diagfdf,grcon,faces_cachees,
		  mask,horiz,typfbid,sdparall_ray);
      t2=cpusyrt();
    }

  else
    {
      for (i=0;i<maill.nelem;i++)
	{
	  xn1=maill.xnf[0][i]; 
	  yn1=maill.xnf[1][i];
	  zn1=maill.xnf[2][i]; 
	  x=maill.coord[0][maill.node[0][i]];  
	  y=maill.coord[1][maill.node[0][i]]; 
	  z=maill.coord[2][maill.node[0][i]];
	  pland[i]=-x*xn1-y*yn1-z*zn1;
	}
      
      facecache_3d(maill.nelem,maill.node,maill.coord,maill.xnf,pland,grcon,&faces_cachees,ligne_deb,ligne_fin);
      t1=cpusyrt();
      facforme_3d(maillnodray.nelem,maill.npoin,maill.nelem,maill.node,maill.coord,pland,maill.xnf,
		  listefdf,*diagfdf,grcon,faces_cachees,
		  mask,horiz,typfbid,sdparall_ray);
      t2=cpusyrt();
    }


  if (syrglob_nparts==1 || syrglob_rang==0)
    if (SYRTHES_LANG == FR)
      printf("\n *** calfdf : temps de calcul des facteurs de forme = %e\n",t2-t1);
    else if (SYRTHES_LANG == EN)
      printf("\n *** calfdf : CPU time to calculate view factors = %e\n",t2-t1);
  
/* --------------------------------------------------------*/

  /* liberation des tableaux locaux */
  if (!maillnodray.iaxisy) {free(pland);perfo.mem_ray-=maillnodray.nelem*sizeof(double);}
  free(grconv);
  perfo.mem_ray-=maillnodray.nelem*sizeof(int);

  perfo.mem_ray-=maillnod2.ndmat*maillnod2.nelem*sizeof(int);
  perfo.mem_ray-=maillnod2.ndim*maillnod2.nelem*sizeof(double);
  perfo.mem_ray-=maillnod2.ndim*maillnod2.npoin*sizeof(double);
  for (i=0;i<maillnod2.ndim;i++) 
    {free(maillnod2.node[i]); free(maillnod2.coord[i]); free(maillnod2.xnf[i]);}
  free(maillnod2.node); free(maillnod2.coord); free(maillnod2.xnf);
  
  if (defsymper.nbsym || defsymper.nbper){
    perfo.mem_ray-=n2*maillnodray.nelem*sizeof(int);
    free(grconv2);
  }


  /* facteur d'echelle inverse */
  /* ------------------------- */    
  if(imult)
    {
      if (maillnodray.ndim==2)
	for (i=0,px=maillnodray.coord[0],py=maillnodray.coord[1];i<maillnodray.npoin;i++,px++,py++)
	  {*px /= xmult; *py /= xmult;}
      else
	for (i=0,px=*(maillnodray.coord),py=*(maillnodray.coord+1),pz=*(maillnodray.coord+2);
	     i<maillnodray.npoin;i++,px++,py++,pz++)
	  {*px /= xmult; *py /= xmult; *pz /= xmult;}

      for(i=0;i<volconnexe.nb;i++) 
	{volconnexe.x[i]/=xmult; volconnexe.y[i]/=xmult; if (maillnodray.ndim==3) volconnexe.z[i]/=xmult;} 
      /* c'est bizarre , je commente chris */
/*       for (i=0,px=maillnodray.coord[0],py=maillnodray.coord[1],pz=maillnodray.coord[2]; */
/* 	   i<maillnodray.npoin;i++,px++,py++,pz++) */
/* 	{*px /= xmult; *py /= xmult; *pz /= xmult;} */


/* en dessous j'ai tout change car cela ne paraissait pas bon */
      if (maillnodray.ndim==3 || (maillnodray.ndim==2 && maillnodray.iaxisy)) xmult*=xmult;
	
      for (i=0; i<nelemloc ;i++) (*diagfdf)[i]/=xmult;

      for (j=0;j<sdparall_ray.npartsray;j++)
	for (i=0; i<nelemloc ;i++) 
	  {
	    pff=listefdf[j][i]; 
	    while (pff){pff->fdf/=xmult; pff=pff->suivant;}
	  }
    }

}



/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#ifndef _CFD_H
#define _CFD_H

/* Include local header */

# include "syr_cfd_coupling.h"

/* Type and structure definitions */

struct Cfd
{
  int    app_num;      /* App. num for the SYRTHES executable */
  char  *name;         /* Name of the current SYRTHES executable
                          in the coupling process */

  int    do_coupling;  /* 0 = do not code coupling, else yes */
  int    coupl_surf;   /* 0 = do not surface coupling, else yes */
  int    coupl_vol;    /* 0 = do not volume coupling, else yes */

  int    n_couplings;  /* Number of cfd codes coupled with SYRTHES */
  char **c_names;      /* Related name instances with each cfd code */

  syr_cfd_coupling_t  **couplings; /* Array of pointers to syr_cfd_coupling_t
                                      structures */

};

#endif
